﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PumpkinHouse.Utils;
using Mail;

namespace TestProject
{
    [TestClass]
    public class EmailUnitTest
    {
        [TestMethod]
        public void TestEmail()
        {
            EmailHelper.BaseDir = @"E:\Work\Source\PumpkinHouse\PumpkinHouseWeb\";
            MailClient.LoadConfiguration(@"E:\Work\Source\PumpkinHouse\PumpkinHouseWeb\mail.config");

            EmailHelper.SendAccountFrozenNotice("我是发动机声", "raymanyoung@gmail.com");
            EmailHelper.SendAccountUnfrozenNotice("我是发动231", "raymanyoung@gmail.com");
            EmailHelper.SendWelcomeEmail("我是发动141", "raymanyoung@gmail.com");
        }
    }
}
