﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PumpkinHouseDatabase;

namespace TestProject
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class DataLayerUnittest
    {
        public DataLayerUnittest()
        {
            DataUtils.ConnectionString = Settings.Default.ConnectionString;
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Constants
        private static string DefaultUnitTestUsername = "UnitTestDefaultUser";
        private static string DefaultUnitTestEmail = "unittestdefault@hello.com";
        #endregion

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestLocation()
        {
            using (DataUtils utils = new DataUtils())
            {
                IList<DB_Location> list = utils.FindAllProvinces();
            }
        }

        [TestMethod]
        public void TestAlbum()
        {
            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(DefaultUnitTestUsername);
                Assert.IsNotNull(user, "user should be found by username: " + DefaultUnitTestUsername);

                user = utils.FindUserByEmail(DefaultUnitTestEmail);
                Assert.IsNotNull(user, "user should be found by email: " + DefaultUnitTestEmail);
            }
        }

        [TestMethod]
        public void TestPicture()
        {
            using (DataUtils utils = new DataUtils())
            {
                DB_Picture pic = utils.FindPictureById(2);
                Assert.IsNotNull(pic, "picture should be found by id 2");

                IQueryable<View_Picture> picList = utils.FindViewPictures().Where(p => p.Username == DefaultUnitTestUsername).OrderByDescending(p => p.Upload_Time);
                Assert.IsTrue(picList.Count() > 0, "by default picture count for default user should be > 0");
            }
        }

        [TestMethod]
        public void TestReply()
        {
            using (DataUtils utils = new DataUtils())
            {
                DB_Reply reply = utils.FindReplyById(1);
                Assert.IsNotNull(reply, "reply should be found by id 1");

                IQueryable<View_Reply> replyList = utils.FindViewRepliesByCollectionId(2);
                Assert.IsTrue(replyList.Count() > 0, "by default reply should be non-empty for default picture");
            }
        }

        [TestMethod]
        public void TestFindThread()
        {
            using (DataUtils utils = new DataUtils())
            {
                var result = utils.FindThread("test1");
            }
        }
    }
}
