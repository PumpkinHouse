﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PumpkinHouse.Utils;

namespace TestProject
{
    [TestClass]
    public class HelperUnitTest
    {
        [TestMethod]
        public void TestMergeTags()
        {
            string s1 = "";
            string s2 = "";

            string r = Helper.MergeTags(s1, s2);
            Assert.AreEqual("", r, "empty result is expected");

            s1 = "";
            s2 = "hello";

            r = Helper.MergeTags(s1, s2);
            Assert.AreEqual("hello", r);

            s1 = " ";
            s2 = "hello ";

            r = Helper.MergeTags(s1, s2);
            Assert.AreEqual("hello", r);

            s1 = "hello";
            s2 = "";
            r = Helper.MergeTags(s1, s2);
            Assert.AreEqual("hello", r);

            s1 = "hello";
            s2 = "world";
            r = Helper.MergeTags(s1, s2);
            Assert.AreEqual("hello world", r);

            s1 = "hello ";
            s2 = " world ";
            r = Helper.MergeTags(s1, s2);
            Assert.AreEqual("hello world", r);

            s1 = "hello ";
            s2 = " world  god";
            r = Helper.MergeTags(s1, s2);
            Assert.AreEqual("hello world god", r);

            s1 = "hello world";
            s2 = " world  god";
            r = Helper.MergeTags(s1, s2);
            Assert.AreEqual("hello world god", r);

            s1 = "hello wor";
            s2 = " world  god";
            r = Helper.MergeTags(s1, s2);
            Assert.AreEqual("hello wor world god", r);

            s1 = "hello wor";
            s2 = " god world  god";
            r = Helper.MergeTags(s1, s2);
            Assert.AreEqual("hello wor god world", r);
        }
    }
}
