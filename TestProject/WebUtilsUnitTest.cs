﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PumpkinHouse.Utils;
using PumpkinHouseDatabase.Contract;

namespace TestProject
{
    [TestClass]
    public class WebUtilsUnitTest
    {
        [TestMethod]
        public void Test()
        {
            string[] uCase = { "A", "B", "C" };
            string[] lCase = { "a", "b", "c" };

            var ul = uCase.SelectMany(
                uc => lCase, (uc, lc) => (uc + lc));

            foreach (string s in ul)
            {
                Console.WriteLine(s);
            }
        }

        [TestMethod]
        public void TestEmailValidation()
        {
            string[] invalidEmails = new string[] {
                @"a",
                @"2",
                @"com",
                @"a.com",
                @"a@",
                @"a@a",
                @"a@a.",
                @"a@.com",
                @"a@com",
                @"a@@a.com"
            };

            foreach (string email in invalidEmails)
            {
                Assert.IsFalse(Validation.ValidateEmail(email), "email {0} should be invalid", email);
            }

            string[] validEmails = new string[] {
                @"raymanyoung@gmail.com",
                @"a@a.com",
                @"a.a@a.com",
                @"a_a@a.com",
                @"a-123@a.com",
                @"a1@a.cn",
                @"a234@b.com.cn",
                @"_123@live.com",
                @"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@aaaaaaaaaaaaaaaa.com",
                @"ABC@ABC.COM"
            };

            foreach (string email in validEmails)
            {
                Assert.IsTrue(Validation.ValidateEmail(email), "Email {0} should be valid", email);
            }
        }

        [TestMethod]
        public void TestRegistration()
        {
            AccountService.AccountServiceClient client = new AccountService.AccountServiceClient();
            for (int i = 0; i < 10000; i++)
            {
                Random random = new Random(DateTime.Now.Millisecond);
                int length = random.Next(6, 19);
                string username = Util.GenerateUnicode(length);

                string pwd = Util.GenerateString(8);

                string email = string.Format("{0}-{1}@qq.com", DateTime.Now.Millisecond, i);

                UserForRegistration user = new UserForRegistration
                {
                    Email = email,
                    Password = pwd,
                    NickName = username
                };

                client.Register(user, "");

            }
        }
    }
}
