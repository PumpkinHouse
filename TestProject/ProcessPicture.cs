﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestProject.AccountService;
using PumpkinHouseDatabase.Contract;
using System.IO;
using PumpkinHouse.Utils;

namespace TestProject
{
    [TestClass]
    public class ProcessPicture
    {
        [TestMethod]
        public void TestProcessPicture()
        {
            string location = @"E:\temp\images";
            DirectoryInfo di = new DirectoryInfo(location);
            if (di.Exists)
            {
                foreach (FileInfo fi in di.GetFiles())
                {
                    if (fi.Name.IndexOf('_') < 0)
                    {
                        using (ImageProcessor ip = new ImageProcessor(new System.Drawing.Bitmap(fi.FullName)))
                        {
                            ip.FitWidthThumbnail(200);
                            ip.Save(fi.DirectoryName + "\\fit_" + fi.Name);
                        }
                    }
                }
            }
        }
    }
}
