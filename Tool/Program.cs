﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PumpkinHouseDatabase;
using System.IO;
using Top.Api;
using Top.Api.Request;
using Top.Api.Response;
using Top.Api.Domain;
using System.Text.RegularExpressions;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using PumpkinHouse.Utils;
using System.Transactions;

namespace Tool
{
    class Program
    {
        static void Main(string[] args)
        {
            DataUtils.ConnectionString = @"Server=localhost\SQLExpress;Database=MutouheziDatabase;Integrated Security=true";
            // RecoverMissingProfilePhotos();

            //ConvertPictureToCollection();

            ImageHelper.ImageFilesRootPath = @"E:\Work\Images\";

            //TestGenerateHeader();

            ComparePerformance();
        }

        private static void ComparePerformance()
        {
            using (DataUtils utils = new DataUtils(false))
            {
                DateTime dt1 = DateTime.Now;
                var result1 = Helper.FindAllOriginalViewCollectionsOld(utils, HomePageMode.Default, string.Empty, false, 0);
                DateTime dt2 = DateTime.Now;
                var result2 = Helper.FindAllOriginalViewCollections(utils, HomePageMode.Default, string.Empty, false, 0, 20000);
                DateTime dt3 = DateTime.Now;
                Console.WriteLine("original time: " + (dt2 - dt1));
                Console.WriteLine("new time: " + (dt3 - dt2));
            }
        }

        private static void TestGenerateHeader()
        {
            using (ImageProcessor im = new ImageProcessor(ImageHelper.GetImage("20120613131215-bvqggf", "jpg")))
            {
                im.GenerateHeader();
                im.Save(@"E:\temp\test5.jpg");
            }
        }

        private static void ConvertPictureToCollection()
        {
            using (DataUtils utils = new DataUtils())
            {
                IEnumerable<DB_Picture> pics = utils.FindPictures();
                foreach (var pic in pics)
                {
                    // first round, migrate all picture info
                    DB_Picture2 pic2 = new DB_Picture2
                    {
                        Id = pic.Id,
                        Image_File_Ext = pic.Image_File_Ext,
                        Image_Filename = pic.Image_Filename,
                        Is_Commodity = pic.Is_Commodity,
                        Price = pic.Price,
                        Ratio = pic.Ratio,
                        Seed = pic.Seed,
                        Shop = pic.Shop,
                        Source_Image_Name = pic.Source_Image_Name,
                        Source_Page_Title = pic.Source_Page_Title,
                        Source_Page_Url = pic.Source_Page_Url,
                        Taobao_Link = pic.Taobaoke_Link
                    };

                    // second round, migrate all original collection info having same ID with the picture
                    DB_Collection2 col2 = new DB_Collection2
                    {
                        Id = pic.Id,
                        Album_Id = pic.Album_Id,
                        Collect_Time = pic.Upload_Time,
                        Description = pic.Description,
                        Is_Original = 1,
                        On_Top = pic.On_Top,
                        On_Top_Update_Time = pic.On_Top_Update_Time,
                        Picture_Id = pic.Id,
                        Username = pic.Username,
                        Tags = pic.Tags
                    };

                    using (TransactionScope scope = new TransactionScope())
                    {
                        var tempP = utils.FindPicture2ById(pic2.Id);
                        if (tempP == null)
                        {
                            Console.WriteLine("Inserting:\tPicture\t" + pic2.Id);
                            utils.InsertPicture(pic2);
                        }
                        else
                        {
                            Console.WriteLine("Exists:\tPicture\t" + pic2.Id);
                        }
                        var tempC = utils.FindCollection2(col2.Id);

                        if (tempC == null)
                        {
                            Console.WriteLine("Inserting:\tOriginal Collection\t" + pic2.Id);
                            col2 = utils.InsertCollection2(col2);
                            pic2.Original_Collection_Id = col2.Id;
                        }
                        else
                        {
                            Console.WriteLine("Exists:\tOriginal Collection\t" + pic2.Id);
                        }
                        utils.Commit();

                        scope.Complete();

                    }
                }

                IEnumerable<DB_Collection> nonOriginalCollections = utils.FindCollection().Where(c => c.Collect_Type == 1).ToList();
                foreach (var col in nonOriginalCollections)
                {
                    DB_Collection2 newCol = new DB_Collection2
                    {
                        Id = col.Id + 40000,
                        Picture_Id = col.Picture_Id, // the picture id has been reserved
                        Tags = col.DB_Picture.Tags,
                        Description = col.DB_Picture.Description,
                        Is_Original = 0,
                        On_Top = 0,
                        Collect_Time = col.Collect_Time,
                        Username = col.Username,
                        Album_Id = col.Album_Id
                    };

                    utils.InsertCollection2(newCol);
                }

                IEnumerable<DB_Picture2> pic2s = utils.FindPicture2().ToList();
                foreach (var pic in pic2s)
                {
                    pic.Collect_Count = utils.FindViewCollection2().Where(c => c.Picture_Id == pic.Id).Count();
                    utils.Commit();
                }
            }
        }

        private static void ConvertPicture()
        {
            ImageHelper.ImageFilesRootPath = @"E:\work\images";
            using (DataUtils utils = new DataUtils(false))
            {
                List<DB_Picture> pics = utils.FindAllPictures(null, false).ToList();
                foreach (var pic in pics)
                {
                    string filename = pic.Image_Filename;
                    string ext = pic.Image_File_Ext;

                    string folder = @"E:\work\images\" + filename.Substring(0, 8);
                    string path = folder + @"\" + filename + "." + ext;

                    if (File.Exists(path))
                    {
                        Console.WriteLine("processing file: " + path);
                        string newfile = ImageHelper.GetThumbnailPath(filename, 200, 200, ext);
                        if (File.Exists(newfile))
                        {
                            string newFolder = folder.Replace(@"E:\work\images", @"E:\imgBackup");
                            if (!Directory.Exists(newFolder)) Directory.CreateDirectory(newFolder);

                            File.Copy(newfile, newfile.Replace(@"E:\work\images", @"E:\imgBackup"), true);
                        }

                        using (ImageProcessor im = new ImageProcessor(new Bitmap(path)))
                        {
                            im.SquareThumbnail(200);
                            im.Save(newfile);
                        }

                        newfile = ImageHelper.GetThumbnailPath(filename, 80, 80, ext);
                        if (File.Exists(newfile))
                        {
                            File.Copy(newfile, newfile.Replace(@"E:\work\images", @"E:\imgBackup"), true);
                        }
                        using (ImageProcessor im = new ImageProcessor(new Bitmap(path)))
                        {
                            im.SquareThumbnail(80);
                            im.Save(newfile);
                        }

                        newfile = ImageHelper.GetThumbnailPath(filename, 200, 0, ext);
                        if (File.Exists(newfile))
                        {
                            File.Copy(newfile, newfile.Replace(@"E:\work\images", @"E:\imgBackup"), true);
                        }
                        using (ImageProcessor im = new ImageProcessor(new Bitmap(path)))
                        {
                            im.FitWidthThumbnail(200);
                            im.Save(newfile);
                        }
                    }
                }
            }
        }

        private static void TestTaobao()
        {
            string url = "http://gw.api.taobao.com/router/rest";
            string appkey = "12579490";
            string appsecret = "37b097f42d074b04f6cd091e66ec02ec";
            ITopClient client = new DefaultTopClient(url, appkey, appsecret);
            using (DataUtils utils = new DataUtils())
            {
                List<DB_Picture> pics = utils.FindAllPictures(null, true).ToList();
                TaobaokeItemsConvertRequest req = new TaobaokeItemsConvertRequest();
                req.Fields = "num_iid,click_url,iid,commission,commission_rate,commission_num,commission_volume";
                req.Nick = "raymanyoung";

                foreach (var pic in pics)
                {
                    if (!string.IsNullOrEmpty(pic.Source_Page_Url) && (pic.Shop == 1 || pic.Shop == 3))
                    {
                        Regex regex = new Regex(@"id=(\d+)");
                        Match match = regex.Match(pic.Source_Page_Url);
                        if (match.Success)
                        {
                            string id = match.Groups[1].Value;

                            req.NumIids = id;
                            TaobaokeItemsConvertResponse response = client.Execute(req);

                            List<TaobaokeItem> items = response.TaobaokeItems;
                            if (items.Count > 0)
                            {
                                TaobaokeItem item = items[0];
                                Console.WriteLine(string.Format("{3}\t{0}\t{1}\t{2}", item.ClickUrl, item.Commission, item.CommissionRate, pic.Id));
                            }
                            else
                            {
                                Console.WriteLine(pic.Id + "\tcannot find");
                            }
                            Thread.Sleep(1000);
                        }
                        else
                        {
                            Console.WriteLine(pic.Id + "\tfailed to parse");
                        }
                    }

                }

            }
        }

        private static void RecoverMissingProfilePhotos()
        {
            using (DataUtils utils = new DataUtils())
            {
                List<string> usernames = utils.FindUsers().Select(u => u.Username).ToList();
                foreach (var user in usernames)
                {
                    string path = @"E:\work\images\profile\";
                    string defaultImage = path + "default";
                    string target = path + user;
                    if (!File.Exists(target + ".jpg"))
                    {
                        File.Copy(defaultImage + ".jpg", target + ".jpg");
                        File.Copy(defaultImage + "_30_30.jpg", target + "_30_30.jpg");
                        File.Copy(defaultImage + "_50_50.jpg", target + "_50_50.jpg");
                        File.Copy(defaultImage + "_80_80.jpg", target + "_80_80.jpg");
                        File.Copy(defaultImage + "_180_180.jpg", target + "_180_180.jpg");
                        Console.WriteLine(user);
                    }
                }
            }
        }
    }
}
