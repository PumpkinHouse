using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Utils;

namespace PumpkinHouse
{
    public partial class Picture : System.Web.UI.Page
    {
        protected long CollectionId;
        protected CollectionToDisplay picture;
        protected DB_Album Album;
        protected string JsonPicture;
        protected string JsonUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            bool success = long.TryParse(Request.Params["pictureId"], out CollectionId);
            if(!success)
            {
                Response.Redirect("~/404.aspx");
            }
            using (DataUtils utils = new DataUtils(false))
            {
                picture = Converter.ConvertCollectionWithOriginal(utils, utils.FindViewCollectionWithOriginalById(CollectionId), -1);
                if (picture == null)
                {
                    Response.Redirect("~/404.aspx");
                }
                string desc = picture.Description;
                if (desc != null && desc.Length > 15)
                {
                    desc = desc.Substring(0, 15) + "...";
                }
                Page.Title = desc + " 来自" + picture.NickName + "在木头盒子的分享";

                JsonPicture = JsonHelper<CollectionToDisplay>.ConvertToJson(picture);

                var user = Converter.ConvertUser(utils, utils.FindUserByUsername(picture.Username), 0);
                JsonUser = JsonHelper<UserToDisplay>.ConvertToJson(user);
            }
        }
    }
}