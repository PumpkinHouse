﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="PumpkinHouse.UserPage" MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        var globalUsername = '<%= Username %>';
        var globalCount;
        function doAction() {
            globalIsMe = globalLogginUser ? (globalUsername == globalLogginUser) : false;
            globalInfoHolder = $('#pInfo');
            globalStatHolder = $('#pStat');

            renderUser();

            var displayHolder = $('#displayHolder');
            displayHolder.empty();
            var displayTemplate = $('#categoryDisplayTemplate').tmpl(globalUser, { isMe: globalIsMe, isDashboard: false });
            displayHolder.append(displayTemplate);

            $('#tabHolder').tabs({
                select: function (event, ui) {
                    var tabName = $(ui.tab).attr('href');
                    document.location.hash = tabName;
                    document.location.reload(true);
                    return false;
                },
                show: function (event, ui) {
                    var index = ($(this).tabs('option', 'selected'));
                    switch (index) {
                        case 0:
                            ajaxGetAlbumsOfUser(true, globalUsername, 0, true, 10, initMyAlbumsPagination, handleFault);
                            break;
                        case 1:
                            ajaxGetAlbumsLikedBy(true, globalUsername, 0, initAlbumILikePagination, handleFault);
                            break;
                        case 2:
                            var func;
                            globalLock = 1;
                            if (location.search == '?username=' + globalUsername + '&target=pub') {
                                func = ajaxGetPicturesOfUser;
                                ajaxGetPicturesOfUser(true, globalUsername, 0, function (pics) { renderPictures(pics); }, handleFault);
                            }
                            else if (location.search == '?username=' + globalUsername + '&target=col') {
                                func = ajaxGetCollectionsOfUser;
                                ajaxGetCollectionsOfUser(true, globalUsername, 0, function (pics) { renderPictures(pics); }, handleFault);
                            }
                            else {
                                func = ajaxGetAllCollectionsOfUser;
                                ajaxGetAllCollectionsOfUser(true, globalUsername, 0, function (pics) { renderPictures(pics); }, handleFault);
                            }

                            var globalUserCollectionPageNumber = 0;

                            var holder = $('#myCollections');
                            holder.scrollLoad({
                                ScrollAfterHeight: 99,
                                getDataAsync: function (callback) {
                                    if (globalUserCollectionPageNumber < Math.floor((globalCount - 1) / 20)) {
                                        globalUserCollectionPageNumber++;
                                        func(true, globalUsername, globalUserCollectionPageNumber, callback, handleFault);
                                    }
                                    else {
                                        callback(null);
                                    }
                                },
                                onload: function (pics) {
                                    if (pics) {
                                        var galleryTemplate = $('#galleryTemplate').tmpl({ pictures: pics.list });
                                        holder.append(galleryTemplate);

                                        holder.masonry('appended', galleryTemplate, true);
                                    }
                                }
                            });

                            break;
                    }

                }
            });

            initCreateAlbumDialog('.CreateBoxBtn');

        }
    </script>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <asp:ScriptManagerProxy ID="ScriptManager2" runat="server">
        <CompositeScript>
            <Scripts>
               <asp:ScriptReference Path="/Scripts/albumInclude.js" />
               <asp:ScriptReference Path="/Scripts/userPage.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManagerProxy>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div class="PictureCon">
<style type="text/css">
    /*重构UI自带CSS代码*/
	.ui-corner-all{border-radius:0px; border:none;}
	.ui-widget-header{background:none; background:#ffffff; border:none;}
	.ui-corner-al{border:none; background:#FFFFFF}
	.ui-tabs-selected,.ui-state-active,.ui-state-hover{border:none; background:none; border-radius:0px; margin-top:-2px;}
	.ui-state-default{border:none; background:none; font-family:'微软雅黑'; color:#28a7ff; font-size:16px;}
	.ui-state-default a:hover{color:#28a7ff; font-size:16px; background:none;}
	.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited { color: #28a7ff; border-top:2px solid #28a7ff; text-decoration: none; cursor:pointer; margin-top:-2px;}
</style>

    <div class="PictureLeft float_l clearfix">
        <div id="pInfo"></div>
        <div id="pStat"></div>
    </div>
    <div class="PictureRight float_r clearfix" style="border: 0px solid red; background: none; _overflow:hidden;">
        <div id="pStuff"></div>
        <div id="displayHolder"></div>
        <div id="paginationHolder"></div>
    </div>
    <div id="dialogHolder"></div>
</div>
<div class="hide" style="display:none">
    <h2><%= Server.HtmlEncode(User.NickName) %></h2>
    <div>
        <span>专辑 <%= User.NumberOfAlbums %></span>
        <span>粉丝 <%= User.NumberOfFans %></span>
        <span>收纳 <%= User.NumberOfPictures %></span>
    </div>
    <div>
        <% foreach (var picture in User.PictureList)
           { %>
           <a href="/picture/<%= picture.PictureId %>">
            <img class="preview" src="<%=picture.Url %>_80_80_thumbnail.<%= picture.FileExt %>" />
        </a>
        <% } %>
    </div>
</div>
<!-- #Include virtual="/template/albumTemplate.html" -->
<!-- #Include virtual="/template/albumThumbnailTemplate.html" -->
<!-- #Include virtual="/template/pictureGalleryTemplate.html" -->
<!-- #Include virtual="/template/userTemplate.html" -->
<!-- #Include virtual="/template/messageTemplate.html" -->
<!-- #Include virtual="/template/replyTemplate.html" -->
</asp:Content>
