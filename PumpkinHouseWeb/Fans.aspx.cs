﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;

namespace PumpkinHouse
{
    public partial class Fans : System.Web.UI.Page
    {
        protected string mode;
        protected string username;
        protected bool IsMe;
        protected void Page_Load(object sender, EventArgs e)
        {
            username = Request.Params["username"];
            mode = Request.Params["mode"]; 

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(mode))
            {
                Response.Redirect("~/404.aspx");
            }
            else
            {
                using (DataUtils utils = new DataUtils(false))
                {
                    DB_User user = utils.FindUserByUsername(username);
                    if (user == null)
                    {
                        Response.Redirect("~/404.aspx");
                    }
                    if (mode == "fans")
                    {
                        Page.Title = user.Nick_Name + "的粉丝 - 木头盒子";
                    }
                    else
                    {
                        Page.Title = user.Nick_Name + "关注的人 - 木头盒子";
                    }
                }
            }

            IsMe = Helper.FindCurrentUsername() == username;
        }

    }
}