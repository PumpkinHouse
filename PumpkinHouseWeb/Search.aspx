﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="PumpkinHouse.Search" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <asp:ScriptManagerProxy ID="ScriptManager2" runat="server">
        <Scripts>
            <asp:ScriptReference Path="/Scripts/likePicture.js" />            
        </Scripts>
    </asp:ScriptManagerProxy>   
</asp:Content>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link href="/Styles/jquery-ui-1.8.20/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        var globalKeyword = '<%= keyword %>';
        var globalType = <%= mode %>;
        var globalCommodityOnly = ('<%= commodityOnly %>' === 'True');
        var globalPageNumber = 0;
        var globalLock = 0;
        var globalAllPictures = [];

        function renderSearchPictures(pics) {
            globalAllPictures = pics;
            if (globalPageNumber == 0 && pics.list.length == 0) {
                renderEmptyResult();
                return;
            }
            var holder = $('#content');
            if (globalPageNumber == 0) holder.empty();

            $.each(pics.list, function (index, item) {
                item.homepage = 1;
            });

            var template;
            if (globalPageNumber == 0) {
                template = $('#searchContentTemplate').tmpl({ pictures: pics.list });
                holder.append(template);
                $('.PublicList').masonry({
                    itemSelector: '.PublicList li',
                    columnWidth: 240
                });
            }
            else {
                template = $('#galleryItemTemplate').tmpl(pics.list);
                $('ul.PublicList').append(template);
                $('.PublicList').masonry('appended', template, true);
            }
            
            initCollectButton();
            
            // bind event on collect buttons
            $('.collectButton', template).each(function (index, item) {
                var li = $(item).parents('li.pic');
                var displayTarget = li.find('a span.collectCount');
                $.collectButtion(item, globalLoggedIn, globalMyAlbums, displayTarget);
            });

            //喜欢按钮
            template.find('.likeBtn').on('click', function (e) {
                updatePictureLike(e, defaultLikeCallback)
            });

            //回复功能
            bindReplyEvent(template);

            globalLock = 0;
       }

        function renderAlbums(albs) {
            if (albs.list.length == 0) {
                renderEmptyResult();
                return;
            }
            var holder = $('#content');
            holder.empty();
            var template = $('#albumListTemplate').tmpl(albs.list);
            holder.append(template);     
            
            $('.albumAction').on('click', function (event, displaySelector) {
                if (!globalLoggedIn) {
                    showLogonDialog();
                }
                else {
                    updateAlbumLike(event, $(event.target).parents('.myLoveBoxPic').find('#numOfLike'));
                }
            });   
        }

        function initAlbumPagination(list) {
            pagination("#pagination", list.count, 20, albumPageSelectCallback);
        }

        function initUserPagination(list) {
            pagination("#pagination", list.count, 20, userPageSelectCallback);
        }

        function renderEmptyResult() {
            renderTemplate('#content', '#emptySearchResultTemplate');
        }

        function renderUsers(users) {
            if (users.list.length == 0) {
                renderEmptyResult();
                return;
            }
            var holder = $('#content');
            holder.empty();
            var template = $('#userListTemplate').tmpl({ users: users.list });
            holder.append(template);

            $('.attBtn').fanButton(globalLoggedIn);
        }

        function albumPageSelectCallback(pageNumber, container) {
            ajaxSearchAlbums(true, globalKeyword, pageNumber, renderAlbums, handleFault);
        }

        function userPageSelectCallback(pageNumber, container) {
            ajaxSearchUser(true, globalKeyword, pageNumber, renderUsers, handleFault);
        }

        function continueSearch(type) {
            var keyword = $('.SearchBox').val();
            if ($('#commodityOnly').attr('checked') == 'checked' && type == 'picture') {
                type = 'commodity';
            }
            document.location = '/search/' + type + "/" + keyword;
            
        }


        function doAction () {
            if (globalCommodityOnly) {
                $('#commodityOnly').attr('checked', 'checked');
            }

            if (globalKeyword != ''){
                switch (globalType) {
                    case 0 : {
                        ajaxGetAllPictures(true, globalKeyword, 0, globalCommodityOnly, 1, true, renderSearchPictures, handleFault);
                        // preloading
                        ajaxGetAllPictures(true, globalKeyword, 1, globalCommodityOnly, 1, true, cacheLoading, handleFault);

                        $(".liSelected1 a").addClass("liSelected");
                        $(".liSelected2 a").removeClass("liSelected");
                        $(".liSelected3 a").removeClass("liSelected");
                        break;
                    }
                    case 1 : {
                        ajaxSearchAlbums(true, globalKeyword, 0, function (albs) { initAlbumPagination(albs); renderAlbums(albs) }, handleFault);

                        $(".liSelected1 a").removeClass("liSelected");
                        $(".liSelected2 a").addClass("liSelected");
                        $(".liSelected3 a").removeClass("liSelected");
                        break;
                    }
                    case 2 : {
                        ajaxSearchUser(true, globalKeyword, 0, function (users) { initUserPagination(users); renderUsers(users) }, handleFault);

                        $(".liSelected1 a").removeClass("liSelected");
                        $(".liSelected2 a").removeClass("liSelected");
                        $(".liSelected3 a").addClass("liSelected");
                        break;
                    }
                }
            }

            $('#contentSearch').on('keypress', function(e) {
                if (e.which == 13) {
                    $('.SearchBtn').trigger('click');
                }
            });

            $(window).scroll(function () {
                if (getScrollTop() + $(window).height() + 100 > $(document).height()) {
                    if (globalLock == 0) {
                        globalLock = 1;
                        if (globalPageNumber < Math.floor((globalAllPictures.count - 1) / 20)) {
                            globalPageNumber++;
                            globalLoadingIndicatorAtBottom = true;
                            renderCache(renderSearchPictures);
                            ajaxGetAllPictures(true, globalKeyword, globalPageNumber, false, 1, true, cacheLoading, handleFault);
                        }
                    }
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <div class="PublicCon SearchTop">
        <div id="searchBox">
            <div class=tabSearMenu>
	            <ul>
		            <li class="liSelected1"><a href="javascript:continueSearch('picture')">内容</a></li>
		            <li class="liSelected2"><a href="javascript:continueSearch('album')">盒子</a></li>
		            <li class="liSelected3"><a href="javascript:continueSearch('user')">好友</a></li>
	            </ul>
            </div>
	        <div class="SearchBtnBox">
            <input type="text" id="contentSearch" class="SearchBox" value="<%= keyword %>" />
            <% switch (type)
               {
                   case "picture": 
                   case "commodity":%>
		        <input type="button" id="contentBtn" value="搜索" onclick="javascript:continueSearch('picture')" class="Public-Btn-Box SearchBtn" />
                <input type="checkbox" id="commodityOnly" style="margin-top:-1px; margin-top:-3px\0; *margin-top:0px; _margin-top:0px;" onclick="javascript:continueSearch('commodity')" />&nbsp;只看商品
            <% break;
                   case "album": %>
		        <input type="button" id="albumBtn" value="搜索" onclick="javascript:continueSearch('album')" class="Public-Btn-Box SearchBtn" />
            <% break;
                   case "user": %>
		        <input type="button" id="userBtn" value="搜索" onclick="javascript:continueSearch('user')" class="Public-Btn-Box SearchBtn" />
            <% break;
               } %>
	        </div>

        </div>

        <div id="content" class="SearchCon">
        </div>

        <div id="bottom" class="clearfix"></div>   
        <div id="pagination"></div>
    </div>
    
    <!-- 搜索 内容显示模版 -->
    <script id="searchContentTemplate" type="text/x-jQuery-tmpl">
    <div class="SearchCon">
        <ul class="PublicList clearfix">
          {{tmpl(pictures) "#galleryItemTemplate"}}
        </ul>
    </div>
    </script>
    <!-- #Include virtual="/template/userTemplate.html" -->
    <!-- #Include virtual="/template/userListTemplate.html" -->
    <!-- #Include virtual="/template/albumThumbnailTemplate.html" -->
    <!-- #Include virtual="/template/collectTemplate.html" -->

</asp:Content>