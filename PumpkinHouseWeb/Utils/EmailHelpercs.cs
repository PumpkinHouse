﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

namespace PumpkinHouse.Utils
{
    public static class EmailHelper
    {
        private static string _baseDir;

        public static string BaseDir
        {
            set
            {
                _baseDir = value;
            }
            private get
            {
                if (string.IsNullOrEmpty(_baseDir)) return System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                else return _baseDir;
            }
        }
        public static void SendResetPasswordEmail(string nickname, string domain, string to, string code)
        {
            string bodyTemplate = GetTemplate("resetPasswordTemplate.html");
            SendMessage(to, "亲，你在木头盒子的密匙找到啦！", string.Format(bodyTemplate, nickname, domain, code, to));
        }

        public static void SendWelcomeEmail(string nickname, string domain, string to)
        {
            string bodyTemplate = GetTemplate("welcomeTemplate.html");
            SendMessage(to, "欢迎加入木头盒子，开启你的分享家之旅", string.Format(bodyTemplate, nickname, domain, to));
        }

        public static void SendAccountFrozenNotice(string nickname, string domain,  string to)
        {
            string bodyTemplate = GetTemplate("accountFrozenTemplate.html");
            SendMessage(to, "Sorry，您在木头盒子的账号被冻结了", string.Format(bodyTemplate, nickname, domain));
        }

        public static void SendAccountUnfrozenNotice(string nickname, string domain, string to)
        {
            string bodyTemplate = GetTemplate("accountUnfrozenTemplate.html");
            SendMessage(to, "恭喜，您在木头盒子的账号已经解冻了！", string.Format(bodyTemplate, nickname, domain));
        }

        public static void SendActivityEmail(string subject, string to, string templatePath, string[] args)
        {
            string bodyTemplate = GetTemplate(templatePath);
            SendMessage(to, subject, string.Format(bodyTemplate, (object[]) args), false);
        }

        private static void SendMessage(string to, string subject, string content, bool withTemplate = true)
        {
            string body = content;
            if (withTemplate)
            {
                body = FormatMessage(subject, content);
            }
            using (Mail.MailClient client = new Mail.MailClient())
            {
                client.SendMessage(new string[] { to }, null, subject, body);
            }
        }

        private static string FormatMessage(string subject, string content)
        {
            string template = GetTemplate("emailTemplate.html");
            return string.Format(template, subject, content);
        }

        private static string GetTemplate(string filename)
        {
            using (TextReader tr = new StreamReader(BaseDir + "template\\emailTemplate\\" + filename, Encoding.UTF8))
            {
                return tr.ReadToEnd();
            }
        }

        public static Uri FindMailHomepageByAccount(string email)
        {
            string[] split = email.Split('@');

            if (split.Length < 2)
            {
                throw new ArgumentException(email + "is not a valid email address");
            }
            string domain = split[1].ToLower();

            if (domain == "126.com") return new Uri("http://www.126.com");
            if (domain == "sina.com" || domain == "sina.cn") return new Uri("http://mail.sina.com.cn");
            if (domain == "163.com") return new Uri("http://email.163.com");
            if (domain == "yeah.net") return new Uri("http://www.yeah.net");
            if (domain == "suhu.com") return new Uri("http://mail.sohu.com");
            if (domain == "yahoo.com.cn" || domain == "yahoo.cn") return new Uri("http://mail.cn.yahoo.com");
            if (domain == "tom.com" || domain == "163.net" || domain == "vip.tom.com") return new Uri("http://mail.tom.com");
            if (domain == "qq.com") return new Uri("http://mail.qq.com");
            if (domain == "139.com") return new Uri("http://mail.10086.cn");
            if (domain == "wo.com.cn") return new Uri("http://mail.wo.com.cn");
            if (domain == "189.cn") return new Uri("http://webmail24.189.cn/webmail");

            if (domain == "hotmail.com" || domain == "msn.com") return new Uri("http://www.hotmail.com");
            if (domain == "gmail.com") return new Uri("http://www.gmail.com");
            if (domain == "yahoo.com" || domain == "ymail.com" || domain == "rocketmail.com") return new Uri("http://mail.yahoo.com");

            return null;
        }
    }
}