﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace PumpkinHouse.Utils
{
    public static class JsonHelper<T>
    {
        public static string ConvertToJson(T obj)
        {
            using (var stream = new MemoryStream())
            {
                DataContractJsonSerializer oSerializer = new DataContractJsonSerializer(typeof(T));
                oSerializer.WriteObject(stream, obj);
                string JsonObject = Encoding.UTF8.GetString(stream.ToArray());
                // escape ' for js string
                return EscapeJsonForJs(JsonObject);
            }
        }

        private static string EscapeJsonForJs(string jsonObject)
        {
            return jsonObject.Replace("\r", "\\r").Replace("\n", "\\n").Replace("\\", "\\\\").Replace("\'", "\\\'");
        }
    }
}