﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using System.Net;
using System.IO;
using System.Threading;

namespace PumpkinHouse.Utils
{
    public static class DownloadHelper
    {
        private static ILog Log = LogManager.GetLogger(typeof(DownloadHelper));

        private static int MaxFileSize = 5 * 1024 * 1024;

        public static void DownloadImageFile(string url, string fullName)
        {
            Log.Debug("starting downloading: " + url);

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
            webRequest.Credentials = CredentialCache.DefaultCredentials;
        //    webRequest.Accept = "image/*";
        //    webRequest.Headers.Add("Accept-Encoding", "gzip, deflate");
            using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
            {
                Int64 fileSize = webResponse.ContentLength;
                string contentType = webResponse.ContentType;

                if (!ImageHelper.IsFileTypeSupported(contentType))
                {
                    throw new InvalidFileTypeException();
                }

                if (fileSize < MaxFileSize)
                {
                    using (Stream s = webResponse.GetResponseStream())
                    {
                        using (Stream file = File.OpenWrite(fullName))
                        {
                            CopyStream(s, file);
                        }
                    }
                }
                else
                {
                    throw new ImageTooLargeException();
                }
            }
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }
    }

    public class InvalidFileTypeException : Exception { }

    public class ImageTooLargeException : Exception { }
}