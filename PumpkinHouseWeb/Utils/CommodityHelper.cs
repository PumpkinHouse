﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace PumpkinHouse.Utils
{
    public static class CommodityHelper
    {
        public static Shop ParseShop(string url)
        {
            try
            {
                Uri uri = new Uri(url);
                string host = uri.DnsSafeHost;
                if (host.EndsWith("taobao.com")) return Shop.Taobao;
                if (host.EndsWith("tmall.com")) return Shop.Tmall;
                if (host.EndsWith("360buy.com")) return Shop.Jingdong;
                if (host.EndsWith("ikea.com")) return Shop.Ikea;
                if (host.EndsWith("vancl.com")) return Shop.Vancl;
                if (host.EndsWith("alibaba.com")) return Shop.Alibaba;
                if (host.EndsWith("dangdang.com")) return Shop.Dangdang;
                if (host.EndsWith("amazon.cn")) return Shop.Amazon;
                if (host.EndsWith("paipai.com")) return Shop.Paipai;
                if (host.EndsWith("quwan.com")) return Shop.Quwan;
                if (host.EndsWith("uya100.com")) return Shop.Uya;
                if (host.EndsWith("51youpin.com")) return Shop.Youpin;
                if (host.EndsWith("eachnet.com")) return Shop.Eachnet;
                if (host.EndsWith("yihaodian.com")) return Shop.Yihaodian;
                if (host.EndsWith("suning.com")) return Shop.Suning;

            }
            catch (Exception) { }
            return Shop.NotShop;
        }

        public static float ParsePrice(Shop shop, string url)
        {
            float price = 0;

            var doc = new HtmlDocument();
            doc.OptionReadEncoding = false;

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    doc.Load(stream, Encoding.UTF8);
                }
            }

            HtmlNode node = null;

            switch (shop)
            {
                case Shop.Taobao:
                case Shop.Tmall: 
                    node = doc.DocumentNode.SelectSingleNode("//*[@id=\"J_StrPrice\"]");
                    if (node != null)
                    {
                        bool success = float.TryParse(node.InnerText, out price);
                        if (!success)
                        {
                            // possible value: "100 - 500"
                            string[] split = node.InnerText.Split('-');
                            success = float.TryParse(split[0], out price);
                        }
                    }
                    break;
                case Shop.Ikea:
                    node = doc.DocumentNode.SelectSingleNode("//*[@id=\"price1\"]");
                    if (node != null)
                    {
                        string priceStr = node.InnerText.Trim();
                        if (priceStr.StartsWith("¥"))
                        {
                            priceStr = priceStr.Substring(1).Trim();
                        }
                        bool success = float.TryParse(priceStr, out price);
                    }
                    break;
                case Shop.Vancl:
                    node = doc.DocumentNode.SelectSingleNode("//*[@class=\"cuxiaoPrice\"]//strong");
                    if (node != null)
                    {
                        string priceStr = node.InnerText.Trim();
                        bool success = float.TryParse(priceStr, out price);
                    }
                    break;

                case Shop.Alibaba:
                    node = doc.DocumentNode.SelectSingleNode("//*[@class=\"de-pnum\"]");
                    if (node != null)
                    {
                        string priceStr = node.InnerText.Trim();
                        bool success = float.TryParse(priceStr, out price);
                    }
                    break;

                case Shop.Dangdang:
                    node = doc.DocumentNode.SelectSingleNode("//*[@id=\"salePriceTag\"]");
                    if (node != null)
                    {
                        string priceStr = node.InnerText.Trim();
                        Regex regex = new Regex(@"([\d.]+)");
                        Match match = regex.Match(priceStr);
                        priceStr = match.Groups[1].Value;

                        bool success = float.TryParse(priceStr, out price);
                    }
                    break;

                case Shop.Amazon: 
                    node = doc.DocumentNode.SelectSingleNode("//*[@class=\"priceLarge\"]");
                    if (node != null)
                    {
                        string priceStr = node.InnerText.Trim();
                        if (priceStr.StartsWith("￥"))
                        {
                            priceStr = priceStr.Substring(1).Trim();
                        } 
                        bool success = float.TryParse(priceStr, out price);
                    }
                    break;

                case Shop.Paipai:
                    break;
                case Shop.Uya:
                    break;  
                case Shop.Quwan: 
                    node = doc.DocumentNode.SelectSingleNode("//*[@class=\" Price\"]");
                    if (node != null)
                    {
                        string priceStr = node.InnerText.Trim();
                        if (priceStr.StartsWith("¥"))
                        {
                            priceStr = priceStr.Substring(1).Trim();
                        } 
                        bool success = float.TryParse(priceStr, out price);
                    }
                    break;
                case Shop.Youpin:
                    break;
                case Shop.Eachnet:
                    node = doc.DocumentNode.SelectSingleNode("//*[@class=\"itemPrice\"]//script");
                    if (node != null)
                    {
                        string priceStr = node.InnerText.Trim();
                        Regex regex = new Regex(@"\('([\d.]+)'\)");
                        Match match = regex.Match(priceStr);
                        priceStr = match.Groups[1].Value;
                        bool success = float.TryParse(priceStr, out price);
                    }
                    break;
                case Shop.Yihaodian:
                    node = doc.DocumentNode.SelectSingleNode("//*[@id=\"nonMemberPrice\"]");
                    if (node != null)
                    {
                        string priceStr = node.InnerText.Trim();
                        bool success = float.TryParse(priceStr, out price);
                    }
                    break;
                case Shop.Suning:
                    //HtmlNodeCollection col = doc.DocumentNode.SelectNodes("//script");
                    //foreach (var n in col.Nodes())
                    //{
                    //    Regex regex = new Regex(@"currPrice:""(\d.+)""");
                    //    Match match = regex.Match(n.InnerText);
                    //    string priceStr = match.Groups[1].Value;
                    //    bool success = float.TryParse(priceStr, out price);
                    //}
                    break;                   
            }
            return price;
        }
    }

    public enum Shop
    {
        NotShop = 0,
        Taobao = 1,
        Jingdong = 2,
        Tmall = 3,
        Ikea = 4,
        Vancl = 5,
        Alibaba = 6,
        Dangdang = 7,
        Amazon = 8,
        Paipai = 9,
        Uya = 10,
        Quwan = 11,
        Youpin = 12,
        Eachnet = 13,
        Yihaodian = 14,
        Suning = 15,
        Gome = 16
    }
}