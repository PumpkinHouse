﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Dispatcher;
using log4net;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel;

namespace PumpkinHouse.Utils.Extention
{
    public class LogErrorHandler : IErrorHandler
    {
        private ILog Log = LogManager.GetLogger(typeof(LogErrorHandler));

        public bool HandleError(Exception error)
        {
            Log.Error("unhanlded exception: " + error.Message);
            Log.Error(error.StackTrace);
            return false;
        }

        public void ProvideFault(Exception error, MessageVersion version, ref System.ServiceModel.Channels.Message fault)
        {
        }
    }
}