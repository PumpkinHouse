﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Utils
{
    public static class KeywordHelper
    {
        private static Dictionary<string, string> _keywords;

        public static Dictionary<string, string> GetKeywords(bool refresh)
        {
            if (_keywords == null || refresh)
            {
                using (DataUtils utils = new DataUtils())
                {
                    List<DB_Keyword_Association> keywords = utils.FindKeywordAssociations().ToList();
                    Dictionary<string, string> result = new Dictionary<string, string>();
                    foreach (var keyword in keywords)
                    {
                        if (result.Keys.Contains(keyword.Keyword1))
                        {
                            result[keyword.Keyword1] += " " + keyword.Keyword2;
                        }
                        else
                        {
                            result.Add(keyword.Keyword1, keyword.Keyword2);
                        }
                        if (result.Keys.Contains(keyword.Keyword2))
                        {
                            result[keyword.Keyword2] += " " + keyword.Keyword1;
                        }
                        else
                        {
                            result.Add(keyword.Keyword2, keyword.Keyword1);
                        }
                    }
                    _keywords = result;
                }
            }
            return _keywords;
        }
    }
}