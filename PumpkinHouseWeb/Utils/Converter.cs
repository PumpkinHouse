﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PumpkinHouseDatabase.Contract;
using PumpkinHouseDatabase;
using System.Data.Linq;
using log4net;

namespace PumpkinHouse.Utils
{
    public static class Converter
    {
        private static ILog Log = LogManager.GetLogger(typeof(Converter));

        //public static IList<PictureToDisplay> ConvertPicture(DataUtils util, IQueryable<DB_Picture> pictureList, int replyLimit)
        //{
        //    return pictureList.Select(p => ConvertPicture(util, p, replyLimit)).ToList();
        //}

        //public static PictureToDisplay ConvertBriefPicture(DataUtils util, DB_Picture picture)
        //{
        //    if (picture == null) return null;
        //    Log.Debug("Start Converting picture (brief)");
        //    PictureToDisplay p = new PictureToDisplay
        //    {
        //        Id = picture.Id,
        //        Url = ImageHelper.GetAccessPath(picture.Image_Filename),
        //        Extention = picture.Image_File_Ext,
        //        UploadTime = picture.Upload_Time,
        //        CollectedTime = picture.Upload_Time,
        //        ReplyCollection = new PagingList<ReplyToDisplay> { Count = 0, List = new List<ReplyToDisplay>() }
        //    };

        //    return p;
        //}

        //public static PictureToDisplay ConvertPicture(DataUtils util, DB_Picture picture, int replyLimit)
        //{
        //    if (picture == null) return null;
        //    Log.Debug("Start Converting picture");
        //    PictureToDisplay p = new PictureToDisplay
        //    {
        //        Id = picture.Id,
        //        Description = picture.Description,
        //        AlbumId = picture.Album_Id.HasValue ? picture.Album_Id.Value : 0,
        //        AlbumName = picture.Album_Id.HasValue ? picture.DB_Album.Name : Resource.DefaultAlbumName,
        //        Url = ImageHelper.GetAccessPath(picture.Image_Filename),
        //        Extention = picture.Image_File_Ext,
        //        Username = picture.Username,
        //        NickName = picture.DB_User.Nick_Name,
        //        UploadTime = picture.Upload_Time,
        //        CollectedTime = picture.Upload_Time,
        //        NumberOfCollects = util.FindCollectionCountByPictureId(picture.Id),
        //        Tags = picture.Tags,
        //        OriginalPageTitle = picture.Source_Page_Title,
        //        OriginalPageUrl = picture.Source_Page_Url,
        //        Price = picture.Price,
        //        Shop = picture.Shop,
        //        Ratio = picture.Ratio,
        //        TaobaoLink = picture.Taobaoke_Link
        //    };

        //    Log.Debug("getting reply collections");

        //    if (replyLimit < 0)
        //    {
        //        p.ReplyCollection = PagingHelper.Page<View_Reply, ReplyToDisplay>(util.FindViewRepliesByCollectionId(picture.Id), 0, r => Converter.ConvertReply(r));
        //    }
        //    if (replyLimit > 0)
        //    {
        //        p.ReplyCollection = ConvertReplies(util.FindViewRepliesByCollectionId(picture.Id), replyLimit);
        //    }
        //    if (replyLimit == 0)
        //    {
        //        p.ReplyCollection = new PagingList<ReplyToDisplay> { Count = 0, List = new List<ReplyToDisplay>() };
        //    }

        //    // Check whether the current user already collected the picture
        //    // if it is, add the information
        //    string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
        //    if (username != null)
        //    {
        //        if (username == p.Username) // 自己的图片，设定收集专辑
        //        {
        //            p.IsMine = true;
        //            p.CollectedInMyAblbumId = p.AlbumId;
        //        }
        //        else
        //        {
        //            p.IsMine = false;
        //            // add my collection information
        //            DB_Collection c = util.FindCollection(username, picture.Id, 1);
        //            if (c != null)
        //            {
        //                DB_Album album;
        //                if (c.Album_Id.HasValue)
        //                {
        //                    // default album
        //                    album = util.FindAlbumById(c.Album_Id.Value, false);
        //                    p.CollectedInMyAblbumId = album.Id;
        //                    p.CollectedInMyAlbumName = album.Name;
        //                }
        //                else
        //                {
        //                    p.CollectedInMyAlbumName = Resource.DefaultAlbumName;
        //                    p.CollectedInMyAblbumId = 0;
        //                }
        //            }
        //            else
        //            {
        //                p.CollectedInMyAblbumId = -1;
        //            }
        //        }
        //    }

        //    return p;
        //}

        //public static PictureToDisplay ConvertPicture(DataUtils util, View_Picture picture, int replyLimit)
        //{
        //    if (picture == null) return null;
        //    Log.Debug("Start Converting picture");
        //    PictureToDisplay p = new PictureToDisplay
        //    {
        //        Id = picture.Id,
        //        Description = picture.Description,
        //        AlbumId = picture.Album_Id.HasValue ? picture.Album_Id.Value : 0,
        //        AlbumName = picture.Album_Id.HasValue ? picture.Name : Resource.DefaultAlbumName,
        //        Url = ImageHelper.GetAccessPath(picture.Image_Filename),
        //        Extention = picture.Image_File_Ext,
        //        Username = picture.Username,
        //        NickName = picture.Nick_Name,
        //        UploadTime = picture.Upload_Time,
        //        CollectedTime = picture.Upload_Time,
        //        NumberOfCollects = util.FindCollectionCountByPictureId(picture.Id),
        //        Tags = picture.Tags,
        //        OriginalPageTitle = picture.Source_Page_Title,
        //        OriginalPageUrl = picture.Source_Page_Url,
        //        Price = picture.Price,
        //        Shop = picture.Shop,
        //        Ratio = picture.Ratio,
        //        TaobaoLink = picture.Taobaoke_Link
        //    };

        //    Log.Debug("getting reply collections");

        //    if (replyLimit < 0)
        //    {
        //        p.ReplyCollection = PagingHelper.Page<View_Reply, ReplyToDisplay>(util.FindViewRepliesByCollectionId(picture.Id), 0, r => Converter.ConvertReply(r));
        //    }
        //    if (replyLimit > 0)
        //    {
        //        p.ReplyCollection = PagingHelper.Page<View_Reply, ReplyToDisplay>(util.FindViewRepliesByCollectionId(picture.Id).Take(replyLimit), 0, r => Converter.ConvertReply(r));
        //    }
        //    if (replyLimit == 0)
        //    {
        //        p.ReplyCollection = new PagingList<ReplyToDisplay> { Count = 0, List = new List<ReplyToDisplay>() };
        //    }

        //    // Check whether the current user already collected the picture
        //    // if it is, add the information
        //    string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
        //    if (username != null)
        //    {
        //        if (username == p.Username) // 自己的图片，设定收集专辑
        //        {
        //            p.IsMine = true;
        //            p.CollectedInMyAblbumId = p.AlbumId;
        //        }
        //        else
        //        {
        //            p.IsMine = false;
        //            // add my collection information
        //            DB_Collection c = util.FindCollection(username, picture.Id, 1);
        //            if (c != null)
        //            {
        //                DB_Album album;
        //                if (c.Album_Id.HasValue)
        //                {
        //                    // default album
        //                    album = util.FindAlbumById(c.Album_Id.Value, false);
        //                    p.CollectedInMyAblbumId = album.Id;
        //                    p.CollectedInMyAlbumName = album.Name;
        //                }
        //                else
        //                {
        //                    p.CollectedInMyAlbumName = Resource.DefaultAlbumName;
        //                    p.CollectedInMyAblbumId = 0;
        //                }
        //            }
        //            else
        //            {
        //                p.CollectedInMyAblbumId = -1;
        //            }
        //        }
        //    }

        //    return p;
        //}

        public static PagingList<ReplyToDisplay> ConvertReplies(IEnumerable<DB_Reply> replies, int take)
        {
            return new PagingList<ReplyToDisplay>
                {
                    Count = replies.Count(),
                    List = replies.OrderByDescending(r => r.Post_Time).Take(take).Select(r => ConvertReply(r)).ToList()
                };
        }

        public static PagingList<ReplyToDisplay> ConvertReplies(IEnumerable<View_Reply> replies, int take)
        {
            return new PagingList<ReplyToDisplay>
            {
                Count = replies.Count(),
                List = replies.OrderByDescending(r => r.Post_Time).Take(take).Select(r => ConvertReply(r)).ToList()
            };
        }

        public static ReplyToDisplay ConvertReply(View_Reply r)
        {
            string username = Helper.FindCurrentUsername() ;

            return new ReplyToDisplay
            {
                Content = r.Text,
                Username = r.Replier_Username,
                NickName = r.Replier_Nickname,
                Id = r.Id,
                ReplyTo = r.Reply_To,
                ReplyToNickname = r.ReplyTo_Nickname,
                Time = r.Post_Time,
                IsMe = username == r.Replier_Username
            };
        }

        public static ReplyToDisplay ConvertReply(DB_Reply r)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            return new ReplyToDisplay
                    {
                        Content = r.Text,
                        Username = r.Replier_Username,
                        NickName = r.DB_User.Nick_Name,
                        Id = r.Id,
                        ReplyTo = r.Reply_To,
                        ReplyToNickname = r.Reply_ToDB_User == null ? null : r.Reply_ToDB_User.Nick_Name,
                        Time = r.Post_Time,
                        IsMe = username == r.Replier_Username
                    };
        }

        public static IList<AlbumToDisplay> ConvertAlbum(IQueryable<View_Album> albumList, DataUtils utils, string currentUsername, int limit, int replyLimit)
        {
            return albumList.ToList().Select(alb => ConvertAlbum(alb, utils, currentUsername, limit, replyLimit)).ToList();
        }

        /// <summary>
        /// Convert the album to data to display
        /// </summary>
        /// <param name="album"></param>
        /// <param name="utils"></param>
        /// <param name="currentUsername"></param>
        /// <param name="limit">the number of pictures to include for display. The picture includes the normal pictures and user collections
        /// Include no picture if limit is 0;
        /// Include (limit) number of pictures if limit > 0
        /// Include all pictures if limit < 0
        /// </param>
        /// <returns></returns>
        public static AlbumToDisplay ConvertAlbum(View_Album album, DataUtils utils, string currentUsername, int picLimit, int replyLimit)
        {
            if (album == null)
            {
                return null;
            }

            IEnumerable<View_Collection_With_Original> cols = utils.FindViewCollectionWithOriginal(album.Id, album.Username).OrderByDescending(c => c.Collect_Time).OrderByDescending(c => c.Collect_Count);
            if (picLimit > 0)
            {
                cols = cols.Take(picLimit).ToList();
            }

            else if (picLimit < 0)
            {
                cols = cols.ToList();
            }

            else
            {
                cols = new List<View_Collection_With_Original>();
            }

            PagingList<CollectionToDisplay> result = Helper.ConvertCollectionList(utils, cols, 0, 0, 2);
                // PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(cols, 0, c => Converter.ConvertCollectionWithOriginal(utils, c, 2));

            AlbumToDisplay alb = new AlbumToDisplay
                {
                    Id = album.Id,
                    Description = album.Description,
                    Name = album.Name,
                    Username = album.Username,
                    NickName = album.Nick_Name,
                    Tags = album.Tags,
                    NumberOfPictures = album.Collection_Count.HasValue ? album.Collection_Count.Value : 0,
                    NumberOfLike = album.Like_Count.HasValue ? album.Like_Count.Value : 0,
                    LikedByMe = string.IsNullOrEmpty(currentUsername) ? false : utils.DoesLikeBy(album.Id, currentUsername),
                    Pictures = result
                };

            if (replyLimit < 0)
            {
                alb.ReplyCollection = PagingHelper.Page<View_Reply, ReplyToDisplay>(utils.FindRepliesByAlbumId(album.Id), 0, r => Converter.ConvertReply(r));
            }
            if (replyLimit > 0)
            {
                alb.ReplyCollection = ConvertReplies(utils.FindRepliesByAlbumId(album.Id), replyLimit);
            }
            if (replyLimit == 0)
            {
                alb.ReplyCollection = new PagingList<ReplyToDisplay> { Count = 0, List = new List<ReplyToDisplay>() };
            }

            return alb;
        }

        //public static AlbumToDisplay ConvertBriefAlbum(View_Album album, DataUtils utils, string currentUsername, int picLimit)
        //{
        //    if (album == null)
        //    {
        //        return null;
        //    }

        //    IEnumerable<DB_Collection> cols = utils.FindCollection(album.Id, album.Username).OrderByDescending(c => c.Collect_Time).OrderByDescending(c => c.DB_Picture.DB_Collection.Count());
        //    if (picLimit > 0)
        //    {
        //        cols = cols.Take(picLimit).ToList();
        //    }

        //    else if (picLimit < 0)
        //    {
        //        cols = cols.ToList();
        //    }

        //    else
        //    {
        //        cols = new List<DB_Collection>();
        //    }

        //    PagingList<CollectionToDisplay> result = PagingHelper.Page<DB_Collection, CollectionToDisplay>(cols, 0, c => Converter.ConvertBriefCollection(utils, c));

        //    AlbumToDisplay alb = new AlbumToDisplay
        //    {
        //        Id = album.Id,
        //        Description = album.Description,
        //        Name = album.Name,
        //        Username = album.Username,
        //        NickName = album.Nick_Name,
        //        LikedByMe = string.IsNullOrEmpty(currentUsername) ? false : utils.DoesLikeBy(album.Id, currentUsername),
        //        Pictures = result
        //    };

        //    return alb;
        //}

        //public static PictureToDisplay ConvertCollection(DataUtils util, View_Collection2 col, int replyLimit)
        //{
        //    PictureToDisplay pic = new PictureToDisplay
        //    {
        //        AlbumId = col.Album_Id.HasValue ? col.Album_Id.Value : 0,
        //        AlbumName = string.IsNullOrWhiteSpace(col.Name) ? Resource.DefaultAlbumName : col.Name,
        //        Description = col.Description,
        //        Extention = col.Image_File_Ext,
        //        Url = ImageHelper.GetAccessPath(col.Image_Filename),
        //        Id = col.Id,
        //        Username = col.Username,
        //        NickName = col.Nick_Name,
        //        UploadTime = col.Collect_Time,
        //        NumberOfCollects = col.Collect_Count,
        //        Tags = col.Tags,
        //        OriginalPageTitle = col.Source_Page_Title,
        //        OriginalPageUrl = col.Source_Page_Url,
        //        Price = col.Price,
        //        Shop = col.Shop,
        //        Ratio = col.Ratio,
        //        TaobaoLink = col.Taobao_Link,

        //        CollectedTime = col.Collect_Time,
        //    };

        //    if (replyLimit < 0)
        //    {
        //        pic.ReplyCollection = PagingHelper.Page<View_Reply, ReplyToDisplay>(util.FindViewRepliesByCollectionId(col.Picture_Id), 0, r => Converter.ConvertReply(r));
        //    }
        //    if (replyLimit > 0)
        //    {
        //        pic.ReplyCollection = ConvertReplies(util.FindViewRepliesByCollectionId(col.Picture_Id), replyLimit);
        //    }
        //    if (replyLimit == 0)
        //    {
        //        pic.ReplyCollection = new PagingList<ReplyToDisplay> { Count = 0, List = new List<ReplyToDisplay>() };
        //    }

        //    return pic;
        //}

        //public static PictureToDisplay ConvertCollection(DataUtils util, View_Collection col, int replyLimit)
        //{
        //    PictureToDisplay pic = new PictureToDisplay
        //    {
        //        AlbumId = col.Picture_Album_Id.HasValue ? col.Picture_Album_Id.Value : 0,
        //        AlbumName = string.IsNullOrWhiteSpace(col.Name) ? Resource.DefaultAlbumName : col.Name,
        //        Description = col.Description,
        //        Extention = col.Image_File_Ext,
        //        Url = ImageHelper.GetAccessPath(col.Image_Filename),
        //        Id = col.Picture_Id,
        //        Username = col.Picture_Username,
        //        NickName = col.Nick_Name,
        //        UploadTime = col.Upload_Time,
        //        NumberOfCollects = col.Picture_Collect_Count.HasValue ? col.Picture_Collect_Count.Value : 0,
        //        Tags = col.Tags,
        //        OriginalPageTitle = col.Source_Page_Title,
        //        OriginalPageUrl = col.Source_Page_Url,
        //        Price = col.Price,
        //        Shop = col.Shop,
        //        Ratio = col.Ratio,
        //        TaobaoLink = col.Taobaoke_Link,

        //        CollectedTime = col.Upload_Time
        //    };

        //    if (replyLimit < 0)
        //    {
        //        pic.ReplyCollection = PagingHelper.Page<View_Reply, ReplyToDisplay>(util.FindViewRepliesByCollectionId(col.Picture_Id), 0, r => Converter.ConvertReply(r));
        //    }
        //    if (replyLimit > 0)
        //    {
        //        pic.ReplyCollection = ConvertReplies(util.FindViewRepliesByCollectionId(col.Picture_Id), replyLimit);
        //    }
        //    if (replyLimit == 0)
        //    {
        //        pic.ReplyCollection = new PagingList<ReplyToDisplay> { Count = 0, List = new List<ReplyToDisplay>() };
        //    }

        //    if (col.Collect_Type == 1)
        //    {
        //        if (col.Collect_Album_Id.HasValue)
        //        {
        //            DB_Album album = util.FindAlbumById(col.Collect_Album_Id.Value, false);
        //            pic.CollectedInAblbumId = album.Id;
        //            pic.CollectedInAlbumName = album.Name;
        //        }
        //        else
        //        {
        //            pic.CollectedInAblbumId = 0;
        //            pic.CollectedInAlbumName = Resource.DefaultAlbumName;
        //        }
        //        DB_User user = util.FindUserByUsername(col.Collect_Username);
        //        pic.CollectedByDisplay = user.Nick_Name;
        //        pic.CollectedTime = col.Collect_Time;
        //    }
        //    return pic;
        //}

        public static CollectionToDisplay ConvertCollectionWithOriginal(DataUtils util, View_Collection_With_Original col, int replyLimit)
        {
            if (col == null)
            {
                return null;
            }
            CollectionToDisplay collection = new CollectionToDisplay
            {
                AlbumId = col.Album_Id.HasValue ? col.Album_Id.Value : 0,
                AlbumName = string.IsNullOrWhiteSpace(col.Name) ? Resource.DefaultAlbumName : col.Name,
                Description = col.Description,
                Extention = col.Image_File_Ext,
                Url = ImageHelper.GetAccessPath(col.Image_Filename),
                Id = col.Id,
                Username = col.Username,
                NickName = col.Nick_Name,
                NumberOfCollects = col.Collect_Count,
                NumberOfLikes = col.Like_Count,
                Tags = col.Tags,
                OriginalPageTitle = col.Source_Page_Title,
                OriginalPageUrl = col.Source_Page_Url,
                Price = col.Price,
                Shop = col.Shop,
                Ratio = col.Ratio,
                TaobaoLink = col.Taobao_Link,

                CollectedTime = col.Collect_Time,
                IsOriginal = col.Is_Original == 1,
                PictureId = col.Picture_Id,
                UploadTime = col.Uploaded_Time,

                Recommended = col.On_Top == 1,

                IsMine = col.Username == Helper.FindCurrentUsername()
            };

            if (replyLimit < 0)
            {
                collection.ReplyCollection = PagingHelper.Page<View_Reply, ReplyToDisplay>(util.FindViewRepliesByCollectionId(col.Id), 0, r => Converter.ConvertReply(r));
            }
            if (replyLimit > 0)
            {
                var count = util.FindViewRepliesByCollectionId(col.Id).Count();
                collection.ReplyCollection = ConvertReplies(util.FindViewRepliesByCollectionId(col.Id), replyLimit);
                collection.ReplyCollection.Count = count;
            }
            if (replyLimit == 0)
            {
                collection.ReplyCollection = new PagingList<ReplyToDisplay> { Count = 0, List = new List<ReplyToDisplay>() };
            }

            if (col.OriginalAlbumId.HasValue)
            {
                collection.OriginalAlbumId = col.OriginalAlbumId.Value;
                collection.OriginalAlbumName = col.OriginalAlbumName;
            }
            else
            {
                collection.OriginalAlbumId = 0;
                collection.OriginalAlbumName = Resource.DefaultAlbumName;
            }

            collection.OwnerNickname = col.OwnerNick;
            collection.OwnerUsername = col.OwnerName;

            // Check whether the current user already collected the picture
            // if it is, add the information
            string username = Helper.FindCurrentUsername();
            if (username != null)
            {
                if (username == collection.Username) // 自己的图片，设定收集专辑
                {
                    collection.IsMine = true;
                    collection.CollectedInMyAblbumId = collection.AlbumId;
                }
                else
                {
                    collection.IsMine = false;
                    // add my collection information
                    View_Collection2 c = util.FindViewCollection2(username, col.Picture_Id);
                    if (c != null)
                    {
                        if (c.Album_Id.HasValue)
                        {
                            // default album
                            collection.CollectedInMyAblbumId = c.Album_Id.Value;
                            collection.CollectedInMyAlbumName = c.Name;
                        }
                        else
                        {
                            collection.CollectedInMyAlbumName = Resource.DefaultAlbumName;
                            collection.CollectedInMyAblbumId = 0;
                        }
                    }
                    else
                    {
                        collection.CollectedInMyAblbumId = -1;
                    }
                }


                // 是否被我喜欢
                DB_Like_Picture lp = util.FindLikePictures().Where(l => l.Picture_Id == collection.PictureId && l.Username == username).SingleOrDefault();

                collection.LikedByMe = lp != null;
            }


            return collection;

        }

        //public static PictureToDisplay ConvertCollection(DataUtils util, DB_Collection col, int replyLimit)
        //{
        //    PictureToDisplay pic = ConvertPicture(util, util.FindViewPictureById(col.Picture_Id), replyLimit);
        //    if (col.Collect_Type == 1)
        //    {
        //        if (col.Album_Id.HasValue)
        //        {
        //            DB_Album album = util.FindAlbumById(col.Album_Id.Value, false);
        //            pic.CollectedInAblbumId = album.Id;
        //            pic.CollectedInAlbumName = album.Name;
        //        }
        //        else
        //        {
        //            pic.CollectedInAblbumId = 0;
        //            pic.CollectedInAlbumName = Resource.DefaultAlbumName;
        //        }
        //        DB_User user = util.FindUserByUsername(col.Username);
        //        pic.CollectedByDisplay = user.Nick_Name;
        //        pic.CollectedTime = col.Collect_Time;
        //    }
        //    return pic;
        //}

        //public static PictureToDisplay ConvertBriefCollection(DataUtils util, DB_Collection col)
        //{
        //    PictureToDisplay pic = ConvertBriefPicture(util, util.FindPictureById(col.Picture_Id));
        //    return pic;
        //}

        public static UserToDisplay ConvertFan(DataUtils utils, DB_Fan fan, int includePictureNumber)
        {
            IList<DB_Notice> list = utils.FindNoticeByFan(fan.FansUsername, fan.WatchedUsername).ToList();
            if (list.Count > 0)
            {
                DB_Notice n = list[0];
                if (n != null && n.Has_Read == 0)
                {
                    n.Has_Read = 1;
                    utils.Commit();
                }
            }
            return ConvertUser(utils, fan.DB_User, includePictureNumber);
        }

        public static UserToDisplay ConvertUser(DataUtils utils, DB_User user, int includePictureNumber)
        {
            if (user == null) return null;

            // Retrieve Location information
            string locationName = "";
            if (user.Location_Id != null)
            {
                DB_Location location = utils.FindLocationById(user.Location_Id.Value);
                if (location.Parent_Id != null)
                {
                    DB_Location province = utils.FindLocationById(location.Parent_Id.Value);
                    locationName = province.Name + " " + location.Name;
                }
                else
                {
                    Log.Warn(string.Format("Location id {0} for user {1} does not have a parent", location.Id, user.Username));
                }
            }

            IQueryable<DB_User> fans = utils.FindFans(user.Username).Select(f => f.DB_User);
            IQueryable<DB_User> attentions = utils.FindAttentions(user.Username);

            // check the current user
            bool hasMyAttention = false;
            bool isMyFan = false;
            if (!string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
            {
                string myName = (string)HttpContext.Current.Session["username"];
                hasMyAttention = utils.IsWatching(myName, user.Username);
                isMyFan = utils.IsWatching(user.Username, myName);
            }

            UserToDisplay result = new UserToDisplay
            {
                Username = user.Username,
                NickName = user.Nick_Name,
                Location = locationName,
                SelfIntroduction = user.Self_Introduction,
                NumberOfFans = fans.Count(),
                Fans = fans.Take(10).Select(f => new LinkItem { Username = f.Username, NickName = f.Nick_Name }).ToList(),
                NumberOfAttentions = attentions.Count(),
                Attentions = attentions.Take(10).Select(f => new LinkItem { Username = f.Username, NickName = f.Nick_Name }).ToList(),
                HasMyAttention = hasMyAttention,
                IsMyFan = isMyFan,
                NumberOfAlbums = user.DB_Album.Count(),
                NumberOfLikedAlbums = utils.FindAlbumByLikeUsername(user.Username).Count(),
                NumberOfOriginal = utils.FindCollection2().Where(c => c.Username == user.Username && c.Is_Original == 1).Count(),
                NumberOfCollect = utils.FindCollection2().Where(c => c.Username == user.Username && c.Is_Original == 0).Count(),
                IsIdentified = user.Is_Identified == 1,
                IdentificationInfo = user.Identification_Info,
                LikeCount = user.Like_Count
            };

            foreach (var aa in user.DB_Account_Association)
            {
                AssociationParty party = (AssociationParty)aa.Party;
                switch (party)
                {
                    case AssociationParty.Sina:
                        {
                            result.SinaWeiboLinkName = aa.External_Link_Name;
                            break;
                        }
                    case AssociationParty.QQ:
                        {
                            result.QQSpaceLinkName = aa.External_Link_Name;
                            break;
                        }
                    case AssociationParty.QQWB:
                        {
                            result.QQWeiboLinkName = aa.External_Link_Name;
                            break;
                        }
                }
            }

            if (includePictureNumber > 0)
            {
                result.PictureList = user.DB_Picture.OrderByDescending(p => p.DB_Collection.Count()).Take(includePictureNumber).Select(p => new BriefPictureInfo
                {
                    Url = ImageHelper.GetAccessPath(p.Image_Filename),
                    FileExt = p.Image_File_Ext,
                    PictureId = p.Id
                }).ToList();
            }
            return result;
        }

        public static NoticeToDisplay ConvertNotice(DataUtils utils, DB_Notice notice)
        {
            NoticeToDisplay n = new NoticeToDisplay
            {
                ActionTime = notice.Notice_Time,
                ActionType = notice.Notice_Type,
                ActionUsername = notice.Action_Username,
                ActionUserNickName = notice.Action_UsernameDB_User.Nick_Name,
                UserImageUrl = ImageHelper.GetProfilePhotoPath(notice.Action_Username),
                TargetUsername = notice.Username
            };

            switch (notice.Notice_Type)
            {
                case (int)NoticeType.Collect:
                    View_Collection_With_Original collection = utils.FindViewCollectionWithOriginalById(notice.Collect_Id.Value);
                    n.AlbumName = collection.Album_Id.HasValue ? collection.Name : Resource.DefaultAlbumName;
                    n.AlbumId = collection.Album_Id.HasValue ? collection.Album_Id.Value : 0;

                    if (collection.Collected_Target_Id.HasValue && collection.OwnerName != notice.Username) // 自己不是原作者
                    {
                        DB_Collection2 col2 = utils.FindCollection2(collection.Collected_Target_Id.Value);
                        if (col2 != null)
                        {
                            n.CollectionDesc = col2.Description;
                            n.CollectionId = col2.Id;
                        }
                    }
                    else
                    {
                        n.CollectionId = collection.Original_Collection_Id.HasValue ? collection.Original_Collection_Id.Value : 0;
                        n.CollectionDesc = collection.Original_Description;
                    }

                    n.TargetImageUrl = ImageHelper.GetAccessPath(collection.Image_Filename);
                    n.TargetImageExt = collection.Image_File_Ext;
                    break;
                case (int)NoticeType.Fan:
                    break;
                case (int)NoticeType.Announcement:
                    {
                        n.AnnouncementText = notice.DB_Announcement.Text;
                        break;
                    }
                case (int)NoticeType.Like:
                    n.AlbumName = notice.DB_Like.DB_Album.Name;
                    n.AlbumId = notice.DB_Like.Album_Id;
                    break;
                case (int)NoticeType.Reply:
                    DB_Reply reply = notice.DB_Reply;
                    n.ReplyId = reply.Id;
                    if (reply.Album_Id.HasValue)
                    {
                        n.AlbumId = reply.Album_Id.Value;
                        n.AlbumName = reply.DB_Album.Name;
                        n.OwnerUsername = reply.DB_Album.Username;
                    }
                    if (reply.Collection_Id.HasValue)
                    {
                        View_Collection2 col = utils.FindViewCollection2(reply.Collection_Id.Value);
                        n.CollectionDesc = col.Description;
                        n.CollectionId = reply.Collection_Id.Value;
                        n.OwnerUsername = col.Username;
                        n.TargetImageUrl = ImageHelper.GetAccessPath(col.Image_Filename);

                        n.TargetImageExt = col.Image_File_Ext;
                    }
                    if (reply.Discussion_Id.HasValue)
                    {
                        n.DiscussionId = reply.Discussion_Id.Value;
                        n.DiscussionSubject = reply.DB_Discussion.Subject;
                        n.OwnerUsername = reply.DB_Discussion.Username;
                    }
                    if (reply.Reply_Id.HasValue)
                    {
                        n.RepliedReplyId = reply.Reply_Id.Value;
                        DB_Reply targetReply = utils.FindReplyById(n.ReplyId);
                        if (targetReply.Album_Id.HasValue)
                        {
                            n.OwnerUsername = targetReply.DB_Album.Username;
                        }
                        if (targetReply.Collection_Id.HasValue)
                        {
                            n.OwnerUsername = targetReply.DB_Collection2.Username;
                        }
                        if (targetReply.Discussion_Id.HasValue)
                        {
                            n.OwnerUsername = targetReply.DB_Discussion.Username;
                        }
                    }
                    n.Reply = notice.DB_Reply.Text;
                    break;
            }
            return n;
        }

        public static DiscussionToDisplay ConvertDiscussion(DB_Discussion discussion, int includeReplyNumbers)
        {
            DiscussionToDisplay result = new DiscussionToDisplay
                {
                    Id = discussion.Id,
                    PostTime = discussion.Post_Time,
                    Subject = discussion.Subject,
                    Text = discussion.Text,
                    Username = discussion.Username,
                    NickName = discussion.DB_User.Nick_Name,
                    BoardId = discussion.Board_Id,
                    BoardName = discussion.DB_Discussion_Board.Board_Name
                };

            if (includeReplyNumbers == 0)
            {
                result.ReplyList = new PagingList<ReplyToDisplay> { Count = discussion.DB_Reply.Count(), List = new List<ReplyToDisplay>() };
            }
            else
            {
                IEnumerable<DB_Reply> replies = discussion.DB_Reply.OrderByDescending(r => r.Post_Time);
                if (includeReplyNumbers > 0)
                {
                    replies = replies.Take(includeReplyNumbers);
                }

                result.ReplyList = PagingHelper.Select(PagingHelper.Page<DB_Reply>(replies, 0), r => Converter.ConvertReply(r));
            }

            return result;
        }
    }
}