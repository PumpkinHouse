﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;

namespace PumpkinHouse.Utils
{
    public class ImageProcessor : IDisposable
    {
        public Bitmap _currentBitmap { private set; get; }

        #region IDisposable Methods

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_currentBitmap != null)
                    {
                        _currentBitmap.Dispose();
                    }
                }

                // Note disposing has been done.
                disposed = true;
            }
        }

        #endregion

        public ImageProcessor(Bitmap bitmap)
        {
            if ((bitmap.PixelFormat & PixelFormat.Indexed) > 0)
            {
                Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
                this._currentBitmap = (Bitmap)bitmap.Clone(rect, PixelFormat.Format32bppPArgb);
            }
            else
            {
                this._currentBitmap = (Bitmap)bitmap.Clone();
            }
        }

        public void SquareThumbnail(int dimension)
        {
            int size = Math.Min(_currentBitmap.Width, _currentBitmap.Height);
            Crop(size, size);
            Resize(dimension, dimension);
        }

        public void FitWidthThumbnail(int width)
        {
            double ratio = 1.0 * width / _currentBitmap.Width;
            if (ratio >= 1) return;

            int height = (int)(_currentBitmap.Height * ratio);
            Resize(width, height);
        }

        public void Crop(int width, int height)
        {
            using (Bitmap temp = (Bitmap)_currentBitmap)
            {
                using (Bitmap bmap = (Bitmap)temp.Clone())
                {
                    if (width > temp.Width) width = temp.Width;
                    if (height > temp.Height) height = temp.Height;

                    int xPosition = (temp.Width - width) / 2;
                    int yPosition = (temp.Height - height) / 2;

                    Rectangle rect = new Rectangle(xPosition, yPosition, width, height);
                    _currentBitmap = (Bitmap)bmap.Clone(rect, bmap.PixelFormat);
                }
            }
        }

        public void Crop(int xStart, int yStart, int width, int height)
        {
            if (xStart < 0) xStart = 0;
            if (yStart < 0) yStart = 0;
            using (Bitmap temp = (Bitmap)_currentBitmap)
            {
                using (Bitmap bmap = (Bitmap)temp.Clone())
                {
                    if (xStart + width > temp.Width) width = temp.Width - xStart;
                    if (yStart + height > temp.Height) height = temp.Height - yStart;

                    if (width < 0 || height < 0)
                    {
                        return;
                    }
                    Rectangle rect = new Rectangle(xStart, yStart, width, height);
                    _currentBitmap = (Bitmap)bmap.Clone(rect, bmap.PixelFormat);
                }
            }
        }

        public void Save(string outputFileName)
        {
            _currentBitmap.Save(outputFileName, ImageFormat.Jpeg);
        }


        public void Resize(int targetSizeW, int targetSizeH)
        {
            Bitmap original = _currentBitmap;
            int targetH, targetW;
            targetW = targetSizeW;
            targetH = (int)(original.Height * ((float)targetSizeW / (float)original.Width));
            if (targetH > targetSizeH)
            {
                targetH = targetSizeH;
                targetW = (int)(original.Width * ((float)targetSizeH / (float)original.Height));
            }
            if (targetSizeW < (int)original.Width || targetSizeH < (int)original.Height)
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    original.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    using (System.Drawing.Image imgPhoto = System.Drawing.Image.FromStream(stream))
                    {
                        // Create a new blank canvas.  The resized image will be drawn on this canvas.
                        using (Bitmap bmPhoto = new Bitmap(targetW, targetH, PixelFormat.Format32bppPArgb))
                        {
                            bmPhoto.SetResolution(72, 72);
                            using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
                            {
                                grPhoto.SmoothingMode = SmoothingMode.HighQuality;
                                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;
                                grPhoto.DrawImage(imgPhoto, new Rectangle(0, 0, targetW, targetH), 0, 0, original.Width, original.Height, GraphicsUnit.Pixel);
                                _currentBitmap = (Bitmap)bmPhoto.Clone();
                            }
                        }
                    }
                }
            }
        }

        public void InsertImage(string imagePath, int xPosition, int yPosition)
        {
            using (Bitmap i_bitmap = new Bitmap(imagePath))
            {
                using (Bitmap temp = (Bitmap)_currentBitmap)
                {
                    using (Bitmap bmap = (Bitmap)temp.Clone())
                    {
                        Graphics gr = Graphics.FromImage(bmap);
                        if (!string.IsNullOrEmpty(imagePath))
                        {
                            Rectangle rect = new Rectangle(xPosition, yPosition,
                        i_bitmap.Width, i_bitmap.Height);
                            gr.DrawImage(Bitmap.FromFile(imagePath), rect);
                        }
                        _currentBitmap = (Bitmap)bmap.Clone();
                    }
                }
            }
        }

        public Bitmap PicSized(Bitmap originBmp, double iSize)
        {
            int w = Convert.ToInt32(originBmp.Width * iSize);
            int h = Convert.ToInt32(originBmp.Height * iSize);
            Bitmap resizedBmp = new Bitmap(w, h);
            using (Graphics g = Graphics.FromImage(resizedBmp))
            {
                //设置高质量插值法  
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                //设置高质量,低速度呈现平滑程度  
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                //消除锯齿
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.DrawImage(originBmp, new Rectangle(0, 0, w, h), new Rectangle(0, 0, originBmp.Width, originBmp.Height), GraphicsUnit.Pixel);
                g.Dispose();
                return resizedBmp;
    }
        }

        public void GenerateHeader()
        {
            const int headerWidth = 1024;
            const int headerHeight = 218;
            const int headerMainHeight = 186;

            string headerFile = Path.Combine(ImageHelper.ImageFilesRootPath, "header.png");
            using (Bitmap temp = (Bitmap)_currentBitmap)
            {
                int width = temp.Width;
                int height = temp.Height;

                int newHeaderHeight = headerHeight * width / headerWidth;
                int newHeaderMainHeight = headerMainHeight * width / headerWidth;

                using (Bitmap bmPhoto = new Bitmap(width, newHeaderMainHeight + height, PixelFormat.Format32bppPArgb))
                {
                    bmPhoto.SetResolution(72, 72);
                    using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
                    {
                        grPhoto.SmoothingMode = SmoothingMode.HighQuality;
                        grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        grPhoto.DrawImage(temp, new Rectangle(0, newHeaderMainHeight, width, height), 0, 0, width, height, GraphicsUnit.Pixel);

                        using (Image headerPhoto = Bitmap.FromFile(headerFile))
                        {
                            double resizeRatio;
                            int offsetW, offsetH;
                            double headerRatio = headerWidth * 1.0 / headerHeight;
                            double imageRatio = width * 1.0 / height;
                            if (headerRatio > 1)
                            {
                                int expectedHeaderHeight = headerHeight * width / headerWidth ;
                                resizeRatio = (expectedHeaderHeight + 2) * 1.0 / headerHeight;
                                offsetH = 1;
                                offsetW = (int)headerRatio;
                            }
                            else
                            {
                                resizeRatio = (width + 2) * 1.0 / headerWidth;
                                offsetW = 1;
                                offsetH = (int)(1.0 / headerRatio);
                            }

                            // 缩放头部模板
                            using (Bitmap newHeaderPhoto = PicSized(new Bitmap(headerFile), resizeRatio))
                            {
                                grPhoto.DrawImage(newHeaderPhoto, new Rectangle(0, 0, width, newHeaderHeight), offsetW, offsetH, width, newHeaderHeight, GraphicsUnit.Pixel);
                            }
                        }
                        _currentBitmap = (Bitmap)bmPhoto.Clone();
                    }
                }


            }
        }
    }
}