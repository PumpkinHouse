﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.ServiceModel.Web;
using HtmlAgilityPack;
using System.IO.Compression;

namespace PumpkinHouse.Utils
{
    public static class UrlHelper
    {
        public static string ProcessUrl(string url, string src)
        {
            if (string.IsNullOrWhiteSpace(url) || string.IsNullOrWhiteSpace(src))
            {
                return null;
            }

            Uri srcUri = new Uri(src);
            if (!srcUri.IsAbsoluteUri)
            {
                return null;
            }

            while (url.StartsWith("./"))
            {
                url = url.Substring(2);
            }

            if (url.StartsWith("//"))
            {
                url = srcUri.Scheme + ":" + url;
            }

            else if (url.StartsWith("/"))
            {
                url = srcUri.GetLeftPart(UriPartial.Authority) + url;
            }
            else if (!url.StartsWith("http://") && !url.StartsWith("https://"))
            {
                // in the format of "abc.html", relative path

                string[] segments = srcUri.Segments;
                string result = string.Join("", segments, 0, segments.Length - 1);
                url = srcUri.Scheme + "://" + srcUri.DnsSafeHost + result + url;
            }

            Uri u = new Uri(url);
            if (!u.IsAbsoluteUri)
            {
                return null;
            }
            return url;
        }

        public static string DownloadHtmlPage(string url)
        {
            try
            {
                byte[] buffer;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
                {
                        if (resp.ContentEncoding.ToLower().Contains("gzip"))
                        {
                            using (Stream cs = new GZipStream(resp.GetResponseStream(), CompressionMode.Decompress))
                            {
                                buffer = ReadStream(cs);
                            }
                        }
                        else if (resp.ContentEncoding.ToLower().Contains("deflategzip"))
                        {
                            using (Stream cs = new DeflateStream(resp.GetResponseStream(), CompressionMode.Decompress))
                            {
                                buffer = ReadStream(cs);
                            }
                        }
                        else
                        {
                            using (Stream s = resp.GetResponseStream())
                            {
                                buffer = ReadStream(s);
                            }
                        }


                    string pageEncoding = "";
                    Encoding e = Encoding.UTF8;
                    if (resp.ContentEncoding != "" && resp.ContentEncoding != "gzip")
                        pageEncoding = resp.ContentEncoding;
                    else if (resp.CharacterSet != "")
                        pageEncoding = resp.CharacterSet;
                    else if (resp.ContentType != "")
                        pageEncoding = GetCharacterSet(resp.ContentType);

                    if (pageEncoding == "")
                        pageEncoding = GetCharacterSet(buffer);

                    string data = ParseData(buffer, pageEncoding);

                    var doc = new HtmlDocument();
                    doc.LoadHtml(data);
                    HtmlNode node = doc.DocumentNode.SelectSingleNode("//meta[translate(@http-equiv,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=\"content-type\"]");
                    if (node == null)
                    {
                        node = doc.DocumentNode.SelectSingleNode("//meta[@charset]");
                        if (node != null)
                        {
                            pageEncoding = node.Attributes["charset"].Value;
                        }
                        else
                        {
                            // 可能是Encoding不正确
                            data = ParseData(buffer, "gb2312");
                            doc.LoadHtml(data);
                            node = doc.DocumentNode.SelectSingleNode("//meta[translate(@http-equiv,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=\"content-type\"]");
                            if (node != null)
                            {
                                pageEncoding = GetCharacterSet(node.Attributes["content"].Value);
                            }
                        }
                    }
                    else
                    {
                        pageEncoding = GetCharacterSet(node.Attributes["content"].Value);
                    }

                    data = ParseData(buffer, pageEncoding);

                    return data;
                }
            }
            catch (Exception)
            {
                throw new WebFaultException<ErrorCode>(ErrorCode.UrlNotValid, HttpStatusCode.BadRequest);
            }
        }

        private static string ParseData(byte[] buffer, string pageEncoding)
        {
            string data = null;
            if (pageEncoding != "")
            {
                try
                {
                    Encoding e = Encoding.GetEncoding(pageEncoding);
                    data = e.GetString(buffer);
                }
                catch
                {
                }
            }

            return data;
        }

        private static string GetCharacterSet(string s)
        {
            s = s.ToUpper();
            int start = s.LastIndexOf("CHARSET");
            if (start == -1)
                return "";

            start = s.IndexOf("=", start);
            if (start == -1)
                return "";

            start++;
            s = s.Substring(start).Trim();
            int end = s.Length;

            int i = s.IndexOf(";");
            if (i != -1)
                end = i;
            i = s.IndexOf("\"");
            if (i != -1 && i < end)
                end = i;
            i = s.IndexOf("'");
            if (i != -1 && i < end)
                end = i;
            i = s.IndexOf("/");
            if (i != -1 && i < end)
                end = i;

            return s.Substring(0, end).Trim();
        }

        private static string GetCharacterSet(byte[] data)
        {
            string s = Encoding.Default.GetString(data);
            return GetCharacterSet(s);
        }

        private static byte[] ReadStream(Stream s)
        {
            try
            {
                byte[] buffer = new byte[8096];
                using (MemoryStream ms = new MemoryStream())
                {
                    while (true)
                    {
                        int read = s.Read(buffer, 0, buffer.Length);
                        if (read <= 0)
                        {
                            return ms.ToArray();
                        }
                        ms.Write(buffer, 0, read);
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}