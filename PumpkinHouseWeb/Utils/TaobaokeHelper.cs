﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Top.Api;
using Top.Api.Request;
using System.Text.RegularExpressions;
using Top.Api.Response;
using Top.Api.Domain;
using log4net;

namespace PumpkinHouse.Utils
{
    public class TaobaokeHelper
    {
        private static ILog Log = LogManager.GetLogger(typeof(TaobaokeHelper));

        public static string FindTaobaokeLink(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentNullException("url");
            }

            string serviceUrl = "http://gw.api.taobao.com/router/rest";
            string appkey = "12579490";
            string appsecret = "37b097f42d074b04f6cd091e66ec02ec";
            ITopClient client = new DefaultTopClient(serviceUrl, appkey, appsecret);

            TaobaokeItemsConvertRequest req = new TaobaokeItemsConvertRequest();
            req.Fields = "click_url";
            req.Nick = "raymanyoung";

            Regex regex = new Regex(@"id=(\d+)");
            Match match = regex.Match(url);
            if (match.Success)
            {
                string id = match.Groups[1].Value;

                req.NumIids = id;
                try
                {
                    TaobaokeItemsConvertResponse response = client.Execute(req);

                    List<TaobaokeItem> items = response.TaobaokeItems;
                    if (items.Count > 0)
                    {
                        TaobaokeItem item = items[0];
                        return item.ClickUrl;
                    }
                    else
                    {
                        Log.Error("Does not have taobao link");
                        return null;
                    }
                }
                catch (Exception e)
                {
                    Log.Fatal("exception occurs when getting taobaolink");
                    Log.Fatal(e.Message);
                    Log.Fatal(e.StackTrace);
                    return null;
                }
            }
            else
            {
                Log.Error("Could not parse the item id");
                return null;
            }
        }
    }
}