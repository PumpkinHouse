﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

namespace PumpkinHouse.Utils
{
    public enum FilterResult
    {
        Pass,
        RecordOnly,
        Replace,
        Banned
    }
    public static class KeywordFilterHelper
    {
        private static Object olock = new Object();

        public static void LoadKeywords(string configFile)
        {
            lock (olock)
            {
                using (TextReader tr = new StreamReader(configFile, Encoding.UTF8))
                {
                    string w;
                    while ((w = tr.ReadLine()) != null)
                    {
                        if (string.IsNullOrWhiteSpace(w)) continue;
                        int index = fastToLower(w[0]);
                        WordGroup fw = _wordTable[index];
                        if (fw == null)
                        {
                            fw = new WordGroup();
                            _wordTable[index] = fw;
                        }
                        fw.AppendWord(w, FilterResult.Banned);
                    }
                }
            }
        }
        
        private static WordGroup[] _wordTable = new WordGroup[65536];

        public static FilterResult Filter(ref string source, char replaceChar)
        {
            //NOTE::
            // 如果方法返回 FilterResult.Replace或者FilterResult.Banned，则原字符串的某些字会被替代为星号，替代后的字符串可以由source取得
            if (String.IsNullOrEmpty(source)) return FilterResult.Pass;
            FilterResult result = FilterResult.Pass;
            char[] tempString = null;
            int start = 0;
            for (; start < source.Length; start++)
            {
                WordGroup fw = _wordTable[fastToLower(source[start])];
                if (fw != null)
                {
                    for (int idx = 0; idx < fw.Count; idx++)
                    {
                        WordEntity we = fw.GetItem(idx);
                        int matchLength = 0;
                        if (we.Word.Length == 0 || checkString(source, we.Word, start, out matchLength))
                        {
                            FilterResult fr = we.HandleType;
                            if (fr > result) result = fr; //记录最高级别的处理方法
                            if (fr == FilterResult.Replace || fr == FilterResult.Banned)
                            {
                                //替换关键字
                                if (tempString == null) tempString = source.ToCharArray(); ;
                                for (int pos = 0; pos < matchLength; pos++)
                                {
                                    tempString[pos + start] = replaceChar;
                                }
                            }
                        }
                    }
                }
            }
            if (result > FilterResult.RecordOnly)
            {
                source = new string(tempString);
            }
            return result;
        }

        private static bool checkString(string source, string keyword, int sourceStart, out int matchLength)
        {
            bool found = false;
            int sourceOffset = 0;
            int keyIndex = 0;
            for (; keyIndex < keyword.Length; keyIndex++)
            {
                if (sourceOffset + sourceStart >= source.Length) break; //原始字符串已经全部搜索完毕
                if (keyword[keyIndex] == '*')
                {
                    //跳过可忽略的字符
                    while (sourceOffset + sourceStart < source.Length)
                    {
                        if (isIgnorableCharacter_CN(source[sourceOffset + sourceStart]))
                            sourceOffset++;
                        else
                            break;
                    }
                }
                else
                {
                    //比较字母
                    if (fastToLower(source[sourceOffset + sourceStart]) == (int)keyword[keyIndex])
                    {
                        if (keyIndex == keyword.Length - 1)
                        {
                            found = true;
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                    sourceOffset++;//移动原始字符串
                }
            }
            //如果匹配中关键字，则返回原字符串中被匹配中的文字的长度，否则返回0
            matchLength = sourceOffset + 1;
            return found;
        }

        private static int fastToLower(char character)
        {
            //将大写英文字母以及全/半角的英文字母转化为小写字母
            int charVal = (int)character;
            if (charVal <= 90)
            {
                if (charVal >= 65) //字母A-Z
                    return charVal - 65 + 97;
            }
            else if (charVal >= 65313)
            {
                if (charVal <= 65338)
                    return charVal - 65313 + 97; //全角大写A-Z
                else if (charVal >= 65345 && charVal <= 65370)
                    return charVal - 65345 + 97; //全角小写a-z
            }
            return charVal;
        }

        private static bool isIgnorableCharacter_CN(char character)
        {
            //NOTE::
            // 中文表意字符的范围 4E00-9FA5
            int charVal = (int)character;
            return !(charVal >= 0x4e00 && charVal <= 0x9fa5);
        }
    }

    // 单个过滤词条目
    class WordEntity
    {
        public string Word { get; set; }
        public FilterResult HandleType { get; set; }
    }

    // 过滤词的组
    class WordGroup
    {
        //NOTE::用于装载一组具有同一个字开头的过滤词
        private List<WordEntity> _words;
        public WordGroup()
        {
            _words = new List<WordEntity>();
        }
        public void AppendWord(string word, FilterResult handleType)
        {
            AppendWord(new WordEntity() { Word = word, HandleType = handleType });
        }
        public void AppendWord(WordEntity word)
        {
            _words.Add(word);
        }
        public int Count
        {
            get { return _words.Count; }
        }
        public WordEntity GetItem(int index)
        {
            return _words[index];
        }
    }
}