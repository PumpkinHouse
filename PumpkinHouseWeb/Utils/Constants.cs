﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PumpkinHouse.Utils
{
    public static class Constants
    {
        public const int UserTagMaxAllowed = 8;
        public const int DefaultPageSize = 20;

        public static int CurrentActivityId = 1;

    }

    public enum ErrorCode
    {
        ItemNotFound = 1,
        ItemAlreadyExist = 2,
        UrlNotValid = 3,
        InvalidImage = 4,
        ImageTooLarge = 5,
        SensitiveWord = 6,
        DownloadError = 7
    }

    
}