﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using System.Text;
using System.Runtime.Serialization;
using System.Net;
using log4net;

namespace PumpkinHouse.Utils
{
    [DataContract]
    public class ImageInfo
    {
        [DataMember]
        public int width { get; set; }
        [DataMember]
        public int height { get; set; }
        [DataMember]
        public string name { get; set; }
    }

    public class FileName
    {
        public string Base { get; set; }
        public string Extention { get; set; }
    }

    public static class ImageHelper
    {
        public static ILog Log = LogManager.GetLogger(typeof(ImageHelper));

        public static string ImageFilesRootPath { get; set; }
        public static string ProfilePhotoRootFolder { get; set; }

        private static string PublicAccessRootPath = @"http://image.mutouhezi.com/";
        private static string PublicPhotoRootPath = @"http://image.mutouhezi.com/";


        public enum PhotoSize
        {
            Tiny,
            Small,
            Medium,
            Large
        }

        private static Random random = new Random();

        public static FileName ParseFileName(string filename)
        {
            string[] split = filename.Split('.');
            string ext;
            string baseName;
            if (split.Length > 1)
            {
                ext = split[split.Length - 1];
                baseName = string.Join(".", split, 0, split.Length - 1);
            }
            else
            {
                baseName = filename;
                ext = null;
            }
            return new FileName { Base = baseName, Extention = ext };
        }

        public static string GetAccessPath(string filename)
        {
            string folderName = GetFolderName(filename);
            return PublicAccessRootPath + folderName + "/" + filename;
        }

        public static string GetProfilePhotoPath(string filename)
        {
            return PublicPhotoRootPath + "profile/" + filename;
        }

        public static string GetImagePath(string filename, string extention)
        {
            return Path.Combine(ImageFilesRootPath, GetFolderName(filename), filename + "." + extention);
        }

        public static string GetThumbnailPath(string filename, int width, int height, string extention)
        {
            string thumbName = string.Format("{0}_{1}_{2}_thumbnail.{3}", filename, width, height, extention);
            return Path.Combine(ImageFilesRootPath, GetFolderName(filename), thumbName);
        }

        public static string GetPhotoPath(string imageName)
        {
            return Path.Combine(ProfilePhotoRootFolder, imageName);
        }

        public static Bitmap GetPhoto(string imageName)
        {
            string path = Path.Combine(ProfilePhotoRootFolder, imageName);
            FileInfo fi = new FileInfo(path);
            if (fi.Exists && fi.DirectoryName.StartsWith(ProfilePhotoRootFolder))
            {
                System.Drawing.Bitmap image = new System.Drawing.Bitmap(path);
                return image;
            }
            throw new FileNotFoundException();
        }

        public static bool ProfilePhotoExists(string username, string ext)
        {
            string path = Path.Combine(ProfilePhotoRootFolder, username + "." + ext);
            FileInfo fi = new FileInfo(path);
            return fi.Exists;
        }

        public static Bitmap GetImage(string imageBaseName, string ext)
        {
            string name = imageBaseName;
            if (ext != null)
            {
                name += "." + ext;
            }
            return GetImage(name);
        }

        public static Bitmap GetImage(string imageName)
        {
            string folderName = GetFolderName(imageName);
            string path = Path.Combine(ImageFilesRootPath, folderName, imageName);

            FileInfo fi = new FileInfo(path);
            if (fi.Exists && fi.DirectoryName.StartsWith(ImageFilesRootPath))
            {
                System.Drawing.Bitmap image = new System.Drawing.Bitmap(path);
                return image;
            }
            throw new FileNotFoundException();
        }

        public static ImageInfo SaveSharedImage(HttpPostedFile file, string originalName)
        {
            string fileBaseName = GenerateFilename();
            string extention = FindExtention(originalName);
            string folderName = GetFolderName(fileBaseName);
            string folderPath = Path.Combine(ImageFilesRootPath, folderName);
            return SaveFile(folderPath, fileBaseName, extention, file);
        }

        public static ImageInfo SaveProfilePhoto(string username, HttpPostedFile file, string originalName)
        {
            string fileBaseName = username + "_temp";
            string folderPath = ProfilePhotoRootFolder;
            return SaveFile(folderPath, fileBaseName, "jpg", file);
        }

        private static ImageInfo SaveFile(string folderPath, string baseName, string extention, HttpPostedFile file)
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string name;
            if (extention == null)
            {
                name = baseName;
            }
            else
            {
                name = baseName + "." + extention;
            }
            string path = Path.Combine(folderPath, name);
            file.SaveAs(path);

            using (Bitmap image = new System.Drawing.Bitmap(path))
            {
                return new ImageInfo { height = image.Height, width = image.Width, name = name };
            }

        }

        private static string GetFolderName(string fileName)
        {
            return fileName.Substring(0, 8);
        }

        private static string GenerateFilename()
        {
            DateTime dt = DateTime.Now;

            return string.Format("{0}-{1}",
                dt.ToString("yyyyMMddHHmmss"),
                Helper.GenerateRandomString(6));
        }

        public static string DownloadFile(string url)
        {
            Log.Debug("DownloadFile() is called : " + url);
            Uri uri = new Uri(url);
            string originalName = uri.Segments[uri.Segments.Length - 1];
            string ext = FindExtention(originalName);
            string filename = GenerateFilename();

            string folderName = GetFolderName(filename);
            string folderPath = Path.Combine(ImageFilesRootPath, folderName);
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string fullname = filename;
            if (ext == null)
            {
                ext = "jpg";
            }

            fullname += "." + ext;

            DownloadHelper.DownloadImageFile(url, Path.Combine(folderPath, fullname));
            return fullname;
        }

        // Writes file to current folder
        private static void WriteToFile(string filePath, ref byte[] Buffer)
        {
            // Create a file
            using (FileStream newFile = new FileStream(filePath, FileMode.Create))
            {
                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);
            }
        }

        private static string FindExtention(string filename)
        {
            string[] split = filename.Split('.');
            string ext = null;
            if (split.Length >= 2)
            {
                ext = split[split.Length - 1];
                if (string.IsNullOrWhiteSpace(ext)) throw new ArgumentException("invalid filename: " + filename);
            }
            return ext;
        }

        public static void CreateDefaultProfilePhoto(string username)
        {
            try
            {
                File.Copy(GetPhotoPath("default.jpg"), GetPhotoPath(username + ".jpg"));
                File.Copy(GetPhotoPath("default_30_30.jpg"), GetPhotoPath(username + "_30_30.jpg"));
                File.Copy(GetPhotoPath("default_50_50.jpg"), GetPhotoPath(username + "_50_50.jpg"));
                File.Copy(GetPhotoPath("default_80_80.jpg"), GetPhotoPath(username + "_80_80.jpg"));
                File.Copy(GetPhotoPath("default_180_180.jpg"), GetPhotoPath(username + "_180_180.jpg"));
            }
            catch (IOException e)
            {
                Log.Error("exception during CreateDefaultProfilePhoto for " + username);
                Log.Error(e.Message);
            }
        }

        public static bool IsFileTypeSupported(string fileType)
        {
            if (fileType.Contains(";"))
            {
                fileType = fileType.Split(';')[0];
            }

            string[] supportedTypes = new string[] { "image/jpeg", "image/pjpeg", "image/gif", "image/png", "image/jpg", "image/bmp" };
            foreach (string type in supportedTypes)
            {
                if (type.Equals(fileType, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }
    }
}