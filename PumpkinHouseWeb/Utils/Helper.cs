﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;
using System.Text;
using System.ServiceModel.Web;
using System.Net;
using System.Drawing;
using System.Transactions;
using log4net;

namespace PumpkinHouse.Utils
{
    public static class Helper
    {
        private static ILog Log = LogManager.GetLogger(typeof(Helper));

        public static string FindCurrentUsername()
        {
            return HttpContext.Current == null ? "" : string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
        }

        public static IQueryable<DB_Tag> FindCommonTags(DataUtils utils)
        {
            return utils.FindCommonTags();
        }

        public static string MergeTags(string existingTags, string newTags)
        {
            string[] eTags = existingTags == null ? new string[0] : existingTags.Split(' ');
            string[] nTags = newTags == null ? new string[0] : newTags.Split(' ');

            List<string> result = new List<string>();
            foreach (string s in eTags)
            {
                string v = s.Trim();
                if (string.IsNullOrWhiteSpace(v)) continue;
                if (!result.Contains(v)) result.Add(v);
            }

            foreach (string s in nTags)
            {
                string v = s.Trim();
                if (string.IsNullOrWhiteSpace(v)) continue;
                if (!result.Contains(v)) result.Add(v);
            }

            return string.Join(" ", result);
        }

        public static AlbumToDisplay GetDefaultAlbumInstance(DataUtils util, DB_User user, int picLimit, int replyLimit)
        {
            PagingList<CollectionToDisplay> pics = PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(util.FindViewCollectionWithOriginal(0, user.Username), 0, p => Converter.ConvertCollectionWithOriginal(util, p, replyLimit), 0, (long)picLimit);
            return new AlbumToDisplay
            {
                Id = 0,
                Description = "",
                Name = Resource.DefaultAlbumName,
                Tags = "",
                NumberOfPictures = pics.Count,
                Username = user.Username,
                NickName = user.Nick_Name,
                Pictures = pics,
                ReplyCollection = new PagingList<ReplyToDisplay> { Count = 0, List = new List<ReplyToDisplay>() }
            };
        }

        public static IQueryable<DB_Tag> FindTagsByUsername(TagType type, string username, DataUtils utils)
        {
            return utils.FindTagsByUsername(username).Where(t => t.Type == (byte)type);
        }

        public static DB_Tag FindTagToReplace(IQueryable<DB_Tag> tags)
        {
            return tags.OrderBy(t => t.Updated_Time).First();
        }

        public static string ValidateUser(string email, string password)
        {
            using (DataUtils util = new DataUtils())
            {
                DB_UserCredential cred = util.FindCredentialByEmail(email);

                if (cred != null)
                {
                    if (Utils.SimpleHash.VerifyHash(password, cred.EncryptedPassword))
                    {
                        return util.FindUserByEmail(email).Username;
                    }
                }
                return null;
            }
        }

        public static string GenerateUsername(DataUtils utils)
        {
            string username = GenerateUsername();
            int count = 0;
            while (utils.FindUserByUsername(username) != null)
            {
                username = Helper.GenerateUsername();
                count++;
                if (count == 1000)
                {
                    //Log.Error("could not find good username even after 100 attempts");
                    throw new WebFaultException<string>(Resource.GeneralError, HttpStatusCode.BadRequest);
                }
            }
            return username;
        }

        private static string GenerateUsername()
        {
            Random random = new Random(DateTime.Now.Millisecond);

            long uid = random.Next(100000, 10000000);
            uid = uid * 100000 + random.Next(1, 100000);
            return "l" + uid.ToString() + DateTime.Now.ToString("hhmmss");
        }


        public static string GenerateRandomString(int length)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                sb.Append((char)(random.Next() % 26 + 'a'));
            }
            return sb.ToString();
        }

        public enum ResetPasswordStatus
        {
            Success = 0,
            InvalidCode = 1,
            CodeExpired = 2,
            ReadyToReset = 3
        }

        public static ResetPasswordStatus ValidateCode(DataUtils utils, string email, string code)
        {
            DB_Token t = utils.FindTokenByEmail(email);
            if (t == null)
            {
                return ResetPasswordStatus.InvalidCode;
            }
            if (t.Code != code)
            {
                return ResetPasswordStatus.InvalidCode;
            }
            if (DateTime.Now.AddDays(-2) > t.Time)
            {
                return ResetPasswordStatus.CodeExpired;
            }

            return ResetPasswordStatus.ReadyToReset;
        }

        public static ResetPasswordStatus ResetPassword(DataUtils utils, string email, string code, string newPassword)
        {
            ResetPasswordStatus status = ValidateCode(utils, email, code);

            if (status == ResetPasswordStatus.ReadyToReset)
            {
                DB_User user = utils.FindUserByEmail(email);
                DB_UserCredential c = utils.FindCredentialByEmail(user.Email);
                c.EncryptedPassword = SimpleHash.ComputeHash(newPassword, null);
                utils.Commit();

                return ResetPasswordStatus.Success;
            }

            return status;
        }

        //public static PagingList<PictureToDisplay> FindAllPictures(DataUtils utils, HomePageMode mode, string keyword, bool commodityOnly, int pageNumber)
        //{
        //    IQueryable<DB_Picture> source = utils.FindAllPictures(keyword, commodityOnly);

        //    switch (mode)
        //    {
        //        case HomePageMode.Default:
        //            {
        //                source = source.OrderByDescending(p=>p.On_Top_Update_Time);
        //                break;
        //            }
        //        case HomePageMode.Hot:
        //            {
        //                source = source.OrderByDescending(p => p.Seed);
        //                break;
        //            }
        //        case HomePageMode.Latest:
        //            {
        //                source = source.OrderByDescending(p => p.Upload_Time);
        //                break;
        //            }
        //    }

        //    return PagingHelper.Page<DB_Picture, PictureToDisplay>(source, pageNumber, p => Converter.ConvertPicture(utils, p, 2));

        //}

        public static PagingList<CollectionToDisplay> FindAllOriginalViewCollectionsOld(DataUtils utils, HomePageMode mode, string keyword, bool commodityOnly, int pageNumber)
        {
            IQueryable<View_Collection_With_Original> source = utils.FindViewCollectionWithOriginal(keyword, commodityOnly).Where(c => c.Is_Original == 1);
            //            IQueryable<View_Picture> source = utils.FindAllViewPictures(keyword, commodityOnly);

            switch (mode)
            {
                case HomePageMode.Default:
                    {
                        source = source.OrderByDescending(c => c.Collect_Count).OrderByDescending(p => p.On_Top_Update_Time);
                        break;
                    }
                case HomePageMode.Hot:
                    {
                        source = source.OrderByDescending(p => p.Seed);
                        break;
                    }
                case HomePageMode.Latest:
                    {
                        source = source.OrderByDescending(p => p.Collect_Time);
                        break;
                    }
            }

            return PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(source, pageNumber, p => Converter.ConvertCollectionWithOriginal(utils, p, 2), 20000);

        }

        public static PagingList<CollectionToDisplay> FindAllOriginalViewCollections(DataUtils utils, HomePageMode mode, string keyword, bool commodityOnly, int pageNumber, int collectCount)
        {
            IQueryable<View_Collection_With_Original> source = utils.FindViewCollectionWithOriginal(keyword, commodityOnly).Where(c => c.Is_Original == 1);
            //            IQueryable<View_Picture> source = utils.FindAllViewPictures(keyword, commodityOnly);

            switch (mode)
            {
                case HomePageMode.Default:
                    {
                        source = source.OrderByDescending(c => c.Collect_Count).OrderByDescending(p => p.On_Top_Update_Time);
                        break;
                    }
                case HomePageMode.Hot:
                    {
                        source = source.OrderByDescending(p => p.Seed);
                        break;
                    }
                case HomePageMode.Latest:
                    {
                        source = source.OrderByDescending(p => p.Collect_Time);
                        break;
                    }
            }

            return ConvertCollectionList(utils, source, pageNumber, collectCount, 2);
            //return PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(source, pageNumber, p => Converter.ConvertCollectionWithOriginal(utils, p, 2), 20000);
        }

        public static PagingList<CollectionToDisplay> ConvertCollectionList(DataUtils utils, IEnumerable<View_Collection_With_Original> source, int pageNumber, int collectCount, int replyLimit)
        {
            IList<View_Collection_With_Original> viewCollections = source.Skip(20 * pageNumber).Take(20).ToList();
            PagingList<CollectionToDisplay> result = new PagingList<CollectionToDisplay>();
            result.List = viewCollections.Select(c => Converter.ConvertCollectionWithOriginal(utils, c, 0)).ToList();
            if (collectCount > 0)
            {
                result.Count = collectCount;
            }
            else
            {
                result.Count = source.Count();
            }

            FindReplyListOfCollectionList(utils, result.List, replyLimit);

            return result;
        }

        /// <summary>
        /// 将一组CollectionToDisplay中的所有回复信息取出并分别赋值
        /// </summary>
        /// <param name="utils"></param>
        /// <param name="viewCollections"></param>
        /// <param name="replyLimit"></param>
        private static void FindReplyListOfCollectionList(DataUtils utils, IList<CollectionToDisplay> viewCollections, int replyLimit)
        {
            if (replyLimit == 0)
            {
                foreach (var c in viewCollections)
                {
                    c.ReplyCollection = new PagingList<ReplyToDisplay> { Count = 0, List = new List<ReplyToDisplay>() };
                }
            }
            else
            {
                long[] ids = viewCollections.Select(c => c.Id).ToArray();

                var tempReplyList = utils.FindReplies().Where(r => ids.Contains(r.Collection_Id.Value)).Select(r => new { Rid = r.Id, Time = r.Post_Time, Cid = r.Collection_Id }).ToList();
                IDictionary<long, IEnumerable<long>> group;
                if (replyLimit < 0)
                {
                    group = tempReplyList.GroupBy(r => r.Cid).ToDictionary(r => r.Key.Value, r => r.Select(g => g.Rid));
                }
                else
                {
                    group = tempReplyList.GroupBy(r => r.Cid).ToDictionary(r => r.Key.Value, r => r.OrderByDescending(g => g.Time).Take(replyLimit).Select(g => g.Rid));
                }
                IEnumerable<long> replyIds = new List<long>();
                foreach (var v in group.Values)
                {
                    replyIds = replyIds.Union(v);
                }
                long[] replyIdArray = replyIds.ToArray();
                IList<View_Reply> viewReplies = utils.FindViewReplies().Where(r => replyIdArray.Contains(r.Id)).ToList();

                foreach (var c in viewCollections)
                {
                    if (replyLimit > 0)
                    {
                        c.ReplyCollection = new PagingList<ReplyToDisplay>
                        {
                            Count = tempReplyList.Where(tr => tr.Cid == c.Id).Count(),
                            List = viewReplies.Where(vr => vr.Collection_Id == c.Id).OrderByDescending(vr => vr.Post_Time).Take(replyLimit).Select(vr => Converter.ConvertReply(vr)).ToList()
                        };
                    }
                    else
                    {
                        c.ReplyCollection = new PagingList<ReplyToDisplay>
                        {
                            Count = tempReplyList.Where(tr => tr.Cid == c.Id).Count(),
                            List = viewReplies.Where(vr => vr.Collection_Id == c.Id).OrderByDescending(vr => vr.Post_Time).Select(vr => Converter.ConvertReply(vr)).ToList()
                        };
                    }
                }
            }
        }

        //        public static PagingList<PictureToDisplay> FindAllViewPictures(DataUtils utils, HomePageMode mode, string keyword, bool commodityOnly, int pageNumber)
        //        {
        //            IQueryable<View_Collection2> source = utils.FindAllOriginalPictures(keyword, commodityOnly);
        ////            IQueryable<View_Picture> source = utils.FindAllViewPictures(keyword, commodityOnly);

        //            switch (mode)
        //            {
        //                case HomePageMode.Default:
        //                    {
        //                        source = source.OrderByDescending(c => c.Collect_Count).OrderByDescending(p => p.On_Top_Update_Time);
        //                        break;
        //                    }
        //                case HomePageMode.Hot:
        //                    {
        //                        source = source.OrderByDescending(p => p.Seed);
        //                        break;
        //                    }
        //                case HomePageMode.Latest:
        //                    {
        //                        source = source.OrderByDescending(p => p.Collect_Time);
        //                        break;
        //                    }
        //            }

        //            return PagingHelper.Page<View_Collection2, PictureToDisplay>(source, pageNumber, p => Converter.ConvertCollection(utils, p, 2), 20000);

        //        }

        public static IList<AccountAssociation> FindAccountAssociations()
        {
            string username = Helper.FindCurrentUsername();
            AccountAssociation[] list = new AccountAssociation[AccountAssociationHelper.PartyNumber];
            using (DataUtils utils = new DataUtils())
            {
                IList<DB_Account_Association> cols = utils.FindAccountAssociation().Where(aa => aa.Username == username).ToList();

                foreach (var aa in cols)
                {
                    list[((int)aa.Party) - 1] = AccountAssociationHelper.ConvertAssociationPartyName((AssociationParty)aa.Party);
                    list[((int)aa.Party) - 1].IsBound = true;
                    list[((int)aa.Party) - 1].Id = aa.Id;
                }

                for (int i = 0; i < AccountAssociationHelper.PartyNumber; i++)
                {
                    if (list[i] == null)
                    {
                        list[i] = AccountAssociationHelper.ConvertAssociationPartyName((AssociationParty)(i + 1));
                        list[i].IsBound = false;
                    }
                }
            }
            return list.ToList();
        }


        public static DB_Collection2 AddPictureEntry(PictureToUpload picture, string username, FileName name, DataUtils utils, Shop shop, float price, string taobaokeLink)
        {
            double ratio = 0;
            using (Bitmap image = ImageHelper.GetImage(name.Base, name.Extention))
            {
                ratio = image.Width * 1.0 / image.Height;
            }

            DB_Picture2 pic = new DB_Picture2
            {
                Image_Filename = name.Base,
                Image_File_Ext = name.Extention ?? "",
                Source_Page_Title = picture.Title,
                Source_Page_Url = picture.SourcePageUrl,
                Source_Image_Name = picture.SourceImageUrl,
                Is_Commodity = (byte)(shop == Shop.NotShop ? 0 : 1),
                Shop = (byte)shop,
                Price = price,
                Ratio = ratio,
                Taobao_Link = taobaokeLink
            };

            DB_Collection2 col = new DB_Collection2
            {
                Description = picture.Description,
                Username = username,
                Tags = picture.Tags,
                Is_Original = 1,
                Collect_Time = DateTime.Now
            };

            // if album id is 0, it is uncategorized picture
            if (picture.AlbumId != 0)
            {
                col.Album_Id = picture.AlbumId;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                Log.Debug("starting transaction");

                Log.Debug("inserting picture");
                pic = utils.InsertPicture(pic);
                long albumId = picture.AlbumId;
                if (!utils.HasCollected(albumId, pic.Id, username))
                {
                    Log.Debug("adding picture in collection");
                    col.Picture_Id = pic.Id;
                    col = utils.InsertCollection2(col);
                    pic.Original_Collection_Id = col.Id;
                    utils.Commit();
                }

                Log.Debug("creating thumbnails");

                // 创建缩略图。
                // 用在transaction中避免缩略图无法生成而造成其他用户看到空图片
                Log.Debug("Generating 200x200 thumbnail for " + picture.ImageName);
                using (ImageProcessor processor = new ImageProcessor(ImageHelper.GetImage(name.Base, name.Extention)))
                {
                    processor.SquareThumbnail(200);
                    processor.Save(ImageHelper.GetThumbnailPath(name.Base, 200, 200, name.Extention));
                }

                Log.Debug("Generating 80x80 thumbnail for " + picture.ImageName);
                using (ImageProcessor processor = new ImageProcessor(ImageHelper.GetImage(name.Base, name.Extention)))
                {
                    processor.SquareThumbnail(80);
                    processor.Save(ImageHelper.GetThumbnailPath(name.Base, 80, 80, name.Extention));
                }

                Log.Debug("Generating fit width 200 thumbnail for " + picture.ImageName);
                using (ImageProcessor processor = new ImageProcessor(ImageHelper.GetImage(name.Base, name.Extention)))
                {
                    processor.FitWidthThumbnail(200);
                    processor.Save(ImageHelper.GetThumbnailPath(name.Base, 200, 0, name.Extention));
                }

                scope.Complete();

                Log.Debug("finishing transaction");
            }

            // did not use transaction here.
            // the evaluation is that it does not matter if the picture is saved but the tags are not updated.
            AddTags(username, picture.Tags, TagType.Collection);
            return col;
        }

        public static void AddTags(string username, string tags, TagType type)
        {
            // TODO: verify the strings here
            if (string.IsNullOrEmpty(tags)) return;
            string[] tagList = tags.Split(' ');

            using (DataUtils utils = new DataUtils())
            {
                IQueryable<DB_Tag> commonTags = Helper.FindCommonTags(utils);
                IQueryable<DB_Tag> selfDefinedTags = Helper.FindTagsByUsername(type, username, utils);
                int existingCount = selfDefinedTags.Count();

                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (string tagValue in tagList)
                    {
                        string tag = tagValue.Trim();
                        if (tag.Length > DB_Tag.SizeLimit) { continue; } // invalid tag
                        if (string.IsNullOrEmpty(tag)) { continue; }
                        if (commonTags.Any(t => t.Name == tag) || selfDefinedTags.Any(t => t.Name == tag)) continue; // skip if already existing
                        if (existingCount >= Constants.UserTagMaxAllowed)
                        {
                            DB_Tag toReplace = Helper.FindTagToReplace(selfDefinedTags);
                            toReplace.Name = tag;
                            toReplace.Updated_Time = DateTime.Now;
                            utils.Commit();
                            existingCount++;
                        }
                        else
                        {
                            DB_Tag toCreate = new DB_Tag { Name = tag, Username = username, Type = (byte)type };
                            utils.InsertTag(toCreate);
                        }
                    }
                    scope.Complete();
                }
            }
        }


        public static IList<EntityName> FindMyAlbumNames(DataUtils utils)
        {
            List<EntityName> result = utils.FindAlbumByOwnerUsername(string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"], false).Select(a => new EntityName { Id = a.Id, Name = a.Name }).ToList();
            result.Insert(0, new EntityName { Id = 0, Name = Resource.DefaultAlbumName });
            return result;
        }

        public static void FollowUser(string toFollow, string follower)
        {
            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(toFollow);
                if (user == null)
                {
                    Log.Error("could not find user");
                    throw new WebFaultException<int>((int)ErrorCode.ItemNotFound, HttpStatusCode.InternalServerError);
                }

                if (!utils.IsWatching(follower, toFollow))
                {
                    if (follower != toFollow)
                    {
                        using (TransactionScope ts = new TransactionScope())
                        {
                            Log.Debug("inserting fan entity");
                            utils.InsertFan(new DB_Fan { WatchedUsername = toFollow, FansUsername = follower, Action_Time = DateTime.Now });
                            Log.Debug("inserting notice");

                            DB_Notice notice = utils.FindNotice(toFollow, NoticeType.Fan).Where(f => f.Action_Username == follower).FirstOrDefault();
                            if (notice == null)
                            {
                                utils.InsertNotice(new DB_Notice
                                {
                                    Username = toFollow,
                                    Action_Username = follower,
                                    Notice_Type = (byte)NoticeType.Fan,
                                    Notice_Time = DateTime.Now
                                });
                            }
                            ts.Complete();
                        }
                    }
                }
            }
        }
    }


}