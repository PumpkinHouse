﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PumpkinHouseDatabase.Contract;
using System.Data.Linq;
using log4net;

namespace PumpkinHouse.Utils
{
    public class PagingHelper
    {
        private static ILog Log = LogManager.GetLogger(typeof(PagingHelper));
        public static PagingList<P> Page<T, P>(IEnumerable<T> query, int pageNumber, Func<T, P> selector, int presetCount = 0, long count = Constants.DefaultPageSize)
            where T : class
            where P : class
        {
            Log.Debug("getting the count");
            int totalCount = presetCount == 0? query.Count() : presetCount;
            Log.Debug("finished getting the count");
            pageNumber = CalcPageNumber(pageNumber, totalCount, count);

            IList<P> result;
            if (pageNumber < 0 || count == 0)
            {
                result = new List<P>();
            }
            else
            {
                int skip = (int)count * pageNumber;
                Log.Debug("taking the count");
                var r = query.Skip(skip).Take((int)count);
                Log.Debug("finished taking the count");
                result = r.Select(selector).ToList();
            }

            return new PagingList<P>
            {
                List = result,
                Count = totalCount
            };
        }

        private static int CalcPageNumber(int pageNumber, int totalCount, long pageSize)
        {
            int pageCount = (totalCount - 1) / (int)pageSize + 1;

            if (pageNumber > pageCount - 1) pageNumber = -1;
            return pageNumber;
        }

        public static PagingList<P> Select<T,P>(PagingList<T> list, Func<T, P> selector)
            where T : class
            where P : class
        {
            return new PagingList<P>
            {
                List = list.List.Select(selector).ToList(),
                Count = list.Count
            };
        }

        public static PagingList<T> Page<T>(IEnumerable<T> query, int pageNumber, int presetCount = 0, long count = Constants.DefaultPageSize ) where T : class
        {
            Log.Debug("getting the count");
            int totalCount = presetCount == 0 ? query.Count() : presetCount;

            Log.Debug("finished getting the count");
            pageNumber = CalcPageNumber(pageNumber, totalCount, count);

            List<T> list;
            if (pageNumber < 0 || count == 0)
            {
                list = new List<T>();
            }
            else
            {
                int skip = (int)count * pageNumber;

                Log.Debug("taking the count");
                list = query.Skip(skip).Take((int)count).ToList();
            }

            var result = new PagingList<T>
            {
                List = list,
                Count = totalCount
            };
            Log.Debug("finished taking the count");
            return result;
        }
    }

}