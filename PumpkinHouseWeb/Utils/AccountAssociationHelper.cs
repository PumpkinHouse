﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Association;

namespace PumpkinHouse.Utils
{
    public class AccountAssociationHelper
    {
        public static int PartyNumber = 4;

        public static AccountAssociation ConvertAssociationPartyName(AssociationParty party)
        {
            string[] names = new string[] { "新浪微博", "腾讯QQ", "腾讯微博", "淘宝" };
            string[] urls = new string[] { "/Account/SinaAuth.aspx?request=2", "/Account/QQAuth.aspx?request=2", "/Account/QQWeiboAuth.aspx?request=2", "/Account/TaobaoAuth.aspx?request=2" };
            return new AccountAssociation { UrlToBind = urls[(int)party - 1], PartyName = names[(int)party - 1], PartyId = (int)party };
        }

        public static void CreateAssociation(DataUtils utils, AssociationInfo info, string username)
        {
            // AssociationInfo info = HttpContext.Current.Session["AssociationInfo"] as AssociationInfo;

            DB_Account_Association aa = new DB_Account_Association
            {
                External_Username = info.OpendId,
                Party = (byte)info.Party,
                Username = username,
                Access_Token = info.PersistToken,
                External_Link_Name = info.LinkName,
                Token_Expire_Time = DateTime.Now.AddDays(89)
            };

            utils.InsertAccountAssociation(aa);
        }
    }
}