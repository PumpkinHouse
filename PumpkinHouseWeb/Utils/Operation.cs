﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PumpkinHouseDatabase;
using System.Transactions;
using log4net;

namespace PumpkinHouse.Utils
{
    public class Operation
    {
        private static ILog Log = LogManager.GetLogger(typeof(Operation));

        //public static void DeletePicture(DataUtils utils, DB_Picture pic)
        //{
        //    using (TransactionScope ts = new TransactionScope())
        //    {
        //        Log.Debug("deleting replies");
        //        IList<DB_Reply> replies = utils.FindRepliesByPictureId(pic.Id).ToList();
        //        foreach (var reply in replies)
        //        {
        //            Operation.DeleteReply(utils, reply);
        //        }

        //        Log.Debug("Deleting notices with collections");
        //        IList<DB_Collection> collections = pic.DB_Collection;
        //        foreach (var col in collections)
        //        {
        //            // @TODO
        //            //IList<DB_Notice> list = col.DB_Notice.ToList();
        //            //foreach (var not in list)
        //            //{
        //            //    utils.DeleteNotice(not);
        //            //}
        //        }

        //        Log.Debug("deleting picture");
        //        utils.DeletePicture(pic);

        //        ts.Complete();
        //    }
        //}

        public static List<long> DeleteReply(DataUtils utils, DB_Reply reply)
        {
            List<long> result = new List<long>();
            List<DB_Reply> replies = utils.FindRepliesByReplyId(reply.Id).ToList();
            foreach (var r in replies)
            {
                result.AddRange(DeleteReply(utils, r));
            }
            result.Add(reply.Id);
            Log.Debug("recursion: deleting reply id : " + reply.Id);

            IList<DB_Notice> notices = utils.FindNoticeByAssociationId(reply.Id, NoticeType.Reply).ToList();
            foreach (var n in notices)
            {
                utils.DeleteNotice(n);
            }

            utils.DeleteReply(reply);
            return result;
        }

        public static void DeleteCollection(DataUtils utils, DB_Collection2 col)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                Log.Debug("deleting replies");
                IList<DB_Reply> replies = utils.FindRepliesByCollectionId(col.Id).ToList();
                foreach (var reply in replies)
                {
                    Operation.DeleteReply(utils, reply);
                }

                IList<DB_Notice> list = utils.FindNoticeByAssociationId(col.Id, NoticeType.Collect).ToList();
                foreach (var not in list)
                {
                    utils.DeleteNotice(not);
                }

                DB_Picture2 pic = utils.FindPicture2ById(col.Picture_Id);
                if (pic != null)
                {
                    pic.Collect_Count--;
                    utils.Commit();
                }

                if (col.Is_Original == 1)
                {
                    pic.Original_Collection_Id = null;
                }

                Log.Debug("deleting collection");
                utils.DeleteCollection(col);

                ts.Complete();
            }
        }

        public static void DeleteLike(DataUtils utils, DB_Like like)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                IList<DB_Notice> list = utils.FindNoticeByAssociationId(like.Id, NoticeType.Like).ToList();
                foreach (var n in list)
                {
                    utils.DeleteNotice(n);
                }

                utils.DeleteLike(like);

                ts.Complete();
            }
        }
    }
}