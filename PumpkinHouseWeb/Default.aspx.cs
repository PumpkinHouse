﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Utils;
using log4net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;

namespace PumpkinHouse
{
    public partial class _Default : System.Web.UI.Page
    {
        public HomePageMode OrderMode;
        public string Tag;
        public IList<CollectionToDisplay> pictures;
        private ILog Log = LogManager.GetLogger(typeof(_Default));
        public bool CommodityOnly = false;
        public string JsonObject;
        public IList<View_Recommended_Tag>[] rTags;
        public int TagCategoryId;

        protected string FindOrderString()
        {
            switch (OrderMode)
            {
                case HomePageMode.Default: return "default";
                case HomePageMode.Hot: return "hot";
                case HomePageMode.Latest: return "latest";
            }
            return null;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string mode = Request.Params["mode"];
            string tag = Request.Params["tag"];

            if (mode == null && tag == null && Request.Params["commodityOnly"] == null)
            {
                // reset
                Session["mode"] = "default";
                Session["tag"] = "";
                Session["commodityOnly"] = false;
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(mode))
                {
                    Session["mode"] = mode;
                }
                if (!string.IsNullOrWhiteSpace(tag))
                {
                    Session["tag"] = tag;
                }
                if (Request.Params["commodityOnly"] != null)
                {
                    CommodityOnly = "commodity".Equals(Request.Params["commodityOnly"] as string, StringComparison.OrdinalIgnoreCase);
                    Session["commodityOnly"] = CommodityOnly;
                }
            }

            this.Tag = (string)Session["tag"] ?? "";

            if (Session["mode"] == null)
            {
                this.OrderMode = HomePageMode.Default;
            }
            else
            {
                string m = (string)Session["mode"];

                if (m == "default")
                {
                    this.OrderMode = HomePageMode.Default;
                }
                else if (m == "hot")
                {
                    this.OrderMode = HomePageMode.Hot;
                }
                else if (m == "latest")
                {
                    this.OrderMode = HomePageMode.Latest;
                }
            }

            if (Session["commodityOnly"] == null)
            {
                this.CommodityOnly = false;
            }
            else
            {
                this.CommodityOnly = (bool)Session["commodityOnly"];
            }

            // 标签推荐的页卡信息
            int cateId;
            bool parseResult = int.TryParse(Request.Params["cate"], out cateId);
            if (parseResult)
            {
                Session["cate"] = cateId;
            }
            TagCategoryId = Session["cate"] == null ? 0 : (int)Session["cate"];

            Log.Debug("prepare querying");

            rTags = new IList<View_Recommended_Tag>[4];

            using (DataUtils utils = new DataUtils(false))
            {
                Log.Debug("ready to query");
                PagingList<CollectionToDisplay> list = Helper.FindAllOriginalViewCollections(utils, this.OrderMode, this.Tag, this.CommodityOnly, 0, 20000);
                Log.Debug("picture list populated");
                pictures = list.List;

                JsonObject = JsonHelper<IList<CollectionToDisplay>>.ConvertToJson(pictures);

                IEnumerable<View_Recommended_Tag> tags = utils.FindRecommendedViewTags().ToList();

                rTags[0] = tags.Where(t => t.Category_Name == "").ToList();
                rTags[1] = tags.Where(t => t.Category_Name == "空间").ToList();
                rTags[2] = tags.Where(t => t.Category_Name == "潮品").ToList();
                rTags[3] = tags.Where(t => t.Category_Name == "风格").ToList();
            }
        }


    }
}
