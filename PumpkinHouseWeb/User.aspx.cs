﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Utils;

namespace PumpkinHouse
{
    public partial class UserPage : System.Web.UI.Page
    {
        protected string Username;
        protected UserToDisplay User;

        protected void Page_Load(object sender, EventArgs e)
        {
            Username = Request.Params["username"];

            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(Username);
                User = Converter.ConvertUser(utils, user, 10);
                if (user == null)
                {
                    Response.Redirect("~/404.aspx");
                }
                else
                {
                    Page.Title = user.Nick_Name + "的个人主页 - 木头盒子";
                }
            }

            if (string.IsNullOrEmpty(Username))
            {
                Response.Redirect("~/404.aspx");
            }

            return;
        }

    }
}