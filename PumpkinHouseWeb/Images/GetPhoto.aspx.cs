﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Drawing;
using System.IO;
using PumpkinHouse.Utils;
using System.Drawing.Imaging;

namespace PumpkinHouse.Images
{
    public partial class GetPhoto : System.Web.UI.Page
    {
        private ILog Log = LogManager.GetLogger(typeof(GetPhoto));

        protected void Page_Load(object sender, EventArgs e)
        {
            string imageName = Request.Params["name"];

            // Change the response headers to output a JPEG image.
            this.Response.Clear();
            this.Response.ContentType = "image/jpeg";

            // Write the image to the response stream in JPEG format.
            try
            {
                using (Bitmap bitmap = ImageHelper.GetPhoto(imageName))
                {
                    bitmap.Save(this.Response.OutputStream, bitmap.RawFormat);
                }
            }
            catch (FileNotFoundException)
            {
                using (Bitmap bitmap = new Bitmap( Request.MapPath("/img/notfound.jpg")))
                {
                    bitmap.Save(this.Response.OutputStream, ImageFormat.Jpeg);
                }
            }
        }
    }
}