﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using log4net;
using PumpkinHouse.Utils;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.SessionState;
using System.Net;

namespace PumpkinHouse
{
    /// <summary>
    /// File Upload httphandler to receive files and save them to the server.
    /// </summary>
    public class FileUpload : IHttpHandler, IRequiresSessionState
    {
        ILog Log = LogManager.GetLogger(typeof(FileUpload));

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count == 0)
            {
                LogRequest("No files sent.");
                context.Response.ContentType = "text/plain";
                context.Response.Write("No files received.");

            }
            else
            {
                HttpPostedFile uploadedfile = context.Request.Files[0];

                string FileName = uploadedfile.FileName;
                string FileType = uploadedfile.ContentType;
                int FileSize = uploadedfile.ContentLength;

                LogRequest(FileName + ", " + FileType + ", " + FileSize);

                if (!ImageHelper.IsFileTypeSupported(FileType))
                {
                    Log.Warn("FileType is not supported");
                    HandleError(context, ErrorCode.InvalidImage);
                    return;
                }

                if (FileSize > 1024 * 1024 * 5)
                {
                    HandleError(context, ErrorCode.ImageTooLarge);
                    return;
                }

                string strFilename = Path.GetFileName(uploadedfile.FileName);
                ImageInfo info = null;

                string type = context.Request.Params["type"];
                if (type == "image")
                {
                    // Write data into a file
                    info = ImageHelper.SaveSharedImage(uploadedfile, strFilename);
                }
                else if (type == "photo")
                {
                    info = ImageHelper.SaveProfilePhoto(string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"], uploadedfile, strFilename);
                }

                DataContractJsonSerializer serializer = new DataContractJsonSerializer(info.GetType());
                using (MemoryStream ms = new MemoryStream())
                {
                    serializer.WriteObject(ms, info);
                    string json = Encoding.Default.GetString(ms.ToArray());
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(json);
                }
            }

        }

        private void HandleError(HttpContext context, ErrorCode code )
        {
            context.Response.ContentType = "text/plain";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            context.Response.Write((int)code);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private void LogRequest(string Log)
        {
            this.Log.Info(Log);
        }
    }
}