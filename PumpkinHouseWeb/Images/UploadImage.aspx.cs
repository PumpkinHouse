﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using PumpkinHouse.Utils;
using System.Runtime.Serialization.Json;
using System.Text;

namespace PumpkinHouse.upload
{
    public partial class UploadImage : System.Web.UI.Page
    {
        protected HtmlInputFile fileUploadControl = new HtmlInputFile();

        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.fileUploadControl.ID = "fileUploadControl";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (fileUploadControl.PostedFile != null)
            {
                HttpPostedFile myFile = fileUploadControl.PostedFile;

                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;

                // make sure the size of the file is > 0
                if (nFileLen > 0)
                {
                    // Allocate a buffer for reading of the file
                    byte[] myData = new byte[nFileLen];

                    // Read uploaded file from the Stream
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    ImageInfo info = null;

                    // Create a name for the file to store
                    string strFilename = Path.GetFileName(myFile.FileName);

                    // Write data into a file
                    info = ImageHelper.SaveSharedImage(ref myData, strFilename);

                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(info.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, info);
                    string json = Encoding.Default.GetString(ms.ToArray());
                    Response.Write(json);
                }
            }
        }


    }
}