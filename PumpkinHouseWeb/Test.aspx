﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="PumpkinHouse.Test" MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style>
    
        #imageArea
        {
            width: 800px;
            height: 600px;

        }
        #background 
        {
            width: 100%; 
            height: 100%; 
            position: absolute; 
            left: 0px; 
            top: 0px; 
            z-index: 0;
        }
        
        .drawArea 
        {
            position: absolute;
            top: 5em;
            left: 3em;
            width: 200px;
            height: 200px;
            cursor:  move;
            background-color: White;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)"; /*IE8*/
             filter:alpha(opacity=30);  /*IE5、IE5.5、IE6、IE7*/
             opacity: .3;  /*Opera9.0+、Firefox1.5+、Safari、Chrome*/

        }
        
        .stretch {
            width:100%;
            height:100%;
        }
    </style>

    <script type="text/javascript">
        var test = '<%= Test1 %>';
        var globalCollectHolder;
        function showError() {
            alert('error');
        };

        function show(pictureId, albumId) {
            alert(pictureId + ' ' + albumId);
        }

        function test() {
            var t = $('.drawArea');
            var m = 'top: ' + t.position().top + ', left: ' + t.position().left + '\n';
            m += 'size: ' + t.width();
            $('#result').text(m);
        }

        function loadUploadingDialog1() {
            if (!globalUploadDialog) {
                globalTags = getTagsAjaxSync(0, handleFault);

                globalUploadTemplateHolder = $('#uploadTemplateHolder');
                globalUploadTemplateHolder.empty();
                $.data(document, 'nameList', null);
                var dialogTemplate = $('#uploadTemplate').tmpl(null);
                globalUploadTemplateHolder.append(dialogTemplate);

                renderUploadingDialogBase();
                renderTemplate('#uploadFileControlHolder', '#albumNamefileTemplate', null);

                // bind events on tabs
                $('#collectToolTab').on('click', function () {
                    renderTabHeader(0);
                    var dialogBase = $.data(document, 'dialogBase');
                    if (dialogBase) {
                        var temp = $('<div style="display:none"></div>');
                        temp.append(dialogBase.children());
                        $.data(document, 'dialogBase', temp);
                    }
                    renderTemplate('#holder', '#collectToolTemplate', { host: globalHost });
                    $('#uploadBtn').removeClass('ready');
                });

                initFileUploadControl();

                $('#copyLinkTab').on('click', function () {
                    globalChosenImageUrl = null;
                    $('.resultMessage', globalUploadDialog).text('');
                    renderTabHeader(1);
                    renderUploadingDialogBase();
                    renderTemplate('#uploadFileControlHolder', '#albumNameDialogWebUrlTemplate', null);
                    $('#uploadBtn').removeClass('ready');
                    $('.DialogWebSure').on('click', function (event) {
                        var target = $(event.target);
                        var targetUrl = $.trim($('.WebUrl').val());
                        if (!targetUrl) {
                            blinkInput('.WebUrl');
                            return;
                        }
                        globalLink = targetUrl;

                        renderTemplate('#uploadFileControlHolder', '<div class=uploadFont>读取中...</div>', null);

                        ajaxDownloadHtmlPage(true, targetUrl, function (page) {
                            processLink(page, targetUrl);
                        }, function (msg) {
                            if (msg == 3) {
                                $('#copyLinkTab').trigger('click');
                                $('.DialogWebUrl span').text('网页不存在').addClass('error');
                                return;
                            }
                            showError(msg);
                        });
                    });
                });

                $('#uploadTab').on('click', function () {
                    globalImageName = null;
                    $('.resultMessage', globalUploadDialog).text('');
                    renderTabHeader(2);
                    renderUploadingDialogBase();
                    renderTemplate('#uploadFileControlHolder', '#albumNamefileTemplate', null);
                    initFileUploadControl();
                });

                renderTabHeader(2);

                globalUploadDialog = createDialog(globalUploadTemplateHolder, 960, 500, true, true);

            }
            else {
                openDialog(globalUploadDialog);

                renderTabHeader(2);
                renderTemplate('#uploadFileControlHolder', '#albumNamefileTemplate', null);
                initFileUploadControl();
            }
        }

        function doAction () {
//            ajaxGuessLike(true, 8, 0, function (r) {
//                alert(r.url);
//            }, handleFault);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <div id="tagPanel"></div>
        <input type="button" id="collectHolder" value="aaaaa" data-picture-id="1"/>
        <div id="imageArea">
            <div id="background">
                <img src="http://localhost:18170/Images/GetPhoto.aspx?name=test1_temp.jpg" class="stretch" alt="" />
            </div>
            <div class="drawArea" ></div>
        </div>
        <input type="button" onclick="test()" />
        <div id="result"></div>
    </div>
    <div style="height: 100px; width: 100px; background-color: green" id="outer"> <span id="a"></span>
        <div id="inner" style="height: 50px; width: 50px; border: 1px solid red; background-color: Yellow"><span id="b"></span></div></div>
    <div><textarea id="testInput"></textarea><input type="button" id="testBtn" /></div>
</asp:Content>

