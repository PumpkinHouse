﻿/// <reference path="lib/jquery-1.7.js" />
/// <reference path="lib/jquery.validate.js" />
/// <reference path="util.js" />
/// <reference path="data.js" />
/// <reference path="ajaxCall.js" />

(function ($) {
    $.fn.fanButton = function (loggedIn, numberHolder) {
        this.each(function (index, item) {
            var i = $(item);
            var status = parseInt(i.attr('data-status'));
            renderUserButton(status, i);

            i.on('click', function (event) {
                if (!loggedIn) {
                    showLogonDialog();
                }
                else {
                    var status = parseInt(i.attr('data-status'));
                    var targetUser = i.attr('data-target-user');
                    if (status & 2) { // I am watching him
                        ajaxUnfanUser(true, targetUser, function () {
                            if (typeof renderUser != 'undefined') {
                                globalUser.hasMyAttention = false;
                                globalUser.numberOfFans--;
                                globalUser.relation = globalUser.relation - 2;
                                renderUser();
                            }
                            else {
                                renderUserButton(status - 2, i);
                                if (numberHolder) {
                                    numberHolder.text(numberHolder.text().charCodeAt() - 1);
                                }
                            }
                        }, handleFault);
                    }
                    else {
                        ajaxFanUser(true, targetUser, function () {
                            if (typeof renderUser != 'undefined') {
                                globalUser.hasMyAttention = true;
                                globalUser.numberOfFans++;
                                globalUser.relation = globalUser.relation + 2;
                                renderUser();
                            }
                            else {
                                renderUserButton(status + 2, i);
                                if (numberHolder) {
                                    numberHolder.text(numberHolder.text().charCodeAt() + 1);
                                }
                            }
                        }, handleFault);
                    }
                }
            });
        });
    }

    function renderUserButton(status, btn) {
        btn.removeClass('HxGz');
        btn.removeClass('yjGz');
        btn.removeClass('GzW');
        btn.removeClass('MsR');
        var watching = status & 2;
        var watchingMe = status & 1;
        if (watching) {
            if (watchingMe) {
                btn.addClass('HxGz'); // 互相关注
                btn.val(' ');
            }
            else {
                btn.addClass('yjGz'); // 已关注     
                btn.val(' ');
            }
        }
        else {
            if (watchingMe) {
                btn.addClass('GzW'); // 关注我
                btn.val(' ');
            }
            else {
                btn.addClass('MsR'); // 陌生人
                btn.val(' ');
            }
        }
        btn.attr('data-status', status);
    }

} (jQuery));

