﻿var maxPagesInPagination = 3;
var refreshPage = 1;

function expandCategory(target) {
    target.show();
    target.prev('a').addClass('selectedKeywordOpen');
    target.removeClass('hide');
}


function renderPic(pictures) {
    globalAllPictures = pictures;
    // render filter panel
    var filterHolder = $('ul.PublicList');

    // only init the controls when the first page loaded
    if (refreshPage) {
        if (globalTag != '') {
            document.title = '(' + globalTag + ') - 木头盒子';
        }
        filterHolder.empty();

        if (globalTag) {
            globalAssociatedKeywords = getAssociatedKeywordsSync(globalTag, handleFault);

            if (globalAssociatedKeywords.length > 0) {
                var associatedKeywordsTemplate = $('#associatedKeywordsTemplate').tmpl({ keywords: globalAssociatedKeywords, current: globalTag });
                filterHolder.append(associatedKeywordsTemplate);

                // bind events on keywords
                $('.keyword').on('click', function (event) {
                    if ($(this).hasClass('selectedKeyword')) {
                        // remove the tag
                        location.href = findLinkUrl(globalOrderMode, globalCommodityOnly, '');
                    }
                    else {
                        var key = $(this).text();
                        location.href = findLinkUrl(globalOrderMode, globalCommodityOnly, key);
                    }
                });
            }
        }
    }
    $.each(pictures.list, function (index, item) {
        item.homepage = 1;
    });
    var picGalleryTemplate = $('#galleryItemTemplate').tmpl(pictures.list);
    filterHolder.append(picGalleryTemplate);

    //喜欢按钮
    picGalleryTemplate.find('.likeBtn').on('click', function (e) {
        updatePictureLike(e, defaultLikeCallback);
    });

    initCollectButton();

    if (refreshPage) {
        //瀑布流控制~~Kevin
        $('.PublicList').masonry({
            itemSelector: '.PublicList li',
            columnWidth: 240
        });
        
        bindReplyEvent($('.PublicList'));
    }
    else {
        $('.PublicList').masonry('appended', picGalleryTemplate, true);
        bindReplyEvent(picGalleryTemplate);
    }

    // bind event on commodityOnly checkbox
    $('#commodityOnly').attr('checked', globalCommodityOnly);
    $('#commodityOnly').change(function (eventObject) {
        globalCommodityOnly = $(eventObject.target).attr("checked") == "checked";
        location.href = findLinkUrl(globalOrderMode, globalCommodityOnly, globalTag);
    });

    // bind event on collect buttons
    $('.collectButton', picGalleryTemplate).each(function (index, item) {
        var li = $(item).parents('li.pic');
        var displayTarget = li.find('a span.collectCount');
        $.collectButtion(item, globalLoggedIn, globalMyAlbums, displayTarget);
    });

    globalLock = 0;
    refreshPage = 0;

}


function toggleFilterCategory(index) {
    var i = globalKeywordCategories.length - 2;
    for (; i >= 0; i--) {
        if (i != index) {
            $('#cate-' + i).hide();
            $('#cate-' + i).prev('a').removeClass('selectedKeywordOpen');
        }
    }
    expandCategory($('#cate-' + index));
}

var globalRTagIndex = 0;
var globalTagSize = (width == 1200 ? 7 : 5);

function moveLeft() {
    var target = $('.Pic-list ul').filter(function (index) {
        return $(this).css('display') == 'block';
    });
    if (globalRTagIndex < 0) {
        globalRTagIndex++;
        target.animate({ left: target.position().left + 156 }, 500);
    }
}

function moveRight() {
    var target = $('.Pic-list ul').filter(function (index) {
        return $(this).css('display') == 'block';
    });
    var count = target.find('li').length;
    if (globalRTagIndex > globalTagSize - count) {
        globalRTagIndex--;
        target.animate({ left: target.position().left - 156 },500);
    }
}

function doAction() {
    // 调整显示区域大小
    $(".TopCon").css({ width: width });
    $(".HotNew").css({ width: width });
    $(".PublicCon").css({ width: width });
    $(".HotNewLine").css({ width: width - 20 });

    //时间延迟函数 //sleep
    (function ($) {
        var _sleeptimer;
        $.sleep = function (time2sleep, callback) {
            $.sleep._sleeptimer = time2sleep;
            $.sleep._cback = callback;
            $.sleep.timer = setInterval('$.sleep.count()', 1000);
        }
        $.extend($.sleep, {
            current_i: 1,
            _sleeptimer: 0,
            _cback: null,
            timer: null,
            count: function () {
                if ($.sleep.current_i === $.sleep._sleeptimer) {
                    clearInterval($.sleep.timer);
                    $.sleep._cback.call(this);
                }
                $.sleep.current_i++;
            }
        });
    })(jQuery);

    //热门-空间-风格 页卡
    $(".HeadTag-a").click(function () {
        $(".HeadTag a").removeClass('selected');
        $(this).addClass('selected');
        $('.Pic-list-ul-a').css('left', 'auto');
        globalRTagIndex = 0;
        $(".Pic-list ul").css("display", "none");
        $(".Pic-list-ul-a").css("display", "block");
    });
    $(".HeadTag-b").click(function () {
        $(".HeadTag a").removeClass('selected');
        $(this).addClass('selected');
        $('.Pic-list-ul-b').css('left', 'auto');
        globalRTagIndex = 0;
        $(".Pic-list ul").css("display", "none");
        $(".Pic-list-ul-b").css("display", "block");
    });
    $(".HeadTag-c").click(function () {
        $(".HeadTag a").removeClass('selected');
        $(this).addClass('selected');
        $('.Pic-list-ul-c').css('left', 'auto');
        globalRTagIndex = 0;
        $(".Pic-list ul").css("display", "none");
        $(".Pic-list-ul-c").css("display", "block");
    });
    $(".HeadTag-d").click(function () {
        $(".HeadTag a").removeClass('selected');
        $(this).addClass('selected');
        $('.Pic-list-ul-d').css('left', 'auto');
        globalRTagIndex = 0;
        $(".Pic-list ul").css("display", "none");
        $(".Pic-list-ul-d").css("display", "block");
    });
    $('.HeadTag-' + String.fromCharCode('a'.charCodeAt() + globalTagCateId)).addClass('selected').trigger('click');

    globalTag = decodeURIComponent(globalTag);
    // ajaxGetAllPictures(true, globalTag, 0, globalCommodityOnly, globalOrderMode, false, renderPic, handleFault);
    globalAllPictures = new PaginationList(20000, json, function (item) { return new CollectionToDisplay(item) });

    renderPic(globalAllPictures);

    ajaxGetAllPictures(true, globalTag, 1, globalCommodityOnly, globalOrderMode, false, cacheLoading, handleFault);

    $(window).scroll(function () {
        if (!globalAllPictures) return;
        if (getScrollTop() + $(window).height() + 100 > $(document).height()) {
            if (globalLock == 0) {
                globalLock = 1;

                if (globalPageNumber < Math.floor((globalAllPictures.count - 1) / 20)) {
                    globalPageNumber++;
                    globalLoadingIndicatorAtBottom = true;
                    renderCache(renderPic);
                    ajaxGetAllPictures(true, globalTag, globalPageNumber + 1, globalCommodityOnly, globalOrderMode, false, cacheLoading, handleFault);
                }

                if (!globalLoggedIn && globalPageNumber > 0 && globalPageNumber % 3 == 0) {
                    showLogonDialog();
                }

                //                if (globalPageNumber < Math.floor((globalAllPictures.count - 1) / 20)) {
                //                    if ((globalPageNumber+1) % maxPagesInPagination != 0 ) {
                //                        globalPageNumber++;
                //                        ajaxGetAllPictures(true, globalTag, globalPageNumber, globalCommodityOnly, globalOrderMode, false, renderPic, handleFault);
                //                    } else {
                //                        pagination("#paginationHolder", globalAllPictures.count, 20 * maxPagesInPagination, pageselectCallback);
                //                    }
                //                }
            }
        }
    });

    function pageselectCallback(pageNumber) {
        refreshPage = 1;
        globalPageNumber = pageNumber * maxPagesInPagination;
        ajaxGetAllPictures(true, globalTag, pageNumber * maxPagesInPagination, globalCommodityOnly, globalOrderMode, false, renderPic, handleFault);
    }
}