﻿/// <reference path="lib/jquery-1.7.js" />
/// <reference path="gettmpl.js" />
/// <reference path="ajaxCall.js" />


(function ($) {
    $.msgButton = function (selector, from) {
        this.button = $(selector);
        this.to = this.button.attr('data-to');
        this.toDisplay = this.button.attr('data-to-display');
        this.from = from;
        var msgBtn = this;

        // bind action to "发私信" button
        this.button.on('click', function () {
            if (!msgBtn.from) {
                showLogonDialog();
            }
            else {
                var dialog = $.data(msgBtn.button[0], 'dialog');
                if (dialog) {
                    dialog.showDialog();
                }
                else {
                    var holder = $('<div></div>');
                    var template = $('#messageDialogTemplate').tmpl({ to: msgBtn.toDisplay });
                    holder.append(template);
                    dialog = holder.mDialog({ width: 500, height: 240 });

                    $.data(msgBtn.button[0], 'dialog', dialog);

                    // bind event to send the message
                    $('input#sendButton').on('click', function () {
                        var text = $.trim($('#msgBody').val());
                        if (!text) {
                            blinkInput('#msgBody');
                            return;
                        }
                        var msg = new MessageToSend(msgBtn.from, msgBtn.to, text);
                        ajaxSendMessage(true, msg,
                            function () {
                                var d = showSmallDialog('#actionCompleteTemplate', '.successDialog', '消息发送成功！');
                                setTimeout(function () { d.children().hide(); }, 1000);
                                $('#msgBody').val('');
                                dialog.closeDialog();
                            },
                            handleFault);
                    });
                }
            }
        });
    }

} (jQuery));