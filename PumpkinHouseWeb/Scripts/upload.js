﻿function initAlbumListControl(holderSelector, albumNames) {
    var holder = $(holderSelector);
    var nameListDialog = $.data(document, 'nameList');
    var dialogHolder = $('#uploadTemplateHolder');
    var inDialog = dialogHolder ? (dialogHolder.length ? true : false) : false;
    if (!inDialog) {
        dialogHolder = $('#upload');
    }

    if (!nameListDialog) {
        nameListDialog = renderTemplate('#nameListHolder', '#albumNameListTemplate', { albums: albumNames });
        nameListDialog.position({
            my: 'center top',
            at: 'left bottom',
            of: holder
        });
        if (!inDialog) {
            nameListDialog.css('margin-right', '-105px');
            nameListDialog.css('top', '32px');
        }

        nameListDialog.hide();
        nameListDialog.id = 0; // 未分类专辑
        $.data(document, 'nameList', nameListDialog);

        $('.albumName', nameListDialog).on('click', function (e) { albumNameClick(e, nameListDialog, holder) });
        $('#nameListHolder').on('click', function (e) {
            e.stopImmediatePropagation();
        });

        $('#newAlbum', nameListDialog).on('keyup', function (eventObject) {
            var target = $(eventObject.target);
            var text = target.val();
            if (text && text.length > 12) {
                target.val(text.substring(0, 12));
            }
        });

        $('#createAlbumBtn', nameListDialog).on('click', function (eventObject) {
            var newInput = $('#newAlbum', nameListDialog);
            var name = $.trim(newInput.val());
            if (name != '') {
                var album = new AlbumToCreate(name);
                var result = createAlbumSync(album, function (errorCode) {
                    handleSensitiveWord(errorCode, '抱歉，专辑名包含非法字符，请修改后再分享。');
                });
                if (result) {
                    renderTemplate('#albumNameList div#list ul', '#albumNameItemTemplate', result, true);
                    $('li[data-album-id=' + result.id + ']').on('click', function (e) { albumNameClick(e, nameListDialog, holder) });
                    newInput.val('');
                    holder.text(result.name);
                    nameListDialog.albumId = result.id;
                    nameListDialog.hide();
                }
            }
        });
    }

    holder.on('click', function (e) {
        nameListDialog.show();
        e.stopImmediatePropagation();
        dialogHolder.on('click', function () {
            if (nameListDialog) nameListDialog.hide();
            dialogHolder.off('click');
        });
    });

    return nameListDialog;
}

function albumNameClick(eventObject, nameListDialog, holder) {
    var target = $(eventObject.target);
    var id = target.attr('data-album-id');
    var name = target.text();
    nameListDialog.albumId = id;
    holder.text(name);
    nameListDialog.hide();
}
