﻿/// <reference path="lib/jquery-1.7.js" />

function cloneObject(target, toClone) {
    for (var obj in toClone) {
        target[obj] = toClone[obj];
    }
}

/// <summary>Constructor of UserForRegistration. </summary>
/// <param name="objectOrEmail"></param>
function UserForRegistration(objectOrEmail, nickname, password, avatar, follow, advertise) {
    if (objectOrEmail == undefined) {
        $.error("the first parameter of UserForRegistration() could not be null");
    }
    this.__type = 'UserForRegistration:#PumpkinHouseDatabase.Contract'
    if ($.type(objectOrEmail) == "object") { // to convert from WCF serialized objects
        var userObject = objectOrEmail;
        cloneObject(this, userObject);
    } else { // to convert to WCF data contracts
        this.email = objectOrEmail;
        this.password = password;
        this.nickname = nickname;
        this.avatar = avatar;
        this.follow = follow;
        this.advertise = advertise;
    }
}

function UserGeneralInfo(gender, locationId, intro, nickname) {
    this.__type = 'UserGeneralInfoToUpdate:#PumpkinHouseDatabase.Contract';

    this.gender = gender;
    this.locationId = locationId;
    this.intro = intro;
    this.nickname = nickname;
}

function PictureToUpload(name, url, albumId, tags, description, title, sourceUrl, originalName, sync, uploadTool) {
    this.__type = 'PictureToUpload:#PumpkinHouseDatabase.Contract';

    this.name = name;
    this.albumId = albumId;
    this.tags = tags;
    this.description = description;
    this.title = title;
    this.src = sourceUrl;
    this.url = url;
    this.sync = sync;
    this.originalName = originalName;
    this.uploadTool = uploadTool;
}

function AlbumToCreate(name, description, tags) {
    this.__type = 'AlbumToCreate:#PumpkinHouseDatabase.Contract';

    this.name = name;
    this.tags = tags;
    this.description = description;
};

function MessageToSend(from, to, text) {
    this.__type = 'MessageToSend:#PumpkinHouseDatabase.Contract';
    this.from = from;
    this.to = to;
    this.text = text;
}

function AlbumToDisplay(jsonObject) {
    cloneObject(this, jsonObject);
    this.tags = [];
    if (jsonObject.tags) {
        var tags = jsonObject.tags.split(' ');
        var i = tags.length - 1;
        for (; i >= 0; i--) {
            if (tags[i]) {
                this.tags.unshift(tags[i]);
            }
        }
    };
    this.pictures = [];

    var obj = this;
    this.pictures = new PaginationList(jsonObject.pictures.count, jsonObject.pictures.list, function (item) { return new CollectionToDisplay(item) });

    if (jsonObject.replyCol) {
        this.replyCol = new PaginationList(jsonObject.replyCol.count, jsonObject.replyCol.list, function (jso) { return new ReplyToDisplay(jso); });
    }

    var count = this.pictures.list.length;
    var largeCount;

    if (count <= 4) {
        largeCount = count;
    }
    else if (count < 8) {
        largeCount = 3;
    } 
    else {
        largeCount = 2;
    }
    this.largePictureList = [];
    this.smallPictureList = [];
    var index = 0;
    for (; index < count; index++) {
        if (index < largeCount) {
            this.largePictureList.push(this.pictures.list[index]);
        }
        else {
            this.smallPictureList.push(this.pictures.list[index]);
        }
    }
}

function CollectionToDisplay(jsonObject) {
    if (!jsonObject) throw 'picture not found';
    cloneObject(this, jsonObject);

    if (jsonObject.price) {
        this.price = jsonObject.price.toFixed(2);
    }

    var uploadTime ;
    if (jsonObject.uploadTime) {
        uploadTime = parseJsonDate(jsonObject.uploadTime);
        this.uploadTime = formatDate(uploadTime);
        this.uploadTimeFormatted = $.timeago(uploadTime);
    }

    var collectTime = parseJsonDate(jsonObject.colTime)
    this.collectTime = formatDate(collectTime);
    this.collectTimeFormatted = $.timeago(collectTime);

    if (jsonObject.original) {

    }

    this.tags = [];
    if (jsonObject.tags) {
        var tags = jsonObject.tags.split(' ');
        var i = tags.length - 1;
        for (; i >= 0; i--) {
            if (tags[i]) {
                this.tags.unshift(tags[i]);
            }
        }
    };
    this.replyCol = new PaginationList(jsonObject.replyCol.count, jsonObject.replyCol.list, function (jso) { return new ReplyToDisplay(jso); });

}

function PictureToDisplay(jsonObject) {
    if (!jsonObject) throw 'picture not found';
    cloneObject(this, jsonObject);
    
    if (jsonObject.price) {
        this.price = jsonObject.price.toFixed(2);
    }

    var time = parseJsonDate(jsonObject.uploadTime);
    this.uploadTime = formatDate(time);
    this.uploadTimeFormatted = $.timeago(time);

    if (jsonObject.colBy) {
        var colTime = parseJsonDate(jsonObject.colTime);
        this.colTime = formatDate(colTime);
        this.colTimeFormatted = $.timeago(colTime);
    }

    this.tags = [];
    if (jsonObject.tags) {
        var tags = jsonObject.tags.split(' ');
        var i = tags.length - 1;
        for (; i >= 0; i--) {
            if (tags[i]) {
                this.tags.unshift(tags[i]);
            }
        }
    };
    this.replyCol = new PaginationList(jsonObject.replyCol.count, jsonObject.replyCol.list, function (jso) { return new ReplyToDisplay(jso); });
}

function ReplyCollection(jsonObject) {
    if (jsonObject) {
        cloneObject(this, jsonObject);
        this.replies = [];
        var obj = this;
        $.each(jsonObject.replies, function (index, item) {
            obj.replies.push(new ReplyToDisplay(item));
        })
    }
    else {
        this.count = 0;
        this.replies = [];
    }
}

function ReplyToDisplay(jsonObject) {
    cloneObject(this, jsonObject);
    var time = parseJsonDate(jsonObject.time);
    this.content = encodeXml(jsonObject.content).replace(/\n/g, '<br/>');
	this.time = formatDate(time);
    this.timeago = $.timeago(time);
}

function ThreadToDisplay(thread) {
    cloneObject(this, thread);
    this.formattedTime = formatDate(parseJsonDate(thread.time));
}

function MessageToDisplay(msg) {
    cloneObject(this, msg);
    this.formattedTime = formatDate(parseJsonDate(msg.time));
}

function ReplyToPost(text, collectionId, albumId, discussionId, replyId) {
    this.__type = 'ReplyToPost:#PumpkinHouseDatabase.Contract';
    this.text = text;
    this.collectionId = collectionId;
    this.albumId = albumId;
    this.replyId = replyId;
    this.discussionId = discussionId;
}

function NoticeToDisplay(jsonObject) {
    cloneObject(this, jsonObject);
	this.time = formatDate(parseJsonDate(jsonObject.time));
}

function PaginationList(count, list, convertFunc) {
    this.count = count;
    if (convertFunc) {
        this.list = $.map(list, function (item, index) { return convertFunc(item); });
    } else {
        this.list = list;
    } 
}

function formatDate(date) {
    return $.format.date(date, 'yyyy-MM-dd HH:mm:ss')
}

function DiscussionToDisplay(jsonObject) {
    this.__type = 'DiscussionToDisplay:#PumpkinHouseDatabase.Contract';
    cloneObject(this, jsonObject);

	if (jsonObject.time) {
        this.time = formatDate(parseJsonDate(jsonObject.time));
    }
    if (jsonObject.replyList) {
        this.replyList = new PaginationList(jsonObject.replyList.count, jsonObject.replyList.list, function (item) { return new ReplyToDisplay(item) });
    }
}

function AnnouncementToDisplay(jsonObject) {
    this.__type = 'AnnouncementToDisplay:#PumpkinHouseDatabase.Contract';
    cloneObject(this, jsonObject);
    this.time = formatDate(parseJsonDate(jsonObject.time));
}
