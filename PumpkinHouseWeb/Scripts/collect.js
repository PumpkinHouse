﻿/// <reference path="lib/jquery-1.7.js" />
/// <reference path="gettmpl.js" />
/// <reference path="ajaxCall.js" />


(function ($) {
    /// create a collect button
    /// require pictureTemplate.html
    $.collectButtion = function (holderSelector, loggedIn, albums, displayTarget, successCallback) {
        return new Collect(holderSelector, loggedIn, albums, displayTarget, successCallback);
    };

    Collect = function (holderSelector, loggedIn, albums, displayTarget, successCallback) {
        this.holder = $(holderSelector);
        this.albums = albums;
        this.pictureId = this.holder.attr('data-picture-id');
        this.ajaxErrorHandler = handleFault;
        this.updateHandler = this.doCollect;
        this.successCallback = successCallback;
        this.displayTarget = displayTarget;

        var collect = this;
        if (loggedIn) {
            this.holder.on('click', function (e) {
                if (loggedIn) {
                    collect.showDialog(collect, $(e.target))
                }
            });
            if (!this.albums) {
                this.albums = getMyAlbumNamesSync(false, this.ajaxErrorHandler);
            }
        }
        else {
            this.holder.attr('title', '登陆才能进行操作哦，点击就可以登陆啦！ ');
            this.holder.on('click', function () {
                showLogonDialog();
                return;
            });
        }
    }

    $.extend(Collect, {
        prototype: {
            getDialog: function () {
                return $.data(document, 'collectDialog');
            },
            showDialog: function (collect, buttonTarget) {
                var dialog = this.getDialog();
                if (dialog) {
                    dialog.showDialog();
                    dialog.position({
                        my: 'center top',
                        at: 'center bottom',
                        of: $(collect.holder)
                    });
                    if (dialog.position().left < 0) {
                        dialog.css('left', 0);
                    }
                }
                else {
                    var dialogTemplate = $('#collectDialogTemplate').tmpl({ albums: collect.albums });
                    dialog = dialogTemplate.mDialog({ width: 452, height: 252, modal: false });

                    dialog.position({
                        my: 'center top',
                        at: 'center bottom',
                        of: $(collect.holder)
                    });
                    if (dialog.position().left < 0) {
                        dialog.css('left', 0);
                    }
                    $.data(document, 'collectDialog', dialog);
                }

                var albumId = buttonTarget.attr('data-album-id');
                dialog.find('a').removeClass('ColorAacd60');
                dialog.find('a[data-album-id=' + albumId + ']').addClass('ColorAacd60');

                $('#btnCreateAlbum', dialog).off('click').on('click', function () {
                    var name = $.trim($('#newAlbumName', dialog).val());
                    if (name == '') {
                        return;
                    }
                    var result = createAlbumSync(new AlbumToCreate(name, '', ''), function (errorCode) {
                        handleSensitiveWord(errorCode, '抱歉，专辑名包含非法字符，请修改后再创建。');
                    });
                    if (result) {
                        renderTemplate('.favDailCon ul', '#albumItemTemplate', result, true);
                        $('#newAlbumName', dialog).val('');
                        $("a.albumName", dialog).on('click', function (event) {
                            collect.updateHandler(collect, collect.pictureId, $(this).attr('data-album-id'));
                            collect.getDialog().closeDialog();
                            return false;
                        });
                    }
                });

                $("a.albumName", dialog).off('click').on('click', function (event) {
                    collect.updateHandler(collect, collect.pictureId, $(this).attr('data-album-id'));
                    collect.getDialog().closeDialog();
                    return false;
                });
            },

            doCollect: function (collect, pictureId, albumId) {
                var li = $('li.pic[data-picture-id=' + pictureId + ']');
                var isMine;
                if (li) {
                    isMine = li.attr('data-mine');
                } else {
                    isMine = "0"; // 图片页，出现按钮必定不是自己的图片
                }

                ajaxCollectPicture(true, pictureId, albumId, function (result) { collect.collectCallback(collect, pictureId, li, result); }
                   , collect.ajaxErrorHandler);
            },

            collectCallback: function (collect, pictureId, li, result) {
                var btn = $('input.collectButton[data-picture-id=' + pictureId + ']');
                if (btn.val() == '收纳') { // 新收纳，更新数字
                    //                    var displayTarget = li.find('span.collectCount');
                    //                    if (displayTarget && displayTarget.length > 0) {
                    //                        var txt = $.trim(li.attr('data-collect-count'));
                    //                        if (txt) {
                    //                            var v = parseInt(txt);
                    //                            displayTarget.text('(' + (v + 1) + ')');
                    //                        }
                    //                    }
                }
                if (collect.successCallback) {
                    collect.successCallback(result);
                }
                else {
                    btn.val('已收纳');
                    btn.removeClass('collect-off');
                    btn.addClass('collect-on');
                    btn.attr('data-album-id', result.id);
                }
            }
        }
    });
} (jQuery));