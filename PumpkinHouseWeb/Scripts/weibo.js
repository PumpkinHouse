﻿function AddShareButtons(holderSelector, text, nickname, picPath, picId, isAdmin) {
    $(holderSelector).empty();
    var pic = 'http://' + document.location.hostname + picPath;
    AddSinaWeiboButton(holderSelector, text + ' 来自' + nickname + '@木头盒子网', picPath, picId, isAdmin);
    //AddQQSpaceButton(holderSelector, text, '');
    AddQQWeiboButton(holderSelector, text + ' 来自' + nickname + '@@mutoubox', picPath, picId, isAdmin);
}

function AddSinaWeiboButton(holderSelector, text, picPath, picId, isAdmin) {
    //    var _w = 20, _h = 16;
    //    var param = {
    //        url: location.href,
    //        type: '3',
    //        count: '0', /**是否显示分享数，1显示(可选)*/
    //        appkey: '981182563', /**您申请的应用appkey,显示分享来源(可选)*/
    //        title: text, /**分享的文字内容(可选，默认为所在页面的title)*/
    //        pic: picPath, /**分享图片的路径(可选)*/
    //        ralateUid: '', /**关联用户的UID，分享微博会@该用户(可选)*/
    //        language: 'zh_cn', /**设置语言，zh_cn|zh_tw(可选)*/
    //        rnd: new Date().valueOf()
    //    }
    //    var temp = [];
    //    for (var p in param) {
    //        temp.push(p + '=' + encodeURIComponent(param[p] || ''))
    //    }
    // $(holderSelector).append('<iframe id="sinaFrame" allowTransparency="true" frameborder="0" scrolling="no" src="http://hits.sinajs.cn/A1/weiboshare.html?' + temp.join('&') + '" width="' + _w + '" height="' + _h + '"></iframe>');

    $(holderSelector).append('<a title="分享到新浪微博" onclick="shareToSina(\'' + encodeURIComponent(location.href) + '\',\'' + encodeURIComponent(text) + '\',\'' + encodeURIComponent(picPath) + '\', ' + picId + ',' + isAdmin + ');" href="javascript:;" hidefocus="true"><img src="http://simg.sinajs.cn/platformstyle/images/common/transparent.gif" class="icon" alt=""></a>')
}

function shareToSina(url, title, picPath, picId, isAdmin) {
    if (isAdmin) {
        var newPath = ajaxGenerateHeader(false, picId, null, handleFault);
        if (newPath) {
            picPath = newPath;
        }
    }
    var appkey = "981182563";
    var relatedUid = "";
    var language = "zh-cn";
    window.open("http://service.weibo.com/share/share.php?url=" + url + "&appkey=" + appkey + "&title=" + title + "&pic=" + picPath + "&ralateUid=" + relatedUid + "&language=" + language, "_blank", "width=615,height=505")

}

function AddQQSpaceButton(holderSelector, text, picPath, picId, isAdmin) {
    var p = {
        url: location.href,
        showcount: '0', /*是否显示分享总数,显示：'1'，不显示：'0' */
        desc: '测试', /*默认分享理由(可选)*/
        summary: 'summary', /*分享摘要(可选)*/
        title: '来自木头盒子的分享', /*分享标题(可选)*/
        site: '木头盒子', /*分享来源 如：腾讯网(可选)*/
        pics: picPath, /*分享图片的路径(可选)*/
        style: '203',
        width: 98,
        height: 22
    };
    var s = [];
    for (var i in p) {
        s.push(i + '=' + encodeURIComponent(p[i] || ''));
    }

    $(holderSelector).append(['<a version="1.0" class="qzOpenerDiv" href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?', s.join('&'), '" target="_blank">分享</a>'].join(''));
}

function AddQQWeiboButton(holderSelector, text, picPath, picId, isAdmin) {
    text = text.replace(/\n/ig, '').replace(/\'/ig, '\\\'');
    $(holderSelector).append('<a href="javascript:void(0)" onclick="postToWb(\'' + text + '\', \'' + picPath + '\',' + picId + ',' + isAdmin + ');return false;" class="tmblog" title="分享到腾讯微博"><img src="/img/share_qqt_ico.png" height=16 height=16></a>');

}

function postToWb(text, picPath, picId, isAdmin) {
    if (isAdmin) {
        var newPath = ajaxGenerateHeader(false, picId, null, handleFault);
        if (newPath) {
            picPath = newPath;
        }
    }

    var _url = encodeURIComponent(document.location);
    var _assname = encodeURI(""); //你注册的帐号，不是昵称
    var _appkey = encodeURI("100247671"); //你从腾讯获得的appkey
    var _pic = encodeURI(picPath); //（例如：var _pic='图片url1|图片url2|图片url3....）
    var _t = text; //标题和描述信息
    if (_t.length > 120) {
        _t = _t.substr(0, 117) + '...';
    }
    _t = encodeURI(_t); //请在这里添加你自定义的分享内容

    var _u = 'http://share.v.t.qq.com/index.php?c=share&a=index&url=' + _url + '&appkey=' + _appkey + '&pic=' + _pic + '&assname=' + _assname + '&title=' + _t;
    window.open(_u, '', 'width=700, height=680, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no');
}
    

