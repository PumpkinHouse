﻿/// <reference path="lib/jquery-1.7.js" />

function loadjscssfile(filename, filetype) {
    var fileref;
    if (filetype == "js") { //if filename is a external JavaScript file
        fileref = document.createElement('script')
        fileref.setAttribute("type", "text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype == "css") { //if filename is an external CSS file
        fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename);
    }
    var head = document.getElementsByTagName('head');
    head[0].appendChild(fileref);
}

function getScrollTop() {
    if (typeof pageYOffset != 'undefined') {
        //most browsers
        return pageYOffset;
    }
    else {
        var B = document.body; //IE 'quirks' 
        var D = document.documentElement; //IE with doctype
        D = (D.clientHeight) ? D : B;
        return D.scrollTop;
    }
}

function doWork() {
    var base = site;

    var baseTag = $('base');
    if (baseTag) {
        base = baseTag.attr('href');
    }

    var c = $.data(document, 'box_collect');
    if (c) return;
    c = $('<div class="Mutou_Pic" id="test"><div class="Mutou_close"><a id="close" href="javascript:void(0)"> </a></div><div class="Mutou_Pic_Con"><div><ul></ul></div></div><br/><div class="fontInfo"><div class="fontInfoCon">下面的图片太小啦，确定分享到木头盒子？</div></div><div class="Mutou_Pic_Con"><div><ul></ul></div><div class="clearfix"></div></div>');

    var img = $('img');
    var recommendMinWidth = 200;
    var recommendMinHeight = 200;

    var displayDimension = 200;

    var totalCount = 0;
    var smallCount = 0;
    img.each(function (index, item) {
        var result = processImage(c, item, base, 190, 190, 10000, 10000, 0);
        if (result) {
            totalCount++;
        }
        if (!result) {
            result = processImage(c, item, base, 100, 100, 190, 190, 1);
            if (result) {
                totalCount++;
                smallCount++;
            }
        }
    });

    c.addClass('XX');
    c.css('position', 'absolute'); // for IE
    c.css('top', getScrollTop());
    c.css('left', 0);
    $(document.body).css('overflow', 'hidden');

    $('body').append(c);

    if (totalCount == 0) {
        $('.Mutou_Pic_Con').text('对不起，本页没有合适的图片');
    }
    if (smallCount == 0) {
        $('.fontInfo').hide();
    }

    $.data(document, 'box_collect', c);

    $('#close').bind('click', function () {
        c.remove(); $.data(document, 'box_collect', null)
        $(document.body).css('overflow', 'auto');
    });
}

function processImage(container, item, base, minWidth, minHeight, maxWidth, maxHeight, sectionIndex) {
    var it = $(item);
    var url = it.attr('src');
    if (base && url && url.indexOf("http://") < 0 && url.indexOf("https://") < 0 && url.charAt(0) != '/') {
        if (base.charAt(base.length - 1) != "/") {
            base += "/";
        }
        url = base + url;
    }
    var w = it.width();
    var h = it.height();
    if (w <= minWidth || h <= minHeight || w > maxWidth || h > maxHeight) {
        return false;
    }

    var ratio = 1.0 * w / h;
    var nw;
    var nh;
    if (w > h) {
        nw = minWidth;
        nh = h * nw / w;
    }
    else {
        nh = minHeight;
        nw = w * nh / h;
    }

    var im = $('<img/>');
    im.css('width', nw);
    im.css('height', nh);
    im.attr('src', url);

    var si = $('<span/>');
    si.text(w + 'x' + h);

    var li = $('<li/>');
    li.append(im);
    li.append(si);
    var mask = $('<div class="collectSure allPngPic"></div>');
    li.append(mask);

    $($('ul', container)[sectionIndex]).append(li);

    li.bind('mouseenter', function () {
        mask.show();
    }).bind('mouseleave', function () {
        mask.hide();
    });

    mask.parent().bind('click', function () {
        var title = document.title;
        if (title.length > 50) {
            title = title.substring(0, 50);
        }
        var iurl = root + 'Collect.aspx?img=' + encodeURIComponent(url) + "&src=" + encodeURIComponent(window.location) + "&t=" + encodeURIComponent(title);
        window.open(iurl, '盒子工具', 'height=600, width=1000, toolbar =no, menubar=no, scrollbars=no, resizable=no, location=no, status=no');
    });

    return true;
}

function wait(count) {
    if (count > 15) {
        doWork();
    }
    else if (window.$ !== undefined && typeof($.data) != "undefined") {
        doWork();
    }
    else {
        setTimeout("wait(" + (count + 1) + ")", 100);
    }
}

var root = "http://www.mutouhezi.com/";

var site = document.location;
var base = site;

if (root.indexOf(site.hostname, 0) >= 0) {
    alert('对不起，不能在本站抓取图片...');
}
else {
    loadjscssfile(root + "Scripts/lib/jquery-1.7.min.js", 'js');
    loadjscssfile(root + "Scripts/lib/jquery-ui-1.8.20.custom.min.core.js", 'js');
    loadjscssfile(root + "Styles/collectTool.css?1", 'css');

    if (window.$ !== undefined && typeof ($.data) != "undefined") {
        doWork();
    }
    else {
        setTimeout("wait(0)", 100);
    }
}

