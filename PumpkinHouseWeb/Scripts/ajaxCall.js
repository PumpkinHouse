﻿/// <reference path="lib/jquery-1.7.js" />
/// <reference path="util.js" />
/// <reference path="data.js" />

function ajaxLogon(isAsync, email, password, rememberMe, successCallback, failCallback) {
    var url = '/PublicService/AccountService.svc/ajax/Logon';
    return ajaxPost(isAsync, { email: email, password: password, rememberMe: rememberMe }, url, successCallback, failCallback);
}

function ajaxRegisterAssociateAccount(isAsync, user, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/RegisterUser';
    return ajaxPost(isAsync, { user: user }, url, successCallback, failCallback);
}

function ajaxAssociateAccount(isAsync, email, password, external, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/AssociateExternal';
    return ajaxPost(isAsync, { email: email, password: password, external: external }, url, successCallback, failCallback);
}

function ajaxRemoveAssociation(isAsync, id, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/RemoveAssociate';
    return ajaxPost(isAsync, { id: id }, url, successCallback, failCallback);
}

function ajaxGetAccountAssociations(isAsync, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/FindAccountAssociations';
    return ajaxPost(isAsync, { }, url, successCallback, failCallback);
}

function ajaxRequestPasswordReset(isAsync, email, successCallback, failCallback) {
    var url = '/PublicService/AccountService.svc/ajax/SendResetPasswordEmail';
    return ajaxPost(isAsync, { email: email }, url, successCallback, failCallback);
}

function ajaxResetPassword(isAsync, email, code, newPassword, successCallback, failCallback) {
    var url = '/PublicService/AccountService.svc/ajax/ResetPassword';
    return ajaxPost(isAsync, { email: email, code: code, newPassword: newPassword }, url, successCallback, failCallback);
}

function getKeywordsSync(failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindKeywordCategories';
    return ajaxGet(false, url, null, failCallback);
}

function getAssociatedKeywordsSync(keyword, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAssociatedKeywords';
    return ajaxPost(false, { keyword: keyword }, url, null, failCallback);
}

function getLocationsSync(failCallback) {
    var url = '/PublicService/Util.svc/ajax/FindLocations';
    return ajaxGet(false, url, null, failCallback);
}

function getTagsAjaxSync(type, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindTags';
    return ajaxPost(false, { type: type }, url, null, failCallback);
}

function ajaxGetUserInfo(isAsync, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/FindUserInfo';
    return ajaxPost(isAsync, {}, url, successCallback, failCallback);
}

function ajaxUpdateUserInfo(isAsync, info, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/UpdateUserInfo';
    return ajaxPost(isAsync, { info: info }, url, successCallback, failCallback);
}

function ajaxUpdateUserIntro(isAsync, intro, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/UpdateUserSelfIntroduction';
    return ajaxPost(isAsync, { intro: intro }, url, successCallback, failCallback);
}

function ajaxUpdateUserEmail(isAsync, email, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/UpdateUserEmail';
    return ajaxPost(isAsync, { email: email }, url, successCallback, failCallback);
}

function ajaxUpdateReceiveEmail(isAsync, receiveEmail, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/UpdateReceiveEmail';
    return ajaxPost(isAsync, { receiveEmail: receiveEmail }, url, successCallback, failCallback);
}

function ajaxUpdatePassword(isAsync, oldPwd, newPwd, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/ChangePassword';
    return ajaxPost(isAsync, { oldPassword: oldPwd, newPassword: newPwd }, url, successCallback, failCallback);
}

function createAlbumSync(album, failCallback) {
    var url = '/Service/UserService.svc/ajax/CreateAlbum';
    var request = { album: album };
    return ajaxPost(false, request, url, null, failCallback);
}

function ajaxCreateAlbums(isAsync, albumNames, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/CreateAlbums';
    var request = { albumNames: albumNames };
    return ajaxPost(isAsync, request, url, successCallback, failCallback);
}

function ajaxUpdateAlbumName(isAsync, albumId, name, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/UpdateAlbumName';
    var request = { albumId: albumId, name: name };
    return ajaxPost(isAsync, request, url, successCallback, failCallback);
}

function ajaxUpdateAlbumDesc(isAsync, albumId, description, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/UpdateAlbumDescription';
    var request = { albumId: albumId, description: description };
    return ajaxPost(isAsync, request, url, successCallback, failCallback);
}

function ajaxUpdateAlbum(isAsync, albumId, albumName, description, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/UpdateAlbum';
    var request = { albumId: albumId, name: albumName, description: description };
    return ajaxPost(isAsync, request, url, successCallback, failCallback);
}

function ajaxUploadPicture(isAsync, picture, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/UploadPicture';
    var request = { picture: picture };
    return ajaxPost(isAsync, request, url, successCallback, failCallback);
}

function ajaxUpdatePictureDescription(isAsync, collectionId, desc, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/UpdatePictureDescription';
    var request = { collectionId: collectionId, description: desc };
    return ajaxPost(isAsync, request, url, successCallback, failCallback);
}

function getMyAlbumNamesSync(includePicture, failCallback) {
    var url = '/Service/UserService.svc/ajax/FindMyAlbumNames';
    return ajaxPost(false, {}, url, null, failCallback);
}

function ajaxGetAlbumsLikedBy(isAsync, username, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAlbumsLikedByUser';
    return ajaxPost(isAsync, { username: username, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new AlbumToDisplay(item) }));
    }, failCallback);
}

function ajaxGetAllPictures(isAsync, keyword, pageNumber, commodityOnly, mode, isSearch, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAllOriginalCollections';

    return ajaxPost(isAsync, { keyword: keyword, pageNumber: pageNumber, commodityOnly: commodityOnly, mode: mode, isSearch: isSearch }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new CollectionToDisplay(item) }));
    }, failCallback);
}

function ajaxGetAllActivityPictures(isAsync, keyword, pageNumber, commodityOnly, mode, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAllActivityCollections';

    return ajaxPost(isAsync, { keyword: keyword, pageNumber: pageNumber, commodityOnly: commodityOnly, mode: mode }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new CollectionToDisplay(item) }));
    }, failCallback);
}


function ajaxGetAllActions(isAsync, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAllActions';

    return ajaxPost(isAsync, { pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new CollectionToDisplay(item) }));
    }, failCallback);
}

function ajaxGetMyThreads(isAsync, pageNumber, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/FindMyThreads';
    return ajaxPost(isAsync, { pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new ThreadToDisplay(item) }));
    }, failCallback);
}

function ajaxGetMyMessages(isAsync, target, pageNumber, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/FindMyMessages';
    return ajaxPost(isAsync, { target: target, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new MessageToDisplay(item) }));
    }, failCallback);
}

function ajaxSendMessage(isAsync, msg, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/SendMessage';
    return ajaxPost(isAsync, { message: msg }, url, function (result) { successCallback(new MessageToDisplay(result)); }, failCallback);
}

function ajaxGetMyInterest(isAsync, pageNumber, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/FindMyInterest';
    return ajaxPost(isAsync, { pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new CollectionToDisplay(item) }));
    }, failCallback);
}

function ajaxGetPicturesOfUser(isAsync, username, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindPicturesOfUser';
    return ajaxPost(isAsync, { username: username, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new CollectionToDisplay(item) }));
    }, failCallback);
}

function ajaxGetCollectionsOfUser(isAsync, username, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindCollectedItemsOfUser';
    return ajaxPost(isAsync, { username: username, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new CollectionToDisplay(item) }));
    }, failCallback);
}

function ajaxGetAllCollectionsOfUser(isAsync, username, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAllCollectionsOfUser';
    return ajaxPost(isAsync, { username: username, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new CollectionToDisplay(item) }));
    }, failCallback);
}

function getUserSync(username, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindUser';
    return ajaxPost(false, { username: username }, url, null, failCallback);
}

function ajaxGetPopularUsers(isAsync, number, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindPopularUsers';
    return ajaxPost(isAsync, { number: number }, url, function (result) { successCallback(new PaginationList(result.count, result.list)); }, failCallback);
}

function ajaxFanUser(isAsync, username, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/Fan';
    return ajaxPost(isAsync, { username: username }, url, successCallback, failCallback);
}

function ajaxUnfanUser(isAsync, username, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/UnFan';
    return ajaxPost(isAsync, { username: username }, url, successCallback, failCallback);
}

function ajaxGetAlbumsOfUser(isAsync, username, pageNumber, includePic, limit, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAlbumsOfUser';
    return ajaxPost(isAsync, { username: username, pageNumber: pageNumber, includePicture: includePic, limit: limit }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new AlbumToDisplay(item); }));
    });
}

function ajaxDeleteAlbum(isAsync, albumId, deleteContent, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/DeleteAlbumById';
    return ajaxPost(isAsync, { albumId: albumId, deleteContent: deleteContent }, url, successCallback, failCallback);
}

function getAlbumSync(albumId, limit, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAlbum';
    return new AlbumToDisplay(ajaxPost(false, { albumId: albumId, limit: limit }, url, null, failCallback));
}

function ajaxGetAlbum(isAsync, albumId, limit, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAlbum';
    return ajaxPost(isAsync, { albumId: albumId, limit: limit }, url, function(result) {
        successCallback(new AlbumToDisplay(result));
    }, failCallback);
}

function getDefaultAlbumSync(username, picLimit, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindDefaultAlbum';
    return new AlbumToDisplay(ajaxPost(false, { username: username, picLimit: picLimit }, url, null, failCallback));
}

function ajaxGetDefaultAlbum(isAsync, username, picLimit, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindDefaultAlbum';
    return ajaxPost(isAsync, { username: username, picLimit: picLimit }, url, function (result) {
        successCallback(new AlbumToDisplay(result));
    }, failCallback);
}

function ajaxGetPopularAlbums(isAsync, number, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindPopularAlbums';
    return ajaxPost(isAsync, { number: number }, url, 
       function (result) { 
          successCallback(new PaginationList(result.count, result.list, function (item) { return new AlbumToDisplay(item); }))
       },
       failCallback);
}

function ajaxLikeAlbum(isAsync, albumId, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/LikeAlbum';
    return ajaxPost(isAsync, { albumId: albumId }, url, successCallback, failCallback);
}

function ajaxCancelLikeAlbum(isAsync, albumId, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/CancelLikeAlbum';
    return ajaxPost(isAsync, { albumId: albumId }, url, successCallback, failCallback);
}

function ajaxLikePicture(isAsync, collectionId, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/LikePicture';
    return ajaxPost(isAsync, { collectionId: collectionId }, url, successCallback, failCallback);
}

function ajaxCancelLikePicture(isAsync, collectionId, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/CancelLikePicture';
    return ajaxPost(isAsync, { collectionId: collectionId }, url, successCallback, failCallback);
}

//function getPictureSync(pictureId, failCallback) {
//    var url = '/PublicService/PublicService.svc/ajax/FindPicture';
//    return new PictureToDisplay(ajaxPost(false, { pictureId: pictureId, replyLimit: -1 }, url, null, failCallback));
//}

function getCollectionSync(collectionId, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindCollection';
    return new CollectionToDisplay(ajaxPost(false, { collectionId: collectionId, replyLimit: -1 }, url, null, failCallback));
}

function ajaxGetCollectionsByAlbum(isAsync, albumId, username, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindCollectionsByAlbum';
    return ajaxPost(isAsync, { albumId: albumId, username: username, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new CollectionToDisplay(item); }));
    }, failCallback);
}

function updateCollectionTagSync(collectionId, tags, failCallback) {
    var request = { collectionId: collectionId, tags: tags };
    var url = '/Service/UserService.svc/ajax/AddCollectionTags';
    return ajaxPost(false, request, url, null, failCallback);
}

function updateAlbumTagSync(albumId, tags, failCallback) {
    var request = { albumId: albumId, tags: tags };
    var url = '/Service/UserService.svc/ajax/AddAlbumTags';
    return ajaxPost(false, request, url, null, failCallback);
}

function removeAlbumTagSync(albumId, tagToRemove, failCallback) {
    var request = { albumId: albumId, tagToRemove: tagToRemove };
    var url = '/Service/UserService.svc/ajax/DeleteAlbumTag';
    return ajaxPost(false, request, url, null, failCallback);
}

function removeCollectionTagSync(collectionId, tagToRemove, failCallback) {
    var request = { collectionId: collectionId, tagToRemove: tagToRemove };
    var url = '/Service/UserService.svc/ajax/DeleteCollectionTag';
    return ajaxPost(false, request, url, null, failCallback);
}

function ajaxCollectPicture(isAsync, collectionId, albumId, successCallback, failCallback) {
    var request = { collectionId: collectionId, albumId: albumId };
    var url = '/Service/UserService.svc/ajax/Collect';
    return ajaxPost(isAsync, request, url, successCallback, failCallback);
}

function ajaxCancelCollect(isAsync, pictureId, albumId, successCallback, failCallback) {
    var request = { pictureId: pictureId, albumId: albumId };
    var url = '/Service/UserService.svc/ajax/CancelCollect';
    return ajaxPost(isAsync, request, url, successCallback, failCallback);
}

function ajaxDeletePicture(isAsync, collectionId, successCallback, failCallback) {
    var request = { collectionId: collectionId };
    var url = '/Service/UserService.svc/ajax/DeletePicture';
    return ajaxPost(isAsync, request, url, successCallback, failCallback);
}

function ajaxMovePicture(isAsync, collectionId, albumId, successCallback, failCallback) {
    var request = { collectionId: collectionId, newAlbumId: albumId };
    var url = '/Service/UserService.svc/ajax/MovePicture';
    return ajaxPost(isAsync, request, url, successCallback, failCallback);
}

//function ajaxDeletePicture(isAsync, pictureId, successCallback, failCallback) {
//    var request = { pictureId: pictureId};
//    var url = '/Service/UserService.svc/ajax/DeletePicture';
//    return ajaxPost(isAsync, request, url, successCallback, failCallback);
//}

function getAlbumsCollectingPicture(pictureId, limit, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAlbumsCollectingPicture';
    return ajaxPost(false, { pictureId: pictureId, limit: limit }, url, null, failCallback);
}

function ajaxCropPhoto(isAsync, xPos, yPos, dim, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/CropPhoto';
    return ajaxPost(isAsync, { xPosition: xPos, yPosition: yPos, dimension: dim }, url, successCallback, failCallback);
}

function ajaxPostReply(isAsync, reply, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/PostReply';
    return ajaxPost(isAsync, { post: reply }, url, successCallback, failCallback);
}

function ajaxDeleteReply(isAsync, replyId, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/DeleteReply';
    return ajaxPost(isAsync, { replyId: replyId }, url, successCallback, failCallback);
}

function ajaxSearchAlbums(isAsync, keyword, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/SearchAlbums';
    return ajaxPost(isAsync, { keyword: keyword, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new AlbumToDisplay(item); }));
    }, failCallback);
}

function ajaxSearchUser(isAsync, keyword, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/SearchUsers';
    return ajaxPost(isAsync, { keyword: keyword, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list));
    }, failCallback);
}

function ajaxGetFans(isAsync, username, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindFans';
    return ajaxPost(isAsync, { username: username, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list));
    }, failCallback);
}
function ajaxGetAttentions(isAsync, username, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAttentions';
    return ajaxPost(isAsync, { username: username, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list));
    }, failCallback);
}

function ajaxGetPictureReply(isAsync, pictureId, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindPictureReply';
    return ajaxPost(isAsync, { pictureId: pictureId, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new ReplyToDisplay(item); }));
    }, failCallback);
}

function ajaxGetAlbumReply(isAsync, albumId, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAlbumReply';
    return ajaxPost(isAsync, { albumId: albumId, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new ReplyToDisplay(item); }));
    }, failCallback);
}

function ajaxGetDiscussionReply(isAsync, discussionId, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindDiscussionReply';
    return ajaxPost(isAsync, { discussionId: discussionId, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new ReplyToDisplay(item); }));
    }, failCallback);
}

function ajaxGetNotice(isAsync, filter, pageNumber, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/FindNotice';
    return ajaxPost(isAsync, { filter: filter, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new NoticeToDisplay(item); }));
    }, failCallback);
}

function ajaxGetNoticeSummary(isAsync, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/FindNoticeSummary';
    return ajaxPost(isAsync, {}, url, successCallback, failCallback);
}

function ajaxGetAllBoards(isAsync, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAllDiscussionBoard';
    return ajaxPost(isAsync, {}, url, function (result) {
        successCallback(new PaginationList(result.count, result.list));
    }, failCallback);
}

function getAllBoardNamesSync(failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindDiscussionBoardNames';
    return ajaxPost(false, {}, url, null, failCallback);
}

function ajaxPostDiscussion(isAsync, discussion, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/PostDiscussion';
    return ajaxPost(isAsync, { discussion: discussion }, url, successCallback, failCallback);
}

function ajaxDeleteDiscussion(isAsync, discussionId, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/DeleteDiscussion';
    return ajaxPost(isAsync, { discussionId: discussionId }, url, successCallback, failCallback);
}

function ajaxGetDiscussions(isAsync, boardId, pageNumber, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindDisucssions';
    return ajaxPost(isAsync, { boardId: boardId, pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new DiscussionToDisplay(item) }));
    }, failCallback);
}

function ajaxGetDiscussion(isAsync, discussionId, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindDisucssion';
    return ajaxPost(isAsync, { discussionId: discussionId }, url, function (result) {
        successCallback(new DiscussionToDisplay(result));
    }, failCallback);
}

function ajaxGetLatestDiscussions(isAsync, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindLatestDiscussions';
    return ajaxPost(isAsync, {}, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new DiscussionToDisplay(item) }));
    }, failCallback);
}

function ajaxGetMyDiscussions(isAsync, pageNumber, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/FindMyDiscussions';
    return ajaxPost(isAsync, { pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new DiscussionToDisplay(item) }));
    }, failCallback);
}

function ajaxGetMyRepliedDiscussions(isAsync, pageNumber, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/FindMyRepliedDiscussions';
    return ajaxPost(isAsync, { pageNumber: pageNumber }, url, function (result) {
        successCallback(new PaginationList(result.count, result.list, function (item) { return new DiscussionToDisplay(item) }));
    }, failCallback);
}

function ajaxDownloadHtmlPage(isAsync, target, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/DownloadHtmlPage';
    return ajaxPost(isAsync, { url: target }, url, successCallback, failCallback);
}

function ajaxAddInitialFavoriates(isAsync, usernames, albumIds, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/AddInitialFavoriates';
    return ajaxPost(isAsync, { usernames: usernames, albumIds: albumIds }, url, successCallback, failCallback);
}

function ajaxGenerateHeader(isAsync, pictureId, successCallback, failCallback) {
    var url = '/Service/UserService.svc/ajax/GenerateHeader';
    return ajaxPost(isAsync, { pictureId: pictureId }, url, successCallback, failCallback);
}

function ajaxGuessLike(isAsync, collectionId, currentLikeId, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/GuessLike';
    return ajaxPost(isAsync, { collectionId: collectionId, currentLikeId: currentLikeId }, url, successCallback, failCallback);
}

function ajaxResponseSurvey(isAsync, like, comment, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/ResponseSurvey';
    return ajaxPost(isAsync, { like: like, comment: comment }, url, successCallback, failCallback);
}

function ajaxFindAdvertisement(isAsync, successCallback, failCallback) {
    var url = '/PublicService/PublicService.svc/ajax/FindAdvertisement';
    return ajaxPost(isAsync, { }, url, successCallback, failCallback);
}
