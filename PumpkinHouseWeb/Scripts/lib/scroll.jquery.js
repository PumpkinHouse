(function ($) {
    $.fn.scrollLoad = function (options) {

        var defaults = {
            url: '',
            data: '',
            ScrollAfterHeight: 90,
            onload: function (data, itsMe) {
                alert(data);
            },
            start: function (itsMe) { },
            continueWhile: function () {
                return true;
            },
            getDataAsync: function (callback) {
                callback(data);
            }
        };

        for (var eachProperty in defaults) {
            if (options[eachProperty]) {
                defaults[eachProperty] = options[eachProperty];
            }
        }

        var target = $(this);

        this.scrolling = false;
        this.scrollPrev = this.onscroll ? this.onscroll : null;
        $(window).bind('scroll', function (e) {
            if (this.scrollPrev) {
                this.scrollPrev();
            }
            if (this.scrolling) return;

            if (Math.round((getScrollTop() * 1.0 + $(window).height()) / target[0].clientHeight * 100) > defaults.ScrollAfterHeight) {
                defaults.start.call(this, this);
                this.scrolling = true;
                $this = $(this);
                defaults.getDataAsync(function (data) {
                    $this[0].scrolling = false;
                    defaults.onload.call($this[0], data, $this[0]);
                    if (!defaults.continueWhile.call($this[0], data)) {
                        $this.unbind('scroll');
                    }
                });
            }
        });
    }
})(jQuery);
