﻿function find(targetOrSelector) {
    if (typeof targetOrSelector === 'string') {
        return $(targetOrSelector);
    }
    else return targetOrSelector;
}

(function ($) {
    $.extend($.fn, {
        mDialog: function (options) {
            // if nothing is selected, return nothing; can't chain anyway
		    if (!this.length) {
			    options && options.debug && window.console && console.warn( "nothing selected, can't validate, returning nothing" );
			    return;
		    }
            
            var defaults = {
                width: 400,
                height: 200,
                modal: true,
                autoOpen: true, 
                closeBtnClass: 'close',
                borderWidth: 10,
                showCloseBtn: true,
                position_x: 'center',
                position_y: 'center'
            }

            var settings = $.extend( true, {}, defaults, options );

            content = $('<div></div>').append(this);
            var dialog = $('#normalDialogTemplate').tmpl({ message: content.html() });
            this.dialog = dialog;
            dialog.settings = settings;

            var target = dialog.find('messageBoxCon');
            dialog.css('width', settings.width).css('height', settings.height);
            dialog.css('z-index', 2001);
            $('.messageBoxConBg', dialog).css('width', settings.width + settings.borderWidth * 2)
                .css('height', settings.height + settings.borderWidth * 2)
                .css('left', settings.borderWidth * -1).css('top', settings.borderWidth * -1);
            var margin = settings.borderWidth;
            if (margin < 0) margin = 0;
            $('.messageDialog', dialog).css('margin', margin);
            centerDialogForIE9(dialog);

            $(document.body).append(dialog);
            if (settings.modal) {
                var overlay = $('<div id="overlay"></div>');
                $(document.body).append(overlay);
                dialog.overlay = overlay;
            }

            if (settings.autoOpen) {
                dialog.showDialog();
            }

            var closeBtn = dialog.find('.close');
            if (settings.showCloseBtn) {
                closeBtn.show();
            }
            else {
                closeBtn.hide();
            }
            if (settings.closeBtnClass) {
                closeBtn.addClass(settings.closeBtnClass)
            }

            closeBtn.on('click', function () {
                dialog.closeDialog();
            });

            return dialog;
        },
        showDialog: function() {
            var top;
            var left;
            switch (this.settings.position_x) {
                case 'center': 
                    left = ($(window).width() - this.width()) / 2;
                    break;
                case 'left':
                    left = 0;
                    break;
                case 'right':
                    left = $(window).width() - this.width();
                    break;
            }

            switch (this.settings.position_y) {
                case 'center':
                    top = ($(window).height() - this.height()) / 2;
                    break;
                case 'top':
                    top = 0;
                    break;
                case 'bottom':
                    top = $(window).height() - this.height();
                    break;
            }
            this.css('top', top).css('left', left);
            this.show();
            centerDialogForIE9(this.dialog);
            var overlay = this.overlay;
            if (overlay) {
                overlay.css('top', 0).css('left', 0);
                overlay.show();
            }     
        },
        closeDialog: function() {
            this.hide();
            var overlay = this.overlay;
            if (overlay) {
                overlay.hide();
            }    
        },
        destroyDialog: function() {
            var overlay = this.overlay;
            if (overlay) {
                overlay.remove();
            }
            this.remove();
        }
    });
})(jQuery);
