﻿/// <reference path="util.js" />

function initReplyDialog(postReplyCallback, loggedIn) {
    // bind event on textarea
    $('textarea#reply').watermark('盒子要说话...');

    if (!loggedIn) {
        $('textarea#reply').on('focus', function () {
            showLogonDialog($('textarea#reply'));
        });
    }

    // bind the event on reply button
    $('#replyBtn').on('click', function () {
        if (!loggedIn) {
            showLogonDialog();
        }
        else {
            var text = $.trim($('textarea#reply').val());
            if (text== '') {
                defaultValidationHandler('textarea#reply');
                return;
            }
            postReplyCallback(text);
        }
    });

}

function initReplyReplyControl(replyReplyCallback, loggedIn) {
    // bind the event on reply link (click to reply on a reply)
    $('a.replyLink').on('click', function (e) {
        if (!loggedIn) {
            showLogonDialog();
        }
        else {
            var obj = $(this);
            var input = $.data(this, 'reply');
            if (!input) {
                var id = obj.attr('data-reply-id');
                input = $('#replyReplyDialogTemplate').tmpl({ id: id });
                input.insertAfter(obj);
                $.data(this, 'reply', input);

                // bind the reply button
                $('.replyReplyBtn').on('click', function () {
                    var btn = $(this);
                    var rid = btn.attr('data-reply-id');
                    var text = $('#reply-' + rid).val();
                    if ($.trim(text) == '') {
                        defaultValidationHandler('#reply-' + rid);
                        return;
                    }
                    replyReplyCallback(text, rid);
                });
            }
        }
    });


    // bind the delete button
    $('.deleteBtn').on('click', function () {
        var btn = $(this);
        var rid = btn.attr('data-reply-id');
        defaultDeleteCallback(rid);
    });
}

function defaultValidationHandler(selector) {
    blinkInput(selector);
}

function defaultDeleteCallback(id) {

    dialog = showSmallDialog('#confirmDialogTemplate', '.confirmDialog', '确定要删除？');
    $.data(document, 'deleteReplyConfirmDialog', dialog);

    $('#confirmHolder .confirm').on('click', function () {
        dialog.children().hide();
        ajaxDeleteReply(true, id, function (ids) {
            // 删除UI显示的元素
            $('#replies').children().each(function (index, item) {
                var target = $(item);
                var id = target.attr('data-reply-id');
                var i = ids.length - 1;
                for (; i >= 0; i--) {
                    if (id == ids[i]) {
                        target.remove();
                    }
                }
            });

            //删除数据元素
            if (ids.length > 0) {
                var newList = [];
                $(globalReplyCol.list).each(function (index, item) {
                    var i = ids.length - 1;
                    for (; i >= 0; i--) {
                        if (item.id != ids[i]) {
                            newList.push(item);
                        }
                    }
                });

                globalReplyCol.count -= ids.length;
                globalReplyCol.list = newList;
            }

            var d = showSmallDialog('#actionCompleteTemplate', '.successDialog', '删除成功');
            setTimeout(function () { d.children().hide(); }, 1000);

            //            var toRemove = $.grep(globalReplyCol.list, function (item) {
            //                var i = ids.length - 1;
            //                for (; i >= 0; i--) {
            //                    if (item.id == ids[i]) {
            //                        return true;
            //                    }
            //                }
            //            });
            //            var index = toRemove.length - 1;
            //            for (; index >= 0; index--) {
            //                $(toRemove[index]).remove();
            //            }

        }, handleFault);
    });



}
