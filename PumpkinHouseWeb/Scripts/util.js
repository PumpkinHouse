﻿/// <reference path="lib/jquery-1.7.js" />
/// <reference path="lib/urlParser.js" />

function handleSensitiveWord(errorCode, message) {
    if (errorCode == "6") {
        showSmallDialog('#actionFailTemplate', '.failDialog', message);
    }
    else {
        handleFault(errorCode);
    }
}

function findLinkUrl(orderMode, commodityOnly, tag) {
    var result = '';
    switch (orderMode) {
        case 0: result += '/default'; break;
        case 1: result += '/hot'; break;
        case 2: result += '/latest'; break;
    }
    switch (commodityOnly) {
        case false: result += '/all'; break;
        case true: result += '/commodity'; break;
    }
    result += '/' + tag;
    return result;
}

function showSmallDialog(template, dialog, message, width, height) {
    var tmpl = $(template).tmpl({ message: message });
    var holder = $('#confirmHolder').empty().append(tmpl);
    var target = $(dialog);
    target.offset({ top: ($(window).height() - target.height()) / 2 + getScrollTop(), left: ($(window).width() - target.width()) / 2 });
    target.show();

    $('input.fail', holder).on('click', function () {
        target.hide();
    });

    $('input.cancel', holder).on('click', function () {
        target.hide();
    });

    $('.close', holder).on('click', function () {
        target.hide();
    });
    return holder;
}

function _blink(targetSelector, css1, css2) {
    var target;
    if (typeof targetSelector === "string") {
        target = $(targetSelector);
    }
    else {
        target = targetSelector;
    }
    target.removeClass(css1);
    target.addClass(css2);
}

function blinkInput(targetSelector) {
    var i = 0;
    var interval = 500;
    for (; i < 3; i++) {
        setTimeout(function () { _blink(targetSelector, 'Rounded', 'errorBlink') }, interval * (i * 2 + 1));
        setTimeout(function () { _blink(targetSelector, 'errorBlink', 'Rounded') }, interval * (i * 2 + 2));
    }
}


function trimInputValue(inputControl) {
    var value = inputControl.val();
    if (value != undefined && value != null) {
        inputControl.val(value.replace(/\s/g, ''));
    }
}

function setValue(control, message) {
    if (control.is('div')) {
        control.text(message);
    } else if (control.is('input')) {
        control.val(message);
    }
}

String.format = function (src) {
    if (arguments.length == 0) return null;
    var args = Array.prototype.slice.call(arguments, 1);
    return src.replace(/\{(\d+)\}/g, function (m, i) {
        return args[i];
    });
};

Date.prototype.toMSJSON = function () {
    var date = '/Date(' + this.getTime() + ')/'; //CHANGED LINE
    return date;
};

function parseJsonDate(dateString) {
    return new Date(parseInt(dateString.replace("/Date(", "").replace(")/", ""), 10));
}


function SyncResult(hasError, data, errorMessage) {
    this.hasError = hasError;
    this.data = data;
    this.errorMessage = errorMessage;
}

function showError(message) {
    alert(message);
}

function handleFault(errorMessage) {
    var errorCode = parseInt(errorMessage);
    switch (errorCode) {
        case 1: // item not found
            document.location = '/404.aspx';
            break;
        case 401: // unauthorized
            showLogonDialog();
            break;
        case 500: // internal server error
            alert('出错了，请等一下重试');
            break;
        case 400: // bad request
            alert('出错了，请等一下重试');
            break;
         default:
            //showError(errorMessage);
            break;
    }
}

function getErrorMessage(jqXHR, textStatus, errorThrown) {
    var message = '';
    if (jqXHR) {
        if (jqXHR.status == 401) return jqXHR.status;
        message = jqXHR.responseText;
    }
    try {
        var errorObject = $.parseJSON(jqXHR.responseText);
        if (errorObject && errorObject.Message) {
            message += "\n" + errorObject.Message;
        }
    }
    catch (err) {
        message += '\n' + jqXHR.responseText;
    }
    return message;

}

function _ajax(isAsync, method, request, url, successCallback, failCallback) {
    if (isAsync) {
        $.ajax({
            type: method,
            url: url,
            data: JSON.stringify(request),
            cache: true,
            context: this,
            contentType: 'application/json',
            success: function (data) {
                hideLoadingIndicator();
                if (undefined != successCallback && null != successCallback) {
                    successCallback(data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                hideLoadingIndicator();

                if (undefined != failCallback && null != failCallback) {
                    failCallback(getErrorMessage(jqXHR, textStatus, errorThrown));
                }
            }
        });
        if (typeof(globalLoadingIndicatorAtBottom) == "undefined") {
            globalLoadingIndicatorAtBottom = false;
            showLoadingIndicator(globalLoadingIndicatorAtBottom);
        }
        else {
            showLoadingIndicator(globalLoadingIndicatorAtBottom);
        }
    }
    else {
        var returnData;
        var message;
        var success;
        showLoadingIndicator();
        $.ajax({
            type: method,
            url: url,
            data: JSON.stringify(request),
            cache: true,
            context: this,
            async: false,
            contentType: 'application/json',
            success: function (data) {
                returnData = data;
                success = true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                message = getErrorMessage(jqXHR, textStatus, errorThrown);
                success = false;
            }
        });
        hideLoadingIndicator();
        if (!success) {
            if (failCallback) {
                failCallback(message);
                return null;
            }
        } else {
            return returnData;
        }
    }    
}

function ajaxGet(isAsync, url, successCallback, failCallback) {
    return _ajax(isAsync, "GET", {}, url, successCallback, failCallback);
}

function ajaxPost(isAsync, request, url, successCallback, failCallback) {
    return _ajax(isAsync, "POST", request, url, successCallback, failCallback);
}

function isDefined(variable) {
    return typeof (variable) !== "undefined";
}

function showLoadingIndicator(showAtBottom) {
    // 显示读取提示
    var loadingIndicator = $('#loadingTemplate').tmpl();
    var loadingHolder = $('#loadingHolder');

    var top = showAtBottom ? $(window).height() + getScrollTop() - 100 : ($(window).height() - loadingIndicator.height()) / 2 + getScrollTop();

    if (loadingIndicator.length > 0 && loadingHolder.length > 0) {
        loadingIndicator.css('top', top);
        loadingIndicator.css('left', ($(window).width() - loadingIndicator.width()) / 2);
        loadingHolder.empty().append(loadingIndicator);
    }
}

function hideLoadingIndicator() {
    var loadingHolder = $('#loadingHolder');
    if (loadingHolder) {
        loadingHolder.empty();
    }
    globalLoadingIndicatorAtBottom = false;
}


/// required data: 
/// selectorToRemove: the control to remove when the button is clicked
/// targetSelector: the display text field to operate
/// callback: function. It will be called with parameter newText and successCallback when the update button is clicked
function updateFiled(event) {
    var textNode = $(event.target);
    var callback = event.data.callback;

    var text = $.trim(textNode.text());
    var newInput = $('<textarea />');
    newInput.val(text);
    newInput.insertBefore(textNode);
    newInput.focus();
    textNode.hide();

    newInput.on('blur', function () {
        var newName = newInput.val();
        callback(newName, function () {
            textNode.text(newName);
            newInput.remove();
            textNode.show();
        });
    }).keydown(function (event) {
        if (event.keyCode == 13) { // enter 
            newInput.trigger('blur');
            return;
        }
        if (event.keyCode == 27) { // esc
            newInput.remove();
            textNode.show();
            return;
        }
    });
}

function renderTemplate(container, template, data, append) {
    var c = $(container);
    if (append !== true) {
        c.empty();
    }
    var tmpl = $(template).tmpl(data);
    c.append(tmpl);
    return c;
}

function actionRequiesLogin(loggedIn, action) {
    if (!loggedIn) {
        showLogonDialog();
    }
    else {
        action();
    }
}

var globalLogonDialog;

function showLogonDialog(offFocusTarget) {
    if (offFocusTarget && offFocusTarget.length) {
        offFocusTarget.off('focus');
    }
    var logon = $('#logonPanel');
    logon.show();
    globalLogonDialog = logon.mDialog({ width: 960, height: 145, borderWidth: -5, position_y: 'top' });
    $('#email', globalLogonDialog).watermark('邮箱');
    $('#password', globalLogonDialog).watermark('密码');

    $('#submitBtn').on('click', function () {
        var email = $.trim($('#email').val());
        var password = $('#password').val();
        var rememberMe = $('#rememberMe').attr('checked') == 'checked';
        ajaxLogon(true, email, password, rememberMe, logonCallFinish, handleFault);
    });

    //$('#logonPanel #email').trigger('focus');
    if (offFocusTarget && offFocusTarget.length) {
        offFocusTarget.on('focus', function () { showLogonDialog(offFocusTarget) });
    }
}

function logonCallFinish(success) {
    if (success) {
        window.location.href = window.location.href;
        window.location.reload(true);
    }
    else {
        alert('用户名或密码不正确');
    }
}

function pagination(target, total, numberPerPage, callback) {
    $(target).pagination(total, {
        num_edge_entries: 2,
        callback: callback,
        items_per_page: numberPerPage,
        prev_text: "上一页",
        next_text: "下一页"
    });
}

function getScrollTop() {
    if (typeof pageYOffset != 'undefined') {
        //most browsers
        return pageYOffset;
    }
    else {
        var B = document.body; //IE 'quirks'
        var D = document.documentElement; //IE with doctype
        D = (D.clientHeight) ? D : B;
        return D.scrollTop;
    }
}

function addFavoriate(url, title) {
    var ctrl = (navigator.userAgent.toLowerCase()).indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL';
    if (document.all) {
        window.external.addFavorite(url, title);
    } 
    else if (window.sidebar) {
        window.sidebar.addPanel(title, url, "");
    } 
    else {
        alert('您可以尝试通过快捷键' + ctrl + ' + D 加入到收藏夹~');
    }
}

function initCollectButton() {
    $(".mbpho").off('mouseover').off('mouseleave');

    //瀑布流鼠标事件~~Kevin
    $(".mbpho").mouseover(function (event) {
        var target = $(event.target);
        var li = target.parents('li.pic');
        li.addClass('liOver');

        var id = $(".a .i", this).attr('data-picture-id');
        // $(".a #i-" + id + ".i", this).css({ border: "2px #4f4f4f solid" });
        $("#fav-" + id + ".picfav input", this).show();
        var mask = $("#divScren-" + id + ".divScren", this);
        mask.show();
        if ($('img', this).height() < 600) {

            mask.css('height', $('img', this).css('height'));
        }
        else {
            mask.css({ height: "600px" });
        }

    });
    $(".mbpho").mouseleave(function (event) {
        var target = $(event.target);
        var li = target.parents('li.pic');
        li.removeClass('liOver');

        var id = $(".a .i", this).attr('data-picture-id');
       // $(".a #i-" + id + ".i", this).css({ border: "2px #f1f1f1 solid" });
        $("#fav-" + id + ".picfav input", this).hide();
        $("#divScren-" + id + ".divScren", this).hide();
    });
}

function processImageUrl(imageUrl, baseUrl) {
    imageUrl = $.trim(imageUrl);
    if (imageUrl.indexOf("http://") == 0 || imageUrl.indexOf("https://") == 0) return imageUrl;
    var p = new Poly9.URLParser(baseUrl);

    if (imageUrl.charAt(0) == '/') {
        if (imageUrl.charAt(1) == '/') {
            return p.getProtocol() + ":" + imageUrl;
        }
        return p.getProtocol() + "://" + p.getHost() + imageUrl;
    }
    else {
        var fragment = p.getFragment();
        if (fragment.length) {
            fragment.pop();
            fragment = fragment.join('/');
        }
        else {
            fragment = "/";
        }
        return p.getProtocol() + "://" + p.getHost() + fragment + imageUrl;
    }
}

function centerDialogForIE9(dialog) {
   // var dialogFrame = dialog.parent();
   // dialogFrame.css('left', ($(window).width() - dialogFrame.width()) / 2);
}

function initCreateAlbumDialog(btnSelector) {
    // bind event on create button
    $(btnSelector).on('click', function () {
        var dialog = $.data(document, 'createAlbumDialog');
        if (dialog) {
            dialog.showDialog();
        }
        else {
            var template = $('#createAlbumDialogTemplate').tmpl();
            template = $('#dialogHolder').append(template);
            dialog = template.mDialog({ width: 580, height: 280 });

            var tagger = new $.tagPanel(false, '#albumTagPanel', [], 1);
            $.data(document, 'createAlbumDialog', dialog);

            $('#albumName', dialog).on('keyup', function (eventObject) {
                var target = $(eventObject.target);
                var text = target.val();
                if (text && text.length > 12) {
                    target.val(text.substring(0, 12));
                }
            });

            $('#createAlbumBtn').on('click', function () {
                if (!tagger.validateTag()) {
                    tagger.validationFailHanlder();
                    return;
                }
                var name = $.trim($('#albumName').val());
                if (!name) {
                    blinkInput('#albumName');
                    return;
                }
                var desc = $.trim($('#comment').val());
                var alb = new AlbumToCreate(name, desc, tagger.value());
                var newAlbum = createAlbumSync(alb, function (errorCode) {
                    handleSensitiveWord(errorCode, '抱歉，专辑名或标签包含非法字符，请修改后再创建。');
                });
                if (newAlbum) {
                    //刷新标签列表
                    var updatedTags = getTagsAjaxSync(1, handleFault);
                    if (updatedTags) {
                        renderTemplate('#commonTags', '#tagDisplayDetailsTemplate', updatedTags);
                        tagger.tag('.tag');
                    }

                    dialog.closeDialog();
                    var message = $('#createAlbumSuccessTemplate').tmpl(newAlbum);
                    showSmallDialog('#actionCompleteTemplate', '.successDialog', message.html());

                    var holder = $('#myAlbums');
                    newAlbum.description = desc;
                    newAlbum.username = globalLogginUser;
                    newAlbum.numberOfPic = 0;
                    newAlbum.numberOfLike = 0;
                    newAlbum.likedByMe = false;
                    newAlbum.pictures = new PaginationList(0, []);

                    var template = $('#albumListTemplate').tmpl(newAlbum, { isMe: globalIsMe });
                    template.appendTo(holder);

                    $('#albumName').val('');
                    $('#comment').val('');
                    tagger.value('');
                }
            });
        }
    });
}

function encodeXml(string) {
    return string.replace(/\&/g, '&' + 'amp;').replace(/</g, '&' + 'lt;')
        .replace(/>/g, '&' + 'gt;').replace(/\'/g, '&' + 'apos;').replace(/\"/g, '&' + 'quot;').replace(/ /g, '&' + 'nbsp;');
}

function cacheLoading(result) {
    if (result) {
        $.data(document, 'cache', result);
    }
}

function renderCache(callback) {
    var data = $.data(document, 'cache');
    if (data && callback) {
        callback(data);
    }
}