﻿/// <reference path="lib/jquery-1.7.js" />
/// <reference path="gettmpl.js" />
/// <reference path="ajaxCall.js" />

(function ($) {
    $.tagPanel = function (dialogMode, panelSelector, existingTags, tagType, updateHandler, deleteHandler, isLoggedIn, isMe) {
        var tagger = new Tagger(dialogMode, panelSelector, existingTags, tagType, updateHandler, deleteHandler, isLoggedIn, isMe);
        $.data(document, panelSelector, tagger);
        tagger.init();
        return tagger;
    };

    $.getTagPanel = function (name) {
        return $.data(document, name);
    }

    Tagger = function (dialogMode, panel, tags, tagType, updateHandler, deleteHandler, isLoggedIn, isMe) {
        this.dialogMode = dialogMode;
        this.existingTags = tags;
        if (!this.existingTags) {
            this.existingTags = [];
        }

        this.name = panel;
        this.displayPanel = $(panel);
        this.selectedClass = 'selectedTag';
        this.ajaxErrorCallback = handleFault;
        this.updateHandler = updateHandler;
        this.deleteHandler = deleteHandler;
        this.validationFailHanlder = function () {
            showSmallDialog('#actionFailTemplate', '.failDialog', '最多只能添加5个标签哦！');
        }
        this.tagType = tagType;
        this.isLoggedIn = isLoggedIn;
        this.isMe = isMe;
    }

    $.extend(Tagger, {
        prototype: {
            init: function () {
                $.getTmplSync('/template/tagTemplate.html');

                if (this.dialogMode === true) { // only render in dialogMode
                    // render the panel
                    this.displayPanel.empty();
                    var panelTemplate = $('#tagDisplayTemplate').tmpl({ isMe: this.isMe, tags: this.existingTags });
                    this.displayPanel.append(panelTemplate);

                    this.bindDeleteEvent();

                    var tagger = this;
                    tagger.tagDialog(true);
                    // bind click to open dialog on add button
                    $('#showTagDialogButton').on('click', function () {
                        if (globalLoggedIn) {
                            tagger.tagDialog();
                            return false;
                        }
                        else {
                            showLogonDialog();
                        }
                    });
                }
                else {
                    // render the panel now
                    // retrieve predefined tags from server
                    var tags = getTagsAjaxSync(this.tagType, this.ajaxErrorCallback);

                    // render the template
                    var dialogTemplate = $('#tagNonDialogTemplate').tmpl({ tags: tags });
                    dialogTemplate.appendTo(this.displayPanel);

                    this.input = $('#tagInput', this.displayPanel); // the input is the display panel
                    this.input.watermark('多个标签请用空格隔开');

                    // bind the tag
                    this.tag('.tag');
                }

            },
            tagDialog: function (initOnly) {
                if (this.dialogMode !== true) return; // only dialog in dialogMode
                if (this.dialog && !initOnly) {
                    this.dialog.showDialog();
                }
                else {
                    // retrieve predefined tags from server
                    var tags = getTagsAjaxSync(this.tagType, this.ajaxErrorCallback);

                    // render the template
                    var dialogTemplate = $('#tagDialogTemplate').tmpl({ tags: tags, isMe: this.isMe });
       
                    // prepare the dialog
                    this.dialog = dialogTemplate.mDialog({ width: 382, height: 227, autoOpen: initOnly == false });

                    // bind the tags
                    this.tag('.tag', this.dialog);

                    // set reference to the input field
                    this.input = $('#tagInput', this.dialog);

                    this.input.watermark('多个标签请用空格隔开');

                    // bind the update event
                    var tagger = this;
                    $('#updateTagButton').on('click',
                        function () {
                            if (!tagger.validateTag()) {
                                tagger.validationFailHanlder();
                                return;
                            }
                            var result = tagger.updateHandler();
                            if (result) {
                                tagger.input.val('');
                                //更新标签显示
                                renderTemplate('#tags', '#tagDetailsTemplate', { tags: result.split(' '), isMe: tagger.isMe });
                                tagger.bindDeleteEvent();
                                tagger.dialog.closeDialog();
                            }
                        });
                }
            },
            bindDeleteEvent: function () {
                var root = this;
                if (root.isMe && root.deleteHandler) {
                    $('#tags .b', root.displayPanel).on('mouseenter', function (e) {
                        $(e.target).find('.canDelete').show();
                    }).on('mouseleave', function (e) {
                        var t = $(e.target);
                        if (t.hasClass('canDelete')) {
                            t = t.parent();
                        }
                        t.find('.canDelete').hide();
                    }).on('click', function (targetObject) {
                        var target = $(targetObject.target);
                        if (target.hasClass('canDelete')) {
                            target = target.parent();
                            var tagToRemove = $.trim(target.text());
                            if (tagToRemove) {
                                root.deleteHandler(tagToRemove);
                                target.remove();
                            }
                        }
                    });
                }
            },
            tag: function (selector, context) {
                var elements = $(selector, context);
                var tagger = this;
                $.each(elements, function (index, tagElement) {
                    var txt = $(tagElement).text();
                    $(tagElement).on('click', function (event) {
                        var selected = tagger.containTag(txt);
                        if (selected) {
                            $(this).removeClass(tagger.selectedClass);
                            tagger.removeTag(txt);
                        }
                        else {
                            $(this).addClass(tagger.selectedClass);
                            tagger.appendTag(txt);
                        }
                    });
                });
            },
            value: function (v) {
                if (v != undefined && v != null) { this.input.val(v); }
                else return $.trim(this.input.val());
            },
            containTag: function (tag) {
                var txt = this.value();
                if ($.trim(txt) == "") return false;
                var tags = txt.split(" ");
                return $.inArray(tag, tags) >= 0;
            },

            appendTag: function (tag) {
                var txt = this.value();
                if (txt.length > 0 && txt.charAt(txt.length - 1) != ' ') {
                    txt += ' ';
                }
                this.value(txt + tag + ' ');
            },

            removeTag: function (tag) {
                var tags = this.value().split(' ');
                var result = '';
                $.each(tags, function (index, item) {
                    if (item != '' && item != tag) {
                        result += item + ' ';
                    }
                    return true;
                });

                return this.value(result);
            },

            validateTag: function (tag) {
                var tags = this.value().split(' ');
                var count = 0;
                $.each(tags, function (index, item) {
                    if (item != '') {
                        count++;
                    }
                });

                return count + this.existingTags.length <= 5;
            }

        }
    });
} (jQuery));