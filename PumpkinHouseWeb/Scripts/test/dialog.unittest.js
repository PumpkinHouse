﻿/// <reference path="/Scripts/lib/jquery-1.7.js" />
/// <reference path="/Scripts/dialog.js" />
/// <reference path="/Scripts/util.js" />
/// <reference path="/Scripts/gettmpl.js" />
/// <reference path="/Scripts/ajaxCall.js" />

//function ut_testfail(context) {
//    context.assertEqual(1, 2);
//}

function ut_createDialog_modal(context) {
    $.getTmplSync('/template/util.html');

    var text = 'UnitTestDialog';
    var target = $('<div id="ut-dialog-target"></div>');
    target.text(text);
    var dialog = target.mDialog();

    var expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length > 0, true, 'could find dialog');
    context.assertEqual($('#overlay').length > 0, true, 'could find overlay');
    context.assertEqual(dialog.css('display'), 'block', 'dialog is visible');
    context.assertEqual($('#overlay').css('display'), 'block', 'overlay is visible');

    // closeDialog(dialog);
    dialog.closeDialog();
    expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length > 0, true, 'could find dialog');
    context.assertEqual($('#overlay').length > 0, true, 'could find overlay');
    context.assertEqual(dialog.css('display'), 'none', 'dialog is invisible after closing');
    context.assertEqual($('#overlay').css('display'), 'none', 'overlay is invisible after closing');

    // showDialog(dialog);
    dialog.showDialog();
    expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length > 0, true, 'could find dialog');
    context.assertEqual($('#overlay').length > 0, true, 'could find overlay');
    context.assertEqual(dialog.css('display'), 'block', 'dialog is visible after reopening');
    context.assertEqual($('#overlay').css('display'), 'block', 'overlay is visible after reopening');

    // destroyDialog(dialog);
    dialog.destroyDialog();
    expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length == 0, true, 'could not find dialog after destorying');
    context.assertEqual($('#overlay').length == 0, true, 'could not find overlay after destorying');

}

function ut_createDialog_non_modal(context) {
    $.getTmplSync('/template/util.html');

    var text = 'UnitTestDialog';
    var target = $('<div id="ut-dialog-target"></div>');
    target.text(text);
    var dialog = target.mDialog({ modal: false });

    var expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length > 0, true, 'could find dialog');
    context.assertEqual($('#overlay').length == 0, true, 'could not find overlay under non-modal mode');
    context.assertEqual(dialog.css('display'), 'block', 'dialog is visible');

    dialog.closeDialog();
    expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length > 0, true, 'could find dialog');
    context.assertEqual($('#overlay').length == 0, true, 'could not find overlay under non-modal mode');
    context.assertEqual(dialog.css('display'), 'none', 'dialog is invisible after closing');

    dialog.showDialog();
    expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length > 0, true, 'could find dialog');
    context.assertEqual($('#overlay').length == 0, true, 'could not find overlay under non-modal mode');
    context.assertEqual(dialog.css('display'), 'block', 'dialog is visible after reopening');

    dialog.destroyDialog();
    expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length == 0, true, 'could not find dialog after destorying');
    context.assertEqual($('#overlay').length == 0, true, 'could not find overlay after destorying');
}

function ut_createDialog_not_open(context) {
    $.getTmplSync('/template/util.html');

    var text = 'UnitTestDialog';
    var target = $('<div id="ut-dialog-target"></div>');
    target.text(text);
    var dialog = target.mDialog({ autoOpen: false });

    var expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length > 0, true, 'could find dialog');
    context.assertEqual($('#overlay').length > 0, true, 'could find overlay if not opened by default');
    context.assertEqual(dialog.css('display'), 'none', 'dialog is invisible');
    context.assertEqual($('#overlay').css('display'), 'none', 'overlay is invisible if not opened by default');

    dialog.showDialog();
    expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length > 0, true, 'could find dialog after opening');
    context.assertEqual($('#overlay').length > 0, true, 'could find overlay after opening');
    context.assertEqual(dialog.css('display'), 'block', 'dialog is visible after opening');
    context.assertEqual($('#overlay').css('display'), 'block', 'overlay is visible after opening');

    dialog.closeDialog();
    expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length > 0, true, 'could find dialog after closing');
    context.assertEqual($('#overlay').length > 0, true, 'could find overlay after closing');
    context.assertEqual(dialog.css('display'), 'none', 'dialog is invisible after closing');
    context.assertEqual($('#overlay').css('display'), 'none', 'overlay is invisible after closing');

    dialog.showDialog() ;
    expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length > 0, true, 'could find dialog after reopening');
    context.assertEqual($('#overlay').length > 0, true, 'could find overlay after reopening');
    context.assertEqual(dialog.css('display'), 'block', 'dialog is visible after reopening');
    context.assertEqual($('#overlay').css('display'), 'block', 'overlay is visible after reopening');

    dialog.destroyDialog();
    expectedDialog = $('#ut-dialog-target');
    context.assertEqual(expectedDialog.length == 0, true, 'could not find dialog after destorying');
    context.assertEqual($('#overlay').length == 0, true, 'could not find overlay after destorying');
}
