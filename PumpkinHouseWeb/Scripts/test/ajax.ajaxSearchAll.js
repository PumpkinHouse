﻿/// <reference path="/Scripts/lib/jquery-1.7.js" />
/// <reference path="/Scripts/ajaxCall.js" />
// 用户搜索行为测试


function failTest(msg) {
    context.fail(msg);
}

function ut_testSearchContentYes(context) {
    var Keyword = "543";
    var result = ajaxGetAllPictures(false, Keyword, 0, false, 1, true, null, failTest);
    context.assertEqual(20, result.count, "结果数量与期望值不符合----------");
}

function ut_testSearchContentNo(context) {
    var Keyword = "xxxxxxxxxxssw";
    var result = ajaxGetAllPictures(false, Keyword, 0, false, 1, true, null, failTest);
    context.assertEqual(0, result.count, "已经有此内容存在----------");
}

function ut_testSearchAlbumsYes(context) {
    var Keyword = "543";
    var result = ajaxSearchAlbums(false, Keyword, 0, null, failTest);
    context.assertEqual(3, result.count, "盒子数量与期望值不符合----------");
}

function ut_testSearchAlbumsNo(context) {
    var Keyword = "xxxxxxxxxxssw";
    var result = ajaxSearchAlbums(false, Keyword, 0, null, failTest);
    context.assertEqual(0, result.count, "已经有此盒子存在----------");
}

function ut_testSearchUserYes(context) {
    var Keyword = "test1@test.com";
    var result = ajaxSearchUser(false, Keyword, 0, null, failTest);
    context.assertEqual(1, result.count, "用户结果与期望值不符合----------");
}

function ut_testSearchUserNo(context) {
    var Keyword = "xxxjjjxxx";
    var result = ajaxSearchUser(false, Keyword, 0, null, failTest);
    context.assertEqual(0, result.count, "已经有此用户存在----------");
}

