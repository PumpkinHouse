﻿/// <reference path="/Scripts/lib/jquery-1.7.js" />
/// <reference path="/Scripts/ajaxCall.js" />
// 用户个人描述


function failTest(msg) {
    context.fail(msg);
}

function ut_testUpdateUserDes(context) {
    var result = ajaxLogon(false, "test1@test.com", "222222", true, null, failTest);
    context.assertEqual(true, result, "logon with wrong password should fail");

    var newInfo = "最新描述";
    var result = ajaxUpdateUserIntro(false, newInfo, null, failTest);
    var resultNew = ajaxGetUserInfo(false, null, failTest);
    context.assertEqual(newInfo, resultNew.intro, "个人描述修改失败");


}


