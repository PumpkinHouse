﻿/// <reference path="/Scripts/lib/jquery-1.7.js" />
/// <reference path="/Scripts/ajaxCall.js" />

function failTest(msg) {
    context.fail(msg);
}

function ut_testLogon(context) {
    var result = ajaxLogon(false, "a.com", "a", true, null, failTest);
    context.assertEqual(result, false, "logon with wrong password should fail");
    result = ajaxLogon(false, 'test1@test.com', '111111', true, null, failTest);
    context.assertEqual(result, true, "logon with correct password should pass");
}

function ut_test(context) {
    var result = ajaxLogon(false, 'test1@test.com', '111111', true, null, failTest);
    context.assertEqual(result, true, "logon with correct password should pass");

    var albumName = "unittest album name";
    var description = "unittest description";
    var tags = "unittest tag";
    var result = createAlbumSync(new AlbumToCreate(albumName, description, tags), failTest);

    context.assertEqual(albumName, result.name, "returned album name should be correct");

    var albumCreated = getAlbumSync(result.id, 0, failTest);
    context.assertEqual(albumName, albumCreated.name, "created album name should be correct");
    context.assertEqual(description, albumCreated.description, "created album description should be correct");
 //   context.assertEqual(tags, albumCreated.tags, "created album name should be correct");

}