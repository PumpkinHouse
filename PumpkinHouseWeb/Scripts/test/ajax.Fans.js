﻿/// <reference path="/Scripts/lib/jquery-1.7.js" />
/// <reference path="/Scripts/ajaxCall.js" />
// 粉丝的相关行为


function failTest(msg) {
    context.fail(msg);
}

var UserEmail = "270689831@qq.com";   // 愤怒的小龙  l40835913492908
var UserName = "l40835913492908"; 
var UserPass = "111111";
var UserEmail2 = "test1@test.com";  //test1@test.com  l21345303886102
var UserName2 = "l21345303886102";
var UserPass2 = "222222"; 
  
//关注、取消粉丝
function ut_testUpdateFans(context) {
    var resultInfo = getUserSync(UserName, null);

    //UserName没有主动去关注UserName2
    if (resultInfo.relation == 0 || resultInfo.relation == 1) {
        alert(resultInfo.relation);

        var result = ajaxLogon(false, UserEmail, UserPass, true, null, failTest);
        context.assertEqual(true, result, "logon with wrong password should fail");

        var result = ajaxFanUser(false, UserName2, null, failTest);
        var resultNu = getUserSync(UserName,null);
        context.assertEqual(true, resultNu.relation == 2 || resultNu.relation == 3, "UserName关注UserName2失败");
    }

    //UserName主动去关注了UserName2
    if (resultInfo.relation == 2) {
        alert(resultInfo.relation);

        var result = ajaxLogon(false, UserEmail2, UserPass2, true, null, failTest);
        context.assertEqual(true, result, "logon with wrong password should fail");

        var result = ajaxFanUser(false, UserName, null, failTest);
        var resultNu = getUserSync(UserName, null);
        context.assertEqual(true, resultNu.relation == 3, "UserName2关注UserName失败");
    }

    //UserName和UserName2互相关注
    if (resultInfo.relation == 3) {
        alert(resultInfo.relation);

        var result = ajaxLogon(false, UserEmail, UserPass, true, null, failTest);
        context.assertEqual(true, result, "logon with wrong password should fail");
        var result = ajaxUnfanUser(false, UserName2, null, failTest);
        var resultNu = getUserSync(UserName, null);
        context.assertEqual(true, resultNu.relation == 1, "UserName取消关注UserName2失败");

        var result = ajaxLogon(false, UserEmail2, UserPass2, true, null, failTest);
        context.assertEqual(true, result, "logon with wrong password should fail");
        var result = ajaxUnfanUser(false, UserName, null, failTest);
        var resultNu = getUserSync(UserName2, null);
        context.assertEqual(true, resultNu.relation == 0, "UserName2取消关注UserName失败");
    }

}

