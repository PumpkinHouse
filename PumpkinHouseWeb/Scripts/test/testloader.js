﻿/// <reference path="lib/jquery-1.7.js" />
(function ($) {
    UnitTestSuite = function () {
        this.suite = [];
    }

    UnitTestContext = function (testName) {
        this.testName = testName;
        this.error = [];
    }

    $.extend(UnitTestSuite, {
        prototype: {
            startTest: function (output) {
                var results = [];
                output.empty();
                var pass = 0;
                var fail = 0;
                for (var obj in window) {
                    if (typeof (window[obj]) === "function" && obj.indexOf("ut_") == 0) {
                        var context = new UnitTestContext(obj);
                        results.push(context);
                        output.append('<div>Running Test ' + obj + '</div>');
                        try {
                            window[obj](context);
                            if (context.error.length === 0) {
                                output.append('<div class="pass">' + obj + ': Success</div>');
                                pass++;
                            }
                            else {
                                output.append('<div>' + obj + ': Fail</div>');
                                for (var i in result.error) {
                                    output.append('<div class="fail">' + context.error[i] + '</div>');
                                }
                                fail++;
                            }
                        }
                        catch (e) {
                            output.append('<div>' + obj + ': Fail</div>');
                            if (e.type === 'unittestError') {
                                output.append('<div class="fail">' + e.arguments[0] + '</div>');
                            }
                            else {
                                output.append('<div class="fail">Unhandled exception: ' + e.type + ' ' + e.arguments[0] + '</div>');
                            }
                            fail++;
                        }
                        output.append("<p/>");
                    }
                }
                this.results = context;
                output.append('<h3>Summary</h3><b><div>Total: ' + (pass + fail) + '</div><div class="pass">Pass: ' + pass + '</div><div class="fail">Fail: ' + fail + '</div></b>');
            },
            printResult: function () {
                var result;
                for (var index in this.results) {
                    var r = this.results[index];

                }
            }
        }
    });

    $.extend(UnitTestContext, {
        prototype: {
            assertEqual: function (expected, actual, message) {
                if (expected !== actual) {
                    throw { type: "unittestError", arguments: { 0: message + ": fail: expected result: " + expected + ", actual: " + actual} };
                }
            },
            fail: function (msg) {
                throw { type: "unittestError", arguments: { 0: msg} };                
            }
        }
    });

} (jQuery));


