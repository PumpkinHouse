﻿/// <reference path="/Scripts/lib/jquery-1.7.js" />
/// <reference path="/Scripts/ajaxCall.js" />
// 用户设置


function failTest(msg) {
    context.fail(msg);
}

var UserNameRight = "test1@test.com";
var UserPassRight = "222222";

//基本信息设置
function ut_testUpdateUserInfo(context) {
    var result = ajaxLogon(false, UserNameRight, UserPassRight, true, null, failTest);
    context.assertEqual(true, result, "logon with wrong password should fail");

    var sex = "1"; //Male
    var city = 189; //湖北，潜江
    var des = "最新描述";
    var info = new UserGeneralInfo(sex, city, des, UserNameRight);
    var result = ajaxUpdateUserInfo(true, info, null, failTest);

    var result = ajaxGetUserInfo(false, null, failTest);

    context.assertEqual("Male", result.gender, "性别修改失败");
    context.assertEqual(city, result.cityId, "城市ID修改失败");
    context.assertEqual(des, result.intro, "个人描述修改失败");    
}

//密码设置
function ut_testUpdatePassword(context) {
    var result = ajaxLogon(false, UserNameRight, UserPassRight, true, null, failTest);
    context.assertEqual(true, result, "logon with wrong password should fail");

    var newPassword = "222222";
    var oldPassword = UserPassRight;
    var result = ajaxUpdatePassword(false, oldPassword, newPassword, null, failTest);
    context.assertEqual(true, result, "fail");
}

//邮件订阅设置
function ut_testUpdateEmail(context) {
    var result = ajaxLogon(false, UserNameRight, UserPassRight, true, null, failTest);
    context.assertEqual(true, result, "logon with wrong password should fail");

    var newEmail = "1";
    var result = ajaxUpdateReceiveEmail(false, newEmail, null, failTest);
    var resultNew = ajaxGetUserInfo(false, null, failTest);
    context.assertEqual(true, resultNew.receiveEmail, "邮箱订阅设置失败");
}

