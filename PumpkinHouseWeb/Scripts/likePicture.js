﻿function updatePictureLike(event, callback) {
    var target = $(event.target);
    if (target[0].nodeName == "SPAN") { // 如果是瀑布流里面的<a><span>
        target = target.parents('a.likeBtn');
    }
    var pid = target.attr('data-picture-id');
    var status = target.hasClass('like');

    var func = status ? ajaxCancelLikePicture : ajaxLikePicture;

    func(true, pid, function (result) {
        target.toggleClass('like');
        if (callback) {
            callback(target, result);
        }
    }, handleFault);
}

function defaultLikeCallback (target, result) {
    if (result < 0) return;
    var text = "(" + result + ")";
    if (result == 0) {
        text = '';
    }
    target.find('.likeCount').text(text);
}