﻿function updateTags() {
    return updateAlbumTagSync(globalAlbumId, $.getTagPanel('#tagPanel').value(), function (errorCode) {
        handleSensitiveWord(errorCode, '抱歉，标签包含非法字符，请修改。');
    });
}

function deleteTag(tagToRemove) {
    return removeAlbumTagSync(globalAlbumId, tagToRemove, handleFault);
}

function renderAlbumInfo() {
    var infoPanel = $('#info');
    infoPanel.empty();
    var infoTemplate = $('#albumInfoTemplate').tmpl(globalAlbum, { isMe: globalIsMe });
    infoPanel.append(infoTemplate);

    $.tagPanel(true, '#tagPanel', globalAlbum.tags, 1, updateTags, deleteTag, globalLoggedIn, globalIsMe);

    $('.albumAction').on('click', function (event, displaySelector) {
        if (!globalLoggedIn) {
            showLogonDialog();
        }
        else {
            updateAlbumLike(event, '#numOfLike');
        }
    });


    $('#updateAlbumNameBtn').on('click', function () {
        updateAlbumBtnClick(globalAlbumId, globalAlbum.name, globalAlbum.description, function (name, desc) {
            $('#albumNameDisplay').text(name);
            $('#albumDesc').text(desc);
            globalAlbum.name = name;
            globalAlbum.description = desc;
        })
    });

    $('#deleteAlbumBtn').on('click', function () {
        var message = $('#deleteAlbumConfirmMessageTemplate').tmpl(null);
        var dialog = showSmallDialog('#confirmDialogTemplate', '.confirmDialog', message.html());
        $('.confirm', dialog).on('click', function () {
            var deleteAll = ($('input[name=toDelete]:checked').val() == "deleteAll");
            ajaxDeleteAlbum(true, globalAlbumId, deleteAll, function () {
                dialog.children().hide();
                document.location = '/dashboard';
            }, handleFault);
        });
    });
}

function updateAlbumNameCallback(newText, successCallback) {
    ajaxUpdateAlbumName(true, globalAlbumId, newText, successCallback, handleFault);
}

function updateAlbumDescCallback(newText, successCallback) {
    ajaxUpdateAlbumDesc(true, globalAlbumId, newText, successCallback, handleFault);
}

function renderGallery(pics) {
    var galleryPanel = $('#gallery');
    var galleryTemplate = $('#galleryTemplate').tmpl({ pictures: pics });
    galleryPanel.append(galleryTemplate);

    if (globalPageNumber == 0) {
        $('.PublicList').masonry({
            itemSelector: '.PublicList li',
            columnWidth: 240
        });
    }
    else {
        $('.PublicList').masonry('appended', galleryTemplate, true);
    }

    // bind collect event
    $('.collectButton', galleryTemplate).each(function (index, item) { $.collectButtion(item, globalLoggedIn, globalMyAlbums); });
    initCollectButton();

    //喜欢按钮
    galleryTemplate.find('.likeBtn').on('click', function (e) {
        updatePictureLike(e, defaultLikeCallback)
    });

    // bind reply event
    bindReplyEvent(galleryTemplate);
}

function renderUser() {
    globalUser = getUserSync(globalAlbum.username, handleFault);
    var userPanel = $('#userInfo');
    userPanel.empty();
    var userTemplate = $('#briefUserInfoTemplate').tmpl(globalUser, { isMe: globalIsMe });
    userPanel.append(userTemplate);

    if (globalIsMe) {
        // 点击换头像按钮
        $('.UserHeaderPic').on('mouseenter', function () {
            $('.UserHeaderPic .changePhotoBtn').show();
        }).on('mouseleave', function () {
            $('.UserHeaderPic .changePhotoBtn').hide();
        });
    }

    // bind send message button
    $.msgButton('#sendMsgButton', globalLogginUser);

    // render fan button
    $('.attBtn').fanButton(globalLoggedIn, $('#pInfo .fansCount'));

    // bind event on update intro button
    if (globalIsMe) {
        $('#selfIntro').on('click', null, { callback: updateUserCallback }, updateFiled);
    }
}

function updateUserCallback(newText, successCallback) {
    ajaxUpdateUserIntro(true, newText, successCallback, handleFault);
}

function renderReplies(paginationList) {
    var t = $('#replyDetailTemplate').tmpl(paginationList.list, { ownedByMe: globalIsMe });
    $('#replies').empty().append(t);

    initReplyReplyControl(replyReply, globalLoggedIn);
}

function postReply(text) {
    ajaxPostReply(true, new ReplyToPost(text, null, globalAlbumId), replySuccess, handleFault);
}

function replyReply(text, replyId) {
    var reply = new ReplyToPost(text, null, globalAlbumId, null, replyId);
    ajaxPostReply(true, reply, replySuccess, handleFault);
}

function replySuccess(newReply) {
    // update the reply list locally and re-render reply
    globalAlbum.replyCol.list.unshift(new ReplyToDisplay(newReply));
    globalAlbum.replyCol.count++;
    renderReplies(globalAlbum.replyCol);

    // update the reply count locally
    $('#numberOfReply').text(globalAlbum.replyCol.count);
    // jump to the first reply page
    $("#paginationHolder").trigger('setPage', [0]);
    // reset the textarea
    $('#reply').val('');
}

function initReplyPagination() {
    var holder = $('#replyHolder');
    holder.empty();
    var replyTemplate = $('#albumReplyTemplate').tmpl({ username: globalLogginUser, list: globalAlbum.replyCol.list, count: globalAlbum.replyCol.count });
    holder.append(replyTemplate);

    initReplyDialog(postReply, globalLoggedIn);

    pagination('#paginationHolder', globalAlbum.replyCol.count, 20, pageSelectCallback);
}

function pageSelectCallback(pageNumber, container) {
    ajaxGetAlbumReply(true, globalAlbumId, pageNumber, renderReplies, handleFault);
}

function renderAdv(result) {
    if (result) {
        renderTemplate('#advHolder', '#projectPicLinkTemplate', result);
    }
}

function doAction() {
    if (globalAlbumId == 0) {
        globalAlbum = getDefaultAlbumSync(globalUsername, 20, handleFault);
    }
    else {
        globalAlbum = getAlbumSync(globalAlbumId, -1, handleFault);
    }
    globalReplyCol = globalAlbum.replyCol;
    globalUsername = globalAlbum.username;
    globalIsMe = globalLogginUser ? (globalUsername == globalLogginUser) : false;

    renderAlbumInfo();
    renderGallery(globalAlbum.pictures.list);
    renderUser();
    initReplyPagination();
    renderReplies(globalAlbum.replyCol);

    //pre-loading
    ajaxGetCollectionsByAlbum(true, globalAlbumId, globalUsername, 1, function (pics) { cacheLoading(pics.list) }, handleFault);

    ajaxFindAdvertisement(true, renderAdv, handleFault);

    $('div.PublicViewCon ul').scrollLoad({
        ScrollAfterHeight: 90,
        getDataAsync: function (callback) {
            if (globalPageNumber < Math.floor((globalAlbum.pictures.count - 1) / 20)) {
                globalPageNumber++;
                globalLoadingIndicatorAtBottom = true;
                renderCache(renderGallery);
                ajaxGetCollectionsByAlbum(true, globalAlbumId, globalUsername, globalPageNumber, callback, handleFault);
            }
            else {
                callback();
            }
        },
        onload: function (pics) {
            if (pics) {
                cacheLoading(pics.list);
            }
        }
    });
}