﻿function bindReplyEvent(context) {
    // bind event on 评论
    $('a.replyInPageBtn', context).on('click', function (event) {
        if (globalLoggedIn) {
            var target = $(event.target);
            var li = target.parents('li.pic');
            var pid = li.attr('data-picture-id');
            $('.replyDialogInPage', li).toggle();
            $('.PublicList').masonry('reload');
        }
        else {
            showLogonDialog();
        }
    });

    $('.postReplyInPage', context).on('click', function (e) {
        var btn = $(e.target);
        var input = btn.prev('textarea.replyTextArea');
        var li = btn.parents('li.pic');
        var pid = li.attr('data-picture-id');
        var comment = $.trim(input.val());
        if (!comment) {
            blinkInput(input);
            return;
        }
        ajaxPostReply(true, new ReplyToPost(comment, pid), function (newReply) {
            var pos = $('.replyDialogInPage', li);
            var template = $('#shortReplyTemplate').tmpl(newReply);
            template.insertAfter(pos);
            var count = parseInt(li.attr('data-reply-count'));
            count++;
            li.find('.replyCount').text('(' + count + ')');
            li.attr('data-reply-count', count);
            li.find('.replyDialogInPage').hide();
            input.val('');
            $('.PublicList').masonry('reload');
        }, handleFault);
    });
}

function renderAlert(summary) {
    var div = renderTemplate('.noticeOpenList ul', '#noticeSummaryTemplate', { username: globalLogginUser, summary: summary });
    $('#reminderLink').text(' ' + summary.total);
    if (summary.total == 0) {
        $('#reminderLink').addClass('noticeNone');
        $('#reminderLink').removeClass('hasNotice');
    }
    else {
        $('#reminderLink').addClass('hasNotice');
        $('#reminderLink').removeClass('noticeNone');
    }
}

function getSummaryFailed(errorCode) {
    if (errorCode == 401) {
        window.clearInterval(globalIntervalId);
    }
}

function initAlert() {
    if (globalLoggedIn) {
        ajaxGetNoticeSummary(true, renderAlert, getSummaryFailed);
        globalIntervalId = setInterval(function () {
            ajaxGetNoticeSummary(true, renderAlert, getSummaryFailed);
        }, 300000);
    }
}

function initGoToTop() {
    var targetElement = $('');
    renderTemplate('#floatingMenuHolder', '#goToTopTemplate', null);
    var menu = $('#floatingMenuHolder');
    var top = $(window).height() - menu.height() - 20;
    menu.css('top', top + 'px');
    menu.css('_top', top - 115 + 'px');
    $(window).scroll(function () {
        menu.css('top', $(window).scrollTop() + top);
        if ($(window).scrollTop() > 600) {
            menu.children().show();
        }
        else {
            menu.children().hide();
        }
    });

    $('.float_top').on('click', function () {
        $('html,body').animate({ scrollTop: '0px' }, 800, 'swing');
    });

    $('.floatMenuHelp .sc').on('click', function () {
        addFavoriate(location.protocol + "//" + location.hostname, '木头盒子');
    });
}

function processLink(page, targetUrl) {
    var dom = $(page);
    var images = $('img', dom);
    var baseUrl = targetUrl;

    var baseTag = $.grep(dom, function (n, i) {
        return n.tagName == 'BASE';
    });
    
    if (baseTag && baseTag.length > 0) {
        baseUrl = $(baseTag[0]).attr('href');
        if (baseUrl && baseUrl.charAt(baseUrl.length - 1) != "/") {
            baseUrl += "/";
        }
    }

    images.each(function (index, item) {
        var im = $(item);
        var src = im.attr('src');
        if (src && src != '') {
            im.attr('src', processImageUrl(src, baseUrl));
        }
        else {
            return;
        }
    });
    var temp = $('<div id="temp"></div>').append(images);
    $('#hidden').append(temp);
    images = $('img', temp);
    var titleNode = $('title', dom);
    var title; 
    if (titleNode.length == 0) {
        // cannot find title node, might be IE8. Use regex
        var pattern = '<title[.]*>(.*)</title>';
        var reg = new RegExp(pattern, 'i');
        var match = page.match(reg);
        if (match.length > 1) {
            title = match[1];    
        }
    }
    else {
        title = $(titleNode).text();
    }

    var link = globalLink;

    var timeoutHandle = setTimeout(function () {
        postProcessLink(images, title, link);
    }, 10000);

    temp.imagesLoaded(function ($images, $proper, $broken) {
        clearTimeout(timeoutHandle);
        postProcessLink(images, title, link);
    });
}

function postProcessLink(images, title, link) {
    $('#hidden').show();

    var list = [];
    images.each(function (index, item) {
        var im = $(item);
        if (item.clientWidth > 150 && item.clientHeight > 150) {
            var alt = im.attr('alt');
            var src = im.attr('src');
            list.unshift({ alt: alt, src: src });
            resizeImage(im, 200, 200);
        }
    });
    $('#hidden').empty();
    if (list.length == 0) {
        $('#copyLinkTab').trigger('click');
        $('.DialogWebUrl span').text('对不起，此页面没有找到合适的图片').addClass('error');
        return;
    }
    renderTemplate('#uploadFileControlHolder', '#externalPageImageTemplate', { title: title, url: link, list: list });
    $('#uploadTemplateHolder #comment').val(title);

    $('li.linkImage img').each(function (index, item) {
        resizeImage($(item), 200, 200);
    });
    $('li.linkImage').on('click', function (eventObject) {
        var target = $(eventObject.target);
        if (target[0].tagName == "IMG") target = target.parent();
        $('img[src="' + globalChosenImageUrl + '"]').parent().removeClass('selectedImg');
        globalChosenImageUrl = target.find('img').attr('src');
        target.addClass('selectedImg');
        $('#uploadBtn').addClass('ready');
    });
}

function renderUploadingDialogBase() {
    var dialogBase = $.data(document, 'dialogBase');
    if (dialogBase) {
        if (dialogBase.attr('id') != 'holder') {
            dialogBase = $('#holder').empty().append(dialogBase.children());
            $.data(document, 'dialogBase', dialogBase);
        }
    }
    else {
        var dialogBase = renderTemplate('#holder', '#uploadDialogTemplate', { tags: globalTags, albums: globalMyAlbums, partyList: globalAssociateAccounts });
        $('#comment').watermark('好图配好文，描述让分享更有价值。');
        // add non-dialog-mode tag panel
        var tagger = new $.tagPanel(false, '#tagPanelDialog', [], 0);
        globalTagger = tagger;

        // bind event on album select div
        globalAlbumListControl = initAlbumListControl('#albumList', globalMyAlbums);

        $.data(document, 'dialogBase', dialogBase);
    }
}

function loadUploadingDialog() {
    if (!globalUploadDialog) {
        globalTags = getTagsAjaxSync(0, handleFault);

        globalUploadTemplateHolder = $('#uploadTemplateHolder');
        globalUploadTemplateHolder.empty();
        $.data(document, 'nameList', null);
        var dialogTemplate = $('#uploadTemplate').tmpl(null);
        globalUploadTemplateHolder.append(dialogTemplate);

        globalUploadDialog = globalUploadTemplateHolder.mDialog({ width: 960, height: 500, closeBtnClass: 'largeClose' });

        renderUploadingDialogBase();
        renderTemplate('#uploadFileControlHolder', '#albumNamefileTemplate', null);

        // bind events on tabs
        $('#collectToolTab').on('click', function () {
            renderTabHeader(0);
            var dialogBase = $.data(document, 'dialogBase');
            if (dialogBase) {
                var temp = $('<div style="display:none"></div>');
                temp.append(dialogBase.children());
                $.data(document, 'dialogBase', temp);
            }
            renderTemplate('#holder', '#collectToolTemplate', { host: globalHost, browser: $.browser });
            $('#uploadBtn').removeClass('ready');
        });

        initFileUploadControl();

        $('#copyLinkTab').on('click', function () {
            globalChosenImageUrl = null;
            $('.resultMessage', globalUploadDialog).text('');
            renderTabHeader(1);
            renderUploadingDialogBase();
            renderTemplate('#uploadFileControlHolder', '#albumNameDialogWebUrlTemplate', null);
            $('#uploadBtn').removeClass('ready');
            $('.DialogWebSure').on('click', function (event) {
                var target = $(event.target);
                var targetUrl = $.trim($('.WebUrl').val());
                if (!targetUrl) {
                    blinkInput('.WebUrl');
                    return;
                }
                globalLink = targetUrl;

                renderTemplate('#uploadFileControlHolder', '<div class=uploadFont>读取中...</div>', null);

                ajaxDownloadHtmlPage(true, targetUrl, function (page) {
                    processLink(page, targetUrl);
                }, function (msg) {
                    if (msg == 3) {
                        $('#copyLinkTab').trigger('click');
                        $('.DialogWebUrl span').text('网页不存在').addClass('error');
                        return;
                    }
                    showError(msg);
                });
            });
        });

        $('#uploadTab').on('click', function () {
            globalImageName = null;
            $('.resultMessage', globalUploadDialog).text('');
            renderTabHeader(2);
            renderUploadingDialogBase();
            renderTemplate('#uploadFileControlHolder', '#albumNamefileTemplate', null);
            initFileUploadControl();
        });

        renderTabHeader(2);
    }
    else {
        globalUploadDialog.showDialog();

        renderTabHeader(2);
        renderTemplate('#uploadFileControlHolder', '#albumNamefileTemplate', null);
        initFileUploadControl();
    }
}

function initFileUploadControl() {
    $('#fileUploadForm').fileupload({
        formData: { type: "image" }, /* 区分头像还是分享 */
        // Unwrapping HTML-encoded JSON required for IE's iframe transport.
        dataType: 'iframejson',
        converters: {
            'html iframejson': function (htmlEncodedJson) {
                return $.parseJSON($('<div/>').html(htmlEncodedJson).text());
            },
            'iframe iframejson': function (iframe) {
                return $.parseJSON(iframe.find('body').text());
            }
        },
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000
    }).
            bind('fileuploaddone', uploadSuccessful).
            bind('fileuploadstart', uploadStarted).
            bind('fileuploadfail', uploadFailed);
}

function addPictureSuccess() {
    //刷新标签列表
    var updatedTags = getTagsAjaxSync(0, handleFault);
    if (updatedTags) {
        renderTemplate('#commonTags', '#tagDisplayDetailsTemplate', updatedTags);
        globalTagger.tag('.tag');
    }

    //清除标签
    globalTagger.value('');
    //清除描述
    $('#comment', globalUploadDialog).val('');

    if (globalTabIndex == 2) {
        $('#uploadTab').trigger('click');
    }
    else if (globalTabIndex == 1) {
        $('#copyLinkTab').trigger('click');
    }

    $('#uploadBtn').removeClass('ready');

    $('.resultMessage', globalUploadDialog).text('分享成功');

    if (typeof (renderPicturePanel) != 'undefined') {
        renderPicturePanel();
    }
}

function addPicture() {

    if (globalTabIndex == 2 && !globalImageName) return;
    if (globalTabIndex == 1 && !globalChosenImageUrl) {
        showSmallDialog('#actionFailTemplate', '.failDialog', '你还没有选择任何图片哦！');
        return;
    };
    var albumId = globalAlbumListControl.albumId;
    if (!globalTagger.validateTag()) {
        globalTagger.validationFailHanlder();
        return;
    }

    var desc = $.trim($('#comment', globalUploadDialog).val());
    if (!desc) {
        blinkInput('#comment');
        return;
    }

    var syncList = $.map($('.syncChkbox:checked'), function (val, index) {
        return $(val).attr('data-party');
    });
    var pic = new PictureToUpload(globalImageName, globalChosenImageUrl, albumId, $('#tagInput', globalUploadDialog).val(), desc, $('#title').text(), $('#source').attr('href'), globalChosenImageUrl, syncList, globalTabIndex == 2 ? 1 : 2);
    $('.resultMessage', globalUploadDialog).text('正在分享到盒子，请稍候...');
    ajaxUploadPicture(true, pic, addPictureSuccess, function (errorCode) {
        if (errorCode == "2") {
            $('.resultMessage', globalUploadDialog).text('你已经分享过它了哦！');
        }
        else if (errorCode == "4") {
            $('.resultMessage', globalUploadDialog).text('对不起，暂不支持此文件格式');
        }
        else if (errorCode == "5") {
            $('.resultMessage', globalUploadDialog).text('文件太大啦！');
        }
        else if (errorCode == "6") {
            handleSensitiveWord(errorCode, '抱歉，标签包含非法字符，请修改后再分享。');
        }
        else if (errorCode == "7") {
            showSmallDialog('#actionFailTemplate', '.failDialog', '请等一下，稍后再试');
        }
        else {
            showError(errorCode);
        }
        $('.resultMessage').text('分享失败');
    });
}

function uploadSuccessful(e, data) {
    $('#uploadFileControlHolder').empty();
    var image = $('#imagePreviewTemplate').tmpl(null);
    image.appendTo('#uploadFileControlHolder');
    var preview = $('#preview');
    var result;
    if (typeof data.result == 'string') {
        result = $.parseJSON(data.result);
    }
    else {
        result = data.result;
    }
    globalImageName = result.name;
    $('#uploadBtn').addClass('ready');
    preview.attr('src', '/Images/GetImage.aspx?imageName=' + globalImageName);

    var img = preview;
    img.on('load', function () {
        resizeImage(img, 210, 100000);
    });
}

function resizeImage(img, maxH, maxW) {
    var h = img[0].clientHeight;
    var w = img[0].clientWidth;
    var nh = h;
    var nw = w;
    var ratio = w * 1.0 / h;
    if (h > maxH) {
        nh = maxH;
        nw = nh * ratio;
    }
    if (nw > maxW) {
        nw = maxW;
        nh = nw / ratio;
    }
    img.css('height', nh).css('width', nw);
}

function uploadStarted(e) {
    var temp = $('<div class=uploadFont></div>').text("上传中……");
    $('.resultMessage', globalUploadDialog).text('');
    $('#uploadFileControlHolder').append(temp);
    $('.FileUploadControl').hide();
}

function uploadFailed(e, data) {
    var msg = getErrorMessage(data.jqXHR, data.textStatus, data.errorThrown);
    var err;
    if (msg == 4) { // invalid image
        err = '不支持的图片格式'
    }
    else if (data.total > 5 * 1024 * 1024) { // image too large
        // 如果文件超出IIS web.config的大小，只能通过此方法判断
        err = '图片不能超过5MB';
    }
    else {
        err = '发生了未知错误，图片无法上传';
    }
    $('#uploadTab').trigger('click');
    $('#fileUploadControlMessage').text(err);
    initFileUploadControl();

}

function renderTabHeader(targetIndex) {
    globalTabIndex = targetIndex;
    var tabs = ['#collectToolTab', '#copyLinkTab', '#uploadTab'];
    var i = tabs.length - 1;
    for (; i >= 0; i--) {
        if (i == targetIndex) {
            $(tabs[i]).addClass('click');
        }
        else {
            $(tabs[i]).removeClass('click');
        }
    }
}

function startSearch(type) {
    var keyword = $.trim($('#search').val());
    document.location = '/search/' + type + "/" + keyword;
}

$(function () {
    //解决加载高度低于窗口时，偏移闪动问题
    var wHeight = $(window).height();
    $("body").css("height", wHeight + 1);



    $('#dropDownMenu').on('mouseenter', function (e) {
        //$('#dropDownMenu').addClass('aroundSelected');
        $('#keywordFilter').show(0);
        var t = $(".category:last");
        t.parents(".aroundPanel dl").css({ "border": "0px" });
    }).on('mouseleave', function (e) {
        //$('#dropDownMenu').removeClass('aroundSelected');
        $('#keywordFilter').hide(100);
    });

    $('.Menu-List-Ul .class-li').on('mouseenter', function (e) {
        $('.Menu-List-Ul .around-List-panel').show(0);
    }).on('mouseleave', function (e) {
        $('.Menu-List-Ul .around-List-panel').hide(0);
    });


    globalKeywordCategories = getKeywordsSync(handleFault);
    if (typeof (globalOrderMode) == 'undefined') {
        var globalOrderMode = 0;
    }
    if (typeof (globalCommodityOnly) == 'undefined') {
        var globalCommodityOnly = false;
    }
    var prefix = findLinkUrl(globalOrderMode, globalCommodityOnly, '');
    renderTemplate('#dropDowndList', '#filterTemplate', { categories: globalKeywordCategories, prefix: prefix });

    $('#dropDownMenu a.keyword').on('click', function (event) {
        if ($(this).hasClass('selectedKeyword')) {
            // remove the tag
            location.href = findLinkUrl(globalOrderMode, globalCommodityOnly, '');
        }
        else {
            var key = $(this).text();
            location.href = findLinkUrl(globalOrderMode, globalCommodityOnly, key);
        }
    });

    // 搜索框水印
    $('#search').watermark('寻找你喜欢的_');

    // my albums
    globalMyAlbums = $.parseJSON(jsonMyAlbums);

    if (typeof (doAction) !== "undefined") {
        doAction();
    }

    initGoToTop();

    initCreateAlbumDialog('#createAlbumLink');

    $('#search').on('keyup', function (event) {
        var target = $(event.target);
        var temp = $('#searchPanel');
        temp.empty().append($('#searchPanelTemplate').tmpl({ key: target.val() }));

        temp.position({
            of: target,
            my: 'center bottom',
            at: 'center top'
        });

        if (event.which == 13) {
            startSearch('picture');
        }
    });

    $('#fakePublishButton').on('click', loadUploadingDialog);

    $('.Login-List-li').on('mouseenter', function () {
        $('.login-List-panel').show();
        $('.login-List-Btn').css({ "color": "#f1f1f1" });
    }).on('mouseleave', function () {
        $('.login-List-panel').hide();
        $('.login-List-Btn').css({ "color": "#fff" });
    });

    //    $('.UserOn .dd3').on('mouseenter', function () {
    //        $('.aboutOpenList').show();
    //    }).on('mouseleave', function () {
    //        $('.aboutOpenList').hide();
    //    });

    $('.UserOn .dd2').on('mouseenter', function () {
        $('.userOpenList').show();
    }).on('mouseleave', function () {
        $('.userOpenList').hide();
    });

    $('.UserOn .dd4').on('mouseenter', function () {
        $('.noticeOpenList').show();
    }).on('mouseleave', function () {
        $('.noticeOpenList').hide();
    });

    // 调查对话框
    var survey = $('.survey');
    survey.find('textarea.TextArea').watermark('告诉我们怎么做的更好');
    $('.loveyn', survey).on('click', function (e) {
        $('.loveyn').removeClass('pressed');
        $(e.target).addClass('pressed');
    });

    $('input.WebSure', survey).on('click', function (e) {
        var text = $.trim(survey.find('textarea.TextArea').val());
        var like = survey.find('#likeBtn').hasClass('pressed');
        ajaxResponseSurvey(true, like, text, function () {
            survey.find('.Con').empty().append('<div>感谢你的参与</div>');
        }, handleFault);
    });

    //调查框动作
    $('.survey a.surBtnClose').hide();
    $('.survey a.surBtn').click(function (targetObject) {
        $(".survey").animate({ right: 0 }, 150);
        $('.survey a.surBtn').hide();
        $('.survey a.surBtnClose').show();
    });

    $('.survey a.surBtnClose').click(function (targetObject) {
        $(".survey").animate({ right: -235 }, 150);
        $('.survey a.surBtn').show();
        $('.survey a.surBtnClose').hide();
    });

    //    $('.survey').click(function (targetObject) {
    //        $(this).animate({ right: 0 }, 50);
    //    }, function (targetObject) {
    //        $(this).animate({ right: -235 }, 50);
    //    });



    initAlert();



    // 解决点击Jquery tabs的链接会跳到页卡的位置
    setTimeout(function () {
        if (location.hash) {
            window.scrollTo(0, 0);
        }
    }, 1);
});