﻿function updateAlbumLike(event, displaySelector) {
    var id = $.attr(event.target, 'data-album-id');
    var toUpdate ;
    if (typeof displaySelector === "string") {
        toUpdate = $(displaySelector);
    }
    else {
        toUpdate = displaySelector;
    }
    
    var target = $(event.target);
    if (target.hasClass('toLike')) { // not "Like" yet
        ajaxLikeAlbum(true, id, function () {
            target.removeClass('toLike');
            target.addClass('like');

            // add the like number
            if (toUpdate) {
                var v = toUpdate.text();
                toUpdate.text(1 + 1 * v); // multiply to avoid string concat
            }
        }, handleFault);
    }
    else { // already "Like"
        ajaxCancelLikeAlbum(true, id, function () {
            target.removeClass('like');
            target.addClass('toLike');
            // target.val('喜欢');
            // decrease the like number
            if (toUpdate) {
                var v = toUpdate.text();
                toUpdate.text(v - 1);
            }
        }, handleFault);
        
    }
}