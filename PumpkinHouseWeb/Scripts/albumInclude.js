﻿function updateAlbumBtnClick(albumId, albumName, albumDesc, updateCallback) {
    var dialogHolder = $('<div id="dialogHolderForUpdateAlbum"></div>');
    $(document.body).append(dialogHolder);
    var dialog = renderTemplate('#dialogHolderForUpdateAlbum', '#editAlbumDialogTemplate', { name: albumName, description: albumDesc });
    dialog = dialog.mDialog({ width: 420, height: 280 });

    $('#newAlbumName').on('keyup', function (eventObject) {
        var target = $(eventObject.target);
        var text = target.val();
        if (text && text.length > 12) {
            target.val(text.substring(0, 12));
        }
    });

    $('#editAlbumBtn').on('click', function () {
        var name = $.trim($('#newAlbumName').val());
        if (name == '') {
            blinkInput('#newAlbumName');
            return;
        }
        var desc = $.trim($('#newComment').val());

        ajaxUpdateAlbum(true, albumId, name, desc, function () {
            updateCallback(name, desc);
            dialog.destroyDialog();
            dialogHolder.remove();
        }, function (errorCode) {
            handleSensitiveWord(errorCode, '抱歉，专辑名包含非法字符，请修改后再分享。');
        });
    });
}