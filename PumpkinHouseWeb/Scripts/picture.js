﻿function updateTags() {
    return updateCollectionTagSync(globalPictureId, $.getTagPanel('#tagPanel').value(), function (errorCode) {
        handleSensitiveWord(errorCode, '抱歉，标签包含非法字符，请修改。');
    });
}

function deleteTag(tagToRemove) {
    return removeCollectionTagSync(globalPictureId, tagToRemove, handleFault);
}

function renderPicture() {
    globalReplyCol = globalPicture.replyCol;

    globalUsername = globalPicture.username;
    globalIsMe = globalLogginUser ? (globalUsername == globalLogginUser) : false;

    var picHolder = $('#pictureHolder');
    var picTemplate = $('#collectionTemplate').tmpl(globalPicture);
    picTemplate.appendTo(picHolder);

    var img = picHolder.find('.display');
    img.imagesLoaded(function ($images, $proper, $broken) {
        var target = picHolder.find('.ImgBox');
        target.css('height', img[0].clientHeight);
        target.css('width', img[0].clientWidth);
        target.on('mouseenter', function () {
            //target.find('.imgScreen').show();
        }).on('mouseleave', function () {
            //target.find('.imgScreen').hide();
        });

        $('.PicturePic .ImgBox').on('mouseenter', function () {
            $('.aImg').show();
        }).on('mouseleave', function() {
            $('.aImg').hide(); 
        });
    });

    var panelTemplate = $('#collectPanelTemplate2').tmpl(globalPicture, { isMe: globalIsMe });
    $('#collectPanel').empty().append(panelTemplate);

    // add the tag panel
    $.tagPanel(true, '#tagPanel', globalPicture.tags, 0, updateTags, deleteTag, globalLoggedIn, globalIsMe);

    // render the collect button
    $.collectButtion('.collectButton', globalLoggedIn, globalMyAlbums, null, function (result) {
        globalPicture.numberOfCol++;
        globalPicture.myColAlbumName = result.name;
        globalPicture.myColAlbumId = result.id;
        var panelTemplate = $('#collectPanelTemplate2').tmpl(globalPicture, { isMe: globalIsMe });
        $('#collectPanel').empty().append(panelTemplate);

        initCancelCollectionBtn();
        $('#moveColBtn').on('click', function () { globalMoveTarget = 'col'; showMoveDialog() });
        $('#movePicBtn').on('click', function () { globalMoveTarget = 'pic'; showMoveDialog() });
    }); 

    $('#editPictureDescBtn').on('click', function () {
        var template = renderTemplate('<div></div>', '#editPictureDescTemplate', { text: globalPicture.description });
        var dialog = template.mDialog({ width: 400, height: 200 });
        $('#updatePictureDescBtn', dialog).on('click', function () {
            var desc = $.trim($('#editPictureDescText', dialog).val());
            ajaxUpdatePictureDescription(true, globalPicture.id, desc, function (result) {
                $('#picDesc').text(result);
                globalPicture.description = result;
                dialog.closeDialog();
            }, handleFault);
        });
    });

    //喜欢按钮
    $('.like-Btn').on('click', function (e) {
        updatePictureLike(e, function (target, result) {
            $('.count', '#collectPanel').text(result);
        })
    });

    // bind event on cancel collect button
    initCancelCollectionBtn();

    // bind event on delete picture button
    $('#deletePicBtn').on('click', function () {
        if (confirm("确定删除此图片？（此操作不可恢复）")) {
            ajaxDeletePicture(true, globalPictureId, function () {
                document.location = '/dashboard';
            }, handleFault)
        }
    });

    initReplyPagination();
    renderReplies(globalPicture.replyCol);
}

function initCancelCollectionBtn() {
    $('#cancelColBtn').on('click', function () {
        showSmallDialog('#confirmDialogTemplate', '.confirmDialog', '确定要取消收纳？');
        $this = $(this);
        $('#confirmHolder .confirm').on('click', function () {
            ajaxCancelCollect(true, globalPicture.pictureId, $this.attr('data-album-id'), function () {
                document.location = '/dashboard';
            }, handleFault);
        });
    });
}

function updateDesc(newText, successCallback) {
    ajaxUpdatePictureDescription(true, globalPictureId, newText, successCallback, handleFault);
}

function initReplyPagination() {
    var holder = $('#repliesHolder');
    holder.empty();
    var template = $('#pictureReplyTemplate').tmpl({ username: globalLogginUser, list: globalPicture.replyCol.list, count: globalPicture.replyCol.count });
    holder.append(template);

    initReplyDialog(postReply, globalLoggedIn);

    pagination('#pagination', globalPicture.replyCol.count, 20, pageSelectCallback);
}

function pageSelectCallback(pageNumber, container) {
    ajaxGetPictureReply(true, globalPictureId, pageNumber, renderReplies, handleFault);
}


function renderReplies(paginationList) {
    var t = $('#replyDetailTemplate').tmpl(paginationList.list, { ownedByMe: globalIsMe });
    $('#replies').empty().append(t);

    initReplyReplyControl(replyReply, globalLoggedIn);
}

function postReply(text) {
    ajaxPostReply(true, new ReplyToPost(text, globalPictureId), replySuccess, handleFault);
}

function replyReply(text, replyId) {
    var reply = new ReplyToPost(text, globalPictureId, null, null, replyId);
    ajaxPostReply(true, reply, replySuccess, handleFault);
}

function replySuccess(newReply) {
    globalPicture.replyCol.list.unshift(new ReplyToDisplay(newReply));
    globalPicture.replyCol.count++;
    renderReplies(globalPicture.replyCol);
    $('#numberOfReply').text(globalPicture.replyCol.count);
    // jump to the first reply page
    $("#paginationHolder").trigger('setPage', [0]);
    // reset the textarea
    $('#reply').val('');
}

function renderUser() {

    var userInfoHolder = $('#userInfoHolder');
    userInfoHolder.empty();
    var userTemplate = $('#collectionOwnerTemplate').tmpl({ user: globalUser, picture: globalPicture }, { isMe: globalIsMe });
    userInfoHolder.append(userTemplate);

    if (globalIsMe) {
        // 点击换头像按钮
        $('.UserHeaderPic').on('mouseenter', function () {
            $('.UserHeaderPic .changePhotoBtn').show();
        }).on('mouseleave', function () {
            $('.UserHeaderPic .changePhotoBtn').hide();
        });
    }
    // bind send message button
    $.msgButton('#sendMsgButton', globalLogginUser);

    // render fan button
    $('.attBtn').fanButton(globalLoggedIn);

    // bind event to update user description
    if (globalIsMe) {
        $('#selfIntro').on('click', null, { callback: updateUserCallback }, updateFiled);
    }
}

function updateUserCallback(newText, successCallback) {
    ajaxUpdateUserIntro(true, newText, successCallback, handleFault);
}

function renderAlbums() {
    // render the album where the image is in
    var album;
    if (globalPicture.albumId) {
        album = getAlbumSync(globalPicture.albumId, 3, handleFault);
    }
    else {
        album = getDefaultAlbumSync(globalUsername, 3, handleFault);
    }
    var albumHolder = $('#myAlbumHolder');
    albumHolder.empty();
    var albumTemplate = $('#briefAlbumInfoTemplate').tmpl(album, { isMe: globalIsMe });
    albumHolder.append(albumTemplate);

    // render related albums
//    var otherAlbums = getAlbumsCollectingPicture(globalPictureId, 3, handleFault);
//    var otherAlbumHolder = $('#otherAlbumsHolder');
//    otherAlbumHolder.empty();
//    var otherAlbumTemplate = $('#briefAlbumInfoTemplate').tmpl(otherAlbums);
//    otherAlbumHolder.append(otherAlbumTemplate);
}

function renderFromAlbum() {
    if (globalPicture.originalAlbumId == 0) {
        ajaxGetDefaultAlbum(true, globalPicture.owner, 10, renderFromAlbumCallback, handleFault);
    }
    else {
        ajaxGetAlbum(true, globalPicture.originalAlbumId, 10, renderFromAlbumCallback, handleFault);
    }
}

function renderFromAlbumCallback(album) {
    renderTemplate('#originalAlbumHolder', '#originalAlbumTemplate', album);
}

function moveSuccss(newAlbum) {
    var holder = $('#albumLink');
    holder.empty();
    var template = $('#albumLinkTemplate').tmpl({ username: globalUsername, id: newAlbum.id, name: newAlbum.name });
    holder.append(template);
    $('#cancelColBtn').attr('data-album-id', newAlbum.id);
    $.data(document, 'albumDialog').closeDialog();
}

function showMoveDialog() {
    var dialog = $.data(document, 'albumDialog');
    if (!dialog) {
        var holder = $('<div/>');
        var template = $('#albumListSelectTemplate').tmpl({ albums: globalMyAlbums });
        holder.append(template);
        dialog = holder.mDialog({ width: 452, height: 252 });

        $.data(document, 'albumDialog', dialog);

        $('#btnCreateAlbum', dialog).on('click', function () {
            var name = $.trim($('#newAlbumName', dialog).val());
            if (name == '') {
                return;
            }
            var result = createAlbumSync(new AlbumToCreate(name, '', ''), function (errorCode) {
                handleSensitiveWord(errorCode, '抱歉，专辑名包含非法字符，请修改。');
            });
            if (result) {
                $('#newAlbumName', dialog).val('');
                renderTemplate('.favDailCon ul', '#albumItemTemplate', result, true);
                bindAlbumNameEvent(dialog);
            }
        });

        bindAlbumNameEvent(dialog);
    }
    else {
        dialog.showDialog();
    }
}

function bindAlbumNameEvent(dialog) {
    $('a', dialog).on('click', function () {
        var id = $(this).attr('data-album-id');
        ajaxCollectPicture(true, globalPictureId, id, moveSuccss, handleFault);
    });
}

function renderGuessLike(result) {
    if (result) {
        renderTemplate('#guessLikeHolder', '#guessLikeTemplate', result);
        $('.refresh-btn', '#myAlbumHolder').on('click', function (e) {
            var target = $(e.target);
            var cid = target.attr('data-id');
            ajaxGuessLike(true, globalPicture.id, cid, renderGuessLike, handleFault);
        });
    }
}

function renderAdv(result) {
    if (result) {
        renderTemplate('#advHolder', '#projectPicLinkTemplate', result);        
    }
}

function doAction() {
    var json = $.parseJSON(jsonUser);
    globalUser = json;
    var jsonP = $.parseJSON(jsonPicture);
    globalPicture = new CollectionToDisplay(jsonP); 
    
    renderUser();
    renderPicture();
    renderAlbums();
    renderFromAlbum();
    ajaxGuessLike(true, globalPicture.id, 0, renderGuessLike, handleFault);
    ajaxFindAdvertisement(true, renderAdv, handleFault);

    $('#moveColBtn').on('click', function () { globalMoveTarget = 'col'; showMoveDialog() });
    $('#movePicBtn').on('click', function () { globalMoveTarget = 'pic'; showMoveDialog() });

    var path = globalPicture.url + "." + globalPicture.ext;

    AddShareButtons('#ckepop', globalPicture.description, globalPicture.displayName, path, globalPicture.id, globalLogginUser === 'l68952085485829');
}