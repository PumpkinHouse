﻿function renderUser(userObject) {
    if (userObject) {
        globalUser = userObject;
    }

    globalInfoHolder.empty();
    var infoTemplate = $('#briefUserInfoTemplate').tmpl(globalUser, { isMe: globalIsMe });
    infoTemplate.appendTo(globalInfoHolder);

    if (globalIsMe) {
        // 点击换头像按钮
        $('.UserHeaderPic').on('mouseenter', function () {
            $('.UserHeaderPic .changePhotoBtn').show();
        }).on('mouseleave', function () {
            $('.UserHeaderPic .changePhotoBtn').hide();
        });
    }

    globalStatHolder.empty();

    // bind send message button
    $.msgButton('#sendMsgButton', globalLogginUser);

    // render fan button
    $('.attBtn').fanButton(globalLoggedIn, $('#pInfo .fansCount'));

    // bind event on update intro button
    if (globalIsMe) {
        $('#selfIntro').on('click', null, { callback: updateCallback }, updateFiled);
    }
}

var globalUser;
var globalInfoHolder;
var globalStatHolder;
var globalStuffHolder;
var globalIsMe;

function updateCallback(newText, successCallback) {
    ajaxUpdateUserIntro(true, newText, successCallback, handleFault);
}

function renderMyAlbums(albums) {
    var holder = $('#myAlbums');
    holder.empty();
    var template = $('#albumListTemplate').tmpl(albums.list, { isMe: globalIsMe });
    template.appendTo(holder);

    $('.albumAction').each(function (index, item) {
        var target = $(this);
        $(this).on('click', function (event) {
            if (!globalLoggedIn) {
                showLogonDialog();
            }
            else {
                var root = $(event.target).parents('.myLoveBoxPic');
                updateAlbumLike(event, root.find('#numOfLike'));
            }
        });
    });

    $('.editAlbumBtn').on('click', function (event) {
        var target = $(event.target);
        var albumId = target.attr('data-album-id');
        var nameHolder = $('.myLoveBoxPic[data-album-id=' + albumId + '] a.AlbumLinkName');
        var descHolder = $('.myLoveBoxPic[data-album-id=' + albumId + '] .BoxDescription');

        updateAlbumBtnClick(albumId, $.trim(nameHolder.text()), $.trim(descHolder.text()), function (name, desc) {
            nameHolder.text(name);
            descHolder.text(desc);
        });
    });
                                bindReplyEvent(galleryTemplate);

    $('.deleteAlbumBtn').on('click', function (event) {
        var target = $(event.target);
        var albumId = target.attr('data-album-id');
        var message = $('#deleteAlbumConfirmMessageTemplate').tmpl(null);
        var dialog = showSmallDialog('#confirmDialogTemplate', '.confirmDialog', message.html());

        $('.confirm', dialog).on('click', function () {
            var deleteAll = ($('input[name=toDelete]:checked').val() == "deleteAll");
            ajaxDeleteAlbum(true, albumId, deleteAll, function () {
                var holder = $('.myLoveBoxPic[data-album-id=' + albumId + ']');
                holder.remove();
                $('.confirmDialog', dialog).hide();
                var d = showSmallDialog('#actionCompleteTemplate', '.successDialog', '删除成功');
                setTimeout(function () { d.children().hide(); }, 1000);
            }, handleFault);
        });
    });
}

function initMyAlbumsPagination(albums) {
    var num_entries = albums.count;
    pagination('#paginationHolder', num_entries, 20, function (pageNumber, container) { ajaxGetAlbumsOfUser(true, globalUsername, pageNumber, true, 10, renderMyAlbums, handleFault); });
    renderMyAlbums(albums);
}

function initAlbumILikePagination(albums) {
    var num_entries = albums.count;
    pagination('#paginationHolder', num_entries, 20, function (pageNumber, container) { ajaxGetAlbumsLikedBy(true, globalUsername, pageNumber, renderAlbumsILike, handleFault); });

    renderAlbumsILike(albums);
}

function renderAlbumsILike(albums) {
    var holder = $('#albumsILike');
    holder.empty();
    var template = $('#albumListTemplate').tmpl(albums.list);
    holder.append(template);
    $('.albumAction').each(function (index, item) {
        $(this).on('click', function (event, displaySelector) {
            if (!globalLoggedIn) {
                showLogonDialog();
            }
            else {
                updateAlbumLike(event, displaySelector);
            }
        });
    });
}

function initPicturePagination(pictures) {
    var num_entries = pictures.count;
    pagination('#paginationHolder', num_entries, 20, function (pageNumber, container) { ajaxGetAllCollectionsOfUser(true, globalUsername, pageNumber, renderPictures, handleFault); });

    renderPictures(pictures);
}

function renderPictures(pics) {
    globalCount = pics.count;
    var holder = $('#myCollections');

    if (globalUserCollectionPageNumber == 0) {
        holder.empty();
    }
    var template = $('#galleryTemplate').tmpl({ pictures: pics.list });
    holder.append(template);

    initCollectButton();

    // bind event on collect buttons
    $('.collectButton', template).each(function (index, item) {
        var li = $(item).parents('li.pic');
        var displayTarget = li.find('a span.collectCount');
        $.collectButtion(item, globalLoggedIn, globalMyAlbums, displayTarget);
    });

    //喜欢按钮
    template.find('.likeBtn').on('click', function (e) {
        updatePictureLike(e, defaultLikeCallback);
    });

    bindReplyEvent(template);

    if (globalUserCollectionPageNumber == 0) {
        holder.masonry({
            itemSelector: '#myCollections li',
            columnWidth: 240
        });
    }
    else {
        holder.masonry('appended', template, true);        
    }
    globalLock = 0;
}
