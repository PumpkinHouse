﻿/// <reference path="lib/jquery-1.7.js" />
/// <reference path="lib/jquery.validate.js" />
/// <reference path="util.js" />
/// <reference path="data.js" />

function configureRegistrationValidation() {

    jQuery.validator.addMethod("password", function (value, element) {
        var result = this.optional(element) || /^[\da-zA-Z]{6,16}$/.test(value);
        return result;
    }, "密码必须是6-16位数字或字母");

    $('form#registerForm').validate({
        rules: {
            nickname: {
                maxlength: 14
            }
        },
        messages: {
            email: {
                required: '请输入信箱地址',
                email: '请输入正确的地址',
                remote: '这个邮箱已经被别人使用啦！'
            },
            password: {
                required: '密码不能为空'
            },
            nickname: {
                required: '昵称不能为空',
                remote: '这个昵称已经被别人使用啦！',
                maxlength: '昵称不能超过14字符/汉字'
            }
        },
        debug: true,
        submitHandler: function () {
            // submit register request
            var email = $.trim($('#email').val());
            var nickname = $.trim($('#nickname').val());
            var password = $('#password').val();

            var user = new UserForRegistration(email, nickname, password);
            var captcha = $('#captcha').val();

            var request = {
                user: user,
                captcha: captcha
            };
            var url = "/PublicService/AccountService.svc/ajax/Register";
            ajaxPost(true, request, url,
                function () { document.location = "/guide/step1.aspx"; },
                handleFault);
        }
    })
}


