﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Explain.aspx.cs" Inherits="PumpkinHouse.Aboutus.Explain" MasterPageFile="~/Site.master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.js"></script> 
<script type="text/javascript">
    $(function () {
        $(".tabStepMenu1").click(function () {
            $(".tabStepMenu1 a").addClass("liSelected");
            $(".tabStepMenu2 a").removeClass("liSelected");
            $(".ConBox1").css("display", "block");
            $(".ConBox2").css("display", "none");
        });
        $(".tabStepMenu2").click(function () {
            $(".tabStepMenu1 a").removeClass("liSelected");
            $(".tabStepMenu2 a").addClass("liSelected");
            $(".ConBox1").css("display", "none");
            $(".ConBox2").css("display", "block");
        });

    });
</script>
<style type="text/css">
.floatMenuHelp{ display:none;}
.ConBox1,.ConBox2,.ConBox3,.ConBox4,.ConBox5{ width:690px;}
</style>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <script>
        function doAction() { }



        $(function () {
            $(".chrome-browser").hide();
            $(".ie-browser").hide();
            $(".firefox-browser").hide();

            if ($.browser.msie) {
                //alert("这是一个IE浏览器");
                $(".ie-browser").show();

            } else if ($.browser.opera) {
                //alert("这是一个opera浏览器");
            } else if ($.browser.mozilla) {
                $(".firefox-browser").show();
                //alert("这是一个火狐浏览器");
            } else if ($.browser.safari) {
                $(".chrome-browser").show();
                //alert("这是一safari浏览器");
            } else {
                // alert("我不知道");
                $(".ie-browser").show();
            }
        })

</script> 
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
 <div class="AboutBox">
    <div class="AboutLeft float_l">
        <ul>
            <li><a href="AboutUs.aspx">关于木头盒子</a></li>
            <li><a href="Community.aspx">社区指南</a></li>
            <li><a href="Explain.aspx" class="selected">收纳工具使用说明</a></li>
            <li style="display:none;"><a href="Friends.aspx">木头盒子的朋友们</a></li>
            <li><a href="Faq.aspx">FAQ</a></li>
            <li><a href="Opinion.aspx">意见反馈</a></li>
        </ul>
    </div>
     <div class="AboutRight float_r">
        <div class="About-Title">收纳工具使用说明</div>
        <div class="Con">
            
            <div class="tabStepMenu clearfix" style="margin-top:10px; width:690px; height:40px;">
    	        <ul>
        	        <li class="tabStepMenu1"><a class="liSelected">如何使用</a></li>
                    <li class="tabStepMenu2"><a>找不到书签栏</a></li>
                </ul>
            </div>
            <div class="ConBox1">
                <a class="chrome-browser" title="盒子工具" href='javascript:void((function(){var d=document,e=d.createElement("script");e.setAttribute("charset","UTF-8");e.setAttribute("src","http://www.mutouhezi.com/scripts/collectTool.js?"+Math.floor(new Date/1E7));d.body.appendChild(e)})());'>
                    <img src="/img/AboutUs/collect-chrome.png"  alt="盒子工具"/>
                </a>

                <a class="ie-browser" title="盒子工具" href='javascript:void((function(){var d=document,e=d.createElement("script");e.setAttribute("charset","UTF-8");e.setAttribute("src","http://www.mutouhezi.com/scripts/collectTool.js?"+Math.floor(new Date/1E7));d.body.appendChild(e)})());'>
                    <img src="/img/AboutUs/collect-ie.png"  alt="盒子工具"/>
                </a>

                <a class="firefox-browser" title="盒子工具" href='javascript:void((function(){var d=document,e=d.createElement("script");e.setAttribute("charset","UTF-8");e.setAttribute("src","http://www.mutouhezi.com/scripts/collectTool.js?"+Math.floor(new Date/1E7));d.body.appendChild(e)})());'>
                    <img src="/img/AboutUs/collect-firefox.png"  alt="盒子工具"/>
                </a>


            </div>
            <div class="ConBox2" style="display:none;">
                <img src="/img/regStepPic/step2_menu2_1.jpg" />
                <img src="/img/regStepPic/step2_menu2_2.jpg" />
                <img src="/img/regStepPic/step2_menu2_3.jpg" />
                <img src="/img/regStepPic/step2_menu2_4.jpg" />
                <img src="/img/regStepPic/step2_menu2_5.jpg" />
                <img src="/img/regStepPic/step2_menu2_6.jpg" />
            </div>


        </div>
    </div>
 </div> 
</asp:Content>

