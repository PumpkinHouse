﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Faq.aspx.cs" Inherits="PumpkinHouse.Aboutus.Faq" MasterPageFile="~/Site.master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<style type="text/css">
.floatMenuHelp{ display:none;}
</style>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <script>
        function doAction() { }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
 <div class="AboutBox">
    <div class="AboutLeft float_l">
        <ul>
            <li><a href="AboutUs.aspx">关于木头盒子</a></li>
            <li><a href="Community.aspx">社区指南</a></li>
            <li><a href="Explain.aspx">收纳工具使用说明</a></li>
            <li style="display:none;"><a href="Friends.aspx">木头盒子的朋友们</a></li>
            <li><a href="Faq.aspx" class="selected">FAQ</a></li>
            <li><a href="Opinion.aspx">意见反馈</a></li>
        </ul>
    </div>
    <div class="AboutRight float_r">
        <div class="About-Title">FAQ</div>
        <div class="Con faq">
            

<table width="600" border="0" cellspacing="15" cellpadding="0" style="text-align:left;">
  <tr>
    <td width="26" valign="top"><span class="listNum">①</span></td>
    <td width="529">
    <h1>如何在木头盒子上找到你想要看到的家居类美图？</h1><br />
    来到木头盒子想要找到自己喜爱的事物，是可以通过两种方法来实现的。<br />
	方法一：点击【逛逛】按钮，查看木头盒子……”<br /><br />
    <img src="/img/AboutUs/FaqPic-Num1-1.png" /><br /><br />
    方法二：在木头盒子的搜索框里面输入你想要找寻的物品，木头盒子给你准备了精致的内容、专辑，让你快速找到你想要的美丽世界。<br /><br />
    <img src="/img/AboutUs/FaqPic-Num1-2.png" /><br /><br />
    </td>
  </tr>
  <tr>
    <td valign="top"><span class="listNum">②</span></td>
    <td>
    	<h1>如何创建盒子，将发现的美图收纳到你的木头盒子里？</h1><br />       	如果你在木头盒子发现了自己喜爱的美图，你可以把它收纳在自己创建的木头盒子里，这时候你先要去创建你的盒子。点开【分享】下拉菜单，点击【创建盒子】，然后就可以了，记得给自己的小盒子想一个好听的名字哦！<br /><br />
        <img src="/img/AboutUs/FaqPic-Num2-1.png" /><br /><br />
        接下来，你只需将鼠标移动至图片上，图片上方会浮现出【收纳】按钮，点击收纳然后选取你想要放的专辑，它就会被你收纳在你自己的木头盒子里了。如图：<br /><br />
        <img src="/img/AboutUs/FaqPic-Num2-2.png" /><br /><br />
    </td>
  </tr>
  <tr>
    <td valign="top"><span class="listNum">③</span></td>
    <td>
    <h1>在别的网站上找到美好的精品如何分享到木头盒子上？</h1><br />
    如果你在外部的网站上也发现了美好的物品，想分享给木头盒子的朋友们你也可以有2种方法来实现：<br />
方法一：建议你安装一下【木头盒子的收纳工具】很简单哦，方法详见。<br />
这样就可以随时随地的将你发现的美图分享到木头盒子上，只需轻轻一点【木头盒子的收纳工具】，就万事OK啦！<br /><br />
<img src="/img/AboutUs/FaqPic-Num3-1.png" /><br /><br />
方法二：你也可以在登陆木头盒子的时候，点击【分享】下拉菜单，选取【粘贴网址抓取】，拷贝完整的网址，分配好专辑，加好标签。这样也可以完美收纳到你的木头盒子里。或者直接下载到电脑里，直接本地上传到自己的盒子里面。<br /><br />
<img src="/img/AboutUs/FaqPic-Num3-2.png" /><br /><br />

    </td>
  </tr>
  <tr>
    <td valign="top"><span class="listNum">④</span></td>
    <td>
    <h1>如何在木头盒子上购买我喜欢的一些物品？</h1><br />
    首先你想要在木头盒子购买一些自己喜欢的物品的话，你可以先点击分类上方的【只看商品】按钮，这样木头盒子会自动帮你筛选出一些可以买得到的商品。<br /><br />
<img src="/img/AboutUs/FaqPic-Num4-1.png" /><br /><br /> 
    然后点开你想要买的商品，在标签下方会有一个【相关连接】。在那里面你可以找到购买地址，如果有你非常喜欢的物品却没有购买链接的话，你可以在下方的评论栏里面留言，善良的木头盒子朋友们会去帮你哦！<br /><br />
<img src="/img/AboutUs/FaqPic-Num4-2.png" /><br /><br />
    


    </td>
  </tr>
  <tr>
    <td valign="top"><span class="listNum">⑤</span></td>
    <td>
    <h1>如何让自己在木头盒子变得越来越受大家欢迎？</h1><br />
想要玩转木头盒子，在木头盒子里变成一个“小明星”的话还是有一些小窍门的，记住啦，一般人我们不会告诉的。<br />
1、发布的图片内容靓丽精彩，清晰吸引人，这样容易快速聚拢人气，被大家关注。<br />
2、对图片的描述生动有趣，同时添加了准确清楚的图片标签。这样会在第一时间被志同道合的朋友们看到。<br />
3、主动去关注其余木头盒子的创建者，多与他们留言沟通，互相收纳对方精彩的图片，这样会很快和对方成为朋友。<br />

    </td>
  </tr>
</table>


        </div>
    </div>
 </div> 
</asp:Content>

