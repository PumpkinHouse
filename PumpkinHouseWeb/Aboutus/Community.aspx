﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Community.aspx.cs" Inherits="PumpkinHouse.Aboutus.Community" MasterPageFile="~/Site.master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.js"></script> 
<script type="text/javascript">
    $(function () {
//        $('.tabStepMenu ul li a').click(function (targetObject) {
//            var t = $(targetObject.target).parent();
//            t.find('.tabStepMenu ul li a').addClass("liSelected");
//        });

        $(".tabStepMenu1").click(function () {
            $(".tabStepMenu1 a").addClass("liSelected");
            $(".tabStepMenu2 a").removeClass("liSelected");
            $(".tabStepMenu3 a").removeClass("liSelected");
            $(".tabStepMenu4 a").removeClass("liSelected");
            $(".tabStepMenu5 a").removeClass("liSelected");
            $(".ConBox1").css("display", "block");
            $(".ConBox2").css("display", "none");
            $(".ConBox3").css("display", "none");
            $(".ConBox4").css("display", "none");
            $(".ConBox5").css("display", "none");
        });
        $(".tabStepMenu2").click(function () {
            $(".tabStepMenu1 a").removeClass("liSelected");
            $(".tabStepMenu2 a").addClass("liSelected");
            $(".tabStepMenu3 a").removeClass("liSelected");
            $(".tabStepMenu4 a").removeClass("liSelected");
            $(".tabStepMenu5 a").removeClass("liSelected");
            $(".ConBox1").css("display", "none");
            $(".ConBox2").css("display", "block");
            $(".ConBox3").css("display", "none");
            $(".ConBox4").css("display", "none");
            $(".ConBox5").css("display", "none");
        });
        $(".tabStepMenu3").click(function () {
            $(".tabStepMenu1 a").removeClass("liSelected");
            $(".tabStepMenu2 a").removeClass("liSelected");
            $(".tabStepMenu3 a").addClass("liSelected");
            $(".tabStepMenu4 a").removeClass("liSelected");
            $(".tabStepMenu5 a").removeClass("liSelected");
            $(".ConBox1").css("display", "none");
            $(".ConBox2").css("display", "none");
            $(".ConBox3").css("display", "block");
            $(".ConBox4").css("display", "none");
            $(".ConBox5").css("display", "none");
        });
        $(".tabStepMenu4").click(function () {
            $(".tabStepMenu1 a").removeClass("liSelected");
            $(".tabStepMenu2 a").removeClass("liSelected");
            $(".tabStepMenu3 a").removeClass("liSelected");
            $(".tabStepMenu4 a").addClass("liSelected");
            $(".tabStepMenu5 a").removeClass("liSelected");
            $(".ConBox1").css("display", "none");
            $(".ConBox2").css("display", "none");
            $(".ConBox3").css("display", "none");
            $(".ConBox4").css("display", "block");
            $(".ConBox5").css("display", "none");
        });
        $(".tabStepMenu5").click(function () {
            $(".tabStepMenu1 a").removeClass("liSelected");
            $(".tabStepMenu2 a").removeClass("liSelected");
            $(".tabStepMenu3 a").removeClass("liSelected");
            $(".tabStepMenu4 a").removeClass("liSelected");
            $(".tabStepMenu5 a").addClass("liSelected");
            $(".ConBox1").css("display", "none");
            $(".ConBox2").css("display", "none");
            $(".ConBox3").css("display", "none");
            $(".ConBox4").css("display", "none");
            $(".ConBox5").css("display", "block");
        });
    });
</script>
<style type="text/css">
.floatMenuHelp{ display:none;}

.ConBox1,.ConBox2,.ConBox3,.ConBox4,.ConBox5{ width:690px;}
</style>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <script>
        function doAction() { }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
 <div class="AboutBox">
    <div class="AboutLeft float_l">
        <ul>
            <li><a href="AboutUs.aspx">关于木头盒子</a></li>
            <li><a href="Community.aspx" class="selected">社区指南</a></li>
            <li><a href="Explain.aspx">收纳工具使用说明</a></li>
            <li style="display:none;"><a href="Friends.aspx">木头盒子的朋友们</a></li>
            <li><a href="Faq.aspx">FAQ</a></li>
            <li><a href="Opinion.aspx">意见反馈</a></li>
        </ul>
    </div>
   <div class="AboutRight float_r">
        <div class="About-Title">社区指南</div>
        <div class="Con">

           <div class="tabStepMenu clearfix" style="margin-top:10px; width:690px; height:40px;">
    	        <ul>
        	        <li class="tabStepMenu1"><a class="liSelected">指导原则</a></li>
                    <li class="tabStepMenu2"><a>管理细则</a></li>
                    <li class="tabStepMenu3"><a>图片政策</a></li>
                    <li class="tabStepMenu4"><a>隐私声明</a></li>
                    <li class="tabStepMenu5"><a>免责声明</a></li>
                </ul>
            </div>
            <div class="ConBox1">
                <div class="testCon">
<br />
<h1>木头盒子欢迎</h1>

1. 发现美，分享美。<br />
2. 提倡个性化空间，宽容对待每一个人的喜好和意见。<br />
3. 尊重他人的隐私和个人空间。<br /><br />


<h1>木头盒子不欢迎</h1>

1. 恶意的进行语言攻击，挑起争端。<br />
2. 有关色情、激进政治、赌博等等不健康内容的话题。<br />
3. 针对种族、国家、民族、宗教、性别、年龄、地缘、性取向、生理特征的歧视和仇恨言论。<br />
4. 频繁不理智的恶劣形式的刷屏或者广告。<br /><br />


<h1>木头盒子禁止</h1>

1. 违反中国或木头盒子成员所在地区的法律法规和以及相关政策内容。<br />
2. 违反道德规范，破坏社会和谐，以及严重偏离正常世界观的事情。<br />
3. 针对网站运营有损害，或者有潜在威胁的行为。<br />
4. 威胁他人或者木头盒子成员的人身安全、法律安全的行为。<br /><br />

                </div>
            </div>
            <div class="ConBox2" style="display:none;">
                <div class="testCon">
                <br />
<h1>使用木头盒子你要懂得</h1>

1. 用户的分享、发布、评论等等内容要符合木头盒子的社区指导原则。<br />
2. 如果用户发布的内容违反了社区指导原则或者相关法律法规，木头盒子管理员有权删除或者做出相关处理。<br />
3. 如果帐号发布了不和谐的内容，将会遭到帐号冻结的惩罚。用户可以通过冻结邮件申请解除冻结，恢复账号。<br />
4. 有下列情况，如果情节严重，将会被永久停用：群发广告，恶意刷屏，使用恶意程序操作，冒充其他用户，帐号滥用以及对木头盒子造成一定的恶劣影响。<br />
5. 如果遇到特殊行为或者政府相关机关的要求时，木头盒子保留直接停用或者注销账号的权利。<br /><br />
                   </div> 
            </div>
            <div class="ConBox3" style="display:none;">
                <div class="testCon">
                <br />
<h1>在木头盒子分享图片时你要懂得</h1>
1. 请不要上传涉及他人隐私、淫秽色情、暴力血腥、违反国家法律法规及可能对网站运营带来潜在威胁的照片或图片。<br />
2. 用户要对所上传的照片或者图片的版权负责，木头盒子不承担因此带来的任何第三方的责任以及法律风险，任何侵权行为与木头盒子无关。<br />
3. 违反使用规则的内容以及根据用户居室投诉，木头盒子可能删除你发布的内容，并保留对用户帐户的处理权利。<br /><br />
                </div>
            </div>
            <div class="ConBox4" style="display:none;">
                <div class="testCon">
　　<p>木头盒子严格保护用户个人信息的安全。我们使用各种安全技术和程序来保护用户的个人信息不被未经授权的访问、使用或泄漏。</p>

　　<p>如果满足以下特殊条件，木头盒子会在法律要求或者符合木头盒子的相关服务条款、软件许可使用协议约定的情况下透露您的个人信息：1、足法律或行政法规的明文规定，相关政法机构的合理要求下。2、 保护木头盒子的权利或财产，以及紧急情况下保护木头盒子员工、木头盒子产品服务的用户和大众个人安全的情况下。</p>

　　<p>本隐私声明适用于木头盒子的所有相关服务，随着木头盒子的服务范围扩大，隐私声明的内容可由木头盒子随时更新，且无须另行通知。更新后的隐私声明一旦在网页上公布即有效代替原来的隐私声明。</p>

                </div>
            </div>
            <div class="ConBox5" style="display:none;">
                <div class="testCon">

　　<p>一旦用户注册成功即表示与木头盒子达成协议，完全接受本服务条款项下的全部条款。对免责声明的解释、修改及更新权均属于木头盒子所有。</p>

　　<p>1. 由于用户将用户密码告知他人或与他人共享注册帐户，由此导致的任何个人信息的泄漏，或其他非因木头盒子原因导致的个人信息的泄漏，木头盒子不承担任何法律责任，如果对木头盒子造成损害，木头盒子有权追究其相关责任。</p>

　　<p>2. 任何第三方根据木头盒子各服务条款及声明中所列明的情况使用木头盒子的个人信息，由此所产生的纠纷，木头盒子不承担任何法律责任。</p>

　　<p>3. 任何由于黑客攻击、电脑病毒侵入或政府管制而造成的暂时性网站关闭，木头盒子不承担任何法律责任。</p>

　　<p>4. 我们鼓励用户充分利用木头盒子平台自由地张贴和共享自己的信息。用户可以自由张贴图片等内容，但这些内容必须位于公共领域内或者用户拥有这些内容的使用权。同时，用户不应在自己的个人主页或社区中张贴其他受版权保护的内容。</p>

　　<p>5. 用户在木头盒子发布侵犯他人知识产权或其他合法权益的内容，木头盒子有权予以删除，并保留移交司法机关处理的权利。</p>

　　<p>6. 用户对于其创作并在木头盒子上发布的合法内容依法享有著作权及其相关权利。</p>

　　<p>7. 互联网是一个开放平台，用户将图片等资料上传到互联网上，有可能会被其他组织或个人复制、转载、擅改或做其它非法用途，用户必须充分意识此类风险的存在。用户明确同意其使用木头盒子服务所存在的风险将完全由其自己承担；因其使用木头盒子服务而产生的一切后果也由其自己承担，木头盒子对用户不承担任何责任。</p>
                </div>
            </div>

        </div>
    </div>
 </div> 
</asp:Content>
