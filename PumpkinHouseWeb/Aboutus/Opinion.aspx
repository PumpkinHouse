﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Opinion.aspx.cs" Inherits="PumpkinHouse.Aboutus.Opinion" MasterPageFile="~/Site.master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<style type="text/css">
.floatMenuHelp{ display:none;}
</style>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <script>
        function doAction() { }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
 <div class="AboutBox">
    <div class="AboutLeft float_l">
        <ul>
            <li><a href="AboutUs.aspx">关于木头盒子</a></li>
            <li><a href="Community.aspx">社区指南</a></li>
            <li><a href="Explain.aspx">收纳工具使用说明</a></li>
            <li style="display:none;"><a href="Friends.aspx">木头盒子的朋友们</a></li>
            <li><a href="Faq.aspx">FAQ</a></li>
            <li><a href="Opinion.aspx" class="selected">意见反馈</a></li>
        </ul>
    </div>
    <div class="AboutRight float_r">
        <div class="About-Title">意见反馈</div>
        <div class="Con">

<P>谢谢您关注木头盒子的成长，您对木头盒子有任何的批评和表扬，或者有一些自己的观点，都请第一时间联系我们，我们会第一时间给您回复。让我们一起努力，让木头盒子茁壮成长。<br />
您可以通过以下方式留下您宝贵的意见：</P><br />

<p><b>1</b> .关注我们的新浪/腾讯微博。<br />
    <a href="http://weibo.com/mutouhezi" target=_blank><img src="/img/Opinion-sina.jpg" style="margin-left:30px; margin-right:70px;" /></a><a href="http://t.qq.com/mutoubox" target=_blank><img src="/img/Opinion-qq.jpg" /></a>
</p><br /><br />
<p><b>2</b> .您也可以发送邮件到 mutouhezi@mutouhezi.com ，我们期待您的意见。</p><br /><br />
<p><b>3</b> .站内直接留言&nbsp;&nbsp;&nbsp;&nbsp;<a href="/picture/21762" target="_blank"><input type=button value="我要留言" class="Public-Btn-Box WebSure"></a></p><br /><br />


        </div>
    </div>
 </div> 
</asp:Content>

