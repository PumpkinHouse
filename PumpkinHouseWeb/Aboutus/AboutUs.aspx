﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="PumpkinHouse.Aboutus.AboutUs" MasterPageFile="~/Site.master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<style type="text/css">
.floatMenuHelp{ display:none;}
</style>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <script>
        function doAction() { }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
 <div class="AboutBox">
    <div class="AboutLeft float_l">
        <ul>
            <li><a href="AboutUs.aspx" class="selected">关于木头盒子</a></li>
            <li><a href="Community.aspx">社区指南</a></li>
            <li><a href="Explain.aspx">收纳工具使用说明</a></li>
            <li style="display:none;"><a href="Friends.aspx">木头盒子的朋友们</a></li>
            <li><a href="Faq.aspx">FAQ</a></li>
            <li><a href="Opinion.aspx">意见反馈</a></li>
        </ul>
    </div>
    <div class="AboutRight float_r">
        <div class="About-Title">关于木头盒子</div>
        <div class="Con">
        

        <br />
            木头盒子是一个家居购物分享社区。发现你喜欢的家居产品，并提供后家装时代的网购参考。这里你可以：<br /><br />
            <img src="/img/AboutUs/fx-fx-jy.png" /><br /><br />

发现：寻找你最喜欢家居商品；<br />
分享：分享你的喜欢和购买经验；<br />
交流：找到你相同口味的朋友。<br /><br />

<strong>为什么叫木头盒子？</strong><br />
“木头”质感温暖平和，“盒子”则在生活中承担收纳物品的功能。我们相信，每个人都应该拥有这样一只木头盒子，收纳关于生活的点滴美好。我们相信，或创意、或非凡、或美丽、或暖意，生活因收纳而变得弥足珍贵。<br /><br />

<strong>我们的口号…</strong><br />
木头盒子 —“关于家的购物分享！”<br />
所有跟家有关的美好物品，只要你喜欢，只要你看到，都可以一键收纳进盒子，与我们分享！<br /><br />

        </div>
    </div>
 </div> 
</asp:Content>
