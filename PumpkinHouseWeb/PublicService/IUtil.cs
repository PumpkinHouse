﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using PumpkinHouseDatabase.Contract;

namespace PumpkinHouse.PublicService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUtil" in both code and config file together.
    [ServiceContract]
    public interface IUtil
    {
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        int Test();

        /// <summary>
        /// Find all the available user locations
        /// Note it is not likely to be updated so GET is good
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        IList<Province> FindLocations();

    }
}
