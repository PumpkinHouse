﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using PumpkinHouseDatabase.Contract;
using System.ServiceModel.Activation;
using System.Web;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;
using log4net;
using System.Web.Services.Description;
using PumpkinHouse.Utils.Extention;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;

namespace PumpkinHouse.PublicService
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PublicService : IPublicService
    {
        #region Exception Handler
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            IErrorHandler handler = new LogErrorHandler();
            foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            {
                dispatcher.ErrorHandlers.Add(handler);
            }
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<System.ServiceModel.Description.ServiceEndpoint> endPoint, BindingParameterCollection collection)
        {
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }
        #endregion

        private ILog Log = LogManager.GetLogger(typeof(PublicService));

        public IList<string> FindTags(TagType type)
        {
            Log.Info("FindTags() is called");
            List<string> result = new List<string>();
            using (DataUtils utils = new DataUtils())
            {
                // find common tags
                result.AddRange(Helper.FindCommonTags(utils).Select(t => t.Name));
                string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
                // if the user is logged in, find self-defined tags
                if (!string.IsNullOrEmpty(username))
                {
                    Log.Debug("logged in user: " + username + ", finding user defined tags");
                    List<string> myTags = Helper.FindTagsByUsername(type, username, utils).Select(t => t.Name).ToList();
                    foreach (var tag in myTags)
                    {
                        if (!result.Contains(tag))
                        {
                            result.Add(tag);
                        }
                    }

                }
            }
            return result;
        }

        public IList<KeywordCategory> FindKeywordCategories()
        {
            Log.Info("FindKeywords() is called");
            using (DataUtils utils = new DataUtils())
            {
                var result = utils.FindKeywordCategories().Select(kc => new KeywordCategory { Name = kc.Name, Keywords = kc.DB_Keyword.Select(k => k.Name).ToList() }).ToList();
                return result;
            }
        }

        public IList<string> FindAssociatedKeywords(string keyword)
        {
            Dictionary<string, string> keywords = KeywordHelper.GetKeywords(false);
            if (keywords.Keys.Contains(keyword))
            {
                return keywords[keyword].Split(' ');
            }
            return new List<string>();
        }


        #region Picture Related
        public PagingList<CollectionToDisplay> FindAllOriginalCollections(string keyword, int pageNumber, bool commodityOnly, HomePageMode mode, bool isSearch)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("FindAllOriginalCollections() is called. keyword: {0}, pageNumber: {1}, commodityOnly: {2}, mode: {3}", keyword, pageNumber, commodityOnly, mode));

            if (pageNumber == 0)
            {
                Log.Debug("record user action");

                using (DataUtils utils = new DataUtils())
                {
                    if (isSearch)
                    {
                        // 搜索动作
                        DB_User_Action action = new DB_User_Action
                        {
                            Action_Time = DateTime.Now,
                            Action_Type = (byte)UserActionType.SearchByKeyword,
                            Username = string.IsNullOrEmpty(username) ? null : username,
                            Action_Content = "p:" + keyword
                        };
                        utils.InsertUserAction(action);

                    }
                    else
                    {
                        // 过滤动作
                        DB_User_Action action = new DB_User_Action
                        {
                            Action_Time = DateTime.Now,
                            Action_Type = (byte)UserActionType.SearchByTag,
                            Username = string.IsNullOrEmpty(username) ? null : username,
                            Action_Content = mode + ":" + commodityOnly + ":" + keyword
                        };
                        utils.InsertUserAction(action);
                    }
                }

            }

            Log.Debug("Find pictures");
            using (DataUtils utils = new DataUtils(false))
            {
                return Helper.FindAllOriginalViewCollections(utils, mode, keyword, commodityOnly, pageNumber, 20000);
            }
        }

        public PagingList<CollectionToDisplay> FindAllActivityCollections(string keyword, int pageNumber, bool commodityOnly, HomePageMode mode)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("FindAllActivityCollections() is called. keyword: {0}, pageNumber: {1}, commodityOnly: {2}, mode: {3}", keyword, pageNumber, commodityOnly, mode));

            using (DataUtils utils = new DataUtils(false))
            {
                IQueryable<View_Collection_With_Original> source =
                    utils.FindViewCollectionWithOriginal(keyword, commodityOnly)
                    .Where(c => c.Tags.Contains("消暑利器") && c.Collect_Time >= new DateTime(2012, 7, 9) && c.Collect_Time <= new DateTime(2012, 8, 10));

                switch (mode)
                {
                    case HomePageMode.Default:
                        {
                            source = source.OrderByDescending(c => c.Like_Count);
                            break;
                        }
                    case HomePageMode.Latest:
                        {
                            source = source.OrderByDescending(p => p.Collect_Time);
                            break;
                        }
                }

                return Helper.ConvertCollectionList(utils, source, pageNumber, -1, 2);
            }
        }

        public PagingList<CollectionToDisplay> FindAllActions(int pageNumber)
        {
            Log.Info(string.Format("FindAllActions() is called. pageNumber: {0}", pageNumber));
            using (DataUtils utils = new DataUtils(false))
            {
                return Helper.ConvertCollectionList(utils, utils.FindAllCollections(), pageNumber, 0, 2);
                // return PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(utils.FindAllCollections(), pageNumber, c => Converter.ConvertCollectionWithOriginal(utils, c, 0));
            }

        }

        public CollectionToDisplay FindCollection(long collectionId, int replyLimit)
        {
            Log.Info(string.Format("FindCollection() is called. collectionId: {0}, replyLimit: {1}", collectionId, replyLimit));

            using (DataUtils utils = new DataUtils(false))
            {
                Log.Debug("Finding the collection");
                CollectionToDisplay c = Converter.ConvertCollectionWithOriginal(utils, utils.FindViewCollectionWithOriginalById(collectionId), replyLimit);

                if (c == null)
                {
                    Log.Error("collection not found");
                    throw new WebFaultException<int>((int)ErrorCode.ItemNotFound, System.Net.HttpStatusCode.BadRequest);
                }

                return c;
            }
        }

        public PagingList<CollectionToDisplay> FindCollectionsByAlbum(long albumId, string username, int pageNumber)
        {
            Log.Info(string.Format("FindCollectionsByAlbum() is called, albumId: {0}, username: {1}, pageNumber: {2}", albumId, username, pageNumber));
            using (DataUtils utils = new DataUtils(false))
            {
                PagingList<CollectionToDisplay> pics = null;
                //if (albumId == 0)
                //{
                //    pics = PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(utils.FindUncategorizedCollections(username), pageNumber, p => Converter.ConvertCollectionWithOriginal(utils, p, 0));
                //}
                //else
                //{
                DB_Album album = utils.FindAlbumById(albumId);
                IEnumerable<View_Collection_With_Original> cols = utils.FindViewCollectionWithOriginal(albumId, album.Username).OrderByDescending(c => c.Collect_Time).OrderByDescending(c => c.Collect_Count);

                if (album != null)
                {
                    //pics = PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(cols, pageNumber, c => Converter.ConvertCollectionWithOriginal(utils, c, 0));
                    pics = Helper.ConvertCollectionList(utils, cols, pageNumber, 0, 0);
                }
                //}
                return pics;
            }
        }

        public PagingList<CollectionToDisplay> FindPicturesOfUser(string username, int pageNumber)
        {
            Log.Info(string.Format("FindPicturesOfUser() is called, username: {0}, pageNumber: {1}", username, pageNumber));
            using (DataUtils utils = new DataUtils(false))
            {
                //return PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(utils.FindViewCollectionWithOriginal().Where(c => c.Username == username).Where(c => c.Is_Original == 1).OrderByDescending(c => c.Collect_Time), pageNumber, c => Converter.ConvertCollectionWithOriginal(utils, c, 2));
                return Helper.ConvertCollectionList(
                    utils,
                    utils.FindViewCollectionWithOriginal().Where(c => c.Username == username).Where(c => c.Is_Original == 1).OrderByDescending(c => c.Collect_Time),
                    pageNumber,
                    0,
                    2);
            }
        }

        /// <summary>
        /// Find the collections of given user. The pictures uploaded by that user are NOT included.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public PagingList<CollectionToDisplay> FindCollectedItemsOfUser(string username, int pageNumber)
        {
            Log.Info(string.Format("FindCollectedItemsOfUser() is called, username: {0}, pageNumber: {1}", username, pageNumber));
            using (DataUtils utils = new DataUtils())
            {
                //return PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(utils.FindCollectedItemsOfUser(username), pageNumber, c => Converter.ConvertCollectionWithOriginal(utils, c, 2));
                return Helper.ConvertCollectionList(utils, utils.FindCollectedItemsOfUser(username), pageNumber, 0, 2);
            }
        }

        /// <summary>
        /// Find the collections of given user. The pictures uploaded by that user are included.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public PagingList<CollectionToDisplay> FindAllCollectionsOfUser(string username, int pageNumber)
        {
            Log.Info(string.Format("FindAllCollectionsOfUser() is called, username: {0}, pageNumber: {1}", username, pageNumber));
            using (DataUtils utils = new DataUtils())
            {
                // return PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(utils.FindAllCollectionsOfUser(username), pageNumber, c => Converter.ConvertCollectionWithOriginal(utils, c, 2));
                return Helper.ConvertCollectionList(utils, utils.FindAllCollectionsOfUser(username), pageNumber, 0, 2);
            }
        }

        #endregion

        #region User Related
        public UserToDisplay FindUser(string username)
        {
            Log.Info("FindUser() is called, username: " + username);
            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(username);
                if (user == null)
                {
                    Log.Error("user is not found");
                    throw new WebFaultException<int>((int)ErrorCode.ItemNotFound, System.Net.HttpStatusCode.BadRequest);
                }
                return Converter.ConvertUser(utils, user, 0);
            }
        }

        public PagingList<UserToDisplay> FindFans(string username, int pageNumber)
        {
            Log.Info(string.Format("FindFans() is called, username: {0}, pageNumber: {1}", username, pageNumber));
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page<DB_Fan, UserToDisplay>(utils.FindFans(username), pageNumber, u => Converter.ConvertFan(utils, u, 6));
            }
        }

        public PagingList<UserToDisplay> FindAttentions(string username, int pageNumber)
        {
            Log.Info(string.Format("FindAttentions() is called, username: {0}, pageNumber: {1}", username, pageNumber));
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page<DB_User, UserToDisplay>(utils.FindAttentions(username), pageNumber, u => Converter.ConvertUser(utils, u, 6));
            }
        }

        public PagingList<UserToDisplay> SearchUsers(string keyword, int pageNumber)
        {
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("SearchUser() is called, keyword: {0}, pageNumber: {1}", keyword, pageNumber));
            using (DataUtils utils = new DataUtils())
            {
                if (pageNumber == 0)
                {
                    Log.Debug("adding user action");
                    DB_User_Action action = new DB_User_Action
                    {
                        Action_Type = (byte)UserActionType.SearchByKeyword,
                        Action_Time = DateTime.Now,
                        Username = string.IsNullOrWhiteSpace(myName) ? null : myName,
                        Action_Content = "u:" + keyword
                    };
                    utils.InsertUserAction(action);
                }
                return PagingHelper.Page<DB_User, UserToDisplay>(utils.SearchUsers(keyword), pageNumber, u => Converter.ConvertUser(utils, u, 6));
            }
        }

        public PagingList<UserToDisplay> FindPopularUsers(int number)
        {
            Log.Debug("FindPopularUsers is called");
            using (DataUtils utils = new DataUtils())
            {
                List<DB_User> list = utils.FindUsers().Where(u => u.Is_Recommended > 0).OrderBy(u => u.Is_Recommended).Take(number).ToList();
                List<UserToDisplay> result = new List<UserToDisplay>();
                list.ForEach(u => result.Add(Converter.ConvertUser(utils, u, 0)));

                return new PagingList<UserToDisplay> { Count = result.Count, List = result };
            }
        }
        #endregion

        #region Album Related
        public PagingList<AlbumToDisplay> FindAlbumsOfUser(string username, int pageNumber, bool includePicture, int picLimit)
        {
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("FindAlbumsOfUser() is called, username: {0}, includePicture: {1}, picLimit: {2}, current user: {3}, pageNumber: {4}", username, includePicture, picLimit, myName, pageNumber));

            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page<View_Album, AlbumToDisplay>(utils.FindViewAlbumByOwnerUsername(username, includePicture), pageNumber, alb => Converter.ConvertAlbum(alb, utils, myName, picLimit, 0));
            }
        }

        public PagingList<AlbumToDisplay> FindAlbumsLikedByUser(string username, int pageNumber)
        {
            Log.Info(string.Format("FindAlbumsLikedByUser() is called, username: {0}, pageNumber: {1}", username, pageNumber));

            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page<View_Album, AlbumToDisplay>(utils.FindAlbumByLikeUsername(username), pageNumber, alb => Converter.ConvertAlbum(alb, utils, myName, 10, 0));
            }
        }

        public AlbumToDisplay FindDefaultAlbum(string username, int picLimit)
        {
            Log.Info(string.Format("FindDefaultAlbum() is called, username: {0}", username));
            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(username);
                return Helper.GetDefaultAlbumInstance(utils, user, picLimit, 2);
            }
        }

        public AlbumToDisplay FindAlbum(long albumId, int limit)
        {
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("FindAlbum() is called, albumId: {0}, limit: {1}, current user: {2}", albumId, limit, myName));

            using (DataUtils utils = new DataUtils())
            {
                AlbumToDisplay result = Converter.ConvertAlbum(utils.FindViewAlbumById(albumId), utils, myName, limit, -1);
                if (result == null)
                {
                    throw new WebFaultException<int>((int)ErrorCode.ItemNotFound, System.Net.HttpStatusCode.BadRequest);
                }
                return result;
            }
        }

        //public IList<AlbumToDisplay> FindAlbumsCollectingPicture(long pictureId, int limit)
        //{
        //    string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
        //    Log.Info(string.Format("FindAlbumsCollectingPicture() is called, pictureId: {0}, limit: {1}, current user: {2}", pictureId, limit, myName));
        //    using (DataUtils utils = new DataUtils())
        //    {
        //        return Converter.ConvertAlbum(utils.FindAlbumsCollectingPicture(pictureId, true), utils, myName, limit, 2);
        //    }
        //}

        public PagingList<AlbumToDisplay> SearchAlbums(string keyword, int pageNumber)
        {
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("SearchAlbums() is called, keyword: {0}, pageNumber: {1}", keyword, pageNumber));
            using (DataUtils utils = new DataUtils())
            {
                if (pageNumber == 0)
                {
                    Log.Debug("adding user action");
                    DB_User_Action action = new DB_User_Action
                    {
                        Action_Type = (byte)UserActionType.SearchByKeyword,
                        Action_Time = DateTime.Now,
                        Username = string.IsNullOrWhiteSpace(myName) ? null : myName,
                        Action_Content = "a:" + keyword
                    };
                    utils.InsertUserAction(action);
                }
                return PagingHelper.Page<View_Album, AlbumToDisplay>(utils.SearchAlbums(keyword), pageNumber, alb => Converter.ConvertAlbum(alb, utils, myName, 10, 0));
            }
        }

        public PagingList<AlbumToDisplay> FindPopularAlbums(int number)
        {
            Log.Info(string.Format("FindPopularAlbums() is called"));
            using (DataUtils utils = new DataUtils())
            {
                List<View_Album> albums = utils.FindViewAlbums().Where(alb => alb.Is_Recommended > 0).OrderBy(alb => alb.Is_Recommended).Take(number).ToList();
                List<AlbumToDisplay> result = new List<AlbumToDisplay>();
                albums.ForEach(alb => result.Add(Converter.ConvertAlbum(alb, utils, "", 1, 0)));
                return new PagingList<AlbumToDisplay> { Count = result.Count, List = result };
            }
        }
        #endregion

        #region Discussion
        public PagingList<DiscussionBoardToDisplay> FindAllDiscussionBoard()
        {
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page(utils.FindAllDiscussionBoards(), 0, board => new DiscussionBoardToDisplay
                {
                    Id = board.Id,
                    Name = board.Board_Name,
                    NumberOfDiscussion = board.DB_Discussion.Count(),
                    NumberOfNew = board.DB_Discussion.Where(d => (d.Post_Time.AddDays(1).Subtract(DateTime.Now)) > TimeSpan.Zero).Count()
                });
            }
        }

        public List<EntityName> FindDiscussionBoardNames()
        {
            using (DataUtils utils = new DataUtils())
            {
                return utils.FindAllDiscussionBoards().Select(board => new EntityName { Name = board.Board_Name, Id = board.Id }).ToList();
            }
        }

        public PagingList<DiscussionToDisplay> FindDisucssions(int boardId, int pageNumber)
        {
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page(utils.FindAllDiscussions().Where(d => d.Board_Id == boardId).OrderByDescending(d => d.Latest_Reply_Time), pageNumber, d => Converter.ConvertDiscussion(d, 1));
            }
        }

        public DiscussionToDisplay FindDisucssion(long discussionId)
        {
            using (DataUtils utils = new DataUtils())
            {
                return Converter.ConvertDiscussion(utils.FindDiscussion(discussionId), -1);
            }
        }

        public PagingList<DiscussionToDisplay> FindLatestDiscussions()
        {
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page(utils.FindAllDiscussions().OrderByDescending(d => d.Latest_Reply_Time).Take(10), 0, d => Converter.ConvertDiscussion(d, 1));
            }
        }

        #endregion

        #region Reply
        public PagingList<ReplyToDisplay> FindAlbumReply(long albumId, int pageNumber)
        {
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page<View_Reply, ReplyToDisplay>(utils.FindRepliesByAlbumId(albumId), pageNumber,
                    r => Converter.ConvertReply(r));
            }
        }

        public PagingList<ReplyToDisplay> FindPictureReply(long pictureId, int pageNumber)
        {
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page<View_Reply, ReplyToDisplay>(utils.FindViewRepliesByCollectionId(pictureId), pageNumber,
                    r => Converter.ConvertReply(r));
            }
        }

        public PagingList<ReplyToDisplay> FindDiscussionReply(long discussionId, int pageNumber)
        {
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page<View_Reply, ReplyToDisplay>(utils.FindRepliesByDiscussionId(discussionId), pageNumber,
                    r => Converter.ConvertReply(r));
            }
        }
        #endregion

        #region Guess Like 

        public CollectionLink GuessLike(long collectionId, long currentLikeId)
        {
            using (DataUtils utils = new DataUtils(false))
            {
                IList<DB_Guess_Like> gl = utils.FindGuessLikeByTargetId(collectionId).ToList();
                if (gl.Count == 0)
                {
                    return null;
                }

                DB_Guess_Like result;
                if (gl.Count == 1)
                {
                    result = gl[0];
                }
                else
                {
                    Random random = new Random((int)DateTime.Now.Ticks);
                    int index = random.Next(0, gl.Count);
                    result = gl[index];
                    if (result.Related_Collection_Id == currentLikeId)
                    {
                        index = (index + 1) % gl.Count;
                        result = gl[index];
                    }
                }

                View_Collection2 col = utils.FindViewCollection2(result.Related_Collection_Id);
                if (col != null)
                {
                    return new CollectionLink
                    {
                        CollectionId = result.Related_Collection_Id,
                        ImageUrl = ImageHelper.GetAccessPath(col.Image_Filename),
                        ImageExt = col.Image_File_Ext
                    };
                }
                return null;
            }
        }

        #endregion

        #region Survey 
        public void ResponseSurvey(bool like, string comment)
        {
            string username = Helper.FindCurrentUsername();
            if (string.IsNullOrEmpty(username)) username = null;

            using (DataUtils utils = new DataUtils())
            {
                DB_Survey existing = utils.FindSurveys().Where(s => s.Username == username).SingleOrDefault();
                if (existing == null)
                {
                    DB_Survey survey = new DB_Survey
                    {
                        Like = (byte)(like ? 1 : 0),
                        Suggestion = comment,
                        Username = username,
                        Time = DateTime.Now
                    };
                    utils.InsertSurvey(survey);
                }
                else
                {
                    existing.Time = DateTime.Now;
                    existing.Like = (byte)(like ? 1 : 0);
                    existing.Suggestion = comment;
                    utils.Commit();
                }
                HttpCookie cookie = new HttpCookie("survey", "done");
                cookie.Expires = DateTime.Now.AddDays(365);
                HttpContext.Current.Request.Cookies.Add(cookie);
            }
        }
        #endregion

        #region Advertisement
        public Advertisement FindAdvertisement()
        {
            using (DataUtils utils = new DataUtils(false))
            {
                DB_Advertisement adv = utils.FindAdvertisement();
                return new Advertisement { ImageUrl = adv.ImageUrl, LinkUrl = adv.LinkUrl, Text = adv.Text };
            }
        }
        #endregion
    }
}
