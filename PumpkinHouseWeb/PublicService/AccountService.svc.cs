﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;
using System.Web;
using log4net;
using System.Globalization;
using System.Transactions;
using System.ServiceModel.Activation;
using System.Web.Security;
using PumpkinHouse.Utils;
using System.ServiceModel.Web;
using System.Net;
using System.Web.Services.Description;
using System.ServiceModel.Dispatcher;
using PumpkinHouse.Utils.Extention;
using System.ServiceModel.Channels;

namespace PumpkinHouse.PublicService
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AccountService : IAccountService
    {
        #region Exception Handler
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            IErrorHandler handler = new LogErrorHandler();
            foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            {
                dispatcher.ErrorHandlers.Add(handler);
            }
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<System.ServiceModel.Description.ServiceEndpoint> endPoint, BindingParameterCollection collection)
        {
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }
        #endregion

        private ILog Log = LogManager.GetLogger(typeof(AccountService));

        public bool Logon(string email, string password, bool rememberMe)
        {
            string username = Helper.ValidateUser(email, password);
            if (username != null)
            {
                FormsAuthentication.SetAuthCookie(username, rememberMe);
                HttpContext.Current.Session["username"] = username;
                return true;
            }
            return false;
        }

        public bool IsEmailAvailable(string email)
        {
            if (string.IsNullOrEmpty(email)) return true;
            using (DataUtils utils = new DataUtils())
            {
                return utils.FindUserByEmail(email) == null;
            }
        }

        public bool IsEmailInUse(string email)
        {
            if (string.IsNullOrEmpty(email)) return false;
            using (DataUtils utils = new DataUtils())
            {
                return utils.FindUserByEmail(email) != null;
            }
        }

        public bool IsNicknameAvailable(string nickname)
        {
            using (DataUtils utils = new DataUtils())
            {
                string name = nickname;
                if (KeywordFilterHelper.Filter(ref name, '*') == FilterResult.Banned)
                {
                    return false;
                }
                return utils.FindUserByNickname(nickname) == null;
            }
        }

        public bool IsNicknameAvailableForChange(string nickname)
        {
            using (DataUtils utils = new DataUtils())
            {
                string name = nickname;
                if (KeywordFilterHelper.Filter(ref name, '*') == FilterResult.Banned)
                {
                    return false;
                }

                DB_User user = utils.FindUserByNickname(nickname);
                if (user != null)
                {
                    string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
                    return username == user.Username;
                }
                else
                {
                    return true;
                }
            }
        }

        //public bool IsUsernameAvailable(string username)
        //{
        //    using (DataUtils utils = new DataUtils())
        //    {
        //        return utils.FindUserByUsername(username) == null;
        //    }
        //}

        public bool IsCaptchaCorrect(string captcha)
        {
            return string.Equals(
                    (string)HttpContext.Current.Session["CaptchaImageText"],
                    captcha,
                StringComparison.InvariantCultureIgnoreCase);
        }

        public string SendResetPasswordEmail(string email)
        {
            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByEmail(email);
                if (user != null)
                {
                    string s = email + Helper.GenerateRandomString(10);
                    s = SimpleHash.ComputeHash(s, null).Replace("+", "");
                    DB_Token t = new DB_Token { Email = email, Code = s, Time = DateTime.Now };
                    utils.InsertOrUpdateToken(t);

                    EmailHelper.SendResetPasswordEmail(user.Nick_Name, HttpContext.Current.Request.Url.Authority, email, s);

                    Uri uri = EmailHelper.FindMailHomepageByAccount(email);
                    if (uri != null) return uri.ToString();
                    return null;
                }
                else
                {
                    throw new WebFaultException<ErrorCode>(ErrorCode.ItemNotFound, HttpStatusCode.BadRequest);
                }
            }
        }

        public void Register(UserForRegistration user, string captcha)
        {
            using (DataUtils utils = new DataUtils())
            {
                // Verification
                DB_User existingUserWithNickName = utils.FindUserByNickname(user.NickName);
                if (existingUserWithNickName != null)
                {
                    Log.Info("Nick name already exist: " + user.NickName);
                    throw new WebFaultException<string>(string.Format(Resource.NicknameAlreadyExist, user.NickName, CultureInfo.InvariantCulture), HttpStatusCode.BadRequest);
                }

                DB_User existingUserWithEmail = utils.FindUserByEmail(user.Email);
                if (existingUserWithEmail != null)
                {
                    Log.Info("Email already exist: " + user.Email);
                    throw new WebFaultException<string>(string.Format(Resource.EmailAlreadyExist, user.Email), HttpStatusCode.BadRequest);
                }

                string name = user.NickName;
                if (KeywordFilterHelper.Filter(ref name, '*') == FilterResult.Banned)
                {
                    throw new WebFaultException<string>(string.Format(Resource.KeywordDetected, user.NickName), HttpStatusCode.BadRequest);
                }

                string username = Helper.GenerateUsername(utils);

                using (TransactionScope scope = new TransactionScope())
                {
                    Log.Debug("Creating User");
                    DB_User newUser = utils.InsertUser(new DB_User
                    {
                        Username = username,
                        Nick_Name = user.NickName,
                        Email = user.Email,
                        Register_Time = DateTime.Now
                    });

                    Log.Debug("Creating Credential");
                    DB_UserCredential credential = utils.InsertCredential(new DB_UserCredential
                    {
                        Email = user.Email,
                        EncryptedPassword = Utils.SimpleHash.ComputeHash(user.Password, null)
                    });

                    FormsAuthentication.SetAuthCookie(user.Email, false); // 注册后登陆不记住密码
                    HttpContext.Current.Session["username"] = username;
                    scope.Complete();
                }

                ImageHelper.CreateDefaultProfilePhoto(username);

                try
                {
                    EmailHelper.SendWelcomeEmail(user.NickName, HttpContext.Current.Request.Url.Authority, user.Email);
                }
                catch (Exception e)
                {
                    Log.Error("Could not send welcome email: " + user.Email);
                    Log.Error(e.Message);
                    Log.Error(e.StackTrace);
                }
            }


        }

        public void ResetPassword(string code, string email, string newPassword)
        {
            using (DataUtils utils = new DataUtils())
            {
                Helper.ResetPasswordStatus status = Helper.ResetPassword(utils, email, code, newPassword);
                
                DB_User user = utils.FindUserByEmail(email);
                FormsAuthentication.SetAuthCookie(user.Username, false); // 重置密码后登陆不记住密码
                HttpContext.Current.Session["username"] = user.Username;
            }
        }
    }
}
