﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using PumpkinHouseDatabase.Contract;
using PumpkinHouseDatabase;
using log4net;
using System.ServiceModel.Activation;
using System.ServiceModel.Dispatcher;
using PumpkinHouse.Utils.Extention;
using System.Web.Services.Description;

namespace PumpkinHouse.PublicService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Util" in code, svc and config file together.

    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Util : IUtil
    {
        #region Exception Handler
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            IErrorHandler handler = new LogErrorHandler();
            foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            {
                dispatcher.ErrorHandlers.Add(handler);
            }
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<System.ServiceModel.Description.ServiceEndpoint> endPoint, System.ServiceModel.Channels.BindingParameterCollection collection)
        {
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }
        #endregion 

        private static readonly ILog Log = LogManager.GetLogger(typeof(Util));

        public int Test()
        {
            Log.Info("Test method is called");
            return 5;
        }

        public IList<Province> FindLocations()
        {
            Log.Info("FindLocations() is called");
            using (DataUtils utils = new DataUtils())
            {
                List<DB_Location> locations = utils.FindAllProvinces();
                IList<Province> result = new List<Province>();
                foreach (DB_Location l in locations)
                {
                    if (l.Parent_Id == null) // province
                    {
                        result.Add(new Province { Id = l.Id,  Name = l.Name, Cities = new List<City>() });
                    }
                }
                foreach (DB_Location l in locations)
                {
                    if (l.Parent_Id != null) // city
                    {
                        result.Where(p => p.Id == l.Parent_Id).Single().Cities.Add(
                            new City { Name = l.Name, Id = l.Id }
                            );
                    }
                }
                return result;
            }
        }
    }
}
