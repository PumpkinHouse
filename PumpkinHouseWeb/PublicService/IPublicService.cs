﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using PumpkinHouseDatabase.Contract;
using System.ServiceModel.Web;
using PumpkinHouseDatabase;

namespace PumpkinHouse.PublicService
{
    [ServiceContract]
    public interface IPublicService
    {
        /// <summary>
        /// 获取所有的关键字大类（首页左方热门关键字的大类）
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        IList<KeywordCategory> FindKeywordCategories();

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        IList<string> FindAssociatedKeywords(string keyword);

        /// <summary>
        /// 获取标签列表
        /// 未登录用户将会获得默认列表，登录用户将会获得默认列表+用户自定义列表
        /// </summary>
        /// <param name="type">标签类型，分为专辑和图片</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        IList<string> FindTags(TagType type);

        /// <summary>
        /// 获取所有原创图片。图片以发布时间倒序排列
        /// Find all pictures. The order is based on the upload time of the picture.
        /// </summary>
        /// <param name="keyword">过滤关键字。如果为null则不过滤</param>
        /// <param name="pageNumber">分页页数</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<CollectionToDisplay> FindAllOriginalCollections(string keyword, int pageNumber, bool commodityOnly, HomePageMode mode, bool isSearch);

        /// <summary>
        /// 获取所有图片（含转发）。图片以动作时间倒序排列。
        /// Find all pictures. The order is based on the action time, i.e. if there is a picture uploaded one year ago but just
        /// collected by someone, it will show up on the top.
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<CollectionToDisplay> FindAllActions(int pageNumber);

        /// <summary>
        /// 获取指定图片。
        /// </summary>
        /// <param name="pictureId">图片ID</param>
        /// <param name="replyLimit">包含的回复数</param>
        /// <returns></returns>
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        //PictureToDisplay FindPicture(long pictureId, int replyLimit);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<CollectionToDisplay> FindCollectionsByAlbum(long albumId, string username, int pageNumber);

        /// <summary>
        /// 获取指定用户的所有收集（非原创）
        /// </summary>
        /// <param name="username"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<CollectionToDisplay> FindCollectedItemsOfUser(string username, int pageNumber);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        CollectionToDisplay FindCollection(long collectionId, int replyLimit);

        /// <summary>
        /// 获取指定用户的所有收集（包含原创和非原创）
        /// </summary>
        /// <param name="username"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<CollectionToDisplay> FindAllCollectionsOfUser(string username, int pageNumber);

        /// <summary>
        /// 获取指定用户的所有原创图片
        /// </summary>
        /// <param name="username"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<CollectionToDisplay> FindPicturesOfUser(string username, int pageNumber);

        /// <summary>
        /// 获得指定用户的所有信息
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        UserToDisplay FindUser(string username);

        /// <summary>
        /// 获得指定用户的所有粉丝信息
        /// </summary>
        /// <param name="username"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<UserToDisplay> FindFans(string username, int pageNumber);

        /// <summary>
        /// 获得粉丝最多的指定数量的用户
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<UserToDisplay> FindPopularUsers(int number);

        /// <summary>
        /// 获得指定用户的所有关注人的信息
        /// </summary>
        /// <param name="username"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<UserToDisplay> FindAttentions(string username, int pageNumber);

        /// <summary>
        /// 获得指定用户的所有专辑
        /// </summary>
        /// <param name="username"></param>
        /// <param name="pageNumber"></param>
        /// <param name="includePicture">是否包含图片信息</param>
        /// <param name="limit">包含的图片张数</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<AlbumToDisplay> FindAlbumsOfUser(string username, int pageNumber, bool includePicture, int limit);

        /// <summary>
        /// 获取指定用户的所有喜欢的专辑
        /// </summary>
        /// <param name="username"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<AlbumToDisplay> FindAlbumsLikedByUser(string username, int pageNumber);

        /// <summary>
        /// 获取指定专辑的信息
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="includePicture"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        AlbumToDisplay FindAlbum(long albumId, int limit);
        
        /// <summary>
        /// 获取指定用户的未分类专辑
        /// </summary>
        /// <param name="username"></param>
        /// <param name="picLimit"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        AlbumToDisplay FindDefaultAlbum(string username, int picLimit);

        /// <summary>
        /// 获取包含有指定图片的所有专辑
        /// </summary>
        /// <param name="pictureId"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        //IList<AlbumToDisplay> FindAlbumsCollectingPicture(long pictureId, int limit);

        /// <summary>
        /// 根据指定关键字搜索专辑
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<AlbumToDisplay> SearchAlbums(string keyword, int pageNumber);

        /// <summary>
        /// 找到最受欢迎的指定数量的专辑
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<AlbumToDisplay> FindPopularAlbums(int number);


        /// <summary>
        /// 根据指定关键字搜索用户
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<UserToDisplay> SearchUsers(string keyword, int pageNumber);

        /// <summary>
        /// 获取指定图片的所有回复
        /// </summary>
        /// <param name="pictureId"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<ReplyToDisplay> FindPictureReply(long pictureId, int pageNumber);

        /// <summary>
        /// 获取指定专辑的所有回复
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<ReplyToDisplay> FindAlbumReply(long albumId, int pageNumber);

        /// <summary>
        /// 获取指定帖子的所有回复
        /// </summary>
        /// <param name="discussionId"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<ReplyToDisplay> FindDiscussionReply(long discussionId, int pageNumber);

        /// <summary>
        /// 获取所有的小区（讨论区）
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<DiscussionBoardToDisplay> FindAllDiscussionBoard();

        /// <summary>
        /// 获取所有的小区名
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        List<EntityName> FindDiscussionBoardNames();

        /// <summary>
        /// 获取所有指定小区的帖子。帖子按最后回复时间倒序排列
        /// </summary>
        /// <param name="boardId"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<DiscussionToDisplay> FindDisucssions(int boardId, int pageNumber);

        /// <summary>
        /// 获取指定帖子（包括回复）
        /// </summary>
        /// <param name="discussionId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        DiscussionToDisplay FindDisucssion(long discussionId);

        /// <summary>
        /// 获取最新的帖子
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<DiscussionToDisplay> FindLatestDiscussions();

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        CollectionLink GuessLike(long collectionId, long currentLikeId);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void ResponseSurvey(bool like, string comment);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        Advertisement FindAdvertisement();

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<CollectionToDisplay> FindAllActivityCollections(string keyword, int pageNumber, bool commodityOnly, HomePageMode mode);

    }
}
