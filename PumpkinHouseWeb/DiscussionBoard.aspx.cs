﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse
{
    public partial class DiscussionBoard : System.Web.UI.Page
    {
        public int BoardId;
        public string BoardName;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!int.TryParse(Request.Params["boardId"], out BoardId))
            {
                Response.Redirect("~/404.aspx");
            }
            using (DataUtils utils = new DataUtils(false))
            {
                DB_Discussion_Board board = utils.FindDiscussionBoard(BoardId);
                if (board == null)
                {
                    Response.Redirect("~/404.aspx");
                }
                BoardName = board.Board_Name;
                Page.Title = BoardName + " - 木头盒子讨论组";
            }
        }
    }
}