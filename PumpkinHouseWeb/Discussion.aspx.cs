﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse
{
    public partial class Discussion : System.Web.UI.Page
    {
        public int BoardId;
        public string BoardName;
        public long DiscussionId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!long.TryParse(Request.Params["DiscussionId"], out DiscussionId))
            {
                Response.Redirect("~/404.aspx");
            }
            using (DataUtils utils = new DataUtils(false))
            {
                DB_Discussion dis = utils.FindDiscussion(DiscussionId);
                if (dis == null)
                {
                    Response.Redirect("~/404.aspx");
                }
                BoardId = dis.Board_Id;
                BoardName = dis.DB_Discussion_Board.Board_Name;

                string subject = dis.Subject;
                if (subject.Length > 15)
                {
                    subject = subject.Substring(0, 15) + "...";
                }

                Page.Title = subject + " 话题 - 木头盒子讨论组";
            }
        }
    }
}