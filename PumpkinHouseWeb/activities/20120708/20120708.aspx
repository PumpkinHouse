﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="20120708.aspx.cs" Inherits="PumpkinHouse.activities._20120708._20120708" MasterPageFile="~/Site.master"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<title>分享你的家居-消暑装备</title>
<style type="text/css">
body{margin:0px; padding:0px; font-family:'宋体'; font-size:14px;}	
body{ background:url(images/banner.jpg) no-repeat center 114px;}
.banner{ width:960px; height:400px; position:relative;}
.banner .share{ width:85px; height:30px; position:absolute; right:10px; bottom:5px;}
.banner .share a{ margin-right:10px;}
.conBox{ width:960px; height:auto;}
.lcon{ width:670px; height:auto; padding:30px 25px; background:#f9f9f9}
    .lcon h1,.hdpic-h1{ width:670px; height:40px; line-height:40px; font-size:30px; color:#27b0ea; font-weight:100; font-family:'微软雅黑';}
    .list{ width:570px; height:auto; padding:25px; line-height:28px; font-size:14px; color:#696d6e}
.rcon{ width:230px; height:auto;}
    .rcon .phb{ width:230px; height:auto; background:#f9f9f9}
    .rcon .phb h1{}
    .rcon .phb .con{ width:230px; height:auto; padding:10px 0px;}
    .rcon .phb .con dl{ padding:12px 15px; border-bottom:1px solid #f1f1f1;}
    .rcon .phb .con dl dt .num{ width:22px; height:22px; line-height:22px; text-align:center; background:url(images/icon-bg.jpg) no-repeat; display:block; font-family:'微软雅黑'; color:#fff}
    .rcon .phb .con dl dt,.rcon .phb .con dl dd{ float:left; margin-right:10px;}
    .rcon .phb .con dl dd a{display:block; float:left;}
    .rcon .phb .con dl dd a.name{display:block; width:85px; height:20px; word-break:keep-all; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;}
    
    .rcon .lucky{ width:230px; height:auto; background:#f9f9f9; margin-top:20px;}
    .rcon .lucky .con{ width:230px; height:auto; padding:10px 0px;}
    .rcon .lucky .con ul{ padding:12px 15px; border-bottom:1px solid #f1f1f1;} 
    .rcon .lucky .con ul li{ float:left; height:30px; line-height:30px; margin-right:10px;}
    .rcon .lucky .con .none{ font-size:16px; font-family:'微软雅黑'; color:#28a7ff; margin:50px 10px; line-height:30px;}
    .rcon .lucky .con b{ font-size:20px; font-weight:100;}
    
    img.icon {
        width: 16px;
        height: 16px;
        background: url(http://img.t.sinajs.cn/t35/appstyle/opent/images/app/btn_trans.gif?id=201104131414) 0 0;
        background-position: 0 -56px;
                    border: none;
    }
</style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script src="/scripts/weibo.js" ></script>
    <script src="/scripts/likePicture.js" ></script>
    <script>
        var globalAllPictures;
        var globalPageNumber = 0;
        var globalTag = '消暑利器';
        function renderPic(pictures) {
            globalAllPictures = pictures;

            var filterHolder = $('ul.PublicList');
            var picGalleryTemplate = $('#galleryItemTemplate').tmpl(pictures.list);
            filterHolder.append(picGalleryTemplate);

            //喜欢按钮
            picGalleryTemplate.find('.likeBtn').on('click', function (e) {
                updatePictureLike(e, defaultLikeCallback);
            });

            initCollectButton();

            if (globalPageNumber == 0) {
                //瀑布流控制~~Kevin
                $('.PublicList').masonry({
                    itemSelector: '.PublicList li',
                    columnWidth: 240
                });

                bindReplyEvent($('.PublicList'));
            }
            else {
                $('.PublicList').masonry('appended', picGalleryTemplate, true);
                bindReplyEvent(picGalleryTemplate);
            }

            // bind event on collect buttons
            $('.collectButton', picGalleryTemplate).each(function (index, item) {
                var li = $(item).parents('li.pic');
                var displayTarget = li.find('a span.collectCount');
                $.collectButtion(item, globalLoggedIn, globalMyAlbums, displayTarget);
            });

            globalLock = 0;
        }

        function doAction() {
            var content = "给大家推荐一个好玩儿的活动，<<盒子召集令 • 分享你的家居消暑装备>>，一起来赢家居大礼包吧！";
            var link = "http://image.mutouhezi.com/activities/banner-act20120708.png";
            AddSinaWeiboButton($('.share'), content + " (分享自@木头盒子网)", link, 0, false);
            AddQQWeiboButton($('.share'), content + " (分享自@@mutoubox)", link, 0, false);

            ajaxGetAllActivityPictures(true, globalTag, 0, false, 0, renderPic, handleFault);
            ajaxGetAllActivityPictures(true, globalTag, 1, false, 0, cacheLoading, handleFault);

            $(window).scroll(function () {
                if (!globalAllPictures) return;
                if (getScrollTop() + $(window).height() + 100 > $(document).height()) {
                    if (globalLock == 0) {
                        globalLock = 1;
                        if (globalPageNumber < Math.floor((globalAllPictures.count - 1) / 20)) {
                            globalPageNumber++;
                            globalLoadingIndicatorAtBottom = true;
                            renderCache(renderPic);
                            ajaxGetAllActivityPictures(true, globalTag, globalPageNumber + 1, false, 0, cacheLoading, handleFault);
                        }
                    }
                }
            });

        }
    </script>
 <div class="banner">
     <div class="share">
        
     </div>
 </div>
 <div class="conBox clearfix" style="margin-top:30px; height:auto;">
    <div class="lcon float_l">
        <h1>活动时间</h1>
        <div class="list clearfix">
            <strong>2012年7月9日——8月9日</strong>
        </div>
        <h1>参与方式</h1>
        <div class="list clearfix">
<strong>A、分享消暑利器赢家居大礼包</strong><br />
活动时间内，在木头盒子分享你为炎夏准备的消暑利器，添加“消暑利器”标签，则成功参加了本活动，活动结束后，获得红心数最多的前六张图片分享者将获得价值500—200不等的盒子家居礼包。<br />
<img src="images/pic_01.jpg" /><br /><br />
 <strong>B、你喜欢，我送礼！</strong><br />
活动期间，凡是对活动图片（即图片标签包含：消暑利器）进行喜欢的盒友，均有机会获得由木头盒子提供的参与小奖品，价值5—20元不等，随机发送，包邮哟亲~
<br />
<img src="images/pic_02.jpg" />
        </div>
        <h1>活动细则</h1>
        <div class="list clearfix">
<strong>活动时间:</strong> 7月9日——8月9日<br /><br />
<strong>1、</strong> 图片必须是在活动期间上传的（即原创分享，收纳其他盒友的分享不能参加本次活动），且不符合活动主题的分享将被移出活动。<br /><br />
<strong>2、</strong> 如发现刷票行为，将被移出活动并取消活动参加资格。<br /><br />
<strong>3、</strong> 为保证活动的公平公正，每个用户仅有一次获奖名额，不得重复中奖。若同一名用户的多张图片入围前六名，则保留红心次数最多的参赛图片，其他入围图片取消参赛资格。<br /><br />
<strong>4、</strong> 活动期间内，每天（工作日）下午五点将从24小时内（17：00—17:00）对活动图片进行喜欢的盒友中，抽取五名幸运用户赠送小礼品。<br /><br />
<strong>5、</strong> 中奖名单公布后，请获奖者在三个工作日内通过站内私信联系<a href="/user/l68952085485829" style="color:#28a7ff; font-weight:bold">木头盒子</a>，提供相关信息，我们将在七个工作日内为你邮寄奖品。<br /><br />
<strong>6、</strong> 奖品不可折合为现金。<br /><br />
        </div>
        <h1>奖品设置</h1>
        <div class="list clearfix" style="background:url(images/pic_03.jpg) no-repeat right bottom;">
            <strong>一等奖</strong><br />
            红心排行第1名的盒友<br />
            获得价值500元的盒子家居礼包<br /><br />

            <strong>二等奖</strong><br />
            红心排行第2、3名的盒友<br />
            获得价值300元的盒子家居礼包<br /><br />

            <strong>三等奖</strong><br />
            红心排行第4—6名的盒友<br />
            获得价值200元的盒子家居礼包<br /><br />

            <strong>参与奖</strong><br />
            每天（工作日）从参与喜欢活动图片的网友中随机抽取5名盒友<br />
            获得创意家居小用品<br /><br />

            <span style="color:#28a7ff;">盒子家居礼包，是指你可以在木头盒子上选购相等价值的家居用品，<br />
            可以选择多样，只要价格（含邮费）在规定的金额范围内即可。<br /><br />
            </span>
        </div>

    </div>

    <div class="rcon float_r">
        <div class="phb">
            <h1><img src="images/menu_01.jpg" /></h1>
            <div class="con clearfix">
                <% for (int i = 0; i < Collections.Count; i++)
                   { %>
                <dl class="clearfix">
                    <dt><span class="num"><%= i + 1 %></span></dt>
                    <dd><a href="/picture/<%= Collections[i].Id  %>"><img src="<%= Collections[i].Url + "_80_80_thumbnail." +  Collections[i].Extention %>" width="55" height="55" /></a></dd>
                    <dd><a class="name" href="/user/<%= Collections[i].Username  %>"><%= Collections[i].OwnerNickname%></a><br /><span class="loveHeart" style="width:80px; text-indent:20px; line-height:24px;"><%= Collections[i].NumberOfLikes %></span></dd>
                </dl>
                <% } %>
            </div>
        </div>

        <div class="lucky">
            <h1><img src="images/menu_02.jpg" /></h1>
            <div class="con clearfix">
                <% if (Lucky.Count == 0)
                   { %>
                   <div class="none">
                    <b>积极参与，</b><br />
                    你的名字就会出现在这里哟
                    </div>
                <% }
                   else
                   {%>
                <% for (int i = 0; i < Lucky.Count; i++)
                   { %>
                <ul class="clearfix">
                    <li><a href="/user/<%= Lucky[i].Username  %>"><img src="http://image.mutouhezi.com/profile/<%= Lucky[i].Username %>_30_30.jpg" width="30" height="30" /></a></li>
                    <li><a href="/user/<%= Lucky[i].Username  %>"><%= Lucky[i].Nick_Name%></a></li>
                </ul>
                <% } %>
                <div style="width:230px; height:30px; line-height:20px; display:block; margin:10px auto; color:#28a7ff; font-family:'宋体'; text-align:center; font-size:13px;">恭喜以上用户获得<%= Activity.Update_Date.ToString("MM-dd")%>幸运小礼品</div>
                <% } %>
            </div>
        </div>
       <% if (CurrentUser != null)
          { %>
       <div style="font-size:13px; color:Red; margin-top:20px; line-height:22px; font-family:'微软雅黑'; padding:15px;">
        <%= CurrentUser.Nick_Name %>，恭喜你成为<%= AwardedDate.ToString("MM-dd") %>的幸运用户，获得由木头盒子提供的小礼物一份。你联系<a href="/user/l68952085485829">木头盒子</a>了吗？
        <% } %>
        </div>
    </div>
 </div>  

 <div class="conBox clearfix" style="margin-top:30px; height:auto;">
    <h1 class="hdpic-h1 clearfix" style="height:50px;">活动图片</h1>
    <ul class="PublicList clearfix">
    </ul>
    
 </div>

 

 <style>
 footer{ +display:none;}
 </style>


</asp:Content>
