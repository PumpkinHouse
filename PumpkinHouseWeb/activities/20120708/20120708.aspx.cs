﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Utils;

namespace PumpkinHouse.activities._20120708
{
    public partial class _20120708 : System.Web.UI.Page
    {
        protected IList<CollectionToDisplay> Collections;
        protected IList<View_Lucky> Lucky;
        protected DB_Activity Activity;
        protected DB_User CurrentUser;
        protected DateTime AwardedDate;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (DataUtils utils = new DataUtils(false))
            {
                IList<View_Collection_With_Original> cols = utils.FindViewCollectionWithOriginal().Where(c => c.Is_Original == 1).Where(c => c.Tags.Contains("消暑利器") && c.Uploaded_Time >= new DateTime(2012, 7, 9) && c.Uploaded_Time <= new DateTime(2012, 8, 10)).OrderByDescending(c => c.Like_Count).Take(6).ToList();
                Collections = cols.Select(u => Converter.ConvertCollectionWithOriginal(utils, u, 0)).ToList();

                Activity = utils.FindActivities().Where(a => a.Id == Constants.CurrentActivityId).SingleOrDefault();

                Lucky = utils.FindViewLucky().Where(l => l.Activity_Id == Constants.CurrentActivityId).Where(l => l.Date == Activity.Update_Date).ToList();

                string username = Helper.FindCurrentUsername();
                if (!string.IsNullOrEmpty(username))
                {
                    DB_Lucky l = utils.FindLucky().Where(lucky => lucky.Activity_Id == Constants.CurrentActivityId && lucky.Username == username).SingleOrDefault();
                    if (l != null)
                    {
                        CurrentUser = utils.FindUserByUsername(username);
                        AwardedDate = l.Date;
                    }
                }
            }
        }
    }
}