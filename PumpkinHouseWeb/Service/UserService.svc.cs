﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using PumpkinHouseDatabase;
using System.Web;
using PumpkinHouse.Utils;
using PumpkinHouseDatabase.Contract;
using System.Transactions;
using log4net;
using System.Drawing;
using System.ServiceModel.Web;
using System.Net;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using PumpkinHouse.Utils.Extention;
using System.ServiceModel.Channels;
using System.Web.Security;
using PumpkinHouse.Association;

namespace PumpkinHouse.Service
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class UserService : IUserService, IServiceBehavior
    {
        #region Exception Handler
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            IErrorHandler handler = new LogErrorHandler();
            foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            {
                dispatcher.ErrorHandlers.Add(handler);
            }
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endPoint, BindingParameterCollection collection)
        {
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }
        #endregion

        private ILog Log = LogManager.GetLogger(typeof(UserService));

        public string Test()
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            return "Test Result: " + username;
        }

        #region User Related

        public void RegisterUser(UserForRegistration user)
        {
            string currentName;
            AssociationInfo info = HttpContext.Current.Session["AssociationInfo"] as AssociationInfo;
            Log.Info(string.Format("RegisterUser() is called. Party : {0}", info.Party));
            using (DataUtils utils = new DataUtils())
            {
                // Verification
                DB_User existingUserWithNickname = utils.FindUserByNickname(user.NickName);
                if (existingUserWithNickname != null)
                {
                    Log.Info("Nickname already exist: " + user.NickName);
                    throw new WebFaultException<string>(string.Format(Resource.NicknameAlreadyExist, user.NickName), HttpStatusCode.BadRequest);
                }

                DB_User existingUserWithEmail = utils.FindUserByEmail(user.Email);
                if (existingUserWithEmail != null)
                {
                    Log.Info("Email already exist: " + user.Email);
                    throw new WebFaultException<string>(string.Format(Resource.EmailAlreadyExist, user.Email), HttpStatusCode.BadRequest);
                }

                string name = user.NickName;
                if (KeywordFilterHelper.Filter(ref name, '*') == FilterResult.Banned)
                {
                    throw new WebFaultException<string>(string.Format(Resource.KeywordDetected, user.NickName), HttpStatusCode.BadRequest);
                }

                string uid = info.OpendId;
                Log.Debug("uid: " + uid);
                AssociationParty party = info.Party;
                UserForRegistration userInfoInSession = (UserForRegistration)HttpContext.Current.Session["UserInfo"];

                currentName = Helper.GenerateUsername(utils);
                Log.Debug("generated username: " + currentName);

                Log.Debug("Downloading Avatar Image");

                try
                {
                    DownloadHelper.DownloadImageFile(user.AvatarUrl, ImageHelper.GetPhotoPath(currentName) + ".jpg");
                    GenerateProfilePhotoThumbnails(currentName);
                }
                catch (Exception e)
                {
                    Log.Error(e.Message);
                    Log.Error(e.StackTrace);
                    Log.Error("cannot download avator image. continue");
                }

                DB_User newUser = new DB_User
                {
                    Username = currentName,
                    Self_Introduction = userInfoInSession.SelfIntroduction,
                    Nick_Name = user.NickName,
                    Email = user.Email,
                    Register_Time = DateTime.Now
                };

                DB_Account_Association aa = new DB_Account_Association
                {
                    External_Username = uid,
                    Party = (byte)party,
                    Username = currentName,
                    Access_Token = info.PersistToken as string,
                    External_Link_Name = info.LinkName as string,
                    Token_Expire_Time = DateTime.Now.AddDays(89)
                };

                IAssociation association = AssociationFactory.GetInstance(aa);
                if (user.Follow)
                {
                    association.FollowUs();
                }
                if (user.Advertise)
                {
                    association.AdvertiseUs();
                }

                using (TransactionScope sc = new TransactionScope())
                {
                    Log.Debug("starting transaction");
                    Log.Debug("creating user");
                    utils.InsertUser(newUser);
                    Log.Debug("creating association");
                    utils.InsertAccountAssociation(aa);

                    Log.Debug("creating password");
                    DB_UserCredential credential = utils.InsertCredential(new DB_UserCredential
                    {
                        Email = user.Email,
                        EncryptedPassword = Utils.SimpleHash.ComputeHash(user.Password, null)
                    });

                    sc.Complete();
                    Log.Debug("transaction completed");
                }
                ImageHelper.CreateDefaultProfilePhoto(currentName);

                HttpContext.Current.Session["username"] = currentName;
                FormsAuthentication.SetAuthCookie(currentName, true); // 外部账号登录，必须要记住用户
            }
        }

        public void AssociateExternal(string email, string password, string external)
        {
            Log.Info(string.Format("AssociateExternal() is called. email : {0}, password: {1}, external: {2}", email, password, external));

            Log.Debug("validating user");
            string username = Helper.ValidateUser(email, password);
            if (username != null)
            {
                Log.Debug("user validated. username: " + username);

                using (DataUtils utils = new DataUtils())
                {
                    Log.Debug("getting user");
                    DB_User user = utils.FindUserByUsername(username);

                    AssociationInfo info = HttpContext.Current.Session["AssociationInfo"] as AssociationInfo;
                    if (info == null)
                    {
                        return;
                    }

                    DB_Account_Association aa = utils.FindAccountAssociation().Where(a => a.External_Username == external && a.Party == (byte)info.Party).SingleOrDefault();
                    if (aa != null)
                    {
                        throw new WebFaultException<string>(Resource.ExternalAlreadyAssociated, HttpStatusCode.BadRequest);
                    }

                    AccountAssociationHelper.CreateAssociation(utils, info, username);

                    FormsAuthentication.SetAuthCookie(user.Email, false);
                    HttpContext.Current.Session["username"] = user.Username;

                }
            }
        }

        public bool RemoveAssociate(long id)
        {
            string username = Helper.FindCurrentUsername();
            Log.Debug(string.Format("RemoveAssociate() is called. id: {0}, current username: {1}", id, username));
            using (DataUtils utils = new DataUtils())
            {
                DB_Account_Association aa = utils.FindAccountAssociationById(id);
                if (aa != null)
                {
                    if (aa.Username != username)
                    {
                        Log.Error("Account Association does not belong to current user");
                        return false;
                    }
                    utils.DeleteAccountAssociation(aa);
                    return true;
                }
                return false;
            }
        }


        public IList<AccountAssociation> FindAccountAssociations()
        {
            return Helper.FindAccountAssociations();
        }

        public UserGeneralInfo FindUserInfo()
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info("FindUserInfo() is called. current user: " + username);

            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(username);

                if (user.Location_Id.HasValue)
                {
                    DB_Location location = utils.FindLocationById(user.Location_Id.Value);
                    DB_Location province = utils.FindLocationById(location.Parent_Id.Value);
                    return new UserGeneralInfo
                    {
                        Email = user.Email,
                        Gender = (Gender)user.Gender,
                        CityId = user.Location_Id.Value,
                        CityName = location.Name,
                        ProvinceId = province.Id,
                        ProvinceName = province.Name,
                        Username = user.Username,
                        NickName = user.Nick_Name,
                        SelfIntroduction = user.Self_Introduction,
                        ReceiveEmail = user.Receive_Email == 1
                    };
                }
                else
                {
                    return new UserGeneralInfo
                    {
                        Email = user.Email,
                        Gender = (Gender)user.Gender,
                        Username = user.Username,
                        NickName = user.Nick_Name,
                        SelfIntroduction = user.Self_Introduction,
                        ReceiveEmail = user.Receive_Email == 1
                    };
                }
            }
        }

        public void UpdateUserSelfIntroduction(string intro)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("UpdateUserSelfIntroduction() is called. current user: {0}, new self introduction: {1} ", username, intro));

            Log.Debug("filtering keyword");
            KeywordFilterHelper.Filter(ref intro, '*');

            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(username);
                user.Self_Introduction = intro;
                utils.Commit();
            }
        }

        public void UpdateReceiveEmail(bool receiveEmail)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("UpdateReceiveEmail() is called. receiveEmail: {0}, user: {1}", receiveEmail, username));
            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(username);
                user.Receive_Email = (byte)(receiveEmail ? 1 : 0);
                utils.Commit();
            }
        }

        private Object emailLock = new Object();
        /// <summary>
        /// Not in use
        /// </summary>
        /// <param name="email"></param>
        public void UpdateUserEmail(string email)
        {
            email = email.Trim();
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("UpdateUserEmail() is called. current user: {0}, email: {1}", username, email));
            using (DataUtils utils = new DataUtils())
            {
                lock (emailLock)
                {
                    DB_User existingUser = utils.FindUserByEmail(email);
                    if (existingUser != null)
                    {
                        if (existingUser.Username == username) return;
                        throw new WebFaultException<string>(Resource.EmailAlreadyExist, HttpStatusCode.InternalServerError);
                    }
                    DB_User user = utils.FindUserByUsername(username);
                    user.Email = email;
                    utils.Commit();
                }

            }
        }

        public void UpdateUserInfo(UserGeneralInfoToUpdate info)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info("UpdateUserInfo() is called. current user: " + username);
            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(username);
                user.Gender = (byte)info.Gender;
                if (info.LocationId > 0)
                {
                    user.Location_Id = info.LocationId;
                }
                else
                {
                    user.Location_Id = null;
                }

                string intro = info.SelfIntroduction;

                KeywordFilterHelper.Filter(ref intro, '*');
                user.Self_Introduction = intro;
                user.Nick_Name = info.NickName;

                utils.Commit();
            }
        }

        public bool ChangePassword(string oldPassword, string newPassword)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info("ChangePassword() is called, current user: " + username);
            using (DataUtils util = new DataUtils())
            {
                DB_User user = util.FindUserByUsername(username);

                DB_UserCredential cred = util.FindCredentialByEmail(user.Email);

                if (cred == null)
                {
                    Log.Warn(string.Format("Credential for user {0} does not exist", username));
                    return false;
                }
                else
                {
                    Log.Debug("verifying existing password");
                    if (!Utils.SimpleHash.VerifyHash(oldPassword, cred.EncryptedPassword))
                    {
                        return false;
                    }

                    Log.Debug("updating password");
                    cred.EncryptedPassword = Utils.SimpleHash.ComputeHash(newPassword, null);
                    util.Commit();
                    return true;
                }

            }
        }
        #endregion

        #region Tag Related
        /// <summary>
        /// Add tags on a picture. The picture does not necessarily belong to the current user.
        /// </summary>
        /// <param name="collectionId"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        public string AddCollectionTags(long collectionId, string tags)
        {
            Log.Info(string.Format("AddPictureTags() is called. pictureId: {0}, tags: {1}", collectionId, tags));
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            string filteredTags = tags;
            if (KeywordFilterHelper.Filter(ref filteredTags, '*') == FilterResult.Banned)
            {
                throw new WebFaultException<ErrorCode>(ErrorCode.SensitiveWord, HttpStatusCode.BadRequest);
            }

            string result;
            using (DataUtils utils = new DataUtils())
            {
                DB_Collection2 col = utils.FindCollection2(collectionId);
                if (col == null)
                {
                    Log.Warn(string.Format("collection with id {0} is not found", collectionId));
                    return string.Empty;
                }

                Log.Debug("updating tags");
                col.Tags = Helper.MergeTags(col.Tags, tags);

                Log.Debug("result tags: " + col.Tags);

                result = col.Tags;

                utils.Commit();
            }
            Helper.AddTags(username, tags, TagType.Collection);
            return result;
        }

        /// <summary>
        /// Add tag on an album. The album does not necessarily belong to the current user.
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        public string AddAlbumTags(long albumId, string tags)
        {
            string result;
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("AddAlbumTags() is called. albumId: {0}, tags: {1}, current user: {2}", albumId, tags, username));

            string filteredTags = tags;
            if (KeywordFilterHelper.Filter(ref filteredTags, '*') == FilterResult.Banned)
            {
                throw new WebFaultException<ErrorCode>(ErrorCode.SensitiveWord, HttpStatusCode.BadRequest);
            }

            using (DataUtils utils = new DataUtils())
            {
                DB_Album alb = utils.FindAlbumById(albumId);
                if (alb == null)
                {
                    Log.Warn(string.Format("album with id {0} is not found", albumId));
                    return string.Empty;
                }
                if (alb.Username != username)
                {
                    Log.Warn(string.Format("album with id {0} does not belong to current user", albumId));
                    return string.Empty;
                }

                Log.Debug("updating tags");
                alb.Tags = Helper.MergeTags(alb.Tags, tags);
                Log.Debug("result tags: " + alb.Tags);
                utils.Commit();
                result = alb.Tags;
            }
            Helper.AddTags(username, tags, TagType.Album);
            return result;
        }

        public string DeleteAlbumTag(long albumId, string tagToRemove)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("DeleteAlbumTag() is called. albumId: {0}, tagToRemove: {1}, current user: {2}", albumId, tagToRemove, username));
            using (DataUtils utils = new DataUtils())
            {
                DB_Album alb = utils.FindAlbumById(albumId);
                if (alb == null)
                {
                    Log.Warn(string.Format("album with id {0} is not found", albumId));
                    return string.Empty;
                }
                if (alb.Username != username)
                {
                    Log.Warn(string.Format("album with id {0} does not belong to current user", albumId));
                    return string.Empty;
                }
                Log.Debug("updating tags");
                string result = RemoveTag(tagToRemove, alb.Tags);
                alb.Tags = result;
                Log.Debug("result tags: " + alb.Tags);
                utils.Commit();
                return alb.Tags;
            }
        }

        public string DeleteCollectionTag(long collectionId, string tagToRemove)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("DeletePictureTag() is called. albumId: {0}, tagToRemove: {1}, current user: {2}", collectionId, tagToRemove, username));
            using (DataUtils utils = new DataUtils())
            {
                DB_Collection2 col = utils.FindCollection2(collectionId);
                if (col == null)
                {
                    Log.Warn(string.Format("collection with id {0} is not found", collectionId));
                    return string.Empty;
                }
                Log.Debug("updating tags");
                string result = RemoveTag(tagToRemove, col.Tags);
                col.Tags = result;
                Log.Debug("result tags: " + col.Tags);
                utils.Commit();
                return col.Tags;
            }
        }

        private static string RemoveTag(string tagToRemove, string originalTags)
        {
            string result;
            if (originalTags.IndexOf(tagToRemove) == 0)
            {
                result = originalTags.Substring(tagToRemove.Length).Trim();
            }

            else if (originalTags.IndexOf(tagToRemove) == originalTags.Length - tagToRemove.Length)
            {
                result = originalTags.Substring(0, originalTags.Length - tagToRemove.Length).Trim();
            }

            else
            {
                string[] tags = originalTags.Split(new string[] { tagToRemove }, StringSplitOptions.RemoveEmptyEntries);
                result = string.Join(" ", tags);
            }
            return result;
        }



        #endregion

        #region Picture
        public long UploadPicture(PictureToUpload picture)
        {
            string username = Helper.FindCurrentUsername();

            Log.Info(string.Format("UploadPicture() is called. current user: {0}, pic file name: {1}, album id: {2}", username, picture.ImageName, picture.AlbumId));
            FileName name = null;

            // 过滤敏感词
            string filteredTags = picture.Tags;
            if (KeywordFilterHelper.Filter(ref filteredTags, '*') == FilterResult.Banned)
            {
                throw new WebFaultException<ErrorCode>(ErrorCode.SensitiveWord, HttpStatusCode.BadRequest);
            }

            string filteredDesc = picture.Description;
            if (KeywordFilterHelper.Filter(ref filteredDesc, '*') == FilterResult.Banned)
            {
            }
            picture.Description = filteredDesc;

            if (!string.IsNullOrEmpty(picture.Title))
            {
                string filteredTitle = picture.Title;
                if (KeywordFilterHelper.Filter(ref filteredTitle, '*') == FilterResult.Banned)
                {
                }
                picture.Title = filteredTitle;
            }
            // 结束过滤敏感词

            using (DataUtils utils = new DataUtils())
            {
                if (!string.IsNullOrEmpty(picture.SourceImageUrl))
                {
                    Log.Debug("picture is collected form Internet, checking whether it was collected by this user before");
                    int count = utils.FindPictures().Where(p => p.Username == username).OrderByDescending(p => p.Upload_Time).Where(p => p.Source_Image_Name == picture.SourceImageUrl && p.Source_Page_Url == picture.SourcePageUrl).Count();
                    if (count != 0)
                    {
                        Log.Info("this pic is already collected by current user");
                        throw new WebFaultException<ErrorCode>(ErrorCode.ItemAlreadyExist, HttpStatusCode.BadRequest);
                    }
                }

                if (picture.ImageName != null)
                {
                    Log.Debug("the picture is uploaded by user");
                    name = ImageHelper.ParseFileName(picture.ImageName);

                }
                else
                {
                    Log.Debug("this picture is collected from Internet.");

                    Log.Debug("Prepare downloading");
                    // download the image
                    if (picture.Url == null)
                    {
                        throw new WebFaultException<ErrorCode>(ErrorCode.UrlNotValid, HttpStatusCode.BadRequest);
                    }

                    Log.Debug("processing the url: " + picture.Url);
                    string url = UrlHelper.ProcessUrl(picture.Url, picture.SourcePageUrl);
                    Log.Debug("processed url: " + url);
                    if (url == null)
                    {
                        Log.Warn("the url is not valid");
                        throw new WebFaultException<ErrorCode>(ErrorCode.UrlNotValid, HttpStatusCode.BadRequest);
                    }

                    Log.Debug("downloading file");
                    try
                    {
                        string filename = ImageHelper.DownloadFile(url);

                        Log.Debug("file downloaded. Name: " + filename);
                        name = ImageHelper.ParseFileName(filename);
                    }
                    catch (ImageTooLargeException)
                    {
                        throw new WebFaultException<ErrorCode>(ErrorCode.ImageTooLarge, HttpStatusCode.BadRequest);
                    }
                    catch (InvalidFileTypeException)
                    {
                        throw new WebFaultException<ErrorCode>(ErrorCode.InvalidImage, HttpStatusCode.BadRequest);
                    }
                    catch (Exception e)
                    {
                        Log.Error(e.Message);
                        Log.Error(e.StackTrace);
                        throw new WebFaultException<ErrorCode>(ErrorCode.DownloadError, HttpStatusCode.BadRequest);
                    }
                }

                // 记录用户动作
                Log.Debug("record user action");
                DB_User_Action action = new DB_User_Action
                {
                    Action_Time = DateTime.Now,
                    Action_Type = (byte)UserActionType.AddPicture,
                    Username = username
                };

                switch (picture.UploadTool)
                {
                    case 1: action.Action_Content = "u:" + name.Base; break;
                    case 2: action.Action_Content = "w:" + name.Base; break;
                    case 3: action.Action_Content = "c:" + name.Base; break;
                }

                utils.InsertUserAction(action);

                if (picture.AlbumId != 0)
                {
                    Log.Debug("not default album, id: " + picture.AlbumId);
                    DB_Album album = utils.FindAlbumById(picture.AlbumId);
                    if (album == null)
                    {
                        Log.Warn(string.Format("album with id {0} is not found", picture.AlbumId));
                        Log.Warn("try to use the default album instead");
                        picture.AlbumId = 0;
                    }
                    else if (album.Username != username)
                    {
                        Log.Warn(string.Format("the album id {0} is not under user: {1}", album.Id, username));
                        return 0;
                    }
                }

                Log.Debug("processing the shop information");

                Shop shop = CommodityHelper.ParseShop(picture.SourcePageUrl);
                float price = 0;
                string taobaokeLink = null;
                if (shop != Shop.NotShop)
                {
                    price = CommodityHelper.ParsePrice(shop, picture.SourcePageUrl);
                    if (price > 0 && (shop == Shop.Taobao || shop == Shop.Tmall))
                    {
                        // 处理淘宝客链接
                        taobaokeLink = TaobaokeHelper.FindTaobaokeLink(picture.SourcePageUrl);
                    }
                }

                Log.Debug("shop: " + shop + " price: " + price);

                DB_Collection2 col = Helper.AddPictureEntry(picture, username, name, utils, shop, price, taobaokeLink);

                if (picture.SyncToAssociationAccount.Length > 0)
                {
                    IList<DB_Account_Association> aa = utils.FindAccountAssociation().Where(a => a.Username == username).ToList();
                    foreach (var sa in picture.SyncToAssociationAccount)
                    {
                        Log.Info("syncing to external resource. Party: " + sa);
                        DB_Account_Association association = aa.Where(a => a.Party == sa).SingleOrDefault();
                        string nickname = utils.FindUsers().Where(u => u.Username == username).Select(u => u.Nick_Name).SingleOrDefault();
                        IAssociation stub = AssociationFactory.GetInstance(association);
                        if (stub != null)
                        {
                            // stub.PostBlog(pic, nickname);
                        }

                    }
                }
                return col.Id;
            }
        }



        /// <summary>
        /// Update the description of a picture. The picture must belongs to current user.
        /// </summary>
        /// <param name="pictureId"></param>
        /// <param name="description"></param>
        public string UpdatePictureDescription(long collectionId, string description)
        {
            Log.Info(string.Format("UpdatePictureDescription() is called, pictureId {0}, description: {1}", collectionId, description));

            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            // 过滤敏感词
            if (KeywordFilterHelper.Filter(ref description, '*') == FilterResult.Banned)
            {
            }

            using (DataUtils utils = new DataUtils())
            {
                DB_Collection2 col = utils.FindCollection2(collectionId);

                //DB_Picture pic = utils.FindPictureById(pictureId);
                if (col == null)
                {
                    Log.Warn("could not find picture with id " + collectionId);
                    return null;
                }
                if (col.Username != username)
                {
                    Log.Error(string.Format("picture owner {0} is not current user {1}", col.Username, username));
                    return null;
                }
                col.Description = description;
                utils.Commit();
            }
            return description;
        }


        public int LikePicture(long collectionId)
        {
            string username = Helper.FindCurrentUsername();
            Log.Info("LikePicture() is called. Current user: " + username + " collectionId Id: " + collectionId);

            int likeCount;

            using (DataUtils utils = new DataUtils())
            {
                DB_Collection2 col = utils.FindCollection2(collectionId);
                if (col == null)
                {
                    Log.Error("collection does not found");
                    return -1;
                }

                long pictureId = col.Picture_Id;
                DB_Picture2 p = utils.FindPicture2ById(pictureId);
                if (p == null)
                {
                    Log.Error("picture does not found");
                    return -1;
                }

                DB_User owner = null;
                if (p.Original_Collection_Id.HasValue)
                {
                    owner = utils.FindUserByUsername(p.Original_Collection_.Username);
                }

                DB_User user = utils.FindUserByUsername(col.Username);
                if (user == null)
                {
                    Log.Error("user does not found");
                    return -1;
                }

                DB_Like_Picture lp = utils.FindLikePictures().Where(l => l.Picture_Id == pictureId && l.Username == username).SingleOrDefault();
                if (lp == null)
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        Log.Debug("creating like entity");
                        lp = new DB_Like_Picture { Username = username, Picture_Id = pictureId };
                        utils.InsertLikePicture(lp);

                        likeCount = ++p.Like_Count;
                        if (owner != null)
                        {
                            owner.Like_Count++;
                        }
                        if (user != owner)
                        {
                            user.Like_Count++;
                        }
                        utils.Commit();

                        ts.Complete();
                    }
                }
                else
                {
                    Log.Debug("like already exists");
                    likeCount = p.Like_Count;
                }
            }
            return likeCount;

        }

        public int CancelLikePicture(long collectionId)
        {
            string username = Helper.FindCurrentUsername();
            Log.Info("CancelLikePicture() is called. Current user: " + username + " collection Id: " + collectionId);
            int likeCount;

            using (DataUtils utils = new DataUtils())
            {
                DB_Collection2 col = utils.FindCollection2(collectionId);
                if (col == null)
                {
                    Log.Error("collection does not found");
                    return -1;
                }

                long pictureId = col.Picture_Id;
                DB_Picture2 p = utils.FindPicture2ById(pictureId);
                if (p == null)
                {
                    Log.Error("picture does not found");
                    return -1;
                }

                DB_User owner = null;
                if (p.Original_Collection_Id.HasValue)
                {
                    owner = utils.FindUserByUsername(p.Original_Collection_.Username);
                }

                DB_User user = utils.FindUserByUsername(col.Username);
                if (user == null)
                {
                    Log.Error("user does not found");
                    return -1;
                }

                DB_Like_Picture lp = utils.FindLikePictures().Where(l => l.Picture_Id == pictureId && l.Username == username).SingleOrDefault();
                if (lp == null)
                {
                    Log.Debug("like does not exists");
                    return p.Like_Count;
                }
                else
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        utils.DeleteLikePicture(lp);
                        p.Like_Count--;
                        if (owner != null)
                        {
                            owner.Like_Count--;
                        }
                        if (user != owner)
                        {
                            user.Like_Count--;
                        }
                        utils.Commit();
                        ts.Complete();
                        return p.Like_Count;
                    }
                }
            }

        }
        #endregion

        #region Album Related
        public EntityName CreateAlbum(AlbumToCreate album)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("CreateAlbum() is called. current user: {0}, album name: {1}", username, album.Name));

            // 过滤敏感词
            string albumName = album.Name;
            if (KeywordFilterHelper.Filter(ref albumName, '*') == FilterResult.Banned)
            {
                throw new WebFaultException<ErrorCode>(ErrorCode.SensitiveWord, HttpStatusCode.BadRequest);
            }

            string tags = album.Tags;
            if (KeywordFilterHelper.Filter(ref tags, '*') == FilterResult.Banned)
            {
                throw new WebFaultException<ErrorCode>(ErrorCode.SensitiveWord, HttpStatusCode.BadRequest);
            }

            string desc = album.Description;
            if (KeywordFilterHelper.Filter(ref desc, '*') == FilterResult.Banned)
            {
            }
            album.Description = desc;

            using (DataUtils utils = new DataUtils())
            {
                DB_Album alb = new DB_Album
                {
                    Description = album.Description,
                    Name = album.Name,
                    Tags = album.Tags,
                    Username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"]
                };

                alb = utils.InsertAlbum(alb);

                Helper.AddTags(username, album.Tags, TagType.Album);

                return new EntityName { Id = alb.Id, Name = alb.Name };
            }
        }

        public void CreateAlbums(string[] albumNames)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("CreateAlbums(string[] albumNames) is called. current user: {0}", username));

            using (DataUtils utils = new DataUtils())
            {
                foreach (var name in albumNames)
                {
                    string s = name;
                    if (KeywordFilterHelper.Filter(ref s, '*') == FilterResult.Banned)
                    {
                        throw new WebFaultException<ErrorCode>(ErrorCode.SensitiveWord, HttpStatusCode.BadRequest);
                    }

                    DB_Album alb = new DB_Album
                    {
                        Name = name,
                        Username = username
                    };
                    utils.InsertAlbum(alb);
                }
            }
        }

        public IList<EntityName> FindMyAlbumNames()
        {
            Log.Info("FindMyAlbumNames() is called");

            using (DataUtils utils = new DataUtils(false))
            {
                return Helper.FindMyAlbumNames(utils);
            }
        }


        public void LikeAlbum(long albumId)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            Log.Info(string.Format("LikeAlbum() is called, albumId: {0}, current user: {1}", albumId, username));

            using (DataUtils utils = new DataUtils())
            {
                DB_Album album = utils.FindAlbumById(albumId);
                if (album == null)
                {
                    Log.Error("Album is not found");
                    throw new WebFaultException<ErrorCode>(ErrorCode.ItemNotFound, HttpStatusCode.InternalServerError);
                }
                using (TransactionScope ts = new TransactionScope())
                {
                    Log.Debug("inserting album entity");
                    DB_Like like = utils.InsertLike(new DB_Like { Username = username, Album_Id = albumId });

                    if (username != album.Username)
                    {
                        Log.Debug("inserting notice entity");
                        utils.InsertNotice(new DB_Notice
                        {
                            Username = album.Username,
                            Notice_Time = DateTime.Now,
                            Notice_Type = (byte)(NoticeType.Like),
                            Like_Id = like.Id,
                            Action_Username = username
                        });
                    }

                    ts.Complete();
                }
            }
        }

        public void CancelLikeAlbum(long albumId)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            Log.Info(string.Format("CancelLikeAlbum() is called, albumId: {0}, current user: {1}", albumId, username));

            using (DataUtils utils = new DataUtils())
            {
                Operation.DeleteLike(utils, new DB_Like { Username = username, Album_Id = albumId });
            }
        }

        public void UpdateAlbumName(long albumId, string name)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            Log.Info(string.Format("UpdateAlbumName() is called, albumId: {0}, new name: {1}, current user: {2}", albumId, name, username));

            string s = name;
            if (KeywordFilterHelper.Filter(ref s, '*') == FilterResult.Banned)
            {
                throw new WebFaultException<ErrorCode>(ErrorCode.SensitiveWord, HttpStatusCode.BadRequest);
            }

            using (DataUtils utils = new DataUtils())
            {
                DB_Album album = utils.FindAlbumById(albumId);
                if (album == null)
                {
                    Log.Warn("could not find album with id " + albumId);
                    return;
                }
                if (album.Username != username)
                {
                    Log.Error(string.Format("album owner: {0} is not current user: {1}", album.Username, username));
                    return;
                }
                album.Name = name;
                utils.Commit();
            }
        }

        public void UpdateAlbumDescription(long albumId, string description)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            Log.Info(string.Format("UpdateAlbumDescription() is called, albumId: {0}, new desc: {1}, current user: {2}", albumId, description, username));

            if (KeywordFilterHelper.Filter(ref description, '*') == FilterResult.Banned)
            {
            }

            using (DataUtils utils = new DataUtils())
            {
                DB_Album album = utils.FindAlbumById(albumId);
                if (album == null)
                {
                    Log.Warn("could not find album with id " + albumId);
                    return;
                }
                if (album.Username != username)
                {
                    Log.Error(string.Format("album owner: {0} is not current user: {1}", album.Username, username));
                    return;
                }
                album.Description = description;
                utils.Commit();
            }
        }

        public void UpdateAlbum(long albumId, string name, string description)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            Log.Info(string.Format("UpdateAlbum() is called, albumId: {0}, new desc: {1}, new name: {2}, current user: {3}", albumId, description, name, username));

            if (KeywordFilterHelper.Filter(ref name, '*') == FilterResult.Banned)
            {
                throw new WebFaultException<ErrorCode>(ErrorCode.SensitiveWord, HttpStatusCode.BadRequest);
            }

            if (KeywordFilterHelper.Filter(ref description, '*') == FilterResult.Banned)
            {
            }

            using (DataUtils utils = new DataUtils())
            {
                DB_Album album = utils.FindAlbumById(albumId);
                if (album == null)
                {
                    Log.Warn("could not find album with id " + albumId);
                    return;
                }
                if (album.Username != username)
                {
                    Log.Error(string.Format("album owner: {0} is not current user: {1}", album.Username, username));
                    return;
                }
                album.Description = description;
                album.Name = name;
                utils.Commit();
            }
        }

        public void DeleteAlbumById(long albumId, bool deleteContent)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("DeleteAlbumById() is called. current user: {0}, albumId: {1}, deleteContent: {2}", username, albumId, deleteContent));

            using (DataUtils utils = new DataUtils())
            {
                Log.Debug("Finding the album");
                DB_Album album = utils.FindAlbumById(albumId);
                if (album == null)
                {
                    Log.Warn("could not find album with id " + albumId);
                    return;
                }
                if (album.Username != username)
                {
                    Log.Error(string.Format("album owner: {0} is not current user: {1}", album.Username, username));
                    return;
                }

                using (TransactionScope ts = new TransactionScope())
                {
                    Log.Debug("starting transaction");
                    if (!deleteContent)
                    {
                        Log.Debug("not to delete content. updating the picture and collections to default album");
                        //IList<DB_Picture> pictures = album.DB_Picture.ToList();
                        //foreach (var pic in pictures)
                        //{
                        //    pic.DB_Album = null;
                        //}

                        IList<DB_Collection2> cols = album.DB_Collection2.ToList();
                        foreach (var col in cols)
                        {
                            col.DB_Album = null;
                        }

                        utils.Commit();
                    }
                    else
                    {
                        Log.Debug("to delete the content");
                        Log.Debug("deleting collections");
                        IList<DB_Collection2> cols = album.DB_Collection2.ToList();
                        foreach (var col in cols)
                        {
                            // @TODO
                            Operation.DeleteCollection(utils, col);
                        }

                        Log.Debug("deleting pictures");
                        //IList<DB_Picture> pics = album.DB_Picture.ToList();
                        //foreach (var pic in pics)
                        //{
                        //    Operation.DeletePicture(utils, pic);
                        //}
                    }

                    Log.Debug("deleting like");
                    List<DB_Like> likes = album.DB_Like.ToList();
                    foreach (var l in likes)
                    {
                        Operation.DeleteLike(utils, l);
                    }

                    Log.Debug("deleting the album");
                    utils.DeleteAlbum(album);

                    ts.Complete();
                    Log.Debug("transaction completed");
                }
            }
        }

        #endregion

        #region Reply Related
        public ReplyToDisplay PostReply(ReplyToPost post)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info("PostReply() is called. current user: " + username);

            string text = post.Text;
            if (KeywordFilterHelper.Filter(ref text, '*') == FilterResult.Banned)
            {
            }
            post.Text = text;

            using (DataUtils utils = new DataUtils())
            {
                string usernameToNotice = null;

                DB_Reply r = post.ReplyId == null ? null : utils.FindReplyById(post.ReplyId.Value);

                string replyTo = null;
                if (r != null)
                {
                    if (r.Album_Id.HasValue)
                    {
                        post.AlbumId = r.Album_Id.Value;
                    }

                    if (r.Collection_Id.HasValue)
                    {
                        post.CollectionId = r.Collection_Id.Value;
                    }

                    if (r.Discussion_Id.HasValue)
                    {
                        post.DiscussionId = r.Discussion_Id.Value;
                    }

                    replyTo = r.DB_User.Username; // set replyTo = the user who post that reply
                }

                DB_Collection2 col = null;
                if (post.CollectionId != null)
                {
                    Log.Debug("Replying to picture, id : " + post.CollectionId);
                    col = utils.FindCollection2(post.CollectionId.Value);
                    usernameToNotice = col.Username;
                }
                DB_Album alb = null;
                if (post.AlbumId != null)
                {
                    Log.Debug("Replying to album, id : " + post.AlbumId);
                    alb = utils.FindAlbumById(post.AlbumId.Value);
                    usernameToNotice = alb.Username;
                }
                DB_Discussion dis = null;
                if (post.DiscussionId != null)
                {
                    Log.Debug("Replying to discussion, id : " + post.DiscussionId);
                    dis = utils.FindDiscussion(post.DiscussionId.Value);
                    usernameToNotice = dis.Username;
                }

                if (r == null && col == null && alb == null && dis == null)
                {
                    Log.Warn(string.Format("could not find valid picture id ({0}), album id ({1}) and discussion id ({2}) at the same time, when reply id is empty", post.CollectionId, post.AlbumId, post.DiscussionId));
                    throw new WebFaultException<string>(Resource.GeneralError, HttpStatusCode.BadRequest);
                }

                DB_Reply reply = new DB_Reply
                {
                    Collection_Id = post.CollectionId,
                    Post_Time = DateTime.Now,
                    Reply_Id = post.ReplyId,
                    Reply_To = replyTo,
                    Replier_Username = username,
                    Text = post.Text,
                    Album_Id = post.AlbumId,
                    Discussion_Id = post.DiscussionId
                };

                using (TransactionScope ts = new TransactionScope())
                {
                    Log.Debug("starting transaction");

                    Log.Debug("inserting reply");
                    reply = utils.InsertReply(reply);

                    if (dis != null)
                    {
                        Log.Debug("updating the discussion last replied time");
                        dis.Latest_Reply_Time = reply.Post_Time;
                        utils.Commit();
                    }

                    if (usernameToNotice != username)
                    {
                        Log.Debug("inserting notice. user to notice: " + usernameToNotice);
                        DB_Notice notice = new DB_Notice
                        {
                            Username = usernameToNotice,
                            Reply_Id = reply.Id,
                            Notice_Time = DateTime.Now,
                            Notice_Type = (byte)NoticeType.Reply,
                            Action_Username = username
                        };

                        utils.InsertNotice(notice);
                    }

                    if (r != null && r.Replier_Username != username && usernameToNotice != r.Replier_Username)
                    {
                        // inserting notice
                        Log.Debug("replying to another person. inserting notice");
                        DB_Notice notice = new DB_Notice
                        {
                            Username = r.Replier_Username,
                            Reply_Id = reply.Id,
                            Notice_Time = DateTime.Now,
                            Notice_Type = (byte)NoticeType.Reply,
                            Action_Username = username
                        };

                        utils.InsertNotice(notice);
                    }
                    ts.Complete();

                    Log.Debug("transaction completed");
                }
                return Converter.ConvertReply(reply);
            }
        }

        public List<long> DeleteReply(long replyId)
        {
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("DeleteReply() is called. current user: {0}, replyId : {1}", myName, replyId));

            using (DataUtils utils = new DataUtils())
            {
                List<long> result;
                using (TransactionScope ts = new TransactionScope())
                {
                    Log.Debug("starting transaction");
                    Log.Debug("find the reply");
                    DB_Reply reply = utils.FindReplyById(replyId);
                    if (reply == null)
                    {
                        Log.Error("could not find the reply");
                        return new List<long>();
                    }

                    string ownerUsername = null;
                    if (reply.Collection_Id.HasValue)
                    {
                        DB_Collection2 collection = utils.FindCollection2(reply.Collection_Id.Value);
                        ownerUsername = collection.Username;
                    }
                    else if (reply.Album_Id.HasValue)
                    {
                        DB_Album album = utils.FindAlbumById(reply.Album_Id.Value);
                        ownerUsername = album.Username;
                    }
                    else if (reply.Discussion_Id.HasValue)
                    {
                        DB_Discussion discussion = utils.FindDiscussion(reply.Discussion_Id.Value);
                        ownerUsername = discussion.Username;
                    }

                    if (reply.Replier_Username != myName && ownerUsername != myName)
                    {
                        Log.Error(string.Format("reply with id {0} does not belong to current user", replyId));
                        return new List<long>();
                    }
                    result = Operation.DeleteReply(utils, reply);
                    ts.Complete();
                    Log.Debug("transaction completed");
                }
                return result;
            }
        }


        #endregion

        #region Message Related
        public MessageToDisplay SendMessage(MessageToSend message)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info("SendMessage() is called. current user: " + username);

            if (username != message.FromUsername)
            {
                Log.Warn("message is not sent from current user");
                return null;
            }

            string text = message.Text;
            if (KeywordFilterHelper.Filter(ref text, '*') == FilterResult.Banned)
            {
                Log.Warn("Message contains invalid keyword: " + message.Text);
                message.Text = text;
            }

            using (DataUtils utils = new DataUtils())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    Log.Debug("inserting message");
                    utils.InsertMessage(message);

                    Log.Debug("find existing conversations");
                    DB_Conversation c1 = utils.FindConverstaionByUser(message.FromUsername, message.ToUsername);
                    DB_Conversation c2;
                    if (c1 == null)
                    {
                        Log.Debug("existing converstaion is not found");

                        c1 = new DB_Conversation
                        {
                            Action = 0,
                            Count = 1,
                            Latest_Update_Time = DateTime.Now,
                            Text = message.Text,
                            Username = message.FromUsername,
                            Target_Username = message.ToUsername,
                            New = 1 // mark the message as not read.
                        };

                        Log.Debug(string.Format("inserting conversation {0} -> {1}", message.FromUsername, message.ToUsername));
                        c1 = utils.InsertConversation(c1);

                        c2 = new DB_Conversation
                        {
                            Count = 1,
                            Action = 1,
                            Latest_Update_Time = DateTime.Now,
                            Text = message.Text,
                            Username = message.ToUsername,
                            Target_Username = message.FromUsername,
                            New = 0 // do not increase new number since it is sent by this user.
                        };

                        Log.Debug(string.Format("inserting conversation {0} -> {1}", message.ToUsername, message.FromUsername));
                        c2 = utils.InsertConversation(c2);
                    }
                    else
                    {
                        Log.Debug("existing conversation found");

                        c1.Count += 1;

                        c2 = utils.FindConverstaionByUser(message.ToUsername, message.FromUsername);
                        c2.Count += 1;

                        c1.Action = 0;
                        c2.Action = 1;

                        c1.New++; // increase new number

                        c1.Latest_Update_Time = c2.Latest_Update_Time = DateTime.Now;
                        c1.Text = c2.Text = message.Text;

                        utils.Commit();
                    }

                    MessageToDisplay result = new MessageToDisplay
                    {
                        Me = message.FromUsername,
                        MyDisplayName = c1.DB_User.Username,
                        SentByMe = true,
                        Target = message.ToUsername,
                        TargetDisplayName = c2.DB_User.Username,
                        Text = message.Text,
                        Time = c1.Latest_Update_Time
                    };

                    ts.Complete();

                    return result;
                }
            }
        }

        public PagingList<ThreadToDisplay> FindMyThreads(int pageNumber)
        {
            Log.Info("FindMyThreads() is called");
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(myName);

                return PagingHelper.Page<DB_Conversation, ThreadToDisplay>(utils.FindThread(myName), pageNumber, c => new ThreadToDisplay
                    {
                        Count = c.Count,
                        New = c.New,
                        Target = c.Username,
                        Me = myName,
                        MyDisplayName = user.Username,
                        TargetDisplay = c.DB_User.Nick_Name,
                        Text = c.Text,
                        Time = c.Latest_Update_Time
                    });
            }
        }

        public PagingList<MessageToDisplay> FindMyMessages(string target, int pageNumber)
        {
            string me = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("FindMyMessages() is called, target: {0}, current user: {1}", target, me));

            using (DataUtils utils = new DataUtils())
            {
                PagingList<DB_Message> list = PagingHelper.Page<DB_Message>(
                    utils.FindMessage(me, target),
                    pageNumber);

                using (TransactionScope ts = new TransactionScope())
                {
                    Log.Debug("starting transaction");
                    Log.Debug("updating the messages as READ");
                    foreach (var m in list.List)
                    {
                        if (m.Send_Username == target) // if the message is sent to me, update it to be read.
                            m.Has_Read = 1;
                    }

                    Log.Debug("updating the conversation as READ");
                    DB_Conversation c = utils.FindConverstaionByUser(target, me); // update the conversation 
                    if (c != null) c.New = 0;

                    utils.Commit();


                    PagingList<MessageToDisplay> result = PagingHelper.Select<DB_Message, MessageToDisplay>(
                        list,
                        m => new MessageToDisplay
                        {
                            Target = target,
                            TargetDisplayName = c.DB_User.Nick_Name,
                            Text = m.Text,
                            Me = me,
                            MyDisplayName = c.Target_UsernameDB_User.Nick_Name,
                            Time = m.Sent_Time,
                            SentByMe = m.Send_Username == me,
                            Has_Read = m.Has_Read == 1
                        });

                    ts.Complete();
                    Log.Debug("transaction completed");

                    return result;
                }
            }
        }

        #endregion


        public PagingList<CollectionToDisplay> FindMyInterest(int pageNumber)
        {
            string myName = Helper.FindCurrentUsername();

            Log.Info(string.Format("FindMyInterest() is called. pageNumber: {0}", pageNumber));

            using (DataUtils utils = new DataUtils())
            {
                // return PagingHelper.Page<View_Collection_With_Original, CollectionToDisplay>(utils.FindMyInterest(myName), pageNumber, c => Converter.ConvertCollectionWithOriginal(utils, c, 2));
                return Helper.ConvertCollectionList(utils, utils.FindMyInterest(myName), pageNumber, 0, 2);
            }
        }

        #region Fan Related
        public void Fan(string username)
        {
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            Log.Info(string.Format("Fan() is called. target user: {0}, current user: {1}", username, myName));

            Helper.FollowUser(username, myName);
        }



        public void UnFan(string username)
        {
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            Log.Info(string.Format("UnFan() is called. target user: {0}, current user: {1}", username, myName));

            using (DataUtils utils = new DataUtils())
            {
                if (utils.IsWatching(myName, username))
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        Log.Debug("Starting transaction");
                        utils.DeleteFan(myName, username);
                        List<DB_Notice> list = utils.FindNoticeByFan(myName, username).ToList();
                        foreach (var n in list)
                        {
                            Log.Debug("Deleting notice");
                            utils.DeleteNotice(n);
                        }
                        ts.Complete();
                        Log.Debug("Transaction completed");
                    }
                }
            }
        }
        #endregion

        #region Notice Related
        public PagingList<NoticeToDisplay> FindNotice(NoticeType filter, int pageNumber)
        {
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            Log.Info(string.Format("FindNotice() is called. pageNumber: {0}, current user: {1}, filter: {2}", pageNumber, myName, filter));

            using (DataUtils utils = new DataUtils())
            {
                PagingList<DB_Notice> result = PagingHelper.Page<DB_Notice>(utils.FindNotice(myName, filter), pageNumber);
                foreach (DB_Notice n in result.List)
                {
                    n.Has_Read = 1;
                }
                utils.Commit();

                return PagingHelper.Select<DB_Notice, NoticeToDisplay>(result, n => Converter.ConvertNotice(utils, n));
            }
        }

        public NoticeSummary FindNoticeSummary()
        {
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("FindNoticeSummary() is called. current user: {0}", myName));

            using (DataUtils utils = new DataUtils())
            {
                IQueryable<DB_Notice> notices = utils.FindUnreadNotice(myName);
                NoticeSummary s = new NoticeSummary
                {
                    Announcement = notices.Count(n => (n.Notice_Type & (byte)NoticeType.Announcement) > 0),
                    Collect = notices.Count(n => (n.Notice_Type & (byte)NoticeType.Collect) > 0),
                    Fan = notices.Count(n => (n.Notice_Type & (byte)NoticeType.Fan) > 0),
                    Like = notices.Count(n => (n.Notice_Type & (byte)NoticeType.Like) > 0),
                    Reply = notices.Count(n => (n.Notice_Type & (byte)NoticeType.Reply) > 0),
                    Message = utils.FindUnreadMessage(myName).Where(m => m.Has_Read == 0).Count()
                };
                s.Total = s.Announcement + s.Collect + s.Fan + s.Like + s.Reply + s.Message;
                return s;
            }
        }
        #endregion

        #region Discussion
        public void PostDiscussion(DiscussionToDisplay discussion)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("PostDiscussion() is called. current user; {0}, subject: {1}, forum name: {2}", username, discussion.Subject, discussion.BoardName));

            string text = discussion.Text;
            if (KeywordFilterHelper.Filter(ref text, '*') == FilterResult.Banned)
            {
                discussion.Text = text;
            }

            string subject = discussion.Subject;
            if (KeywordFilterHelper.Filter(ref subject, '*') == FilterResult.Banned)
            {
                throw new WebFaultException<ErrorCode>(ErrorCode.SensitiveWord, HttpStatusCode.BadRequest);
            }

            DB_Discussion d = new DB_Discussion
            {
                Post_Time = DateTime.Now,
                Subject = discussion.Subject,
                Text = discussion.Text,
                Username = username,
                Latest_Reply_Time = DateTime.Now,
                Board_Id = discussion.BoardId
            };

            using (DataUtils utils = new DataUtils())
            {
                utils.InsertDiscussion(d);
            }
        }

        public void DeleteDiscussion(long discussionId)
        {
            string myName = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("DeleteDiscussion() is called. id: {0}, current user: {1}", discussionId, myName));
            using (DataUtils utils = new DataUtils())
            {
                DB_Discussion d = utils.FindDiscussion(discussionId);
                if (d == null)
                {
                    Log.Error(string.Format("could not find discussion with id {0}", discussionId));
                    return;
                }
                if (d.Username != myName)
                {
                    Log.Error(string.Format("discussion with id {0} does not belong to current user", discussionId));
                }

                using (TransactionScope ts = new TransactionScope())
                {
                    Log.Debug("starting transaction");

                    Log.Debug("deleting all replies");
                    IList<DB_Reply> replies = d.DB_Reply.ToList();
                    foreach (var r in replies)
                    {
                        Operation.DeleteReply(utils, r);
                    }

                    Log.Debug("deleting the discussion");
                    utils.DeleteDiscussion(d);
                    ts.Complete();
                    Log.Debug("transaction completed");
                }
            }
        }
        #endregion

        /// <summary>
        /// Collect a picture into an album. If the picture is already in another album, it will be moved to the new album.
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="pictureId"></param>
        /// <returns></returns>
        public EntityName Collect(long albumId, long collectionId)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("Collect() is called. albumId: {0}, collectionId: {1}, current user: {2}", albumId, collectionId, username));

            using (DataUtils utils = new DataUtils())
            {
                DB_Album album = null;
                if (albumId != 0)
                {
                    Log.Debug("finding album : " + albumId);
                    album = utils.FindAlbumById(albumId);
                    if (album.Username != username)
                    {
                        Log.Error(string.Format("album with id {0} and owner {1} does not belong to current user {2}", album.Id, album.Username, username));
                        return null;
                    }
                }

                View_Collection_With_Original collection = utils.FindViewCollectionWithOriginalById(collectionId);

                if (collection == null)
                {
                    Log.Error("collection is not found");
                    return null;
                }

                using (TransactionScope ts = new TransactionScope())
                {
                    Log.Debug("starting the transaction");
                    Log.Debug("Finding collection");
                    DB_Collection2 c = utils.FindCollection2(username, collection.Picture_Id);
                    if (c == null)
                    {
                        Log.Debug("no existing collection, creating new one");
                        c = utils.InsertCollection2(albumId, collection.Picture_Id, username, false, collection.Description, collection.Tags);
                        if (collection.OwnerName != collection.Username)
                        {
                            c.Collected_Target_Id = collectionId;
                            utils.Commit();
                        }

                        if (collection.OwnerName != username)
                        {
                            Log.Debug("picture owner and album owner are not same, sending notice");
                            DB_Notice notice = new DB_Notice
                            {
                                Username = collection.OwnerName, // the target is the picture owner
                                Notice_Time = DateTime.Now,
                                Notice_Type = (byte)NoticeType.Collect,
                                Collect_Id = c.Id,
                                Action_Username = username
                            };
                            utils.InsertNotice(notice);
                        }

                        if (collection.OwnerName != collection.Username && collection.Username != username)
                        {
                            Log.Debug("collection owner and album owner are not same, sending notice");
                            DB_Notice notice = new DB_Notice
                            {
                                Username = collection.Username, // the target is the collection owner
                                Notice_Time = DateTime.Now,
                                Notice_Type = (byte)NoticeType.Collect,
                                Collect_Id = c.Id,
                                Action_Username = username
                            };
                            utils.InsertNotice(notice);
                        }



                        // if there is no existing collection, increase the collect count
                        DB_Picture2 picture = utils.FindPicture2ById(collection.Picture_Id);
                        picture.Collect_Count++;
                        utils.Commit();
                    }
                    else
                    {
                        Log.Debug("existing collection found, updating");

                        c.Album_Id = albumId == 0 ? (long?)null : albumId;
                        utils.Commit();
                    }


                    ts.Complete();
                    Log.Debug("transaction completed");

                    if (albumId != 0)
                    {
                        return new EntityName { Name = album.Name, Id = c.Album_Id.Value };
                    }
                    else
                    {
                        return new EntityName { Id = 0, Name = Resource.DefaultAlbumName };
                    }
                }
            }
        }

        public void DeletePicture(long collectionId)
        {
            string username = Helper.FindCurrentUsername();
            Log.Info(string.Format("CancelCollect() is called. collectionId: {0}, current user: {1}", collectionId, username));

            using (DataUtils utils = new DataUtils())
            {
                Log.Debug("trying to find collection");
                DB_Collection2 c = utils.FindCollection2().Where(col => col.Id == collectionId && col.Is_Original == 1).SingleOrDefault();
                if (c == null)
                {
                    Log.Warn("collection is not found");
                    return;
                }
                if (c.Username != username)
                {
                    Log.Error("The collection does not belongs to the user");
                    return;
                }
                if (c.Is_Original == 0)
                {
                    Log.Error("the original picture does not belongs to the user");
                    return;
                }

                Operation.DeleteCollection(utils, c);
            }
        }

        public void CancelCollect(long albumId, long pictureId)
        {
            string username = Helper.FindCurrentUsername();
            Log.Info(string.Format("CancelCollect() is called. albumId: {0}, pictureId: {1}, current user: {2}", albumId, pictureId, username));

            using (DataUtils utils = new DataUtils())
            {
                Log.Debug("trying to find collection");
                DB_Collection2 c;
                if (albumId == 0)
                {
                    c = utils.FindCollection2().Where(col => col.Picture_Id == pictureId && col.Album_Id.HasValue == false).SingleOrDefault();
                }
                else
                {
                    c = utils.FindCollection2().Where(col => col.Picture_Id == pictureId && col.Album_Id == albumId).SingleOrDefault();
                }
                if (c == null)
                {
                    Log.Warn("collection is not found");
                    return;
                }
                if (c.Username != username)
                {
                    Log.Error("The collection does not belongs to the user");
                    return;
                }
                //if (c.Collect_Type == 0)
                //{
                //    Log.Error("the picture belongs to current user");
                //    return;
                //}
                Operation.DeleteCollection(utils, c);
            }
        }
        /// <summary>
        /// Move a picture into another album. Both the picture and the new album must belong to the current user.
        /// </summary>
        /// <param name="pictureId"></param>
        /// <param name="newAlbumId"></param>
        /// <returns></returns>
        //public EntityName MovePicture(long collectionId, long newAlbumId)
        //{
        //    string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

        //    Log.Info(string.Format("MovePicture() is called. newAlbumId: {0}, collectionId: {1}, current user: {2}", newAlbumId, collectionId, username));

        //    using (DataUtils utils = new DataUtils())
        //    {
        //        Log.Debug("Finding the target picture");
        //        View_Collection_With_Original pic = utils.FindViewCollectionWithOriginalById(collectionId);

        //        if (pic == null)
        //        {
        //            Log.Warn("collection not found. Id: " + collectionId);
        //            return null;
        //        }

        //        if (pic.Username != username)
        //        {
        //            Log.Error(string.Format("Attempt to move collection id {0} but the owner {1} is not current user {2}", collectionId, pic.Username, username));
        //            return null;
        //        }

        //        DB_Album album = null;
        //        if (newAlbumId != 0)
        //        {
        //            Log.Debug("Finding the new album");
        //            album = utils.FindAlbumById(newAlbumId, false);
        //            if (album.Username != username)
        //            {
        //                Log.Error(string.Format("album with id {0} and owner {1} does not belong to current user {2}", album.Id, album.Username, username));
        //                return null;
        //            }
        //        }

        //        using (TransactionScope ts = new TransactionScope())
        //        {
        //            DB_Collection2 c = utils.FindCollection2(pic.Username, pic.Id, 0);
        //            if (c == null)
        //            {
        //                Log.Error("Could not find the collection information");
        //            }

        //            Log.Debug("updating the picture");
        //            pic.Album_Id = newAlbumId == 0 ? (long?)null : newAlbumId;
        //            if (c != null)
        //            {
        //                c.Album_Id = pic.Album_Id;
        //            }

        //            utils.Commit();
        //            ts.Complete();
        //        }

        //        if (newAlbumId != 0)
        //        {
        //            return new EntityName { Name = album.Name, Id = newAlbumId };
        //        }
        //        else
        //        {
        //            return new EntityName { Id = 0, Name = Resource.DefaultAlbumName };
        //        }
        //    }
        //}

        public void CropPhoto(int xPosition, int yPosition, int dimension)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            Log.Info(string.Format("CropPhoto() is called, x: {0}, y: {1}, d: {2}", xPosition, yPosition, dimension));

            string name = username + "_temp.jpg";

            Log.Debug("Crop the photo");
            using (Bitmap bitmap = ImageHelper.GetPhoto(name))
            {
                using (ImageProcessor ip = new ImageProcessor(bitmap))
                {
                    ip.Crop(xPosition, yPosition, dimension, dimension);
                    ip.Save(ImageHelper.GetPhotoPath(string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"] + ".jpg"));
                }
            }

            GenerateProfilePhotoThumbnails(username);
        }

        private void GenerateProfilePhotoThumbnails(string username)
        {
            using (Bitmap bitmap = ImageHelper.GetPhoto(username + ".jpg"))
            {
                Log.Debug("Generating 180x180 thumbnail");
                using (ImageProcessor ip = new ImageProcessor(bitmap))
                {
                    ip.SquareThumbnail(180);
                    ip.Save(ImageHelper.GetPhotoPath(username + "_180_180.jpg"));
                }

                Log.Debug("Generating 80x80 thumbnail");
                using (ImageProcessor ip = new ImageProcessor(bitmap))
                {
                    ip.SquareThumbnail(80);
                    ip.Save(ImageHelper.GetPhotoPath(username + "_80_80.jpg"));
                }

                Log.Debug("Generating 50x50 thumbnail");
                using (ImageProcessor ip = new ImageProcessor(bitmap))
                {
                    ip.SquareThumbnail(50);
                    ip.Save(ImageHelper.GetPhotoPath(username + "_50_50.jpg"));
                }

                Log.Debug("Generating 30x30 thumbnail");
                using (ImageProcessor ip = new ImageProcessor(bitmap))
                {
                    ip.SquareThumbnail(30);
                    ip.Save(ImageHelper.GetPhotoPath(username + "_30_30.jpg"));
                }
            }
        }

        public PagingList<DiscussionToDisplay> FindMyDiscussions(int pageNumber)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("FindMyDiscussions() is called. current user: {0}, pagenumber: {1}", username, pageNumber));
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page(utils.FindAllDiscussions().Where(d => d.Username == username).OrderByDescending(d => d.Latest_Reply_Time), pageNumber, d => Converter.ConvertDiscussion(d, 1));
            }
        }

        public PagingList<DiscussionToDisplay> FindMyRepliedDiscussions(int pageNumber)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("FindMyRepliedDiscussions() is called. current user: {0}, pagenumber: {1}", username, pageNumber));
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page(utils.FindRepliedDiscussions(username).Distinct(), pageNumber, d => Converter.ConvertDiscussion(d, 1));
            }
        }

        public PagingList<AnnoucementToDisplay> FindMyAnnouncements(int pageNumber)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            Log.Info(string.Format("FindMyRepliedDiscussions() is called. current user: {0}, pagenumber: {1}", username, pageNumber));
            using (DataUtils utils = new DataUtils())
            {
                return PagingHelper.Page(utils.FindAllAnnouncements(), pageNumber, d => new AnnoucementToDisplay { Id = d.Id, PostTime = d.Post_Time, Text = d.Text });
            }
        }

        public string DownloadHtmlPage(string url)
        {
            return UrlHelper.DownloadHtmlPage(url);
        }

        public void AddInitialFavoriates(string[] usernames, long[] albumIds)
        {
            string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];

            Log.Info("AddInitialFavoriates() is called. Current user: " + username);
            using (DataUtils utils = new DataUtils())
            {
                foreach (string name in usernames)
                {
                    DB_Fan fan = new DB_Fan
                    {
                        Action_Time = DateTime.Now,
                        FansUsername = username,
                        WatchedUsername = name
                    };
                    utils.InsertFan(fan);
                }
                foreach (long albumId in albumIds)
                {
                    DB_Like like = new DB_Like
                    {
                        Album_Id = albumId,
                        Username = username
                    };
                    utils.InsertLike(like);
                }
            }
        }

        public string GenerateHeader(long collectionId)
        {
            string username = Helper.FindCurrentUsername();
            if (!username.Equals("l68952085485829", StringComparison.OrdinalIgnoreCase)) return null; // none admin calls

            using (DataUtils utils = new DataUtils(false))
            {
                View_Collection2 col = utils.FindViewCollection2(collectionId);
                if (col != null)
                {
                    string imagePath = ImageHelper.GetImagePath(col.Image_Filename, col.Image_File_Ext);
                    using (ImageProcessor ip = new ImageProcessor(ImageHelper.GetImage(col.Image_Filename, col.Image_File_Ext)))
                    {
                        ip.GenerateHeader();
                        ip.Save(ImageHelper.GetImagePath(col.Image_Filename + "_p", col.Image_File_Ext));
                    }
                    return ImageHelper.GetAccessPath(col.Image_Filename + "_p") + "." + col.Image_File_Ext;
                }
                return null;
            }
        }
    }
}
