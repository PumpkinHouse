﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Web;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;

namespace PumpkinHouse.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserService" in both code and config file together.
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        string Test();

        /// <summary>
        /// 在用微博登录状态下创建用户
        /// </summary>
        /// <param name="user"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void RegisterUser(UserForRegistration user);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void AssociateExternal(string email, string password, string external);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        bool RemoveAssociate(long id);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        IList<AccountAssociation> FindAccountAssociations();
        /// <summary>
        /// 获得当前用户信息
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        UserGeneralInfo FindUserInfo();

        /// <summary>
        /// 更新当前用户基本信息
        /// </summary>
        /// <param name="info"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void UpdateUserInfo(UserGeneralInfoToUpdate info);

        /// <summary>
        /// 更新当前用户介绍信息
        /// </summary>
        /// <param name="intro"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void UpdateUserSelfIntroduction(string intro);

        /// <summary>
        /// 更新是否接收系统邮件
        /// </summary>
        /// <param name="receiveEmail"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void UpdateReceiveEmail(bool receiveEmail);

        /// <summary>
        /// 更新邮箱
        /// </summary>
        /// <param name="email"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void UpdateUserEmail(string email);        

        /// <summary>
        /// 更改密码
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        bool ChangePassword(string oldPassword, string newPassword);

        /// <summary>
        /// 为指定图片添加标签（当前的逻辑是登录用户可为任意图片添加标签）
        /// </summary>
        /// <param name="pictureId"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        string AddCollectionTags(long collectionId, string tags);

        /// <summary>
        /// 为指定专辑添加标签（当前的逻辑是登录用户可为任意专辑添加标签-不包含未分类）
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        string AddAlbumTags(long albumId, string tags);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        string DeleteAlbumTag(long albumId, string tagToRemove);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        string DeleteCollectionTag(long collectionId, string tagToRemove);

        /// <summary>
        /// 添加一张图片
        /// </summary>
        /// <param name="picture"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        long UploadPicture(PictureToUpload picture);

        /// <summary>
        /// 更新图片简介
        /// </summary>
        /// <param name="pictureId"></param>
        /// <param name="description"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        string UpdatePictureDescription(long collectionId, string description);

        /// <summary>
        /// 创建专辑
        /// </summary>
        /// <param name="album"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        EntityName CreateAlbum(AlbumToCreate album);

        /// <summary>
        /// 创建多个专辑
        /// </summary>
        /// <param name="albumNames"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void CreateAlbums(string[] albumNames);


        /// <summary>
        /// 获取当前用户所有的专辑名
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        IList<EntityName> FindMyAlbumNames();

        /// <summary>
        /// 获取当前用户所有的私信讨论。（一个讨论是与指定用户的私信的集合，但只包括最近的一条及一些集合信息）
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<ThreadToDisplay> FindMyThreads(int pageNumber);

        /// <summary>
        /// 获取当前用户与指定用户的所有私信
        /// </summary>
        /// <param name="target"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<MessageToDisplay> FindMyMessages(string target, int pageNumber);

        /// <summary>
        /// 发送私信
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        MessageToDisplay SendMessage(MessageToSend message);

        /// <summary>
        /// 获得当前用户的动态
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<CollectionToDisplay> FindMyInterest(int pageNumber);

        /// <summary>
        /// 成为指定用户的粉丝
        /// </summary>
        /// <param name="username"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void Fan(string username);

        /// <summary>
        /// 取消成为指定用户的粉丝
        /// </summary>
        /// <param name="username"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void UnFan(string username);

        /// <summary>
        /// 喜欢指定专辑
        /// </summary>
        /// <param name="albumId"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void LikeAlbum(long albumId);

        /// <summary>
        /// 取消喜欢指定专辑
        /// </summary>
        /// <param name="albumId"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void CancelLikeAlbum(long albumId);

        /// <summary>
        /// 喜欢指定专辑
        /// </summary>
        /// <param name="collectionId"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        int LikePicture(long collectionId);

        /// <summary>
        /// 取消喜欢指定专辑
        /// </summary>
        /// <param name="collectionId"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        int CancelLikePicture(long collectionId);

        /// <summary>
        /// 更新指定专辑的名字
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="name"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void UpdateAlbumName(long albumId, string name);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void UpdateAlbumDescription(long albumId, string description);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void UpdateAlbum(long albumId, string name, string description);

        /// <summary>
        /// 删除指定专辑
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="deleteContent">是否删除其中内容？如否，所有内容将会移到未命名专辑</param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void DeleteAlbumById(long albumId, bool deleteContent);

        /// <summary>
        /// 收集图片
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="pictureId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        EntityName Collect(long albumId, long collectionId);

        /// <summary>
        /// 转移已收集的图片去指定专辑
        /// </summary>
        /// <param name="pictureId"></param>
        /// <param name="newAlbumId"></param>
        /// <returns></returns>
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        //EntityName MovePicture(long collectionId, long newAlbumId);

        /// <summary>
        /// 删除图片（不可恢复）
        /// </summary>
        /// <param name="pictureId"></param>
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        //void DeletePicture(long pictureId);

        /// <summary>
        /// 为用户上传的头像截取指定区域
        /// </summary>
        /// <param name="xPosition"></param>
        /// <param name="yPosition"></param>
        /// <param name="dimension"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void CropPhoto(int xPosition, int yPosition, int dimension);

        /// <summary>
        /// 发布回复
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        ReplyToDisplay PostReply(ReplyToPost post);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        List<long> DeleteReply(long replyId);

        /// <summary>
        /// 取消收集
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="pictureId"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void CancelCollect(long albumId, long pictureId);

        /// <summary>
        /// 取消收集
        /// </summary>
        /// <param name="albumId"></param>
        /// <param name="collectionId"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void DeletePicture(long collectionId);

        /// <summary>
        /// 获取提醒列表
        /// </summary>
        /// <param name="filter">过滤条件</param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<NoticeToDisplay> FindNotice(NoticeType filter, int pageNumber);

        /// <summary>
        /// 获取提醒（导航栏右上角的提醒）
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        NoticeSummary FindNoticeSummary();

        /// <summary>
        /// 发帖
        /// </summary>
        /// <param name="discussion"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void PostDiscussion(DiscussionToDisplay discussion);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void DeleteDiscussion(long discussionId);

        /// <summary>
        /// 获取当前用户所发所有的帖子
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<DiscussionToDisplay> FindMyDiscussions(int pageNumber);

        /// <summary>
        /// 获取当前用户所回复的所有帖子
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<DiscussionToDisplay> FindMyRepliedDiscussions(int pageNumber);

        /// <summary>
        /// 获取当前用户收到的所有通知
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        PagingList<AnnoucementToDisplay> FindMyAnnouncements(int pageNumber);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        string DownloadHtmlPage(string url);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void AddInitialFavoriates(string[] usernames, long[] albumIds);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        string GenerateHeader(long pictureId);
    }
}
