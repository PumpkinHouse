﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;
using PumpkinHouseDatabase.Contract;

namespace PumpkinHouse
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected string CurrentUser;
        protected string NickName;
        protected string Party;
        protected bool SurveyDone;
        protected string JsonMyAlbums = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentUser = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ?  "" : (string) HttpContext.Current.Session["username"];
            using (DataUtils utils = new DataUtils(false))
            {
                DB_User user = utils.FindUserByUsername(CurrentUser);
                if (user != null)
                {
                    NickName = user.Nick_Name;
                    SurveyDone = user.DB_Survey.Count() > 0;
                    IList<EntityName> names = Helper.FindMyAlbumNames(utils);
                    JsonMyAlbums = JsonHelper<IList<EntityName>>.ConvertToJson(names);
                }

                IList<byte> partyList = utils.FindAccountAssociation().Where(aa => aa.Username == CurrentUser).Select(aa => aa.Party).ToList();
                Party = string.Join(",", partyList);
            }

            if (!SurveyDone) {
                HttpCookie cookie = Request.Cookies.Get("survey");
                if (cookie != null)
                {
                    SurveyDone = true;
                }
            }
        }

        public void LoggedOut(Object sender, System.EventArgs e)
        {
            Session["Party"] = null;
        }
    }
}
