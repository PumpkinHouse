﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShopHome.aspx.cs" Inherits="PumpkinHouse.ShopHome" MasterPageFile="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
 <style> 
    .shopehome{ width:960px; height:auto; margin-top:30px;}
    .shop-ad{ width:960px; height:150px; background:#333;}
    .shopList-title{ width:960px; height:40px; color:#6d6e76; font-size:24px; font-family:'微软雅黑'; font-weight:100; margin-top:30px;}
    .shopList-title span{ display:block; float:left; height:40px; line-height:40px;}
    .shopList-title span.icon{ width:40px; height:40px; background:url(/img/icon-list.png) repeat-x 0px -313px; margin-right:10px;}
    .shopList{ width:960px; height:325px; margin-top:20px; overflow:hidden}
         .shopList .F_pic{ width:300px; height:325px;}
         .shopList .F_pic img{ width:300px; height:325px;}
         
         .shopList .C_shop{ width:655px; height:325px; overflow:hidden;}
         .shopList .C_shop .title{ width:605px; height:90px; background:#f1f1f1; padding:35px 25px; line-height:22px; color:#6d6e76; position:relative;}
         .shopList .C_shop .title .icon{width:30px; height:25px; background:url(/img/icon-list.png) repeat-x 0px -280px; position:absolute; left:6px; top:8px;}
         .shopList .C_shop dl{ width:656px; height:160px; margin-top:5px;} 
         .shopList .C_shop dl dd{ width:159px; height:158px; float:left; margin-right:5px; position:relative;}
         .shopList .C_shop dl dd .pic{ width:159px; height:158px; position:absolute; z-index:1; margin:0px; border:0px;}
         .shopList .C_shop dl dd .pic img{ width:159px; height:158px;}
         .shopList .C_shop dl dd .pannel{ width:143px; padding:0px 8px; height:25px; line-height:25px; color:#fff; background:url(/img/icon-list.png) repeat-x 0px -249px; position:absolute; left:1px; bottom:0px; z-index:22;} 
         
         .shopHome{ width:960px; height:auto; overflow:hidden;}
         .shopHome ul{ width:975px;}
         .shopHome ul li{ float:left; margin-right:15px; width:310px; height:255px; margin-bottom:30px; margin-top:10px; position:relative;}
         .shopHome ul li img{ width:310px; height:255px; position:absolute; z-index:1;}
         .shopHome ul li .Desc{ width:270px; height:162px; background:url(/img/opacity07black.png) repeat 0 0; position:absolute; top:0px; z-index:11; color:#fff; padding:20px; line-height:22px; text-indent:24px; display:none;}
 </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

<div class="shopehome">
    <div class="shop-ad hide"></div>
    <h1 class="shopList-title"><span class="icon"></span><span>小编淘店</span></h1>

    <div class="shopList">
        <div class="F_pic float_l">
            <a href="http://souphome.taobao.com/" target="_blank"><img src="/zt/ShopHome/20120710/a_1.jpg" /></a>
        </div>
        <div class="C_shop float_r">
            <div class="title">SOUP,是一份关于美好生活的浓淡可调的美味。用心体会生活，用心妆点生活，美好不仅限于物，还有内心。素朴，用美好的事物，让您体会生活的味道。<div class="icon"></div></div>
            <dl>
                <dd>
                    
                        <div class="pic"><a href="http://item.taobao.com/item.htm?id=14508171983&" target="_blank"><img src="/zt/ShopHome/20120710/a_2.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">鹅颈简约花瓶</span><span class="float_r">￥19.00</span>
                        </div>
                     
                </dd>
                <dd>
                    
                        <div class="pic"><a href="http://item.taobao.com/item.htm?id=17775988671&" target="_blank"><img src="/zt/ShopHome/20120710/a_3.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">紫砂盆景花盆</span><span class="float_r">￥24.00</span>
                        </div>
                     
                </dd>
                <dd>
                    
                        <div class="pic"><a href="http://item.taobao.com/item.htm?id=15184822324&" target="_blank"><img src="/zt/ShopHome/20120710/a_4.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">哑光棕色陶瓷花盆</span><span class="float_r">￥7.5 .00</span>
                        </div>
                     
                </dd>
                <dd>
                    
                        <div class="pic"><a href="http://item.taobao.com/item.htm?id=14941412187&" target="_blank"><img src="/zt/ShopHome/20120710/a_5.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">禅意古典陶瓷花瓶</span><span class="float_r">￥5.00</span>
                        </div>
                     
                </dd>
            </dl>
        </div>
    </div>

   <div class="shopList">
        <div class="F_pic float_r">
            <a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG69RGcaJPb63yl1mhTVNbXgQfI6vqZpTYPprxDG3n2QMjspQGNOT0NTuFBo4bE6xu225UlGa3yWasd2%2BLsqwX2iYjXLFzsXlpqWHjt8YLGCLHrLRyMN9QAPZ7I%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/b_1.jpg" /></a>
        </div>
        <div class="C_shop float_l">
            <div class="title">这家店集合了众多国际设计师的产品，风格中融汇北欧简约风格和自然主义精神，产品方面除家居用品主题以外复合了背包和服饰，其中可见大师深泽直人的产品系列,以及中国优秀设计师的限量首饰作品。<div class="icon"></div></div>
            <dl>
                <dd>
                    
                        <div class="pic"><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB07S4%2FK0CITy7klxn%2F7bvn0ayzfszCc1Dove4ka%2Fm8ar5kMlOiVETlL7kjyuZdIAdvPUA4LEWSa53wmyzzIrzdvk1aa3aH%2BOS0lH204OQ%2BSAmudiXeZY77X8ESL4Cdq1dJE76%2B5%2F0UbqscRIwNW&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/b_2.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">Nava情侣手表</span><span class="float_r">￥1480.00</span>
                        </div>
                     
                </dd>
                <dd>
                    
                        <div class="pic"><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB07S4%2FK0CITy7klxn%2F7bvn0ayzfszCc0agpyN%2B%2B63sxvA5NWYWb3F1JufZKvq06LhXG7TYDBohXDD98GymBNIvDqMM2UrZ%2F%2FQ4DW04ufcNVmtPh2PujzKxnR%2B2m4fOZt3XM4jwl2Awk03H4O5qY&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/b_3.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">创意厨房开瓶器</span><span class="float_r">￥228.00</span>
                        </div>
                     
                </dd>
                <dd>
                    
                        <div class="pic"><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB07S4%2FK0CITy7klxn%2F7bvn0ayzfszCT8GvlLaVlGstAlWzArGVeStcpNaW9IAUYDsujhUlvufozYVwlUby9GQ8c%2BYKY%2FgalXLbHOuaYdYFz7Zl%2B8P7vFtf0wwj%2BIQ8%2F%2FTkwGcZ3UQzDN6H98GjxWw%3D%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/b_4.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">数码相机</span><span class="float_r">￥1580.00</span>
                        </div>
                     
                </dd>
                <dd>
                    
                        <div class="pic"><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB07S4%2FK0CITy7klxn%2F7bvn0ayzfszCT8iI6GGTXRA8pF55oYjRetcQbBGEwTr04dwMqgoZRa1B1uGecQ98F8eOA29lvfq2l8p7D33c%2FK%2BuzC8cQ9tiSs7r8UPTYd8eOpAPcyEgQ7Hciv5XFQA9fzQ%3D%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/b_5.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">玫瑰花茶具</span><span class="float_r">￥450.00</span>
                        </div>
                     
                </dd>
            </dl>
        </div>
    </div>

   <div class="shopList">
        <div class="F_pic float_l">
            <a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG69RGcaJPb63yl1mhTVNbXvjsNT70fN5UOeuUKQyabdbejqoyeogXh8Vi%2B8CWQdbm%2BK4WzHcWjoQN9TnafFjmUPjAXZYnA7kmUxEFptSBlx2feN%2Bf1%2Bcn3hlxs%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/c_1.jpg" /></a>
        </div>
        <div class="C_shop float_r">
            <div class="title">雨季.花季——从一滴水珠看到一个世界， 从一朵野花看到一个天堂， 把握在你手心里的不是雨伞而是永恒；然而雨后，永恒也就消融于一个时辰,期待下一次邂逅......<div class="icon"></div></div>
            <dl>
                <dd>
                    
                        <div class="pic"><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB07S4%2FK0CITy7klxn%2F7bvn0ayzfszCSxbdKwUQ1ksN6V%2FC5o7Ja%2BP1k8LSIJMVPKWt3G1tjmCqytLHYDIEU6KFkHLCOXWBhf8300zT1ZFc1%2BtZv%2FFsF%2BHlK0Amxxb4vctbD4m0e6V57gPNS%2FOHjAw%3D%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0 " target="_blank"><img src="/zt/ShopHome/20120710/c_2.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">日单超轻长柄伞</span><span class="float_r">￥48.00</span>
                        </div>
                     
                </dd>
                <dd>
                    
                        <div class="pic"><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB07S4%2FK0CITy7klxn%2F7bvn0ayzfszCSw%2F0vBWcUh6Jfoj5Zecxg2BVZbv9Iv5UU42Cf%2F5YNRK51R3%2Bs93hueGhodEqtvgQeHZO%2FXweKgORU4lUnEGmtNQR6lGBMJKB%2Bzx66f8p45TEJ8A0D0BPznA%3D%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/c_3.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">遮阳公主伞</span><span class="float_r">￥78.00</span>
                        </div>
                     
                </dd>
                <dd>
                    
                        <div class="pic"><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB07S4%2FK0CITy7klxn%2F7bvn0ayzfszCSzwe3RIky1GHarKKjf1mE0SRPk1ZZDRG3QG%2BAn%2F4VMvnGp30Z6ri40gLDQ7XCCXDdDR1p9Hs6LEyoFiyGJVd%2Fd5xY%2FWhYy%2ByiqVcP14YU82B8JMtPzdaTJQ%3D%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/c_4.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">向日葵三折晴雨伞</span><span class="float_r">￥38.00</span>
                        </div>
                     
                </dd>
                <dd>
                    
                        <div class="pic"><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB07S4%2FK0CITy7klxn%2F7bvn0ayzfszErx18VJbE19n5KSXlbL8JU63oRO4yyV7g8jv4NAXk7FsnwLfN%2FuLbjg6kccuZXcUKXPkVtqMKNZ93774NhzfWtAtmWKNL7BPfRkBUVzlYLehq0KSsTm8vGng%3D%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/c_5.jpg" /></a></div>
                        <div class="pannel">
                            <span class="float_l">拉菲草帽子</span><span class="float_r">￥126.40</span>
                        </div>
                     
                </dd>
            </dl>
        </div>
    </div>

    <h1 class="shopList-title"><span class="icon"></span><span>店铺</span></h1>

 <script type="text/javascript">
     $(document).ready(function () {
         $('.shopHome ul li a').hover(function (targetObject) {
             var t = $(targetObject.target).parent();
             t.find('.Desc').show();
         }, function (targetObject) {
             var t = $(targetObject.target).parent();
             t.find('.Desc').hide();

         });
     });
 </script>

    <div class="shopHome">
        <ul>
            <li><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG3VSuWRIvnJbEpKV5LmPFr4IL5tTF8XjUOET9sKFIWzcR5jU8veaF1GELfZ8scXC%2B%2FOFOLQwqug6dXb%2Fss%2BPt3b3mYKAdEr2fB887YRQpTFU28YIwHTNkKuOnya&pid=mm_30981152_0_0&spm=2014.12579490.1.0"><img src="/zt/ShopHome/20120710/s_1.jpg" /><div class="Desc">清凉一夏，一壶菊花，一杯绿茶，沁人心脾，浅浅淡淡，清清爽爽。</div></a></li>
            <li><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG69RGcaJPb63yl1mhTVNbTTta%2FOjQ570KT8S8rECgxgsVnLrtpJfgH7k52HEflYSoQjcb7n1x9IP527bPBOGTZT65dT8cmswZkQSzR8gNUaiYfhHNpEjVQrEjKX&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/s_2.jpg" /><div class="Desc">看到这些美好的东西，会觉得喜欢极了，也一直痴迷于布置自己的家，我想，大家都会觉得，假如我有一个美丽的家，仿佛在普罗旺斯一般的家，那真是太幸福的事。</div></a></li>
            <li><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG3VSuWRIvnJbEpKV5LmOrpBKK1bIuAxUg5h4VxWqbVP9js3Kw4r0zH4Q4aYpG%2BWnfJ5HnUFyHOLUvWh21SughbH0mecwuN8YVAVHDZ%2B71xnlrbwcBJnBdko3YoF&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/s_3.jpg" /><div class="Desc">简约不失时尚的外观设计，低调却显华丽的色泽展现，让这款韩式田园床尽显艺术气息，将它摆放在卧室中，不仅是一件家具，更算得上一件赋有现代风格的艺术品！。</div></a></li>
            <li><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG69RGcaJPb63yl1mhTVNbTRyCYH8ObQh9IkEomWoutVCjie1iv5vngFBdw5LMibxP5X4szu5ixwhrniU9wkKjWJN270baWKlentoPIbT%2FCN%2F90m2uckBKdGK1Xr&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/s_4.jpg" /><div class="Desc">烘焙的过程充满着乐趣，充满着细腻的心思，充满着一种特别的爱，更是充满着幸福。这种幸福，是难以言表的，只有亲身去体验</div></a></li>
            <li><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG69RGcaJPb63yl1mhTVNbTVbFgAQCtH%2FkAJD3zLEIGlJ39oOXoo%2B3SFxM622Lc9wKkXChh7LCZ%2Ft8Z%2FGSDdEzAYLpo3AdBcdbOQNP5sN4wTU4wF0kHteG61aYE%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/s_5.jpg" /><div class="Desc">爱上生活，喜欢zakka,温馨，浪漫，潮流家居用品的你，还在等神马……</div></a></li>
            <li><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG69RGcaJPb63yl1mhTVNbTWoYGmOSMTv8645oeSitIc0QNUY9oPrQUnvueKqCph49ubHw0C%2BpEethJwsXOT9aJ%2BADQnl%2F8W8bUSF6fMSueU%2FZQd5E7k5PTFToE%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/s_6.jpg" /><div class="Desc">墙贴非常适合忙碌而追求品位精致生活的人，快节奏的生活一切讲究快捷简便，一个已雕刻好的漂亮图案只需把它贴在需要装饰的位置就行，比起请装修队伍来设计制作成本较高的装饰墙来说，真算是方便又实用。</div></a></li>
            <li><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG3VSuWRIvnJbEpKV5LmPFr7x8GJILWGnG33rZBwnmIq6Gux%2FOz1GeuRKb2aokVIlzzquxi2GXWBxR7QHh69MgNkgW9HdeGC%2BIBNiY5G0pwgT%2FwdYsHFF0ZEw5gC&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/s_7.jpg" /><div class="Desc">吉屋家的这款沙发布艺系列，创意的裙摆，花样的设计。尽情地舞动出大自然的色彩，酣畅、开怀。大片柔和纯色，搭配经典简朴方格图案，流露出惬意的田园感觉。</div></a></li>
            <li><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG69RGcaJPb63yl1mhTVNbTWoOryggeotqPQVqvUSZPeFNeCqLcknU74WJwrZEVnPIyY3TwTJGPGPBp9pLN9oiQpYL4F7Tv0GM8QL6LRwPi74lypKP0azp%2FhvRY%3D&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/s_8.jpg" /><div class="Desc">使用蔺草编织的产品，在夏季能保持适度的干燥，使人的皮肤感触异常舒适，具吸汗、安眠、透气、除臭功效，尤适用于幼儿及体虚人群。</div></a></li>
            <li><a href="http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB04MQzdgG69RGcaJPb63yl1mhTVNbTVaaQZIFqB7oGY7M181BIpUCWOhP7WKmLUhcoMOSjls4qESLf%2BdNX%2BHa1i%2Fm2W6PYxUpDyknTNlB5ndyHkWODi5Y9ZW6ZZihHnLFK7&pid=mm_30981152_0_0&spm=2014.12579490.1.0" target="_blank"><img src="/zt/ShopHome/20120710/s_9.jpg" /><div class="Desc">初夏的微风轻拂，在宜人的清晨醒来，爱的温度，风的轻盈，一切美好，在世界上最美的家！</div></a></li>
        </ul>
    </div>
</div>

</asp:Content>
