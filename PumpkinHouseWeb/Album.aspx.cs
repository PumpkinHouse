﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Utils;

namespace PumpkinHouse
{
    public partial class Album : System.Web.UI.Page
    {
        protected long AlbumId;
        protected string Username;
        protected AlbumToDisplay albumToDisplay;
        protected void Page_Load(object sender, EventArgs e)
        {
            bool success = long.TryParse(Request.Params["albumId"], out AlbumId);
            if (success)
            {
                using (DataUtils utils = new DataUtils(false))
                {
                    View_Album album = utils.FindViewAlbumById(AlbumId);
                    albumToDisplay = Converter.ConvertAlbum(album, utils, "", 10, 0);
                    if (album == null)
                    {
                        Response.Redirect("~/404.aspx");
                    }
                    else
                    {
                        Page.Title = album.Name + "盒子 - 木头盒子";
                    }
                }
            }
            else
            {
                // check whether it is uncategoried album
                Username = Request.Params["username"];
                if (Username == null)
                    Response.Redirect("~/404.aspx");
                else
                {
                    AlbumId = 0;
                    Page.Title = "未命名盒子 - 木头盒子";
                    using (DataUtils utils = new DataUtils(false))
                    {
                        DB_User user = utils.FindUserByUsername(Username);
                        albumToDisplay = Helper.GetDefaultAlbumInstance(utils, user, 3, 0);
                    }
                }
            }
        }
    }
}