﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NoticeAnnouncement.aspx.cs" Inherits="PumpkinHouse.NoticeAnnouncement"
  Title="通知 - 木头盒子" MasterPageFile="~/Notice/Notice.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="NoticeScriptContent" runat="server">
    <script language="javascript" type="text/javascript">
        function announcementSelectCallback(pageNumber, container) {
            ajaxGetNotice(
                true,
                16,
                0, renderAnnouncement, handleFault);
        }
        
        function renderAnnouncement(announceList) {
            renderTemplate('#noticeContent', '#announcementTemplate', announceList);
        }

        function initAnnouncementPagination(result) {
            pagination('#pagination', 0, 20, announcementSelectCallback);
            renderAnnouncement(result);
        }

        function doAction() {
            ajaxGetNotice(
                true,
                16,
                0, initAnnouncementPagination, handleFault);
        }
        
    </script>

<style>
.tagMenu ul li.tabStepMenu3 a,.tagMenu ul li.tabStepMenu3 a:hover{height:40px; background:#e1e1e1; color:#8AB506;}
</style>
</asp:Content>