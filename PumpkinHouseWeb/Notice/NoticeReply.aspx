﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NoticeReply.aspx.cs" Inherits="PumpkinHouse.NoticeReply"
 Title="回复 - 木头盒子" MasterPageFile="~/Notice/Notice.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="NoticeScriptContent" runat="server">
    <script language="javascript" type="text/javascript">
        function initReplyPagination(result) {
            pagination('#pagination', result.count, 20, replyPageSelectCallback);
            renderReply(result);
        }
        
        function replyPageSelectCallback(pageNumber, container) {
            ajaxGetNotice(
                true, 
                4, //         Collect = 1, Fan = 2, Like = 8
                pageNumber,
                renderReply,
                handleFault);
        }

        function renderReply(noticeList) {
            var t = renderTemplate('#noticeContent', '#noticeTemplate', noticeList.list);
            t.find('.replyBtn').on('click', function (e) {
                var target = $(e.target);
                var holder = target.parents('div.NoticeAboutMe').find('.replyHolder');

                var rd = $.data(holder[0], 'replyDialog');
                if (rd) {
                    rd.toggle();
                }
                else {
                    var rd = $('#replyDialogTemplate').tmpl({ username: globalLogginUser });
                    holder.append(rd);
                    $.data(holder[0], 'replyDialog', rd);
                    $('#replyBtn', rd).on('click', function (e) {
                        var text = $.trim($('#reply', rd).val());
                        if (text) {
                            var tempHolder = target.parents('li.li2');
                            var rid = tempHolder.attr('data-reply-id');

                            ajaxPostReply(true, new ReplyToPost(text, null, null, null, rid), function (result) {
                                $('#reply', rd).val('');
                                rd.hide();
                                var d = showSmallDialog('#actionCompleteTemplate', '.successDialog', '回复成功');
                                setTimeout(function () { d.children().hide(); }, 1000);
                            }, handleFault);
                        }
                        else {
                            blinkInput($('textarea#reply', rd));
                        }
                    });

                }

            });

            t.find('.deleteBtn').on('click', function (e) {
                var target = $(e.target);
                var rid = target.parents('li.li2').attr('data-reply-id');
                var d = showSmallDialog('#confirmDialogTemplate', '.confirmDialog', '确定要删除此回复？');
                $('#confirmHolder .confirm').on('click', function () {
                    ajaxDeleteReply(true, rid, doAction, handleFault);
                    d.children().hide();
                });
            });
        }

        function doAction() {
            ajaxGetNotice(
                true,
                4, //         Reply = 4
                0,
                initReplyPagination,
                handleFault);
        }
        
    </script>

<style>
.tagMenu ul li.tabStepMenu2 a,.tagMenu ul li.tabStepMenu2 a:hover{height:40px; background:#e1e1e1; color:#8AB506;}
</style>
<!-- #Include virtual="/template/replyTemplate.html" -->
</asp:Content>