﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NoticeAboutMe.aspx.cs" Inherits="PumpkinHouse.NoticeAboutMe" MasterPageFile="~/Notice/Notice.master" 
 Title="提醒 - 木头盒子"%>
<asp:Content ID="Content1" ContentPlaceHolderID="NoticeScriptContent" runat="server">
    <script language="javascript" type="text/javascript">
        function initNoticePagination(result) {
            pagination('#pagination', result.count, 20, noticePageSelectCallback);
            renderNotice(result);
        }
        
        function renderNotice(noticeList) {
            renderTemplate('#noticeContent', '#noticeTemplate', noticeList.list);
        }

        function noticePageSelectCallback(pageNumber, container) {
            ajaxGetNotice(
                true,
                11, //         Collect = 1, Fan = 2, Like = 8
                pageNumber,
                renderNotice,
                handleFault);
        }
        function doAction () {
            ajaxGetNotice(
                true,
                11, //         Collect = 1, Fan = 2, Like = 8
                0,
                initNoticePagination,
                handleFault);
        }
        
    </script>

<style>
.tagMenu ul li.tabStepMenu1 a,.tagMenu ul li.tabStepMenu1 a:hover{height:40px; background:#e1e1e1; color:#8AB506;}
</style>
</asp:Content>