﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PumpkinHouse
{
    public partial class Message : System.Web.UI.Page
    {
        protected string TargetUsername;
        protected string Username ; 

        protected void Page_Load(object sender, EventArgs e)
        {
            Username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"]; 
            TargetUsername = Request.Params["target"];

            if (string.IsNullOrEmpty(TargetUsername))
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }
}