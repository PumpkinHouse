﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Album.aspx.cs" Inherits="PumpkinHouse.Album"
    MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

    <script type="text/javascript">
    var globalAlbumId = <%= AlbumId %>;
    var globalUsername;
    if (globalAlbumId === 0) {
        globalUsername = '<%= Username %>';
    }

    var globalAlbum;
    var globalUser;
    var globalIsMe;
    var globalPageNumber = 0;
    var globalReplyCol;
    </script>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <asp:ScriptManagerProxy ID="ScriptManager2" runat="server">
        <CompositeScript>
            <Scripts>
                <asp:ScriptReference Path="/Scripts/reply.js" />
                <asp:ScriptReference Path="/Scripts/albumInclude.js" />
                <asp:ScriptReference Path="/Scripts/album.js" />
                <asp:ScriptReference Path="/Scripts/likePicture.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManagerProxy>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<style>
.myOnlyBoxPic{ padding-bottom:5px;}
</style>
<div class="PictureCon">
    <div class="PictureLeft float_r">
        <div id="userInfo"></div>
        <div id="advHolder"></div>
    </div>
    <div class="PictureRight float_l clearfix" style="border: 0px solid #aaaaaa; background: none;">
        <div id="info"></div>
        <div class="PublicViewCon clearfix">
                <ul class="PublicList clearfix" id="gallery">
                </ul>
        </div>
        <div id="paginationHolder">
        </div>

    </div>
</div>
<div class="hide" style="display:none">
    <h2><%= Server.HtmlEncode(albumToDisplay.Name)%></h2><span><%= albumToDisplay.NumberOfLike%>人喜欢</span>
    <div><%= Server.HtmlEncode(albumToDisplay.Description)%></div>
    <a href="/user/<%= albumToDisplay.Username %>"><%= Server.HtmlEncode(albumToDisplay.NickName)%></a>

    <% foreach (var picture in albumToDisplay.Pictures.List)
       { %>
        <a href="/picture/<%= picture.Id %>">
            <img class="preview" alt="<%= Server.HtmlEncode(picture.Description) %>" src="<%=picture.Url %>_200_0_thumbnail.<%= picture.Extention %>" />
            <div>
                <%= Server.HtmlEncode(picture.Description)%></div>
            <div>
                <%= Server.HtmlEncode(picture.Tags)%></div>
        </a>
         <% if (picture.AlbumId == 0)
           { %>
        <a href="/album/default/<%= picture.Username %>">
            <%= Server.HtmlEncode(picture.AlbumName) %></a>
        <% }
           else
           { %>
        <a href="/album/<%= picture.AlbumId %>">
            <%= Server.HtmlEncode(picture.AlbumName)%></a>
        <% } %>
        <span><%= picture.UploadTime %></span>
    <% } %>
</div>
<!-- #Include virtual="/template/albumTemplate.html" -->
<!-- #Include virtual="/template/tagTemplate.html" -->
<!-- #Include virtual="/template/userTemplate.html" -->
<!-- #Include virtual="/template/collectTemplate.html" -->
<!-- #Include virtual="/template/albumThumbnailTemplate.html" -->
</asp:Content>
