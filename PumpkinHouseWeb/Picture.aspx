<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Picture.aspx.cs" Inherits="PumpkinHouse.Picture"
    MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        var globalPictureId = '<%= CollectionId %>';

        var globalPicture;
        var globalTagDialog;
        var globalTags;
        var globalTagHolder;
        var globalUser;
        var globalUsername;
        var globalIsMe;
        var globalMyAlbums;
        var globalMoveTarget; /* 'pic' means the move action is to move a pic to another album; 'col' means to move a collection */
        var globalReplyCol;
        var jsonPicture = '<%= JsonPicture %>';
        var jsonUser = '<%= JsonUser %>';
    </script>
    <style>
        img.icon {
            width: 16px;
            height: 16px;
            background: url(http://img.t.sinajs.cn/t35/appstyle/opent/images/app/btn_trans.gif?id=201104131414) 0 0;
            background-position: 0 -56px;
                        border: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <asp:ScriptManagerProxy ID="ScriptManager2" runat="server">
        <CompositeScript>
            <Scripts>
               <asp:ScriptReference Path="/Scripts/reply.js" />
               <asp:ScriptReference Path="/Scripts/picture.js" />
               <asp:ScriptReference Path="/Scripts/likePicture.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManagerProxy>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

<div class="hide" style="display:none">
    <div>
        <a href="/picture/%= picture.Id %>" target="_blank">
               <img class="preview" src="<%= picture.Url %>.<%= picture.Extention %>" alt="<%= Server.HtmlEncode(picture.Description) %>" />
        </a>
    <div><%= Server.HtmlEncode(picture.Description) %></div>
    <div><%= Server.HtmlEncode(picture.Tags) %></div>
    </div>
    <a href="/user/<%= picture.Username %>"><%= Server.HtmlEncode(picture.NickName) %></a>
            <% if (picture.AlbumId == 0)
           { %>
        <a href="/album/default/<%= picture.Username %>">
            <%= Server.HtmlEncode(picture.AlbumName)%></a>
        <% }
           else
           { %>
        <a href="/album/<%= picture.AlbumId %>">
            <%= Server.HtmlEncode(picture.AlbumName)%></a>
        <% } %>

    <% foreach (var reply in picture.ReplyCollection.List)
       { %>
       <div>
           <a href="/user/<%= reply.NickName %>"><%= reply.Username %></a>
           <%= Server.HtmlEncode(reply.Content) %>
       </div>
    <% } %>
</div>

<div class="PictureCon">
    <div class="PictureLeft float_r" style="background:none; border:none;">               
        <div id="myAlbumHolder" class="inBox-randomLike">
        
        </div>
    </div>

    <div class="PictureRight float_l clearfix" id="pictureHolder">
            <div id="userInfoHolder">
            </div> 
    </div>
   
    <div class="Picture-Bottom-album float_l clearfix" id="originalAlbumHolder">
    </div>

</div>
<!-- #Include virtual="/template/tagTemplate.html" -->
<!-- #Include virtual="/template/userTemplate.html" -->
<!-- #Include virtual="/template/pictureTemplate.html" -->
<!-- #Include virtual="/template/replyTemplate.html" -->
<!-- #Include virtual="/template/albumThumbnailTemplate.html" -->
<!-- #Include virtual="/template/collectTemplate.html" -->
</asp:Content>
