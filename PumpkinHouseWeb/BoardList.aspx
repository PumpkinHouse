﻿<%@ Page Language="C#" Title="木头盒子交流讨论组 - 木头盒子" AutoEventWireup="true" CodeBehind="BoardList.aspx.cs" Inherits="PumpkinHouse.BoardList" MasterPageFile="~/Site.Master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

        function renderBoards(boardList) {
            renderTemplate('#boardList', '#discussionBoardListTemplate', boardList);
        }

        function renderDiscussions(discussionList) {
            renderTemplate('#lastestDiscussion', '#discussionListTemplate', discussionList);
        }

        function doAction() {
            ajaxGetAllBoards(true, renderBoards, handleFault);
            ajaxGetLatestDiscussions(true, renderDiscussions, handleFault);
            renderTemplate('#ctrlPanel', '#discussionControlPanelTemplate', null);
            
        }
    
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div class="BoardList">
	<div class="BoardLeft float_l">
    	<div class="BoardTheme clearfix" id="boardList">
            
        </div>
        <div class="NewSpeech" id="lastestDiscussion">
            
        </div>
    </div>
    <div class="BoardRight float_r" id="ctrlPanel">

    </div>
</div>
<!-- #Include virtual="/template/discussionTemplate.html" -->
</asp:Content>
