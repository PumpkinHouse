﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using PumpkinHouse.Utils;
using PumpkinHouseDatabase;

namespace PumpkinHouse
{
    public partial class Collect : System.Web.UI.Page
    {
        protected string ImageUrl;
        protected string ImageName;
        protected string ErrorMessage;
        protected string OriginalPageUrl;
        protected string OriginalTitle;
        protected string OriginalImageName;
        protected string Party;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = Request.Params["img"];
            string src = Request.Params["src"];
            OriginalImageName = url;

            url = UrlHelper.ProcessUrl(url, src);
            if (url == null)
            {
                Response.Redirect("~/404.aspx");
            }

            try
            {
                //ImageName = ImageHelper.DownloadFile(url);
                //ImageUrl = ImageHelper.GetAccessPath(ImageName);
                ImageUrl = url;
                OriginalPageUrl = src;
                OriginalTitle =  Request.Params["t"].Replace("\'", @"\'");
                using (DataUtils utils = new DataUtils(false))
                {
                    IList<byte> partyList = utils.FindAccountAssociation().Where(aa => aa.Username == Helper.FindCurrentUsername()).Select(aa => aa.Party).ToList();
                    Party = string.Join(",", partyList);
                }
            }
            catch (ImageTooLargeException)
            {
                ErrorMessage = "这张图片太大了！";
            }
            catch (InvalidFileTypeException)
            {
                ErrorMessage = "对不起，暂不支持此文件格式";
            }
        }
    }
}