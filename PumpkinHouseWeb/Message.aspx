﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Message.aspx.cs" Inherits="PumpkinHouse.Message" MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<script type="text/javascript">
    var globalTarget = '<%= TargetUsername %>';
    var globalUsername = '<%= Username %>';
    var globalMessages;

    function sendSuccess(msg) {
        // jump to first page
        $("#pagination").trigger('setPage', [0]);
        $('#reply').val('');
    }

    function initMessagePaging(messages) {
        globalMessages = messages;

        var num_entries = messages.count;
        // Create pagination element
        pagination("#pagination", num_entries, 20, pageselectCallback);

        // render message holder and send message textarea
        var t = $('#messageListTemplate').tmpl({ target: messages.list[0].target, me: messages.list[0].me });
        $('#messageHolder').empty().append(t);        

        // bind event on send button
        $('#sendMsgBtn').on('click', function (e) {
            var text = $('#reply').val();
            var msg = new MessageToSend(globalUsername, globalTarget, text);
            ajaxSendMessage(true, msg, sendSuccess, handleFault);
        });

        showPage(messages);
    }

    function showPage(messages) {
        var t = $('#messageDetailTemplate').tmpl(messages.list);
        $('#messages').empty().append(t);

    }

    function pageselectCallback(pageIndex, container) {
        ajaxGetMyMessages(true, globalTarget, pageIndex, showPage, handleFault);
    }

    function doAction () {
        ajaxGetMyMessages(true, globalTarget, 0, initMessagePaging, handleFault);
    };
</script>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div id="messageHolder"></div>
    <div id="pagination"></div>

<!-- #Include virtual="/template/messageTemplate.html" -->
</asp:Content>