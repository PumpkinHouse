﻿<%@ Page Language="C#" Title="发表话题 - 木头盒子" AutoEventWireup="true" CodeBehind="NewDiscussion.aspx.cs" Inherits="PumpkinHouse.NewDiscussion" MasterPageFile="~/Site.Master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function doAction() {
            var boardList = getAllBoardNamesSync(handleFault);
            
            renderTemplate('#postNewPanel', '#newDiscussionTemplate', { boardList: boardList });
            $('#subject').watermark('请输入标题');
            $('textarea#content').watermark('珍爱ID，规矩你懂的。乱说话，盒子会不高兴的');

            $('#newDiscussionForm').validate({ 
                messages: {
                    subject: {
                        required: '请输入标题',
                    },
                    content: {
                        required: '内容不能为空'
                    }
                },
                debug: true,
                submitHandler: function () {
                    var id = $('#boardList').val();
                    var subject = $.trim($('#subject').val());
                    var content = $.trim($('#content').val());
                    var discussion = new DiscussionToDisplay({ subject: subject, text: content, boardId: id });
                    ajaxPostDiscussion(true, discussion, function() { 
                        document.location = "/DiscussionBoard.aspx?boardId=" + id 
                    }, function (errorCode) {
                        handleSensitiveWord(errorCode, '抱歉，标题包含非法字符，请修改后再发布。'); 
                    });                    
                } 
            });
        }
    
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

<div class="BoardList">
    <div class="siteLink clearfix float_l"><a href=BoardList.aspx>讨论组首页</a> > 发表话题</div>
	<div class="BoardLeft float_l">
    	<div class="NewDiscussion clearfix" id="postNewPanel">
            
        </div>
    </div>
    <div class="BoardRight float_r">
         <div class="ftopic"><a href="MyDiscussions.aspx?mode=owner">&nbsp;>我发起的话题</a></div>
         <div class="ftopic"><a href="MyDiscussions.aspx?mode=replier">&nbsp;>我回复的话题</a></div>
    </div>
</div>
<!-- #Include virtual="/template/discussionTemplate.html" -->

</asp:Content>

