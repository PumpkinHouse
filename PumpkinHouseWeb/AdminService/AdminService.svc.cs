﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using PumpkinHouseDatabase;

namespace PumpkinHouse.AdminService
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AdminService : IAdminService
    {
        public void DoWork()
        {
        }

        public void RandomPictureSeed()
        {
            using (DataUtils utils = new DataUtils())
            {
                Random r = new Random(DateTime.Now.Millisecond);
                utils.FindAllPictures(null, false).Where(p => p.DB_Collection.Count() > 5).ToList().ForEach(p => p.Seed = r.Next(1, 10000));
                utils.Commit();
            }
        }
    }
}
