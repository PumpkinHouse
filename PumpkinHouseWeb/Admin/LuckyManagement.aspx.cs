﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;

namespace PumpkinHouse.Admin
{
    public partial class LuckyManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (DataUtils utils = new DataUtils(false))
            {
                DB_Activity activity = utils.FindActivities().Where(a => a.Id == Constants.CurrentActivityId).SingleOrDefault();
                lblCurrentDisplayDate.Text = activity.Update_Date.ToString("yyyy-MM-dd");
            }
        }

        protected void btnAddLucky_Click(object sender, EventArgs e)
        {
            string nickname = textNewUser.Text.Trim();
            lblResult.Text = "";

            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByNickname(nickname);
                if (user == null)
                {
                    lblResult.Text = "找不到用户！";
                    return;
                }
                else
                {
                    DB_Lucky l = new DB_Lucky { Username = user.Username, Activity_Id = Constants.CurrentActivityId, Date = DateTime.Now.Date };
                    utils.InsertLucky(l);
                    GridView1.DataBind();
                }
            }
        }

        protected void btnUpdateActivity_Click(object sender, EventArgs e)
        {
            using (DataUtils utils = new DataUtils())
            {
                DB_Activity activity = utils.FindActivities().Where(a => a.Id == Constants.CurrentActivityId).SingleOrDefault();
                if (activity != null)
                {
                    activity.Update_Date = DateTime.Now.Date;
                    utils.Commit();
                }
            }
        }
    }
}