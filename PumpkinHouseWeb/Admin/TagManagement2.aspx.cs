﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using System.Collections;

namespace PumpkinHouse.Admin
{
    public partial class TagManagement2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                Dictionary<string, int> pictureTagDict = new Dictionary<string, int>();
                Dictionary<string, int> albumTagDict = new Dictionary<string, int>();

                using (DataUtils utils = new DataUtils())
                {
                    IList<string> picTags = utils.FindAllPictures(null, false).Where(p => p.Tags != null).Select(p => p.Tags).ToList();
                    calc(pictureTagDict, picTags);

                    IList<string> albumTags = utils.FindAlbums().Where(p => p.Tags != null).Select(alb => alb.Tags).ToList();
                    calc(albumTagDict, albumTags);
                }
                
                PictureTagListGridView.DataSource = pictureTagDict.OrderByDescending(k => k.Value);
                PictureTagListGridView.DataBind();

                AlbumTagListGridView.DataSource = albumTagDict.OrderByDescending(k => k.Value);
                AlbumTagListGridView.DataBind();
            }
        }

        private static void calc(Dictionary<string, int> dict, IList<string> tags)
        {
            foreach (var tag in tags)
            {
                string[] split = tag.Split(' ');
                foreach (var s in split)
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        if (dict.ContainsKey(s))
                        {
                            dict[s]++;
                        }
                        else
                        {
                            dict[s] = 1;
                        }
                    }
                }
            }
        }
    }
}