﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShopStat.aspx.cs" Inherits="PumpkinHouse.Admin.ShopStat" MasterPageFile="~/Admin/Admin.Master" %>

<asp:Content ContentPlaceHolderID="MainPageHolder" ID="mainPage" runat="server">
    <form id="Form1" runat="server">
    <asp:Button ID="StartCalcBtn" Text="点击开始计算" runat="server" OnClick="StartCalc" />
    <p></p>

    <b>抓取商品统计</b>
    <asp:GridView ID="ShopGridView" runat="server" AutoGenerateColumns="false">
         <Columns>
            <asp:BoundField DataField="Code" HeaderText="代号" ReadOnly="True" />
            <asp:BoundField DataField="Name" HeaderText="电商" ReadOnly="True" />
            <asp:BoundField DataField="Lt50" HeaderText="<=50" ReadOnly="True" />
            <asp:BoundField DataField="Btw50N100" HeaderText="50 - 100" ReadOnly="True" />
            <asp:BoundField DataField="Btw100N200" HeaderText="100 - 200" ReadOnly="True" />
            <asp:BoundField DataField="Btw200N300" HeaderText="200 - 300" ReadOnly="True" />
            <asp:BoundField DataField="Btw300N500" HeaderText="300 - 500" ReadOnly="True" />
            <asp:BoundField DataField="Gt500" HeaderText="> 500" ReadOnly="True" />
            <asp:BoundField DataField="NoPrice" HeaderText="无法抓取" ReadOnly="True" />
            <asp:BoundField DataField="Total" HeaderText="总数" ReadOnly="True" />
        </Columns>
    </asp:GridView>
    <p></p>
    </form>
</asp:Content>
