﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FilterStat.aspx.cs" Inherits="PumpkinHouse.Admin.FilterStat"
    MasterPageFile="~/Admin/Stat.Master" %>

<asp:Content ContentPlaceHolderID="StatPanel" ID="mainPage" runat="server">
    <asp:Button ID="StartCalcBtn" Text="点击开始计算" runat="server" OnClick="StartCalc" />
    <p>
    </p>
    <b>首页过滤器使用情况</b><p>
    </p>
    <b>展示模式</b>
    <asp:GridView ID="ModeGridView" runat="server" AutoGenerateColumns="true"
        runat="server">
    </asp:GridView>
    <p></p>
    <b>标签使用</b>
    <asp:GridView ID="TagGridView" runat="server" AutoGenerateColumns="true"
        runat="server">
    </asp:GridView>
    <p></p>
    <b>只看商品</b>
    <asp:GridView ID="CommodityGridView" runat="server" AutoGenerateColumns="true"
        runat="server">
    </asp:GridView>
    <p></p>
</asp:Content>
