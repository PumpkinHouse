﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Admin
{
    public partial class AnnouncementManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                string username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
                string txt = textAnnouncement.Text;
                lblStatus.InnerText = "处理中";
                submit.Enabled = false;
                using (DataUtils utils = new DataUtils())
                {
                    DB_Announcement announcement = new DB_Announcement
                    {
                        Post_Time = DateTime.Now,
                        Text = txt,
                        Admin_Username = username
                    };

                    announcement = utils.InsertAnnouncement(announcement);

                    List<string> users = utils.FindUsers().Select(u => u.Username).ToList();
                    foreach (string uname in users)
                    {
                        DB_Notice notice = new DB_Notice
                        {
                            Action_Username = username,
                            Announcement_Id = announcement.Id,
                            Has_Read = 0,
                            Username = uname,
                            Notice_Time = DateTime.Now,
                            Notice_Type = (byte)NoticeType.Announcement
                        };
                        utils.InsertNotice(notice);
                    }
                }
                textAnnouncement.Text = "";
                lblStatus.InnerText = "创建成功！";
                submit.Enabled = true;

            }
        }
    }
}