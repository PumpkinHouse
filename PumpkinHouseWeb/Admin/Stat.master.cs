﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Admin
{
    public partial class Stat : System.Web.UI.MasterPage
    {
        public global::System.Web.UI.WebControls.TextBox DateStart;
        public global::System.Web.UI.WebControls.TextBox DateEnd;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime today = DateTime.Now;
                DateTime weekago = today.AddDays(-7);

                CalendarStart.SelectedDate = weekago;
                DateStart.Text = CalendarStart.SelectedDate.ToString("yyyy/MM/dd");

                CalendarEnd.SelectedDate = today;
                DateEnd.Text = CalendarEnd.SelectedDate.ToString("yyyy/MM/dd");
            }
        }

        protected void DateStartChanged(object sender, EventArgs e)
        {
            DateStart.Text = ((Calendar)sender).SelectedDate.ToString("yyyy/MM/dd");
        }

        protected void DateEndChanged(object sender, EventArgs e)
        {
            DateEnd.Text = ((Calendar)sender).SelectedDate.ToString("yyyy/MM/dd");
        }

        public List<DB_User_Action> FindAction(DataUtils utils, UserActionType type)
        {
            DateTime start = DateTime.Parse(DateStart.Text);
            DateTime end = DateTime.Parse(DateEnd.Text).AddDays(1);
            List<DB_User_Action> actions = utils.FindUserAction().Where(a => a.Action_Time >= start && a.Action_Time <= end).Where(a => a.Action_Type == (byte)type).ToList();
            return actions;
        }
    }
}