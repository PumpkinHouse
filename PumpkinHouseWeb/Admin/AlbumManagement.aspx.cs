﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PumpkinHouse.Admin
{
    public partial class AlbumManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void FilterAlbum()
        {
            string name = txtSearchAlbum.Text.Trim();
            if (!string.IsNullOrEmpty(name))
            {
                SqlDataSource1.FilterExpression = "Name = '" + name + "'";
                GridView1.DataBind();
            }
        }
        
        protected void btnSearchAlbum_Click(object sender, EventArgs e)
        {
            FilterAlbum();
        }

        protected void OnUpdating(object sender, GridViewUpdateEventArgs e)
        {
            FilterAlbum();
        }

        protected void OnDateChanged(object sender, EventArgs e)
        {
            FilterAlbum();

        }

        protected void PageIndexChanged(object sender, EventArgs e)
        {
            FilterAlbum();

        }

        protected void Sorted(object sender, EventArgs e)
        {
            FilterAlbum();

        }
        protected void OnEditing(object sender, GridViewEditEventArgs e)
        {
            FilterAlbum();
        }

        protected void SqlDataSource1_Selected(object sender, SqlDataSourceStatusEventArgs e)
        {
            lblCount.Text = e.AffectedRows.ToString();
        }
    }
}