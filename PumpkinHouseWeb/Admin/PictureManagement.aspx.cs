﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;
using System.Data;

namespace PumpkinHouse.Admin
{
    public partial class PictureManagement : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime start = DateTime.Parse("2012/1/1");
                DateTime today = DateTime.Now;

                CalendarStart.SelectedDate = start;

                CalendarEnd.SelectedDate = today;

                BindDataSource();
            }
        }

        protected void BindDataSource()
        {
            //            SqlDataSource1.SelectCommand =
            //                    @"SELECT [Id], [Description], [Upload_Time], [Tags], u.[Username], [Image_Filename], [Image_File_Ext], [Nick_Name], [On_Top]
            //                    FROM [DB_Picture] d, [DB_User] u 
            //                    where d.Username = u.Username ";
            //            SqlDataSource1.UpdateCommandType = SqlDataSourceCommandType.Text;
            //            SqlDataSource1.UpdateCommand = "Update DB_Picture set Description = @Description, Tags = @Tags, On_Top = @On_Top where Id = @Id";

            //            if (!string.IsNullOrEmpty(textNickName.Text))
            //            {
            //                string name = textNickName.Text.Trim();
            //                SqlDataSource1.SelectCommand += " and u.Nick_Name = '" + name + "'";
            //            }
            //            if (!string.IsNullOrEmpty(textPictureId.Text))
            //            {
            //                long id = 0;
            //                bool success = long.TryParse(textPictureId.Text, out id);
            //                if (success)
            //                {
            //                    SqlDataSource1.SelectCommand += " and d.Id = " + id;
            //                }
            //            }

            SqlDataSource1.FilterExpression = string.Format("(Nick_Name LIKE '{0}' AND Collect_Time >= '{1}' and Collect_Time <= '{2}') ",
    textNickName.Text, CalendarStart.SelectedDate.ToString("yyyy-MM-dd"), CalendarEnd.SelectedDate.AddDays(1).ToString("yyyy-MM-dd"));

            if (!string.IsNullOrWhiteSpace(textPictureId.Text))
            {
                SqlDataSource1.FilterExpression += "and Id = " + textPictureId.Text;
            }

            GridView1.DataBind();

        }


        protected void btnFilter_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textNickName.Text))
            {
                textNickName.Text = "%";
            }
            BindDataSource();
        }

        protected void OnUpdating(object sender, GridViewUpdateEventArgs e)
        {
            using (DataUtils utils = new DataUtils())
            {
                long id = (long)e.Keys[0];
                DB_Collection2 pic = utils.FindCollection2(id);
                if (pic != null)
                {
                    pic.Tags = e.NewValues["Tags"] as string;
                    pic.Description = e.NewValues["Description"] as string;
                    if (e.NewValues["On_Top"].Equals("1") && (e.OldValues["On_Top"] == null || e.OldValues["On_Top"].Equals("0")))
                    {
                        pic.On_Top = 1;
                        pic.On_Top_Update_Time = DateTime.Now;
                    }
                    if (e.NewValues["On_Top"].Equals("0") && e.OldValues["On_Top"].Equals("1"))
                    {
                        pic.On_Top = 0;
                        pic.On_Top_Update_Time = null;
                    }
                    utils.Commit();
                }
            }
            BindDataSource();
        }

        protected void OnDelete(object sender, GridViewDeleteEventArgs e)
        {
            e.Cancel = true;

            long id = (long)e.Keys[0];
            using (DataUtils utils = new DataUtils())
            {
                DB_Collection2 col = utils.FindCollection2(id);
                if (col != null)
                {
                    Operation.DeleteCollection(utils, col);
                }
            }

            BindDataSource();
        }

        protected void OnDateChanged(object sender, EventArgs e)
        {
            BindDataSource();
        }

        protected void PageIndexChanged(object sender, EventArgs e)
        {
            BindDataSource();
        }

        protected void Sorted(object sender, EventArgs e)
        {
            BindDataSource();
        }

        protected void OnEditing(object sender, GridViewEditEventArgs e)
        {
            BindDataSource();
        }

        protected void SqlDataSource1_Selected(object sender, SqlDataSourceStatusEventArgs e)
        {
            lblCount.Text = e.AffectedRows.ToString();
        }

        protected void deleteSelected_Click(object sender, EventArgs e)
        {
            using (DataUtils utils = new DataUtils())
            {
                for (int i = 0; i <= GridView1.Rows.Count - 1; i++)
                {
                    CheckBox CheckBox = (CheckBox)GridView1.Rows[i].FindControl("CheckBox");
                    if (CheckBox.Checked)
                    {
                        long id = long.Parse(GridView1.Rows[i].Cells[2].Text);
                        DB_Collection2 col2 = utils.FindCollection2(id);
                        Operation.DeleteCollection(utils, col2);
                    }
                }
            }
            BindDataSource();

        }

        protected void selectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i <= GridView1.Rows.Count - 1; i++)
            {
                CheckBox CheckBox = (CheckBox)GridView1.Rows[i].FindControl("CheckBox");
                if (this.selectAll.Checked == true)
                {
                    CheckBox.Checked = true;
                }
                else
                {
                    CheckBox.Checked = false;
                }
            }
        }
    }
}