﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Admin
{
    public partial class UserManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (DataUtils utils = new DataUtils(false))
                {
                    lblUserCount.Text = utils.FindUsers().Count().ToString();
                }
            }
        }
        
        private void FilterUser()
        {
            string name = txtUsername.Text.Trim();
            if (!string.IsNullOrEmpty(name))
            {
                SqlDataSource1.FilterExpression = "Nick_Name = '" + name + "'";
                GridView1.DataBind();
            }
        }

        protected void OnUpdating(object sender, GridViewUpdateEventArgs e)
        {
            FilterUser();
        }

        protected void OnDateChanged(object sender, EventArgs e)
        {
            FilterUser();

        }

        protected void PageIndexChanged(object sender, EventArgs e)
        {
            FilterUser();

        }

        protected void Sorted(object sender, EventArgs e)
        {
            FilterUser();

        }

        protected void OnEditing(object sender, GridViewEditEventArgs e)
        {
            FilterUser();
        }

        protected void btnSearchUser_Click(object sender, EventArgs e)
        {
            FilterUser();
        }
    }

}