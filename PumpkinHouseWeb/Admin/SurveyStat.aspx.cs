﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Admin
{
    public partial class SurveyStat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void StartCalc(object sender, EventArgs e)
        {
            Stat master = (Stat)Master;
             DateTime start = DateTime.Parse(master.DateStart.Text);
            DateTime end = DateTime.Parse(master.DateEnd.Text).AddDays(1);
            using (DataUtils utils = new DataUtils(false))
            {
                IEnumerable<DB_Survey> list = utils.FindSurveys().Where(s => s.Time > start && s.Time < end).OrderByDescending(s => s.Id);
                surveyGridView.DataSource = list;
                surveyGridView.DataBind();

                likeCount.Text = "" + list.Where(s => s.Like == 1).Count();
                dislikeCount.Text = "" + list.Where(s => s.Like == 0).Count();
            }

        }
    }
}