﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="PumpkinHouse.Admin.UserManagement" MasterPageFile="~/Admin/Admin.Master" %>

<asp:Content ContentPlaceHolderID="MainPageHolder" ID="mainPage" runat="server">
    <form id="form1" runat="server">
    总用户数：<br /> <asp:Label ID="lblUserCount" runat="server"></asp:Label>
    <br />
    用户名 <asp:TextBox ID="txtUsername" runat="server" />
    <asp:Button ID="btnSearchUser" runat="server" Text="搜索用户" 
        onclick="btnSearchUser_Click" />
    <br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString %>" 
        SelectCommand="SELECT [Is_Locked], [Username], [Email], [Nick_Name], [Register_Time], [Is_Recommended], [Is_Test_User], [Is_Identified], [Identification_Info] FROM [DB_User] order by [Register_Time] desc" 
        UpdateCommand="Update [DB_User] SET [Is_Locked] = @Is_Locked, [Nick_Name] = @Nick_Name, [Is_Recommended]=@Is_Recommended, [Is_Identified] = @Is_Identified, [Identification_Info]=@Identification_Info, Is_Test_User = @Is_Test_User WHERE Username=@Username">
    </asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AutoGenerateEditButton="True" 
        DataKeyNames="Username" DataSourceID="SqlDataSource1" AllowSorting="True" 
        PageSize="20" AllowPaging="True"
        OnRowUpdating="OnUpdating" 
        onpageindexchanged="PageIndexChanged" onrowediting="OnEditing" onsorted="Sorted">
        <Columns>
            <asp:BoundField DataField="Username" HeaderText="用户名" ReadOnly="True" 
                SortExpression="Username" />
            <asp:BoundField DataField="Nick_Name" HeaderText="昵称" SortExpression="Nick_Name" />
            <asp:BoundField DataField="Email" HeaderText="邮箱" SortExpression="Email" />
            <asp:BoundField DataField="Register_Time" HeaderText="注册时间" SortExpression="Register_Time" />                                  
            <asp:BoundField DataField="Is_Locked" HeaderText="冻结" SortExpression="Is_Locked" />  
            <asp:BoundField DataField="Is_Recommended" HeaderText="推荐" SortExpression="Is_Recommended" />  
            <asp:BoundField DataField="Is_Test_User" HeaderText="内部用户" SortExpression="Is_Test_User" />  
            <asp:BoundField DataField="Is_Identified" HeaderText="认证用户" SortExpression="Is_Identified" />  
            <asp:BoundField DataField="Identification_Info" HeaderText="认证信息" SortExpression="Identification_Info" />
        </Columns>
    </asp:GridView>
    </form>
    <h2>说明：</h2>
    <ul>
        <li>推荐：只有推荐不为0的用户会显示在引导页。如果超过15个只显示前15个用户，不足15个不会自动补齐，请保证有15个用户置顶。推荐顺序由小到大依次排列（1在最顶部）。顺序数字可以相同，可以不连续。
        </li>
    </ul>
</asp:Content>

