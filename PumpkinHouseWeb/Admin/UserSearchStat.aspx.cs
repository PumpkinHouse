﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Admin
{
    public partial class UserSearchStat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void StartCalc(object sender, EventArgs e)
        {
            Dictionary<string, int> dictP = new Dictionary<string, int>();
            Dictionary<string, int> dictA = new Dictionary<string, int>();
            Dictionary<string, int> dictU = new Dictionary<string, int>();
            using (DataUtils utils = new DataUtils())
            {
                List<DB_User_Action> actions = ((Stat)this.Master).FindAction(utils, UserActionType.SearchByKeyword);

                Dictionary<string, int> dict = null;
                foreach (var action in actions)
                {
                    string[] split = action.Action_Content.Split(':');
                    switch (split[0])
                    {
                        case "p":
                            {
                                dict = dictP;
                                break;
                            }
                        case "u":
                            {
                                dict = dictU;
                                break;
                            }
                        case "a":
                            {
                                dict = dictA;
                                break;
                            }
                    }

                    if (dict.ContainsKey(split[1]))
                    {
                        dict[split[1]]++;
                    }
                    else
                    {
                        dict[split[1]] = 1;
                    }
                }
            }
            SearchUserGridView.DataSource = dictU;
            SearchUserGridView.DataBind();

            SearchAlbumGridView.DataSource = dictA;
            SearchAlbumGridView.DataBind();

            SearchPictureGridView.DataSource = dictP;
            SearchPictureGridView.DataBind();

        }



    }
}