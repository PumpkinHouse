﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TagManagement2.aspx.cs" Inherits="PumpkinHouse.Admin.TagManagement2" MasterPageFile="~/Admin/Admin.Master" %>

<asp:Content ContentPlaceHolderID="MainPageHolder" ID="mainPage" runat="server">
    <form runat="server">
    <asp:Button ID="StartCalcBtn" Text="点击开始计算" runat="server" />
    <p></p>

    <b>图片标签使用情况</b>
    <asp:GridView ID="PictureTagListGridView" runat="server" AutoGenerateColumns="true" runat="server">
        
    </asp:GridView>
    <p></p>
    <b>专辑标签使用情况</b>
    <asp:GridView ID="AlbumTagListGridView" runat="server" AutoGenerateColumns="true" runat="server">
        
    </asp:GridView>
    </form>
</asp:Content>

