﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SurveyStat.aspx.cs" Inherits="PumpkinHouse.Admin.SurveyStat"
    MasterPageFile="~/Admin/Stat.master" %>

<asp:Content ContentPlaceHolderID="StatPanel" ID="mainPage" runat="server">
    <asp:Button ID="StartCalcBtn" Text="点击开始计算" runat="server" OnClick="StartCalc" />
    <p>
    </p>

    喜欢数：<asp:Label ID="likeCount" runat="server"></asp:Label><br />
    不喜欢数：<asp:Label ID="dislikeCount" runat="server"></asp:Label>
    <asp:GridView ID="surveyGridView" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataKeyNames="Id" PageSize="50">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True"
                SortExpression="Id" Visible="false"/>
            <asp:BoundField DataField="Username" HeaderText="用户" SortExpression="Username" />
            <asp:BoundField DataField="Suggestion" HeaderText="建议" SortExpression="Suggestion"
                ReadOnly="true" />
            <asp:BoundField DataField="Time" HeaderText="反馈时间" SortExpression="Time" />
        </Columns>
    </asp:GridView>
</asp:Content>
