﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Admin
{
    public partial class UserStat : System.Web.UI.Page
    {
        class Stat
        {
            public string Username { get; set; }
            public string Nickname { get; set; }
            public int AlbumCount { get; set; }
            public int FansCount { get; set; }
            public int WatchingCount { get; set; }
            public int PictureReplyCount { get; set; }
            public byte IsTest { get; set; }
            public int PictureCount { get; set; }
            public int CollectCount { get; set; }
            public int CollectedCount { get; set; }

        }

        private IDictionary<string, Stat> Dict = new Dictionary<string, Stat>(StringComparer.InvariantCultureIgnoreCase);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void StartCalc(object sender, EventArgs e)
        {
            DateTime dateStart = DateTime.Parse(((PumpkinHouse.Admin.Stat)Master).DateStart.Text);
            DateTime dateEnd = DateTime.Parse(((PumpkinHouse.Admin.Stat)Master).DateEnd.Text);

            Dict.Clear();
            using (DataUtils utils = new DataUtils(false))
            {
                var result = utils.FindUsers().Select(u => new Stat { Username = u.Username, Nickname = u.Nick_Name, IsTest = u.Is_Test_User });
                foreach (Stat s in result)
                {
                    Dict.Add(s.Username, s);
                }

                var albumCountResult = (from alb in utils.context.DB_Album where alb.Creation_Time >= dateStart && alb.Creation_Time <= dateEnd group alb by alb.Username into albumGroup select new { Username = albumGroup.Key, Count = albumGroup.Count() });
                foreach (var r in albumCountResult)
                {
                    Dict[r.Username].AlbumCount = r.Count;
                }

                var pictureCountResult = (from c in utils.context.DB_Collection2 where c.Is_Original == 1 && c.Collect_Time >= dateStart && c.Collect_Time <= dateEnd group c by c.Username into pGroup select new { Username = pGroup.Key, Count = pGroup.Count() });
                foreach (var r in pictureCountResult)
                {
                    Dict[r.Username].PictureCount = r.Count;
                }

                var collectCountResult = (from c in utils.context.DB_Collection2 where c.Is_Original == 0 && c.Collect_Time >= dateStart && c.Collect_Time <= dateEnd group c by c.Username into pGroup select new { Username = pGroup.Key, Count = pGroup.Count() });
                foreach (var r in collectCountResult)
                {
                    Dict[r.Username].CollectCount = r.Count;
                }

                var collectedCountResult = (from c in utils.context.DB_Collection2 from p in utils.context.DB_Picture where c.Is_Original == 0 && p.Id == c.Picture_Id && c.Collect_Time >= dateStart && c.Collect_Time <= dateEnd group c by p.Username into pGroup select new { Username = pGroup.Key, Count = pGroup.Count() });
                foreach (var r in collectedCountResult)
                {
                    Dict[r.Username].CollectedCount = r.Count;
                }

                var fanCountResult = (from f in utils.context.DB_Fan where f.Action_Time >= dateStart && f.Action_Time <= dateEnd group f by f.WatchedUsername into fGroup select new { Username = fGroup.Key, Count = fGroup.Count() });
                foreach (var r in albumCountResult)
                {
                    Dict[r.Username].FansCount = r.Count;
                }
                
                var watchCountResult = (from f in utils.context.DB_Fan where f.Action_Time >= dateStart && f.Action_Time <= dateEnd group f by f.FansUsername into fGroup select new { Username = fGroup.Key, Count = fGroup.Count() });
                foreach (var r in watchCountResult)
                {
                    Dict[r.Username].WatchingCount = r.Count;
                }

                var replyCountResult = (from r in utils.context.DB_Reply from c in utils.context.DB_Collection where c.Id == r.Collection_Id && r.Post_Time >= dateStart && r.Post_Time <= dateEnd  group r by c.Username into fGroup select new { Username = fGroup.Key, Count = fGroup.Count() });
                foreach (var r in replyCountResult)
                {
                    Dict[r.Username].PictureReplyCount = r.Count;
                }
            }
            IEnumerable<Stat> values = Dict.Values;

            values = values.Where(s => s.PictureCount + s.PictureReplyCount + s.WatchingCount + s.FansCount + s.AlbumCount + s.CollectCount + s.CollectedCount > 0);
            
            Session["value"] = values;
            this.userStatGridView.DataSource = values;
            
            this.userStatGridView.DataBind();
        }

        protected void userStatGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            IEnumerable<Stat> result = Session["value"] as IEnumerable<Stat>;

            result = result.OrderByDescending(stat => GetValue(stat, e.SortExpression));
            
            this.userStatGridView.DataSource = result;

            this.userStatGridView.DataBind();
        }

        private object GetValue(Stat stat, string propertyName)
        {
            return typeof(Stat).GetProperty(propertyName).GetValue(stat, null);
        }
    }
}