﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserSearchStat.aspx.cs"
    Inherits="PumpkinHouse.Admin.UserSearchStat" MasterPageFile="~/Admin/Stat.Master" %>

<asp:Content ContentPlaceHolderID="StatPanel" ID="mainPage" runat="server">
    <asp:Button ID="StartCalcBtn" Text="点击开始计算" runat="server" 
        onclick="StartCalc" />
    <p></p>
    <b>图片搜索关键字</b>
    <asp:GridView ID="SearchPictureGridView" runat="server" AutoGenerateColumns="true"
        runat="server">
    </asp:GridView>
    <p></p>
    <b>专辑搜索关键字</b>
    <asp:GridView ID="SearchAlbumGridView" runat="server" AutoGenerateColumns="true"
        runat="server">
    </asp:GridView>
    <p></p>
    <b>用户搜索关键字</b>
    <asp:GridView ID="SearchUserGridView" runat="server" AutoGenerateColumns="true" runat="server">
    </asp:GridView>
</asp:Content>
