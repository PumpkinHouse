﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Admin
{
    public partial class UploadToolStat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void StartCalc(object sender, EventArgs e)
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();
            dict.Add("本地上传", 0);
            dict.Add("粘贴网址下载", 0);
            dict.Add("收纳工具", 0);
            using (DataUtils utils = new DataUtils())
            {
                List<DB_User_Action> list = ((Stat)Master).FindAction(utils, UserActionType.AddPicture);
                foreach (var action in list)
                {
                    string[] split = action.Action_Content.Split(':');
                    switch (split[0])
                    {
                        case "u":
                            {
                                dict["本地上传"]++;
                                break;
                            }
                        case "w":
                            {
                                dict["粘贴网址下载"]++;
                                break;
                            }
                        case "c":
                            {
                                dict["收纳工具"]++;
                                break;
                            }
                    }
                }
            }

            this.uploadGridView.DataSource = dict;
            this.uploadGridView.DataBind();
        }
    }
}