﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Admin
{
    public partial class FilterStat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void StartCalc(object sender, EventArgs e)
        {
            Dictionary<string, int> dictMode = new Dictionary<string, int>();
            Dictionary<string, int> dictCommodity = new Dictionary<string, int>();
            Dictionary<string, int> dictTag = new Dictionary<string, int>();

            dictMode.Add("Default", 0);
            dictMode.Add("Hot", 0);
            dictMode.Add("Latest", 0);

            dictCommodity.Add("True", 0);
            dictCommodity.Add("False", 0);

            using (DataUtils utils = new DataUtils())
            {
                List<DB_User_Action> actions = ((Stat)Master).FindAction(utils, UserActionType.SearchByTag);

                foreach (var action in actions)
                {
                    string[] split = action.Action_Content.Split(':');
                    dictMode[split[0]]++;
                    dictCommodity[split[1]]++;

                    if (!dictTag.ContainsKey(split[2]))
                    {
                        dictTag.Add(split[2], 1);
                    }
                    else
                    {
                        dictTag[split[2]]++;
                    }
                }
            }

            dictMode.Add("默认", dictMode["Default"]);
            dictMode.Add("最新", dictMode["Latest"]);
            dictMode.Add("热门", dictMode["Hot"]);

            dictMode.Remove("Default");
            dictMode.Remove("Latest");
            dictMode.Remove("Hot");

            dictCommodity.Add("是", dictCommodity["True"]);
            dictCommodity.Add("否", dictCommodity["False"]);

            dictCommodity.Remove("True");
            dictCommodity.Remove("False");

            ModeGridView.DataSource = dictMode;
            ModeGridView.DataBind();

            CommodityGridView.DataSource = dictCommodity;
            CommodityGridView.DataBind();

            TagGridView.DataSource = dictTag;
            TagGridView.DataBind();
        }
    }
}