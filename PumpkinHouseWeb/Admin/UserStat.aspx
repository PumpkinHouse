﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserStat.aspx.cs" Inherits="PumpkinHouse.Admin.UserStat"
    MasterPageFile="~/Admin/Stat.master" %>

<asp:Content ContentPlaceHolderID="StatPanel" ID="mainPage" runat="server">
    <asp:Button ID="StartCalcBtn" Text="点击开始计算" runat="server" OnClick="StartCalc" />
    <p style="color: Red">
        说明：显示结果仅限于选择日期之内的行为，如要查看总体数据，请调整开始和结束日期
    </p>
    <b>用户数据统计工具</b>
    <asp:GridView ID="userStatGridView" runat="server" AutoGenerateColumns="false" AllowSorting="true"
        OnSorting="userStatGridView_Sorting">
        <Columns>
            <asp:BoundField DataField="Username" Visible="false" SortExpression="Username" />
            <asp:TemplateField>
                <ItemTemplate>
                    <a href="/User/<%# Eval("Username") %>" target="_blank"><%# Eval("Nickname")%></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="AlbumCount" SortExpression="AlbumCount" HeaderText="创建专辑数" />
            <asp:BoundField DataField="PictureCount" SortExpression="PictureCount" HeaderText="发表图片数" />
            <asp:BoundField DataField="CollectCount" SortExpression="CollectCount" HeaderText="收集数" />
            <asp:BoundField DataField="FansCount" SortExpression="FansCount" HeaderText="增加粉丝数" />
            <asp:BoundField DataField="WatchingCount" SortExpression="WatchingCount" HeaderText="增加关注数" />
            <asp:BoundField DataField="CollectedCount" SortExpression="CollectedCount" HeaderText="被收集数" />
            <asp:BoundField DataField="PictureReplyCount" SortExpression="PictureReplyCount"
                HeaderText="发表回复数（图片）" />
            <asp:BoundField DataField="IsTest" SortExpression="IsTest" HeaderText="是否测试用户" />
        </Columns>
    </asp:GridView>
</asp:Content>
