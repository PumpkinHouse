﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdvertisementManagement.aspx.cs" Inherits="PumpkinHouse.Admin.AdvertisementManagement" MasterPageFile="~/Admin/Admin.Master" %>
<asp:Content ContentPlaceHolderID="MainPageHolder" ID="mainPage" runat="server">
    <form id="form1" runat="server">
    <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="false" AutoGenerateEditButton="true" >
        <Columns>
            <asp:BoundField DataField="ImageUrl" HeaderText="图片链接" SortExpression="ImageUrl"/>
            <asp:BoundField DataField="LinkUrl" HeaderText="目标链接" SortExpression="LinkUrl" />
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString %>" 
        SelectCommand="SELECT [Id], [ImageUrl], [LinkUrl], [Text] FROM [DB_Advertisement]"
        UpdateCommand="UPDATE [DB_Advertisement] set ImageUrl=@ImageUrl, LinkUrl = @LinkUrl"
        >
    </asp:SqlDataSource>
    </form>
</asp:Content>
