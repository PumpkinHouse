﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;

namespace PumpkinHouse.Admin
{
    public class ShopPriceStat
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Lt50 { get; set; }
        public int Btw50N100 { get; set; }
        public int Btw100N200 { get; set; }
        public int Btw200N300 { get; set; }
        public int Btw300N500 { get; set; }
        public int Gt500 { get; set; }
        public int NoPrice { get;set;}
        public int Total { get { return Lt50 + Btw50N100 + Btw100N200 + Btw200N300 + Btw300N500 + Gt500 + NoPrice; } }
    }

    public partial class ShopStat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        
        private string[] names = new string[] { "", "淘宝", "京东", "（天猫）淘宝商城", "宜家", "凡客", "阿里巴巴", "当当", "亚马逊中国", "拍拍", "优雅", "趣玩", "51悠品", "易趣网", "一号店", "苏宁", "国美" };

        protected void StartCalc(object sender, EventArgs e)
        {
            Dictionary<Shop, ShopPriceStat> dict = new Dictionary<Shop, ShopPriceStat>();

            foreach (var s in (Shop[])Enum.GetValues(typeof(Shop)))
            {
                if (s == Shop.NotShop) continue;
                dict.Add(s, new ShopPriceStat { Code = Enum.GetName(typeof(Shop), s), Name = names[(byte)s] });
            }

            using (DataUtils utils = new DataUtils())
            {
                IList<DB_Picture> pictures = utils.FindAllPictures(null, true).ToList();
                foreach (var pic in pictures)
                {
                    Shop shop = (Shop)pic.Shop;
                    ShopPriceStat stat = dict[shop];
                    if (pic.Price == 0)
                    {
                        stat.NoPrice++;
                    }
                    else if (pic.Price <= 50)
                    {
                        stat.Lt50++;
                    }
                    else if (pic.Price <= 100)
                    {
                        stat.Btw50N100++;
                    }
                    else if (pic.Price <= 200)
                    {
                        stat.Btw100N200++;
                    }
                    else if (pic.Price <= 300)
                    {
                        stat.Btw200N300++;
                    }
                    else if (pic.Price <= 500)
                    {
                        stat.Btw300N500++;
                    }
                    else
                    {
                        stat.Gt500++;
                    }
                }
            }

            ShopGridView.DataSource = dict.Values.OrderByDescending(i => i.Total);
            ShopGridView.DataBind();
        }
    }
}