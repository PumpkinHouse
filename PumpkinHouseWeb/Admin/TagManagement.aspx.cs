﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;

namespace PumpkinHouse.Admin
{
    public partial class TagManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RefreshKeywordAssociation(false);
            }
        }

        private void RefreshKeywordAssociation(bool force)
        {
            Dictionary<string, string> keywordAssocations = KeywordHelper.GetKeywords(force);
            GridView3.DataSource = keywordAssocations;
            GridView3.DataBind();
        }

        protected void doInsert(Object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert")
            {
                string newName = ((TextBox)((GridView)sender).FooterRow.FindControl("add_Category")).Text;
                using (DataUtils utils = new DataUtils())
                {
                    DB_Keyword_Category kc = new DB_Keyword_Category { Name = newName };
                    utils.InsertKeywordCategory(kc);
                }
                GridView1.DataBind();
            }
        }

        protected void doInsertKeyword(Object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert")
            {
                string newName = ((TextBox)((GridView)sender).FooterRow.FindControl("KeywordToAdd")).Text;
                int id = int.Parse(((DropDownList)((GridView)sender).FooterRow.FindControl("CategoryOfKeywordToAdd")).SelectedValue);
                using (DataUtils utils = new DataUtils())
                {
                    DB_Keyword k = new DB_Keyword { Name = newName, Category_Id = id };
                    utils.InsertKeyword(k);
                }
                GridView2.DataBind();
            }
        }

        protected void AddAssociationBtn_Click(object sender, EventArgs e)
        {
            string keyword1 = ((TextBox)AssociationToModifyKeyword1).Text;
            string keyword2 = ((TextBox)AssociationToModifyKeyword2).Text;
            if (string.IsNullOrEmpty(keyword1) || string.IsNullOrEmpty(keyword2))
            {
                return;
            }
            using (DataUtils utils = new DataUtils())
            {
                utils.InsertKeywordAssication(keyword1, keyword2);
                RefreshKeywordAssociation(true);
            }
        }

        protected void RemoveAssicationBtn_Click(object sender, EventArgs e)
        {
            string keyword1 = ((TextBox)AssociationToModifyKeyword1).Text;
            string keyword2 = ((TextBox)AssociationToModifyKeyword2).Text;
            if (string.IsNullOrEmpty(keyword1) || string.IsNullOrEmpty(keyword2))
            {
                return;
            }
            using (DataUtils utils = new DataUtils())
            {
                utils.RemoveKeywordAssication(keyword1, keyword2);
                RefreshKeywordAssociation(true);
            }
        }

        
    }
}