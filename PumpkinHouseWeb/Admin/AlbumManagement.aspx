﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlbumManagement.aspx.cs" Inherits="PumpkinHouse.Admin.AlbumManagement" MasterPageFile="~/Admin/Admin.Master" %>

<asp:Content ContentPlaceHolderID="MainPageHolder" ID="mainPage" runat="server">
    
    <form id="form1" runat="server">
    专辑总数：<asp:Label ID="lblCount" runat="server"></asp:Label><br />
    专辑名 <asp:TextBox ID="txtSearchAlbum" runat="server" />
    <asp:Button ID="btnSearchAlbum" runat="server" Text="搜索专辑" 
        onclick="btnSearchAlbum_Click" />

    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" DataSourceID="SqlDataSource1" DataKeyNames="Id" 
        AllowSorting="True" AutoGenerateEditButton="True"
        OnRowUpdating="OnUpdating" 
        onpageindexchanged="PageIndexChanged" onrowediting="OnEditing" 
        onsorted="Sorted">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" 
                SortExpression="Id"/>
            <asp:BoundField DataField="Nick_Name" HeaderText="用户昵称" ReadOnly="True" 
                SortExpression="Nick_Name" />
            <asp:BoundField DataField="Name" HeaderText="专辑名" 
                SortExpression="Name" />
            <asp:BoundField DataField="Description" HeaderText="专辑描述" 
                SortExpression="Description" />
            <asp:BoundField DataField="Tags" HeaderText="标签" 
                SortExpression="Tags" />
            <asp:BoundField DataField="Is_Recommended" HeaderText="推荐" 
                SortExpression="Is_Recommended" />
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString %>" 
        SelectCommand="SELECT [Id], [Name], [Description], [Tags], [Nick_Name], a.[Is_Recommended] FROM [DB_Album] a, [DB_User] u where a.Username = u.Username"
        
        UpdateCommand="UPDATE [DB_Album] set Name=@Name, Description = @Description, Tags = @Tags, Is_Recommended=@Is_Recommended where Id=@Id" onselected="SqlDataSource1_Selected"
        >
    </asp:SqlDataSource>
    </form>
    <h2>说明：</h2>
    <ul>
        <li>推荐：只有推荐不为0的专辑会显示在引导页。如果超过12个只显示前12个用户，不足12个不会自动补齐，请保证有12个用户置顶。推荐顺序由小到大依次排列（1在最顶部）。顺序数字可以相同，可以不连续。
        </li>
    </ul>
</asp:Content>
