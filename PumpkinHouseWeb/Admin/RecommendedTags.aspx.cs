﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Admin
{
    public partial class RecommendedTags : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string tag = txtTag.Text.Trim();
            string imageUrl = txtImageUrl.Text.Trim();
            int cId = int.Parse(comboCategory.SelectedValue);

            using (DataUtils utils = new DataUtils())
            {
                utils.InsertRecommendedTag(new DB_Recommended_Tag { Name = tag, ImageUrl = imageUrl, Keyword_Category_Id = cId });
            }

            //GridView1.DataBind();
        }
    }
}