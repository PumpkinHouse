﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PumpkinHouse.Account.Login" MasterPageFile="~/Account/Account.Master" %>

<asp:Content ID="Head" runat="server" ContentPlaceHolderID="headHolder">
    <style type="text/css">
        .rgleft input.error{color:Red; font-size:17px;}
        .rgleft .error{color:Red;}
        #failureText{ color:Red; font-size:15px;}
    </style>
    <script type="text/javascript">

        $(function () {
           //邮箱补全
            var sinaSuggest = new InputSuggest({
                input: document.getElementById('email'),
                data: ['qq.com', 'vip.qq.com', 'sina.com', 'sina.cn', 'vip.sina.com', '163.com', '126.com', 'yahoo.com', 'gmail.com', 'hotmail.com', 'foxmail.com'],
                opacity: 0.9
            });

            $('#failureText').text($('.failure').text());

            $('#email').watermark('这是你的登录账号哦！');
            $('#password').watermark('6-16位数字/字母 ');

            $('#inPageLogonForm').validate({
                messages: {
                    email: {
                        email: '邮箱格式不对哦！',
                        required: '请输入注册邮箱'
                    },
                    password: {
                        required: '请输入密码'
                    }
                },
                submitHandler: function () {
                    $('#AccountContentHolder_aspLogin_UserName').val($('#email').val());
                    $('#AccountContentHolder_aspLogin_Password').val($('#password').val());
                    $('#AccountContentHolder_aspLogin_RememberMe').attr('checked', $('#rememberMe').attr('checked'));
                    $('#AccountContentHolder_aspLogin_LoginButton').trigger('click');
                }
            });


            $("#email").focus(function () {
                $("#email").css({ "background-color": "#fff", "color": "#5d5d5d" });
            });
            $("#email").blur(function (e) {
                var nameVal = $(e.target).val()
                if (nameVal.length == 0) {
                    $("#email").css({ "background-color": "#F8F8F8", "color": "#c1c1c1" });
                } else {
                    $("#email").css({ "background-color": "#fff", "color": "#5d5d5d" });
                }
            });

            $("#password").focus(function () {
                $("#password").css({ "background-color": "#fff", "color": "#5d5d5d" });
            });
            $("#password").blur(function (e) {
                var nameVal = $(e.target).val()
                if (nameVal.length == 0) {
                    $("#password").css({ "background-color": "#F8F8F8", "color": "#c1c1c1" });
                } else {
                    $("#password").css({ "background-color": "#fff", "color": "#5d5d5d" });
                }
            });

        });
    </script>
</asp:Content>

<asp:Content ID="Content" runat="server" ContentPlaceHolderID="AccountContentHolder">
            <div id="logonPanel">
            <form id="inPageLogonForm">
            <div class="Register clearfix">
                <div class="RgLeft float_l">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		                  <tr>
                            <td colspan="2"><h1>使用已注册邮箱登录</h1></td>
                          </tr>
                        <tr>
                            <td style="width:58px; font-size:20px;">帐号</td>
                            <td><div><input type="text" value="" id="email" name="email" class="required email" /></div></td>
                        </tr>
                        <tr>
                            <td style="width:58px; font-size:20px;">密码</td>
                            <td><div><input type="password" id="password" value="" name="password" class="required" style="_margin-left:-10px;*margin-left:-10px;margin-left:-10px\0;" /></div>
                            </td>
                        </tr>
                        <tr valign=middle>
                            <td>&nbsp;</td>
                            <td id="failureText"></td>
                        </tr>
                        <tr>
                            <td style="width:58px; font-size:20px;"></td>
                            <td valign=bottom style="font-size:14px;"><input type="checkbox" id="rememberMe" value="" name="rememberMe" checked/>&nbsp;下次自动登录</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            <br />
                                <input type="submit" id="submitBtn" value="登 录" class="Public-Btn-Box WebSure" />&nbsp;&nbsp;<a href="/Account/ForgetPassword.aspx">忘记密码？</a>&nbsp;&nbsp;<a href="/Account/Register.aspx">注册</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="RgRight float_r" style="background: #fff">
                    <h2>使用其他账号快速登陆</h2>
                    <ul>
                        <li><a href="/Account/QQAuth.aspx?request=1" class="btn-qq"></a></li>
                        <li><a href="/Account/TaobaoAuth.aspx?request=1" class="btn-taobao"></a></li>
                        <li><a href="/Account/SinaAuth.aspx?request=1" class="btn-sinaweibo"></a></li>
                        <li><a href="/Account/QQWeiboAuth.aspx?request=1" class="btn-qqweibo"></a></li>
                    </ul>
                </div>
            </div>
            </form>

            <form id="logonForm" runat="server" style="display: none">
                <div class="Register" align="center">
	                <div class="RgLeft float_l" style="position:relative">
                        <h1 style="padding-left:15px;">木头盒子登录</h1>
                        <asp:Login ID="aspLogin" runat="server" LoginButtonText=" " LabelStyle-CssClass="a" ValidatorTextStyle-CssClass="b" TextBoxStyle-CssClass="required" FailureTextStyle-CssClass="failure" UserNameLabelText="邮箱" PasswordLabelText="密码" FailureText="邮箱或密码错误哦！" RememberMeText = "下次自动登录" RememberMeSet="true" TitleText=" " TitleTextStyle-CssClass="a" />
                    </div>
                </div>
            </form>
            </div>
</asp:Content>





