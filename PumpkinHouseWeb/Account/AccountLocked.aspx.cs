﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Account
{
    public partial class AccountLocked : System.Web.UI.Page
    {
        public string NickName;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (DataUtils utils = new DataUtils(false))
            {
                string username = HttpContext.Current.Session["username"] as string;
                DB_User user = utils.FindUserByUsername(username);
                if (user != null)
                {
                    NickName = user.Nick_Name;
                }
            }
        }
    }
}