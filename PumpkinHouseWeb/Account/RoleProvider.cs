﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using PumpkinHouseDatabase;

namespace PumpkinHouse
{
    public class RoleProvider : SqlRoleProvider
    {
        public override bool IsUserInRole(string username, string roleName)
        {
            if (roleName == "admin")
            {
                using (DataUtils utils = new DataUtils(false))
                {
                    DB_User user = utils.FindUserByNameOrEmail(username);
                    if (user == null)
                    {
                        return false;
                    }
                    return user.Is_Admin == 1;
                }
            }
            if (roleName == "locked")
            {
                using (DataUtils utils = new DataUtils(false))
                {
                    DB_User user = utils.FindUserByNameOrEmail(username);
                    if (user == null)
                    {
                        return false;
                    }
                    if (user.Is_Locked == 1)
                    {
                        HttpContext.Current.Session["username"] = user.Username;
                        HttpContext.Current.Session["lockedReason"] = user.Locked_Reason;
                        return true;
                    };
                    return false;
                }
            }
            return true;
        }

        public override string[] GetRolesForUser(string username)
        {
            using (DataUtils utils = new DataUtils(false))
            {
                DB_User user = utils.FindUserByNameOrEmail(username);
                if (user.Is_Locked == 1)
                {
                    return new string[] { "locked" };
                }
                if (user.Is_Admin == 1)
                {
                    return new string[] { "admin" };
                }
                return new string[] { };
            }
        }

        public override string[] GetAllRoles()
        {
            return new string[] { "admin", "locked" };
        }
        
    }
}