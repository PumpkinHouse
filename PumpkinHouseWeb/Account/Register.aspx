﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="PumpkinHouse.Account.Register"
    MasterPageFile="~/Site.Master" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <script src="/Scripts/account.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript" language="javascript" >
        function doAction() {
            $('#email').watermark('这是你的登录账号哦！');
            $('#nickname').watermark('中英文皆可，14个字符/汉字');
            $('#password').watermark('6-16位数字/字母 ');

            configureRegistrationValidation();

            $("#nickname").focus(function () {
                $("#nickname").css({"background-color":"#fff","color":"#5d5d5d"});
            });
            $("#nickname").blur(function (e) {
                var nameVal = $(e.target).val()
                if (nameVal.length == 0) {
                    $("#nickname").css({ "background-color": "#F8F8F8", "color": "#c1c1c1" });
                }else{
                    $("#nickname").css({ "background-color": "#fff", "color": "#5d5d5d" });
                }
            });

            $("#email").focus(function () {
                $("#email").css({ "background-color": "#fff", "color": "#5d5d5d" });
            });
            $("#email").blur(function (e) {
                var nameVal = $(e.target).val()
                if (nameVal.length == 0) {
                    $("#email").css({ "background-color": "#F8F8F8", "color": "#c1c1c1" });
                } else {
                    $("#email").css({ "background-color": "#fff", "color": "#5d5d5d" });
                }
            });

            $("#password").focus(function () {
                $("#password").css({ "background-color": "#fff", "color": "#5d5d5d" });
            });
            $("#password").blur(function (e) {
                var nameVal = $(e.target).val()
                if (nameVal.length == 0) {
                    $("#password").css({ "background-color": "#F8F8F8", "color": "#c1c1c1" });
                } else {
                    $("#password").css({ "background-color": "#fff", "color": "#5d5d5d" });
                }
            });

        }

    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .error{color:Red; font-size:16px; display:block; height:20px; font-weight:100; padding-top:20px; }
    </style>

    <form id="registerForm" action="">
        <div class="Register">
            <h1 class="Register-title">使用其他账号快速注册</h1>
            <div class="fast-login-Btn">
                <ul>
                    <li><a href="/Account/QQAuth.aspx?request=1" class="btn-qq"></a></li>
                    <li><a href="/Account/TaobaoAuth.aspx?request=1" class="btn-taobao"></a><a href="/Account/TaobaoAuth.aspx?request=1"></a></li>
                    <li><a href="/Account/SinaAuth.aspx?request=1" class="btn-sinaweibo"></a></li>
                    <li><a href="/Account/QQWeiboAuth.aspx?request=1" class="btn-qqweibo"></a></li>
                </ul>
            </div>
    
            <h1 class="Register-title">邮箱注册</h1>
            <div class="email-login">
                <ul>
                    <li><span>昵称</span><input type="text" value="" id="nickname" name="nickname" remote="/PublicService/AccountService.svc/ajax/IsNicknameAvailable" class="required" style="font-size:17px;" /></li>
                    <li><span>邮箱</span><input type="text" value="" id="email" name="email" class="email required" remote="/PublicService/AccountService.svc/ajax/IsEmailAvailable" style="font-size:17px;"/></li>
                    <li><span>密码</span><input type="password" value="" id="password" name="password" class="required" style="font-size:17px;" /></li>
                </ul>
            </div>
            <div class="submitBox"><input type="submit" value="注册" class="RgSure" </div>

        </div>
    </form>
</asp:Content>
