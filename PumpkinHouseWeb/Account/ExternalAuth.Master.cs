﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Association;
using System.Web.Security;
using PumpkinHouse.Utils;

namespace PumpkinHouse.Account
{
    public partial class ExternalAuth : System.Web.UI.MasterPage
    {
        public UserForRegistration User;
        public IAssociation association;
        public bool ShowWeiboRequest;
        public bool ShowAdvertiseRequest;
        public string WeiboName;
        public string ReturnUrl = "/dashboard";

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void BindWithCurrentUser(AssociationInfo info)
        {
            using (DataUtils utils = new DataUtils())
            {
                // 绑定用户信息
                string user = Helper.FindCurrentUsername();
                DB_User u = utils.FindUserByUsername(user);

                DB_Account_Association aa = utils.FindAccountAssociation().Where(a => a.External_Username == info.OpendId && a.Party == (byte)info.Party).SingleOrDefault();
                if (aa == null)
                {
                    AccountAssociationHelper.CreateAssociation(utils, info, user);
                }
                else
                {
                    throw new AssociationAlreadyBound();
                }
            }
        }

        public void DoAuth()
        {
            if (association == null)
            {
                Response.Redirect("~/404.aspx");
            }

            if (Request.Params["request"] != null)
            {
                if (Request.Params["request"] == "2")
                {
                    Session["bind"] = true;
                }

                association.SendAuthorizationRequest();
            }
            else
            {
                AssociationInfo info = association.ParseAccessToken();
                Session["AssociationInfo"] = info;
                if (Session["bind"] != null)
                {
                    try
                    {
                        BindWithCurrentUser(info);
                    }
                    catch (AssociationAlreadyBound)
                    {
                        Session["BoundError"] = true;
                    }
                    Response.Redirect("/profile#tab4");

                }
                else
                {
                    using (DataUtils utils = new DataUtils())
                    {
                        //查找绑定用户
                        DB_User user = utils.FindUserByAssociation(info.OpendId, info.Party);

                        if (Session["ReturnUrl"] != null)
                        {
                            ReturnUrl = Session["ReturnUrl"] as string;
                        }

                        if (user != null)
                        {
                            Session["username"] = user.Username;
                            FormsAuthentication.SetAuthCookie(user.Username, false);
                            DB_Account_Association aa = utils.FindAccountAssociation().Where(a => a.External_Username == info.OpendId && a.Party == (byte)info.Party).SingleOrDefault();
                            aa.Access_Token = info.PersistToken;
                            aa.Token_Expire_Time = DateTime.Now.AddDays(89);
                            utils.Commit();

                            // 直接重定向
                            Response.Redirect(ReturnUrl);
                        }
                        else
                        {
                            // 在网站注册
                            UserForRegistration u = association.CreateUserData();
                            User = u;

                            string tempUsername = info.Party + "_" + info.OpendId;
                            Session["username"] = tempUsername;
                            FormsAuthentication.SetAuthCookie(tempUsername, false);
                            Session["UserInfo"] = u;
                        }
                    }
                }
            }
        }
    }
}