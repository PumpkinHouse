﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QWeiboSDK;
using System.Web.Security;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Association;

namespace PumpkinHouse.Account
{
    public partial class QQWeiboAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ExternalAuth auth = ((ExternalAuth)Master);
            auth.ShowAdvertiseRequest = true;
            auth.ShowWeiboRequest = true;
            auth.WeiboName = "腾讯微博";
            ((ExternalAuth)Master).association = AssociationFactory.GetInstance(AssociationParty.QQWB);
            ((ExternalAuth)Master).DoAuth();        
        }
    }
}