﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;

namespace PumpkinHouse.Account
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        public Helper.ResetPasswordStatus status;
        public string code;
        public string email;

        protected void Page_Load(object sender, EventArgs e)
        {
            code = Request.Params["code"];
            email = Request.Params["email"];

            using (DataUtils utils = new DataUtils(false))
            {
                status = Helper.ValidateCode(utils, email, code);
            }
        }
    }
}