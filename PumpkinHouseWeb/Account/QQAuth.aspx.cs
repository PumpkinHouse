﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QConnectSDK.Context;
using QConnectSDK;
using PumpkinHouseDatabase;
using System.Web.Security;
using PumpkinHouseDatabase.Contract;
using System.Threading;
using PumpkinHouse.Utils;
using PumpkinHouse.Association;

namespace PumpkinHouse.Account
{
    public partial class QQAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ExternalAuth auth = ((ExternalAuth)Master);
            auth.ShowAdvertiseRequest = true;
            auth.ShowWeiboRequest = true;
            auth.WeiboName = "腾讯微博";
            auth.association = AssociationFactory.GetInstance(AssociationParty.QQ);
            auth.DoAuth();
        }
    }
}