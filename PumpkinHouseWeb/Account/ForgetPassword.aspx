﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgetPassword.aspx.cs" Inherits="PumpkinHouse.Account.ForgetPassword" MasterPageFile="~/Account/Account.Master" %>


<asp:Content ID="Head" runat="server" ContentPlaceHolderID="headHolder">
    <title>忘记密码</title>
    
    <script type="text/javascript">
        $(function () {
            $('#email').watermark('请输入你的注册邮箱地址。');
            $('#forgetPasswordForm').validate({
                messages: {
                    email: {
                        required: '请输入邮箱地址',
                        email: '邮箱格式不正确',
                        remote: '此邮箱未注册'
                    }
                },
                submitHandler: function () {
                    ajaxRequestPasswordReset(true, $('#email').val(), function (homepage) {
                        if (homepage) {
                            $('#emailHomepage').html(', 点击<a href="' + homepage + '">' + homepage + '</a>直接登录邮箱!');
                        }
                        $('#forgetPasswordForm').hide();
                        $('#confirm').show();
                    }, handleFault);
                }
            });


            $("#email").focus(function () {
                $("#email").css({ "background-color": "#fff", "color": "#5d5d5d" });
            });
            $("#email").blur(function (e) {
                var nameVal = $(e.target).val()
                if (nameVal.length == 0) {
                    $("#email").css({ "background-color": "#F8F8F8", "color": "#c1c1c1" });
                } else {
                    $("#email").css({ "background-color": "#fff", "color": "#5d5d5d" });
                }
            });

        });
    </script>

<style type="text/css">
        /*特殊单页面*/
        .RgLeft{ font-size:20px; border:none; width:900px; margin-bottom:180px;}
        .resetPwd{margin-left:246px;}
        .RgLeft .email{ width:320px; margin-right:10px;}
        .RgLeft .error{ font-size:17px; color:Red;}
    </style>
</asp:Content>

<asp:Content ID="Content" runat="server" ContentPlaceHolderID="AccountContentHolder">
     <div class="Register">
            
            <div id="confirm" style="display:none; width:960px; height:200px; line-height:35px; font-size:18px; font-family:'微软雅黑'; padding:25px; text-align: left; font-weight:100; float:left;">
                <b>你的密码已发送到该邮箱!</b><br />
                <span>请登录邮箱查看</span><span id="emailHomepage"></span><br /><br />

                <a href="/">回到木头盒子首页>></a>
            </div>



	    <div class="RgLeft float_l" style="width:960px;">  
        <form id="forgetPasswordForm">
             <h1>密码忘了？木有关系！</h1>
           请输入你的注册邮箱地址： 
           <input type="text" id="email" name="email" class="required email" remote="/PublicService/AccountService.svc/ajax/IsEmailInUse" />&nbsp;&nbsp;<br />
           <input type="submit" value="找回密码" class="Public-Btn-Box resetPwd" />
        </form>
        </div>

    </div>
</asp:Content>