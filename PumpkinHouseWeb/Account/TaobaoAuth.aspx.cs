﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QAuth2.Context;
using QAuth2;
using OAuth2;
using Top.Api;
using Top.Api.Request;
using Top.Api.Response;
using System.Web.Security;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Association;

namespace PumpkinHouse.Account
{
    public partial class TaobaoAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ExternalAuth auth = ((ExternalAuth)Master);
            auth.ShowAdvertiseRequest = false;
            auth.ShowWeiboRequest = false;
            ((ExternalAuth)Master).association = AssociationFactory.GetInstance(AssociationParty.Taobao);
            ((ExternalAuth)Master).DoAuth();
        }
    }

}