﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountLocked.aspx.cs" Inherits="PumpkinHouse.Account.AccountLocked" MasterPageFile="~/Account/Account.Master" %>

<asp:Content ID="Head" runat="server" ContentPlaceHolderID="headHolder">
</asp:Content>

<asp:Content ID="Content" runat="server" ContentPlaceHolderID="AccountContentHolder">

<h1>账号被冻结</h1>
<div>你的账号<%= NickName %>由于以下原因被冻结了：<b><%= Session["lockedReason"] %></b></div>
<div></div>

</asp:Content>
