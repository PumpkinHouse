﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="PumpkinHouse.Account.ResetPassword" MasterPageFile="~/Account/Account.Master" %>

<asp:Content ID="Head" runat="server" ContentPlaceHolderID="headHolder">
    <script type="text/javascript">
        var code = '<%= code %>';
        var email = '<%= email %>';

        function updatePasswordSuccess() {
            document.location = '/';
        }

        $(function () {
            $('#resetPasswordForm').validate({
                messages: {
                    password: {
                        required: '请输入新密码'
                    },
                    repeatPassword: {
                        required: '请重复输入新密码',
                        equalTo: '两次密码输入必须一致'
                    }
                },
                submitHandler: function () {
                    var newPassowrd = $('#password').val();
                    ajaxResetPassword(true, email, code, newPassowrd, updatePasswordSuccess, handleFault);
                }

            });
        })
    </script>

<style type="text/css">
        /*特殊单页面*/
        .RgLeft{ font-size:20px; border:none; width:900px; margin-bottom:180px;}
        .WebSure{width:100px; height:38px; margin-left:246px; font-size:18px;}
        .RgLeft .required{ width:320px; margin-right:10px;}
        .RgLeft .error{ font-size:17px; color:Red;}
    </style>
</asp:Content>

<asp:Content ID="Content" runat="server" ContentPlaceHolderID="AccountContentHolder">
<div class="Register">
	<div class="RgLeft float_l">
            <h1>重新设置你的密码</h1>
    <% if (status == PumpkinHouse.Utils.Helper.ResetPasswordStatus.ReadyToReset)
       { %>
        <form id="resetPasswordForm">
            <table cellpadding="5" cellspacing="5">
                <tr>
                    <td>输入新密码：</td>
                    <td><input type="password" id="password" name="password" class="required" /></td>
                </tr>
                <tr>
                    <td>确认新密码：</td>
                    <td><input type="password" id="repeatPassword" name="repeatPassword" equalTo="#password" class="required" /></td>
                </tr>
                <tr>
                   <td colspan="2">
                    <input type="submit" value="确 认" class="Public-Btn-box WebSure" style="margin-left:110px;" />
                   </td> 
                </tr>
            </table>       
        </form>


    <% }
       else if (status == PumpkinHouse.Utils.Helper.ResetPasswordStatus.InvalidCode)
       { %>
       <div style="font-size:20px; color:#aacd06; margin-top:20px;">找不到请求的用户!</div>
    <% }
       else if (status == PumpkinHouse.Utils.Helper.ResetPasswordStatus.CodeExpired)
       { %>
       <div>请求已过期，请重新发送邮件</div>
    <% } %>
    </div>
</div>
</asp:Content>