﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;

namespace PumpkinHouse
{
    public class MembershipProvider : SqlMembershipProvider
    {
        public override bool ValidateUser(string email, string password)
        {
            string username = Helper.ValidateUser(email, password);
            if (username != null)
            {
                string returnUrl = HttpContext.Current.Request.Params["ReturnUrl"];
                HttpContext.Current.Session["username"] = username;

                if (Roles.IsUserInRole(username, "locked"))
                {
                    HttpContext.Current.Response.Redirect("/Account/AccountLocked.aspx");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return username != null;
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            using (DataUtils utils = new DataUtils(false))
            {
                IQueryable<DB_User> users = utils.FindAllUsers();
                totalRecords = users.Count();
                IList<MembershipUser> result = users.Skip(pageSize * pageIndex).Take(pageSize).Select(u => new MembershipUser("AspNetSqlMembershipProvider",
                    u.Username,
                    u.Username,
                    u.Email,
                    "",
                    u.Self_Introduction,
                    true,
                    u.Is_Locked == 1,
                    DateTime.Now,
                    DateTime.Now,
                    DateTime.Now,
                    DateTime.Now,
                    DateTime.Now)).ToList();
                MembershipUserCollection c = new MembershipUserCollection();
                foreach (MembershipUser u in result)
                {
                    c.Add(u);
                }
                return c;
            }
        }

    }
}