﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using AMicroblogAPI.Common;
using AMicroblogAPI;
using PumpkinHouseDatabase;
using System.Web.Security;
using AMicroblogAPI.DataContract;
using PumpkinHouseDatabase.Contract;
using System.Threading;
using PumpkinHouse.Utils;
using PumpkinHouse.Association;

namespace PumpkinHouse.Account
{
    public partial class SinaAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ExternalAuth auth = ((ExternalAuth)Master);
            auth.ShowAdvertiseRequest = true;
            auth.ShowWeiboRequest = true;
            auth.WeiboName = "新浪微博"; 
            ((ExternalAuth)Master).association = AssociationFactory.GetInstance(AssociationParty.Sina);
            ((ExternalAuth)Master).DoAuth();
        }
    }

    [DataContract]
    public class Token
    {
        [DataMember(Name = "access_token")]
        public string AccessToken { get; set; }

        [DataMember(Name = "expires_in")]
        public int ExpiresIn { get; set; }
    }
}