﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Discussion.aspx.cs" Inherits="PumpkinHouse.Discussion" MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="/Scripts/reply.js"></script>
    <script type="text/javascript">
        var globalBoardId = <%= BoardId %>;
        var globalBoardName = '<%= BoardName %>';
        var globalDiscussionId = <%= DiscussionId %>;
        var globalDiscussion;
        var globalReplyCol;

        function renderDiscussion(result) {
            renderTemplate('#discussion', '#discussionTemplate', {boardName: globalBoardName, discussion: result, username: globalLogginUser});
            globalDiscussion = result;
            globalReplyCol = globalDiscussion.replyList;

            initReplyDialog(replyAction, globalLoggedIn);
            initReplyReplyControl(replyReplyAction, globalLoggedIn);
            initReplyPagination();

            // 删除按钮
            $('.deleteDiscussionBtn').on('click', function () {
                showSmallDialog('#confirmDialogTemplate', '.confirmDialog', '确定要删除？');
                $('#confirmHolder .confirm').on('click', function () {
                    ajaxDeleteDiscussion(true, globalDiscussionId, function() {
                        document.location = "/DiscussionBoard.aspx?boardId=" + globalBoardId;
                    }, handleFault);
                });
            });
        }

        function initReplyPagination() {
            pagination('#pagination', globalDiscussion.replyList.count, 20, pageSelectCallback);
        }

        function pageSelectCallback(pageNumber, container) {
            ajaxGetDiscussionReply(true, globalDiscussionId, pageNumber, renderReplies, handleFault);
        }

        function replyAction(text) {
            ajaxPostReply(true, new ReplyToPost(text, null, null, globalDiscussionId), replySuccess, handleFault);
        }

        function replyReplyAction(text, replyId) {
            var reply = new ReplyToPost(text, null, null, globalDiscussionId, replyId);
            ajaxPostReply(true, reply, replySuccess, handleFault);
        }

        function renderReplies(paginationList) {
            var t = $('#replyDetailTemplate').tmpl(paginationList.list, { ownedByMe: globalIsMe });
            $('#replies').empty().append(t);

            initReplyReplyControl(replyReplyAction, globalLoggedIn);
        }

        function replySuccess(newReply) {
            globalDiscussion.replyList.list.unshift(newReply);
            globalDiscussion.replyList.count++;
            renderReplies(globalDiscussion.replyList);
            $('#numberOfReply').text(globalDiscussion.replyList.count);
            // jump to the first reply page
            $("#paginationHolder").trigger('setPage', [0]);
            $('#reply').val('');
        }

        function doAction() {
            ajaxGetDiscussion(true, globalDiscussionId, renderDiscussion, handleFault);
            renderTemplate('#ctrlPanel', '#discussionControlPanelTemplate', null);
        }
    
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div class="BoardList">
    <div class="siteLink clearfix float_l"><a href="/BoardList.aspx">讨论组首页</a> > <a href="/DiscussionBoard.aspx?boardId=<%= BoardId %>"><%= BoardName %></a></div>
	<div class="BoardLeft BoardDiscussion float_l" id="discussion">
        
    </div>
    <div class="BoardRight float_r" id="ctrlPanel">

    </div>
</div>
<div id="pagination"></div>

<!-- #Include virtual="/template/discussionTemplate.html" -->
<!-- #Include virtual="/template/replyTemplate.html" -->

</asp:Content>

