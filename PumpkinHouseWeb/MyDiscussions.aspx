﻿<%@ Page Language="C#" Title="我的话题 - 木头盒子" AutoEventWireup="true" CodeBehind="MyDiscussions.aspx.cs" Inherits="PumpkinHouse.MyDiscussions" MasterPageFile="~/Site.Master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<script type="text/javascript">
    var globalMode = '<%= mode %>';

    function initPagination(list) {
        var num_entries = list.count;
        // Create pagination element
        pagination("#paginationHolder", num_entries, 20, pageselectCallback);
        renderDiscussion(list);
    }

    function pageselectCallback(pageNumber, container) {
        if (globalMode == 'owner') {
            ajaxGetMyDiscussions(true, pageNumber, renderDiscussion, handleFault);
        }
        else if (globalMode == 'replier') {
            ajaxGetMyRepliedDiscussions(true, pageNumber, renderDiscussion, handleFault);
        }
    }

    function renderDiscussion(result) {
        var title = globalMode == 'owner' ? '我发布的话题' : '我回复的话题';
        renderTemplate('#list', '#discussionListTemplate', { boardName: title, list: result.list });       
    }

    function doAction() {
        if (globalMode == 'owner') {
            document.title = '我发布的话题 - 木头盒子';
            ajaxGetMyDiscussions(true, 0, initPagination, handleFault);
        }
        else if (globalMode == 'replier') {
            document.title = '我回复的话题 - 木头盒子';
            ajaxGetMyRepliedDiscussions(true, 0, initPagination, handleFault);
        }
    }
</script>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
 <div class="BoardList">
    <div class="siteLink clearfix float_l"><a href=BoardList.aspx>讨论组首页</a> > 已发话题</div>
	<div class="BoardLeft float_l clearfix">
       <div class="NewSpeech" id="list"></div>
    </div>
    <div class="BoardRight float_r">
    	 <div class="fbtn"><a href="/NewDiscussion.aspx">发表话题</a></div>
         <div class="ftopic"><a href="MyDiscussions.aspx?mode=owner">&nbsp;>我发起的话题</a></div>
         <div class="ftopic"><a href="MyDiscussions.aspx?mode=replier">&nbsp;>我回复的话题</a></div>
    </div>
    <div id="paginationHolder"></div>
</div>
<!-- #Include virtual="/template/discussionTemplate.html" -->
</asp:Content>
