﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PumpkinHouseDatabase;

namespace PumpkinHouse.Association
{
    public static class AssociationFactory
    {
        public static IAssociation GetInstance(AssociationParty party)
        {
            switch (party)
            {
                case AssociationParty.QQ:
                    {
                        return new QQSpace();
                    }
                case AssociationParty.Sina:
                    {
                        return new Sina();
                    }
                case AssociationParty.QQWB:
                    {
                        return new QQWeibo();
                    }
                case AssociationParty.Taobao:
                    {
                        return new Taobao();
                    }
            }
            return null;

        }
        public static IAssociation GetInstance(DB_Account_Association association)
        {
            switch ((AssociationParty)association.Party)
            {
                case AssociationParty.QQWB:
                    {
                        string savedToken = association.Access_Token;
                        string[] split = savedToken.Split('|');
                        return new QQWeibo(association.External_Username, split[0], split[1], split[2], split[3]);
                    }
                case AssociationParty.Sina:
                    {
                        string savedToken = association.Access_Token;
                        string[] split = savedToken.Split('|');
                        return new Sina(association.External_Username, split[0], split[1]);
                    }
                case AssociationParty.QQ:
                    {
                        string savedToken = association.Access_Token;
                        return new QQSpace(association.External_Username, savedToken);
                    }
                default:
                    return null;
            }
        }
    }
}