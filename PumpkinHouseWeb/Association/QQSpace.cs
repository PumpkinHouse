﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PumpkinHouseDatabase;
using QConnectSDK;
using PumpkinHouse.Utils;
using System.Drawing;
using System.IO;
using QConnectSDK.Context;
using System.Threading;
using System.Web.Security;
using PumpkinHouseDatabase.Contract;

namespace PumpkinHouse.Association
{
    public class QQSpace : IAssociation
    {
        private QOpenClient qzone;
        private AssociationInfo info = new AssociationInfo();

        public QQSpace()
        {
        }

        public QQSpace(string openId, string token)
        {
            QConnectSDK.Models.OAuthToken oToken = new QConnectSDK.Models.OAuthToken();
            oToken.AccessToken = token;
            oToken.OpenId = openId;

            qzone = new QOpenClient(oToken);
        }

        public void PostBlog(DB_Picture picture, string nickname)
        {
            PostBlog(ImageHelper.GetAccessPath(picture.Image_Filename) + "." + picture.Image_File_Ext,
                "http://www.mutouhezi.com/picture/" + picture.Id,
                picture.Description);
        }

        private void PostBlog(string imagePath, string linkUrl, string content)
        {
            qzone.AddFeeds(content,
                        linkUrl,
                        "",
                        "",
                        imagePath
                        );
        }


        public void SendAuthorizationRequest()
        {
            var context = new QzoneContext();
            string state = Guid.NewGuid().ToString().Replace("-", "");
            string scope = "get_user_info,add_share,list_album,upload_pic,check_page_fans,add_t,add_pic_t,del_t,get_repost_list,get_info,get_other_info,get_fanslist,get_idolist,add_idol,del_idol,add_one_blog,add_topic,get_tenpay_addr";
            var authenticationUrl = context.GetAuthorizationUrl(state, scope);

            HttpContext.Current.Session["requeststate"] = state;
            HttpContext.Current.Response.Redirect(authenticationUrl);
        }

        public AssociationInfo ParseAccessToken()
        {
            var verifier = HttpContext.Current.Request.Params["code"];
            string state = HttpContext.Current.Session["requeststate"].ToString();

            qzone = new QOpenClient(verifier, state);

            HttpContext.Current.Session["QzoneOauth"] = qzone;

            string username = "QQ_" + qzone.OAuthToken.OpenId;
            FormsAuthentication.SetAuthCookie(username, false);

            AssociationInfo info = new AssociationInfo();

            info.OpendId = qzone.OAuthToken.OpenId;
            info.Party = AssociationParty.QQ;
            info.PersistToken = qzone.OAuthToken.AccessToken;
            info.LinkName = "";
            return info;
        }

        public PumpkinHouseDatabase.Contract.UserForRegistration CreateUserData()
        {
            int retry = 0;
            QConnectSDK.Models.User currentUser = null;
            while (currentUser == null)
            {
                try
                {
                    currentUser = qzone.GetCurrentUser();
                }
                catch (QConnectSDK.Exceptions.QzoneException)
                {
                }
                retry++;
                Thread.Sleep(100);
                if (retry >= 10)
                {
                    throw new AssociationCommunicationException("cannot connect to QQSpace service");
                }
            }

            UserForRegistration user = new UserForRegistration();
            user.NickName = currentUser.Nickname;
            user.SelfIntroduction = currentUser.Msg;
            user.AvatarUrl = currentUser.Figureurl.Remove(currentUser.Figureurl.Length - 3) + "/100";
            return user;
        }

        public void FollowUs()
        {
            qzone.AddIdol("mutoubox", "");
        }

        public void AdvertiseUs()
        {
            PostBlog("http://image.mutouhezi.com/weibo_pic.png",
                "http://www.mutouhezi.com",
                "我刚在@mutoubox 安了家，你也一起来玩儿吧！>>>http://www.mutouhezi.com");
        }
    }
}