﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PumpkinHouseDatabase;
using AMicroblogAPI.DataContract;
using PumpkinHouse.Utils;
using AMicroblogAPI;
using AMicroblogAPI.Common;
using System.Threading;
using PumpkinHouseDatabase.Contract;
using log4net;

namespace PumpkinHouse.Association
{
    public class Sina : IAssociation
    {
        private AssociationInfo info = new AssociationInfo();

        private static ILog Log = LogManager.GetLogger(typeof(Sina));

        public Sina()
        {
        }

        public Sina(string userId, string token, string secret)
        {
            OAuthAccessToken oToken = new OAuthAccessToken();
            oToken.Secret = secret;
            oToken.Token = token;
            oToken.UserID = userId;

            AMicroblogAPI.Environment.AccessToken = oToken;
        }


        public void SendAuthorizationRequest()
        {
            OAuthRequestToken token = null;

            // 发送请求
            bool retry = false;
            int retryCount = 0;
            do
            {
                try
                {
                    token = AMicroblog.GetRequestToken();
                }
                catch (AMicroblogException err)
                {
                    if (err.ErrorCode == 503)
                    {
                        retry = true;
                        retryCount++;
                        Thread.Sleep(500);
                    }
                }
            } while (retry && retryCount < 10);

            HttpContext.Current.Session["RequestToken"] = token;
            var callbackUrl = HttpUtility.UrlEncode("http://www.mutouhezi.com/Account/SinaAuth.aspx");

            HttpContext.Current.Response.Redirect(string.Format("{0}?oauth_token={1}&oauth_callback={2}", APIUri.Authorize, token.Token, callbackUrl));

        }

        public AssociationInfo ParseAccessToken()
        {
            var verifer = HttpContext.Current.Request.QueryString["oauth_verifier"];

            var reqToken = HttpContext.Current.Session["RequestToken"] as OAuthRequestToken;
            var accessToken = AMicroblogAPI.AMicroblog.GetAccessToken(reqToken, verifer);

            AMicroblogAPI.Environment.AccessToken = accessToken;
            
            string username = accessToken.UserID;
            username = "Sina_" + username;

            info.OpendId = accessToken.UserID;
            info.Party = AssociationParty.Sina;
            info.PersistToken = accessToken.Token + "|" + accessToken.Secret;
            info.LinkName = accessToken.UserID;

            return info;
        }

        public UserForRegistration CreateUserData()
        {
            UserForRegistration u = new UserForRegistration();
            long uid = long.Parse(info.OpendId);
            UserInfo userInfo = AMicroblog.GetUserInfo(uid);
            u.SelfIntroduction = userInfo.Description;
            u.NickName = userInfo.ScreenName;
            u.AvatarUrl = userInfo.ProfileImageUrl.Replace("/50/", "/180/");
            return u;
        }

        
        public void PostBlog(DB_Picture picture, string nickname)
        {
            UpdateStatusWithPicInfo info = new UpdateStatusWithPicInfo();
            info.Status = picture.Description +  "来自" + nickname + "@木头盒子网 http://www.mutouhezi.com/picture/" + picture.Id;
            info.Pic = ImageHelper.GetImagePath(picture.Image_Filename, picture.Image_File_Ext);

            AMicroblog.PostStatusWithPic(info);
        }

        public void FollowUs()
        {
            try
            {
                AMicroblog.Follow(2642927004); // 官方账号
            }
            catch (AMicroblogException e)
            {
                if (e.ErrorCode == 40303)
                {
                    // Already followed
                }
                else
                {
                    Log.Error("error code: " + e.ErrorCode + ", msg: " + e.Message);
                    Log.Error(e.StackTrace);
                }
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                Log.Error(e.StackTrace);
            }
        }

        public void AdvertiseUs()
        {
            try
            {
                AMicroblog.PostStatus(new UpdateStatusInfo { Status = "我刚在@木头盒子网  安了家，你也一起来玩儿吧！>>>http://www.mutouhezi.com/" });
            }
            catch (AMicroblogException e)
            {
                if (e.ErrorCode == 40025)
                {
                    // repeated content
                }
                else
                {
                    Log.Error("error code: " + e.ErrorCode + ", msg: " + e.Message);
                    Log.Error(e.StackTrace);
                }
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                Log.Error(e.StackTrace);
            }
        }
    }
}