﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QAuth2.Context;
using OAuth2;
using System.Web.Security;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Account;
using Top.Api;
using Top.Api.Request;
using Top.Api.Response;

namespace PumpkinHouse.Association
{
    public class Taobao : IAssociation
    {
        private AssociationInfo info = new AssociationInfo();
        TaobaoClient oclient = null;

        public void PostBlog(PumpkinHouseDatabase.DB_Picture pic, string nickname)
        {
            throw new NotImplementedException();
        }

        public void SendAuthorizationRequest()
        {
            OAuthContext context = new OAuthContext("TaobaoSectionGroup/TaobaoSection");
            string state = Guid.NewGuid().ToString().Replace("-", "");
            string scope = "r1,r2,w1,w1";
            var authenticationUrl = context.GetAuthorizationUrl(state, scope);
            HttpContext.Current.Session["requeststate"] = state;
            HttpContext.Current.Response.Redirect(authenticationUrl);
        }

        public AssociationInfo ParseAccessToken()
        {
            var verifier = HttpContext.Current.Request.Params["code"];
            string state = HttpContext.Current.Session["requeststate"].ToString();

            oclient = new TaobaoClient(verifier, state);

            string username = "TB_" + oclient.OAuthToken.UserId;
            FormsAuthentication.SetAuthCookie(username, false);

            info.OpendId = oclient.OAuthToken.UserId;
            info.Party = AssociationParty.Taobao;
            info.PersistToken = oclient.OAuthToken.AccessToken;
            info.LinkName = "";

            return info;
        }


        public PumpkinHouseDatabase.Contract.UserForRegistration CreateUserData()
        {
            UserForRegistration user = new UserForRegistration();
            user.NickName = HttpUtility.UrlDecode(oclient.OAuthToken.NickName);

            string url = "http://gw.api.taobao.com/router/rest";
            string appkey = "12579490";
            string appsecret = "37b097f42d074b04f6cd091e66ec02ec";
            ITopClient client = new DefaultTopClient(url, appkey, appsecret);
            UserGetRequest req = new UserGetRequest();
            req.Fields = "user_id,nick,avatar";
            req.Nick = user.NickName;
            UserGetResponse response = client.Execute(req, "");

            user.SelfIntroduction = "";
            user.AvatarUrl = response.User.Avatar;

            return user;
        }


        public void FollowUs()
        {
            throw new NotImplementedException();
        }

        public void AdvertiseUs()
        {
            throw new NotImplementedException();
        }
    }
}