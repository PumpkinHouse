﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PumpkinHouseDatabase;
using PumpkinHouseDatabase.Contract;

namespace PumpkinHouse.Association
{
    public interface IAssociation
    {
        void PostBlog(DB_Picture pic, string nickname);

        void SendAuthorizationRequest();

        AssociationInfo ParseAccessToken();

        UserForRegistration CreateUserData();

        void FollowUs();

        void AdvertiseUs();
    }

    public class AssociationInfo
    {
        public string OpendId { get; set; }
        public AssociationParty Party { get; set; }
        public string AccessToken { get; set; }
        public string PersistToken { get; set; }
        public string LinkName { get; set; }
    }

    public class AssociationCommunicationException : Exception
    {
        public AssociationCommunicationException() : base() { }
        public AssociationCommunicationException(string message) : base(message) { }
        public AssociationCommunicationException(string message, Exception e) : base(message, e) { }
    }

    public class AssociationAlreadyBound : Exception
    {
        public AssociationAlreadyBound() : base() { }
        public AssociationAlreadyBound(string message) : base(message) { }
        public AssociationAlreadyBound(string message, Exception e) : base(message, e) { }
    }
}
