﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PumpkinHouseDatabase;
using System.IO;
using PumpkinHouse.Utils;
using System.Drawing;
using QWeiboSDK;
using System.Text;

namespace PumpkinHouse.Association
{
    public class QQWeibo : IAssociation
    {
        private OauthKey oToken;
        private AssociationInfo info = new AssociationInfo();
        private QWeiboSDK.User webUser;


        public QQWeibo()
        {
        }

        public QQWeibo(string userId, string consumerKey, string consumerSecret, string token, string secret)
        {
            oToken = new OauthKey();
            oToken.customKey = consumerKey;
            oToken.customSecret = consumerSecret;
            oToken.tokenKey = token;
            oToken.tokenSecret = secret;
        }

        public void PostBlog(DB_Picture picture, string nickname)
        {
            t weiboStub = new t(oToken, "json");
            UTF8Encoding utf8 = new UTF8Encoding();
            string result = weiboStub.add_pic(utf8.GetString(utf8.GetBytes(picture.Description +  " 来自" + nickname + "@@mutoubox " + "http://www.mutouhezi.com/picture/" + picture.Id)), utf8.GetString(utf8.GetBytes("120.31.128.20")), "", "", utf8.GetString(utf8.GetBytes(ImageHelper.GetImagePath(picture.Image_Filename, picture.Image_File_Ext))));
        }


        public void SendAuthorizationRequest()
        {
            string appKey = "801100268";
            string appSecret = "3376070180c075aec3775afd4b808a6d";

            OauthKey oauthKey = new OauthKey();
            oauthKey.customKey = appKey;
            oauthKey.customSecret = appSecret;
            oauthKey.callbackUrl = "http://www.mutouhezi.com/account/QQWeiboAuth.aspx";

            string accessKey;
            string accessSecret;

            GetRequestToken(oauthKey, out accessKey, out accessSecret);
            oauthKey.tokenKey = accessKey;
            oauthKey.tokenSecret = accessSecret;

            HttpContext.Current.Session["oauthKey"] = oauthKey;

            HttpContext.Current.Response.Redirect("http://open.t.qq.com/cgi-bin/authorize?oauth_token=" + accessKey);
        }

        public AssociationInfo ParseAccessToken()
        {
            OauthKey oauthKey = HttpContext.Current.Session["oauthKey"] as OauthKey;
            string OauthVerify = HttpContext.Current.Request.Params["oauth_verifier"] as string;

            string accessKey;
            string accessSecret;
            GetAccessToken(oauthKey.customKey, oauthKey.customSecret, oauthKey.tokenKey, oauthKey.tokenSecret, OauthVerify, out accessKey, out accessSecret);

            oauthKey.tokenKey = accessKey;
            oauthKey.tokenSecret = accessSecret;

            HttpContext.Current.Session["oauthKey"] = oauthKey;

            user webUserWrapper = new user(oauthKey, "json");
            webUser = webUserWrapper.info();

            info.OpendId = webUser.openid;
            info.Party = AssociationParty.QQWB;
            info.PersistToken = oauthKey.customKey + "|" + oauthKey.customSecret + "|" + accessKey + "|" + accessSecret;
            info.LinkName = webUser.name;

            return info;
        }

        public PumpkinHouseDatabase.Contract.UserForRegistration CreateUserData()
        {
            PumpkinHouseDatabase.Contract.UserForRegistration user = new PumpkinHouseDatabase.Contract.UserForRegistration();
            user.NickName = webUser.nick;
            user.SelfIntroduction = webUser.introduction;
            user.AvatarUrl = webUser.head + "/180";
            return user;
        }

        private bool GetRequestToken(OauthKey authKey, out string tokenKey, out string tokenSecret)
        {
            string url = "https://open.t.qq.com/cgi-bin/request_token";
            List<QWeiboSDK.Parameter> parameters = new List<QWeiboSDK.Parameter>();
            OauthKey oauthKey = new OauthKey();
            oauthKey.customKey = authKey.customKey;
            oauthKey.customSecret = authKey.customSecret;
            oauthKey.callbackUrl = authKey.callbackUrl;

            QWeiboRequest request = new QWeiboRequest();
            return ParseToken(request.SyncRequest(url, "GET", oauthKey, parameters, null), out tokenKey, out tokenSecret);
        }

        private bool ParseToken(string response, out string tokenKey, out string tokenSecret)
        {
            tokenKey = null;
            tokenSecret = null;
            if (string.IsNullOrEmpty(response))
            {
                return false;
            }

            string[] tokenArray = response.Split('&');

            if (tokenArray.Length < 2)
            {
                return false;
            }

            string strTokenKey = tokenArray[0];
            string strTokenSecrect = tokenArray[1];

            string[] token1 = strTokenKey.Split('=');
            if (token1.Length < 2)
            {
                return false;
            }
            tokenKey = token1[1];

            string[] token2 = strTokenSecrect.Split('=');
            if (token2.Length < 2)
            {
                return false;
            }
            tokenSecret = token2[1];

            return true;
        }

        private bool GetAccessToken(string customKey, string customSecret, string requestToken, string requestTokenSecrect, string verify, out string accessKey, out string accessSecret)
        {
            string url = "https://open.t.qq.com/cgi-bin/access_token";
            List<QWeiboSDK.Parameter> parameters = new List<QWeiboSDK.Parameter>();
            OauthKey oauthKey = new OauthKey();
            oauthKey.customKey = customKey;
            oauthKey.customSecret = customSecret;
            oauthKey.tokenKey = requestToken;
            oauthKey.tokenSecret = requestTokenSecrect;
            oauthKey.verify = verify;

            QWeiboRequest request = new QWeiboRequest();
            return ParseToken(request.SyncRequest(url, "GET", oauthKey, parameters, null), out accessKey, out accessSecret);
        }


        public void FollowUs()
        {
            friends friends = new friends(this.oToken, "json");
            friends.add("mutoubox", "");
        }

        public void AdvertiseUs()
        {
            t weiboStub = new t(oToken, "json");
            UTF8Encoding utf8 = new UTF8Encoding();
            string result = weiboStub.add_pic(utf8.GetString(utf8.GetBytes("我刚在@mutoubox 安了家，你也一起来玩儿吧！>>>http://www.mutouhezi.com ")), utf8.GetString(utf8.GetBytes("120.31.128.20")), "", "", utf8.GetString(utf8.GetBytes(Path.Combine(ImageHelper.ImageFilesRootPath, "weibo_pic.png"))));
        }
    }
}