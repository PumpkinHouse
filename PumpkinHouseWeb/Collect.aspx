﻿<%@ Page Language="C#" Title="盒子工具抓取 - 木头盒子" AutoEventWireup="true" CodeBehind="Collect.aspx.cs" Inherits="PumpkinHouse.Collect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Styles/jquery-ui-1.8.20/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="/Styles/common.css?d=20120701" rel="stylesheet" type="text/css" />


    <style type="text/css">
        #albumNameList 
            {
                position: absolute;
                z-index: 1000000;
                display: block;
                right:0px;
                top:-62px;
            }
    </style>
</head>
<body>

    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    <CompositeScript ScriptMode="Release">
        <Scripts>
            <asp:ScriptReference Path="/Scripts/lib/jquery-1.7.min.js" />
            <asp:ScriptReference Path="/Scripts/lib/jquery-ui-1.8.20.custom.min.core.js" />
            <asp:ScriptReference Path="/Scripts/lib/jquery.tmpl.js" />
            <asp:ScriptReference Path="/Scripts/lib/jquery.validate.min.js" />
            <asp:ScriptReference Path="/Scripts/lib/jquery.watermark.min.js" />
            <asp:ScriptReference Path="/Scripts/lib/jquery.data.js" />
            <asp:ScriptReference Path="/Scripts/lib/json2.js" />
            <asp:ScriptReference Path="/Scripts/upload.js" />
            <asp:ScriptReference Path="/Scripts/gettmpl.js" />
            <asp:ScriptReference Path="/Scripts/util.js" />
            <asp:ScriptReference Path="/Scripts/data.js" />
            <asp:ScriptReference Path="/Scripts/ajaxCall.js" />
            <asp:ScriptReference Path="/Scripts/tag.js" />
        </Scripts>
    </CompositeScript>
</asp:ScriptManager>
    <script type="text/javascript">
        var globalImageUrl = decodeURIComponent('<%= ImageUrl %>');
        var globalOriginalUrl = '<%= OriginalPageUrl %>';
        var globalTitle = decodeURIComponent('<%= OriginalTitle %>');
        var globalImageName = '<%= ImageName %>';
        var globalAlbumListControl;
        var globalAssociateAccounts = [<%= Party %>];

        var globalOriginalImageName = '<%= OriginalImageName %>';
        var globalErrorMessage = '<%= ErrorMessage %>';

        function showError(message) {
            alert(message);
        }

        function addPicture() {
            var albumId = globalAlbumListControl.albumId;
            var syncList = $.map($('.syncChkbox:checked'), function (val, index) {
                return $(val).attr('data-party');
            });
            var pic = new PictureToUpload(null, globalImageUrl, albumId, $('#tagInput').val(), $('#comment.Rounded').val(), $('#title').text(), $('#source').text(), globalImageUrl, syncList, 3);
            ajaxUploadPicture(true, pic, addPictureSuccess, function (errorCode) {
                if (errorCode == "2") {
                    $('.resultMessage').text('你已经分享过它了哦！');
                }    
                else if (errorCode == "4") {
                    $('.resultMessage', globalUploadDialog).text('对不起，暂不支持此文件格式');
                }
                else if (errorCode == "5") {
                    $('.resultMessage', globalUploadDialog).text('文件太大啦！');
                }
                else if (errorCode == "6") {
                    handleSensitiveWord(errorCode, '抱歉，标签包含非法字符，请修改后再分享。'); 
                }
                else if (errorCode == "7") {
                    showSmallDialog('#actionFailTemplate', '.failDialog', '请等一下，稍后再试');
                } else {
                    showError(errorCode);
                }
                $('.resultMessage').text('分享失败');
            });
            $('.resultMessage').text('分享中...');
        }

        function addPictureSuccess(pid) {
            $('.resultMessage').text('分享成功！');

            var message = $('#uploadSuccessTemplate').tmpl({pid: pid});      
            showSmallDialog('#collectCompleteTemplate', '.successDialog', message.html());

            setTimeout(closeWindow, 3000);
        }

        function closeWindow() {
            window.open('', '_parent', '');
            window.close();
        }

        function renderDialog() {
            var holder = $('#upload');
            holder.empty();
            var template = $('#collectTemplate').tmpl({ url: globalImageUrl, originalUrl: globalOriginalUrl, originalTitle: globalTitle,  tags: globalTags, albums: globalAlbums, partyList: globalAssociateAccounts, error: globalErrorMessage });
            holder.append(template);
            // bind event on album select div
            globalAlbumListControl = initAlbumListControl('#albumList', globalAlbums);

            var img = $('#preview');
            img.on('load', function() {
                var h = img[0].clientHeight;
                var w = img[0].clientWidth;
                var nh = h;
                var nw = w;
                var ratio = w * 1.0 / h;
                if (h > 210) {
                    nh = 210;
                    nw = nh * ratio;
                }
                img.css('height', nh).css('width', nw);
            });

            $('#comment').watermark('好图配好文，描述让分享更有价值。');
            $('#uploadBtn').addClass('ready');
        }

        $(function () {
            globalTags = getTagsAjaxSync(0, showError);
            globalAlbums = getMyAlbumNamesSync(showError);

            renderDialog();

            // add non-dialog-mode tag panel
            var tagger = new $.tagPanel(false, '#tagPanel', [], 0);

        });
    </script>
    <div id="upload">
    
    </div>
    <div id="confirmHolder"></div>

    </form>
</body>
<!-- #Include virtual="/template/uploadTemplate.html" -->
<!-- #Include virtual="/template/util.html" -->
</html>
