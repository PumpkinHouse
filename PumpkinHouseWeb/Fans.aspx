﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fans.aspx.cs" Inherits="PumpkinHouse.Fans" MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        var globalMode = '<%= mode %>';
        var globalUsername = '<%= username %>';
        var globalUser;

        function renderUserList(users) {
            var holder = $('#userList');
            holder.empty();
            var template = $('#userListTemplate').tmpl({ users: users.list, mode: globalMode });
            holder.append(template);

            $('.attBtn').fanButton(globalLoggedIn);
        }

        function pageselectCallback(pageIndex, container) {
            retrieveInfo(pageIndex, renderUserList);
        }

        function initPaging(results) {
            var num_entries = results.count;
            // Create pagination element
            pagination("#pagination", num_entries, 20, pageselectCallback);

            renderUserList(results);
        }

        function doAction () {
            globalUser = getUserSync(globalUsername, handleFault);
            retrieveInfo(0, initPaging);
        };

        function retrieveInfo(pageNumber, callback) {
            $('#userList').empty();

            if (globalMode == 'fans') {
                ajaxGetFans(true, globalUsername, pageNumber, callback, handleFault);
                $(".tagMenu ul li a.b").removeClass("cl");
                $(".tagMenu ul li a.a").addClass("cl");
            }
            else if (globalMode == 'follows') {
                ajaxGetAttentions(true, globalUsername, 0, callback, handleFault);
                $(".tagMenu ul li a.a").removeClass("cl");
                $(".tagMenu ul li a.b").addClass("cl");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="MessageBox" style="border:none;">
        <div class="tagMenu" >
    	    <ul>
        	    <li><a href="fans" class="a cl">
                    <%if (IsMe)
                      { %>我<% }
                      else
                      { %>TA<% } %>的粉丝 
                </a></li>
        	    <li><a href="follows" class="b">
                      <%if (IsMe)
                      { %>我<% }
                      else
                      { %>TA<% } %>的关注
                </a></li>
            </ul>
        </div>
        <div id="userList">
        </div>
    </div>
    <div id="pagination"></div>
<!-- #Include virtual="/template/userTemplate.html" -->
<!-- #Include virtual="/template/userListTemplate.html" -->

</asp:Content>
