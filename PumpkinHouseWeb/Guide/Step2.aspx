<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Step2.aspx.cs" Inherits="PumpkinHouse.Guide.Step2" MasterPageFile="/Account/Account.Master" %>

<asp:Content ID="Head" runat="server" ContentPlaceHolderID="headHolder">
<script type="text/javascript">
    $(document).ready(function () {
        $(".tabStepMenu1").click(function () {
            $(".tabStepMenu1 a").addClass("liSelected");
            $(".tabStepMenu2 a").removeClass("liSelected");
            $(".ConBox1").css("display", "block");
            $(".ConBox2").css("display", "none");
        });
        $(".tabStepMenu2").click(function () {
            $(".tabStepMenu2 a").addClass("liSelected");
            $(".tabStepMenu1 a").removeClass("liSelected");
            $(".ConBox2").css("display", "block");
            $(".ConBox1").css("display", "none");
        });
    });
</script>
</asp:Content>

<asp:Content ID="Content" runat="server" ContentPlaceHolderID="AccountContentHolder">
<div class="RegStep">
	<div class="step">
    	<ul>
        	<li><a href="javascript:void(0);">1</a> <span>关注你可能感兴趣的</span></li>
            <li class="stepet"><a href="javascript:void(0);">2</a> <span>入门教程</span></li>
            <li><a href="javascript:void(0);">3</a> <span>创建盒子</span></li>
        </ul>
    </div>

    <div class="tabStepMenu clearfix">
    	<ul class="float_l">
        	<li class="tabStepMenu1"><a class="liSelected">如何使用</a></li>
            <li class="tabStepMenu2"><a>找不到书签栏?</a></li>
        </ul>
        <div class="head float_r" style="width:300px; margin:0px; margin-bottom:5px;"><h2 class="float_r"><a href="Step3.aspx"><input type="button" value="安装成功，下一步" class="Public-Btn-Box stepBtn" />   跳过</a></h2></div>
    </div>
    <div class="ConBox1">
        <a title="盒子工具" href='javascript:void((function(){var d=document,e=d.createElement("script");e.setAttribute("charset","UTF-8");e.setAttribute("src","http://www.mutouhezi.com/scripts/collectTool.js?"+Math.floor(new Date/1E7));d.body.appendChild(e)})());'>
        <img src="/img/regStepPic/step2_menu1_1.jpg"  alt="盒子工具" /></a>
        <img src="/img/regStepPic/step2_menu1_2.jpg" />
        <a title="盒子工具" href='javascript:void((function(){var d=document,e=d.createElement("script");e.setAttribute("charset","UTF-8");e.setAttribute("src","http://www.mutouhezi.com/scripts/collectTool.js?"+Math.floor(new Date/1E7));d.body.appendChild(e)})());'>
        <img src="/img/regStepPic/step2_menu1_3.jpg" alt="盒子工具" /></a>
		<br /><br />
        <h1>还有问题？</h1>
		<br />
        详细操作，欢迎关注我们的官方微博（新浪 & 腾讯）<br/>
        或者点击&nbsp;<a href="http://www.mutouhezi.com/picture/21762" target="_blank"><input type="text" value="我要咨询" class="Public-Btn-Box WebSure" /></a>

    </div>
    <div class="ConBox2" style="display:none;">
        <img src="/img/regStepPic/step2_menu2_1.jpg" />
        <img src="/img/regStepPic/step2_menu2_2.jpg" />
        <img src="/img/regStepPic/step2_menu2_3.jpg" />
        <img src="/img/regStepPic/step2_menu2_4.jpg" />
        <img src="/img/regStepPic/step2_menu2_5.jpg" />
        <img src="/img/regStepPic/step2_menu2_6.jpg" />
    </div>
    <div class="stepBtnBox">
    	<div><a href="Step3.aspx" class="stepBtn"></a></div>
    </div>
</div>
</asp:Content>


