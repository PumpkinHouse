﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Step1.aspx.cs" Inherits="PumpkinHouse.Guide.Step1" MasterPageFile="/Account/Account.Master" %>

<asp:Content ID="Head" runat="server" ContentPlaceHolderID="headHolder">
   <script src="/Scripts/lib/jquery.dateFormat-1.0.js" type="text/javascript"></script>
   <script type="text/javascript">
        function renderUsers(users) {
            renderTemplate('.loveUser', '#peopleTemplate', users.list);
            $('#fanAllUsers').on('change', function (eventObject) {
                var target = $(eventObject.target);
                var checked = target.attr('checked') == 'checked';
                $('.fanUserChk').attr('checked', checked);
            });
        }

        $(function () {
            ajaxGetPopularUsers(true, 15, renderUsers, handleFault);

            $('.stepBtn').on('click', function () {
                var usernames = [];
                $('.fanUserChk').each(function (index, item) {
                    if ($(item).attr('checked') == 'checked') {
                        usernames.push($(item).attr('data-username'));
                    }
                });

                if (usernames.length < 1) {
                    showSmallDialog('#actionFailTemplate', '.failDialog', '至少关注1个好友哦！');
                    return;
                }

                ajaxAddInitialFavoriates(true, usernames, [], function () {
                    document.location = "/guide/step2.aspx";
                }, handleFault);

            });
        });
    </script>

    <style type="text/css">
        body{ text-align:left;}
    </style>
</asp:Content>

<asp:Content ID="Content" runat="server" ContentPlaceHolderID="AccountContentHolder">

<div class="RegStep">
	<div class="step">
    	<ul>
        	<li class="stepet"><a href="javascript:void(0);">1</a> <span>关注你可能感兴趣的</span></li>
            <li><a href="javascript:void(0);">2</a> <span>入门教程</span></li>
            <li><a href="javascript:void(0);">3</a> <span>创建盒子</span></li>
        </ul>
    </div>
   
    <div class="head">
    	<h1 class="float_l">恭喜，账号已生效。现在开始寻找你感兴趣的人吧！</h1> <h2 class="float_r"><b style="margin-right:35px; font-size:12px; font-family:'宋体'; font-weight:100;"><input type="checkbox" id="fanAllUsers" />一键关注 </b><input type="button" value="关注所选，下一步" class="Public-Btn-Box stepBtn" />   <a href="Step2.aspx">跳过</a></h2>
    </div>
    <div class="loveUser clearfix">
    </div>
    
</div>

<script id="albumTemplate" type="text/x-jQuery-tmpl">
    <li><div class="boxMargin">
        {{if pictures.list.length > 0}}
        <a href="/album/${id}" target="_blank"><img height="195" width="195" src="${pictures.list[0].url}_200_200_thumbnail.${pictures.list[0].ext}"/></a> <br />
        {{/if}}
        <div class="Name">
            <span class="float_l">${name}</span><span class="float_r"><input type="checkbox" data-album-id="${id}" class="likeAlbumChk"/></span>
        </div>
        </div>
    </li>
</script>
<script id="peopleTemplate" type="text/x-jQuery-tmpl">
    	<ul>
        	<li>{{tmpl({username: username, dd: 80, pd: 80}) '#userProfilePhotoTemplate'}}</li>
            <li>
            	<span class="name" style="width:85px; display:block; height:18px; word-break:keep-all; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">${displayName}</span>
                <span class="ColorC1" style="width:85px; display:block; height:18px; word-break:keep-all; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">${selfIntroduction}</span>
                <input type="checkbox" data-username="${username}" class="fanUserChk" /><br />
            </li>
        </ul>
</script>

<!-- #Include virtual="/template/util.html" -->
<!-- #Include virtual="/template/albumTemplate.html" -->

</asp:Content>


