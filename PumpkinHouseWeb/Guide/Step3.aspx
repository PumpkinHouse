<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Step3.aspx.cs" Inherits="PumpkinHouse.Guide.Step3" MasterPageFile="/Account/Account.Master"%>

<asp:Content ID="Head" runat="server" ContentPlaceHolderID="headHolder">

<script type="text/javascript">
    function addBox(content, insertInFront) {
        var holder = $('#boxHolder .InputBox');
        if (content.length > 0 && content.charAt(0) == '+') {
            content = content.substring(1);
        }
        var template = $('<div class="InputBoxText"><input type="text" value="' + content + '" class="Rounded toAddInput" /> <a class="remove" style="cursor: pointer"> 删除 </a></div>');
        if (insertInFront) {
            holder.prepend(template);
        }
        else {
            $('.newBox', holder).parent().before(template);
        }
        $('a.remove', template).on('click', function (eventObject) {
            var target = $(eventObject.target);
            target.parent().remove();
        });
        $('input', template).watermark('输入要创建的盒子名称');
    }
    $(function () {
        $('a.newBox').on('click', function () { addBox('', false) });
        $('a.toAdd').on('click', function (eventObject) {
            var target = $(eventObject.target);
            addBox(target.text(), true);
        });

        $('.stepBtn3').on('click', function () {
            var names = [];
            $('input.toAddInput').each(function (index, item) {
                var name = $.trim($(item).val());
                if (name) {
                    names.push(name);
                }
            });
            ajaxCreateAlbums(true, names, function () {
                document.location = '/';
            }, function (errorCode) {
                handleSensitiveWord(errorCode, '抱歉，专辑名包含非法字符，请修改后再创建。');
            });
        });
    });
</script>
</asp:Content>

<asp:Content ID="Content" runat="server" ContentPlaceHolderID="AccountContentHolder">
<div class="RegStep">
	<div class="step">
    	<ul>
        	<li><a href="javascript:void(0);">1</a> <span>关注你可能感兴趣的</span></li>
            <li><a href="javascript:void(0);">2</a> <span>入门教程</span></li>
            <li class="stepet"><a href="javascript:void(0);">3</a> <span>创建盒子</span></li>
        </ul>
    </div>

    <div class="head">
    	<h1 class="float_l">盒子是你的分享分类，它能够让你的宝贝们收藏井井有条哦！</h1> <h2 class="float_r"><input type="button" value="添加完成，下一步" class="stepBtn3 Public-Btn-Box stepBtn" /></h2>
    </div>

   <div class="step3ConBox clearfix">
   		<div class="float_l" id="boxHolder">
            <div class="InputBox clearfix">
            	<div class="InputBoxText"><input type="text" value="我想买" class="Rounded toAddInput" /><a class="remove"> 删除 </a></div>
                <div class="InputBoxText"><input type="text" value="我买过" class="Rounded toAddInput" /><a class="remove"> 删除 </a></div>
                <div class="InputBoxText"><a href="javascript:void(0);" class="newBox">+添加盒子</a></div>
            </div>
        </div>
         <div class="float_l fast-Add">
        	    <h1>快速添加盒子</h1><br />
                <a href="#" class="toAdd">+客厅</a>
                <a href="#" class="toAdd">+餐厅</a>
                <a href="#" class="toAdd">+卧室</a>
                <a href="#" class="toAdd">+厨房</a>
                <a href="#" class="toAdd">+沙发</a>
                <a href="#" class="toAdd">+椅子</a>
                <a href="#" class="toAdd">+茶几</a>
                <a href="#" class="toAdd">+衣柜</a>
         </div>
   </div>
    
    <div class="stepBtnBox">
    	<input type="button" value="" class="stepBtn3" />
    </div>
</div>
</asp:Content>