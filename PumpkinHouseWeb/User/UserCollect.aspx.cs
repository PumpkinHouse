﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PumpkinHouse.User
{
    public partial class UserCollect : System.Web.UI.Page
    {
        public string Mode;
        protected void Page_Load(object sender, EventArgs e)
        {
            Mode = Request.Params["mode"];
            if (string.IsNullOrEmpty(Mode))
            {
                Mode = "all";
            }
        }
    }
}