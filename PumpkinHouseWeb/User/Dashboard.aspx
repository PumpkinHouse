﻿<%@ Page Language="C#" Title="我的首页 - 木头盒子" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="PumpkinHouse.User.Dashboard"
    MasterPageFile="~/User/UserMaster.master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="UserScriptContent">
    <script type="text/javascript">
        var globalUsername = '<%= ((PumpkinHouse.User.UserMaster)this.Master).Username %>';
        var globalTags;
        var globalTabs;
        var globalMyInterestMasonry;
        var globalUser;
        var globalIsMe = true;
        var globalPictures;
        var globalPageNumber = 0;
        var globalAllPictures;
        var globalLock = 0;
        var globalCount;
        
        function updateCallback(newText, successCallback) {
            ajaxUpdateUserIntro(true, newText, successCallback, handleFault);
        }

        function getPicturesSuccess(pictures) {
            globalAllPictures = pictures;
            var galleryHolder = $('#news');

            var picGalleryTemplate;
            if (globalPageNumber == 0) {
                galleryHolder.empty();
                picGalleryTemplate = $('#myGalleryTemplate').tmpl({ pictures: pictures });
                picGalleryTemplate.appendTo(galleryHolder);

                globalMyInterestMasonry = $('#news').masonry({
                    itemSelector: '#news li',
                    columnWidth: 240
                });
            }
            else {
                picGalleryTemplate = $('#myGalleryTemplate').tmpl({ pictures: pictures });
                $('#news').append(picGalleryTemplate);
                $('#news').masonry('appended', picGalleryTemplate, true);
            }

            globalLock = 0;

            // bind event on collect buttons
            $('.collectButton').each(function (index, item) {
                var li = $(item).parents('li.pic');
                var displayTarget = li.find('a.collectCount');               
                $.collectButtion(item, globalLoggedIn, globalMyAlbums, displayTarget); 
            });

            initCollectButton();

            //喜欢按钮
            picGalleryTemplate.find('.likeBtn').on('click', function (e) {
                updatePictureLike(e, defaultLikeCallback);
            });

            bindReplyEvent(picGalleryTemplate);
        }

        function renderPicturePanel() {
            if (!globalPictures) {
                ajaxGetMyInterest(true, 0, getPicturesSuccess, handleFault);
            }
        }

        function loadUserContent() {
            renderPicturePanel();
            ajaxGetMyInterest(true, 1, cacheLoading, handleFault);            
            $(window).scroll(function () {
                if (globalAllPictures) {
                    if (getScrollTop() + $(window).height() + 100 > $(document).height()) {
                        if (globalLock == 0) {
                            if (globalPageNumber < Math.floor((globalAllPictures.count - 1) / 20)) {
                                globalLock = 1;
                                globalPageNumber++;
                                globalLoadingIndicatorAtBottom = true;
                                renderCache(getPicturesSuccess);
                                ajaxGetMyInterest(true, globalPageNumber + 1, cacheLoading, handleFault);
                            }
                        }
                    }
                }
            });
        };
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="UserContent">
    <style type="text/css">
	    .tabDashMenu ul .liSelectedDash{height:40px; background:#fff; color:#92b006; border:1px solid #e1e1e1; border-bottom:none; margin-top:0px;}
	    .tabDashMenu ul .liSelectedDash a{color:#8AB506;} 
    </style>

    <div id="dashboard" style="margin-left:0px; padding:0px; margin-top:3px;">
        <div class="PublicViewCon clearfix">
            <ul class="PublicList clearfix" id="news">
                        
            </ul>
        </div>
	</div>
<!-- #Include virtual="/template/dashboardTemplate.html" -->
<!-- #Include virtual="/template/collectTemplate.html" -->
<!-- #Include virtual="/template/replyTemplate.html" -->
</asp:Content>
