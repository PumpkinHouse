﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PumpkinHouse.User
{
    public partial class Profile : System.Web.UI.Page
    {
        protected string Username ;
        protected bool HasBoundError;
        protected void Page_Load(object sender, EventArgs e)
        {
            Username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ?  "" : (string) HttpContext.Current.Session["username"];
            HasBoundError = Session["BoundError"] != null;
            Session["BoundError"] = null;
        }
    }
}