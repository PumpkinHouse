﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PumpkinHouseDatabase.Contract;
using PumpkinHouseDatabase;
using PumpkinHouse.Utils;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace PumpkinHouse.User
{
    public partial class UserMaster : System.Web.UI.MasterPage
    {
        public string Username;
        protected UserToDisplay User;
        public bool IsDashboard = false;
        protected bool IsMe;
        protected string LinkPrefix;
        public string UserJsonObject;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsDashboard) IsDashboard = Request.Params["dashboard"] != null;
            if (IsDashboard)
            {
                Username = string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? "" : (string)HttpContext.Current.Session["username"];
            }
            else
            {
                Username = Request.Params["username"];
            }

            using (DataUtils utils = new DataUtils())
            {
                DB_User user = utils.FindUserByUsername(Username);
                User = Converter.ConvertUser(utils, user, 10);
                if (user == null)
                {
                    Response.Redirect("~/404.aspx");
                }
                else
                {
                    Page.Title = user.Nick_Name + "的个人主页 - 木头盒子";
                }
            }

            if (string.IsNullOrEmpty(Username))
            {
                Response.Redirect("~/404.aspx");
            }

            IsMe = IsDashboard ? true : Username == Helper.FindCurrentUsername();
            LinkPrefix = IsDashboard ? "/dashboard" : "/user/" + Username;

            UserJsonObject = JsonHelper<UserToDisplay>.ConvertToJson(User);
            return;
        }

    }
}