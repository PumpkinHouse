﻿<%@ Page Language="C#" Title="用户设置 - 木头盒子" AutoEventWireup="true" CodeBehind="Profile.aspx.cs"
    Inherits="PumpkinHouse.User.Profile" MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="ScriptContent">
    <link href="/Styles/jquery-ui-1.8.20/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <script src="/Scripts/lib/jquery-ui-1.8.20.custom.min.draggable.js" language="javascript" type="text/javascript"></script>
    <script src="/Scripts/lib/jquery.fileupload.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript">
        var globalUsername = '<%= Username %>';
        var globalProvinces;
        var globalRatio;
        var globalFlags = [];
        var globalDialog;
        var globalUserInfo;
        var globalProfilePhotoRoot = "http://image.mutouhezi.com/profile/";
        var globalCount = 0;

        function uploadPhotoSuccessful(e, data) {
            var result;
            if (typeof data.result == 'string') {
                result = $.parseJSON(data.result); /* other modern browers */
            }
            else {
                result = data.result; /* IE */
            }
            var imageName = result.name;
            var width = result.width;
            var height = result.height;

            // calculate the size
            globalRatio = 1.0;
            var maxHeight = 225;
            var maxWidth = 440;
            var fixedRatio = maxHeight * 1.0 / maxWidth;
            if (height > maxHeight || width > maxWidth) { // in that case we need to scale
                var ratio = 1.0 * height / width;
                if (ratio > fixedRatio) {
                    // fit the height
                    var originalHeight = height;
                    height = maxHeight;
                    width = height / ratio;
                    globalRatio = 1.0 * originalHeight / height;
                }
                else {
                    // fit the width
                    var originalWidth = width;
                    width = maxWidth;
                    height = width * ratio;
                    globalRatio = 1.0 * originalWidth / width;
                }
            }

            $('#statusText', globalDialog).hide();
            $('#imageArea', globalDialog).show();

            var rect = $('.drawArea').draggable({ containment: "parent" });
            $('.drawArea').resizable({ containment: "parent", aspectRatio: 1 });

            var minLength = width;
            if (height < minLength) minLength = height;

            if (minLength < 200) {
                $('.drawArea').css('height', minLength).css('width', minLength);
            }

            $('#imageArea').css('width', width + 'px');
            $('#imageArea').css('height', height + 'px');
            // set the image as the background
            var img = $('#bgImage');
            img.attr('src', '/Images/GetPhoto.aspx?name=' + imageName + "&a=Math.floor(new Date/10)");
            img.css('width', width + 'px');
            img.css('height', height + 'px');

            $('#selBtn').show();
        }

        function select() {
            var t = $('.drawArea');
            var m = 'top: ' + t.position().top + ', left: ' + t.position().left + '\n';
            m += 'size: ' + t.width();

            var x = Math.floor(t.position().left * globalRatio);
            var y = Math.floor(t.position().top * globalRatio);
            var d = Math.floor(t.width() * globalRatio);

            ajaxCropPhoto(true, x, y, d, cropSuccess, handleFault);
        }

        function cropSuccess() {
            if (globalDialog) globalDialog.closeDialog();
            $('#photo').attr('src', '/Images/GetPhoto.aspx?name=' + globalUsername + '_180_180.jpg&a=Math.floor(new Date/10)');
        }

        function uploadPhotoStarted() {
            if (globalDialog) {
                // globalDialog.dialog({ width: 570 , height: 370 });
                globalDialog.showDialog();
            }
            else {
                var template = $('#photoDialogTemplate').tmpl();
                globalDialog = template.mDialog({ width: 610, height: 410 });

                $('#selBtn').on('click', select);
            }

            $('#imageArea', globalDialog).hide();
            $('#selBtn').hide();
            $('#statusText', globalDialog).show().text('头像上传中……');

        }

        function renderUploadTemplate() {
            var template = $('#profilePhotoTemplate').tmpl();
            var holder = $('#photoHolder');
            holder.empty();
            holder.append(template);
            $('#photo').attr('src', '/Images/GetPhoto.aspx?name=' + globalUsername + '_180_180.jpg');

            $('#photoUploadForm').fileupload({
                'formData': { type: 'photo' }, /* 区分头像还是分享 */
                // Unwrapping HTML-encoded JSON required for IE's iframe transport.
                dataType: 'iframejson',
                converters: {
                    'html iframejson': function (htmlEncodedJson) {
                        return $.parseJSON($('<div/>').html(htmlEncodedJson).text());
                    },
                    'iframe iframejson': function (iframe) {
                        return $.parseJSON(iframe.find('body').text());
                    }
                }
            }).
                bind('fileuploaddone', uploadPhotoSuccessful).
                bind('fileuploadstart', uploadPhotoStarted);
        }

        function renderGeneralInfo(userInfo) {
            globalUserInfo = userInfo;
            var holder = $('#generalInfoHolder');
            holder.empty();
            var template = $('#userGeneralInfoTemplate').tmpl({ user: userInfo, provinces: globalProvinces });
            holder.append(template);

            // bind the event on province select control
            $('#province').on('change', onProvinceChange);

            // render the province
            var provinceId = userInfo.provinceName ? userInfo.provinceId : globalProvinces[0].Id;

            renderCities(provinceId);

            if (userInfo.provinceName) {
                // render the city
                $('#province').val(provinceId);
                $('#city').val(userInfo.cityId);
            }

            // render gender info
            $("input[name=gender][value=" + userInfo.gender + ']').attr("checked", "checked");

            // bind event on upate info button

            $('#generalInfoForm').validate({
                submitHandler: function () {
                    var g = $('input[name=gender]:checked').val();
                    var lid = $('#city').val();
                    var info = new UserGeneralInfo(g, lid, $('#intro').val(), $('#nickname').val());
                    ajaxUpdateUserInfo(true, info, updateUserInfoSuccess, handleFault);
                },
                messages: {
                    nickname: {
                        required: '昵称不能为空',
                        remote: '这个昵称已经被别人使用啦！',
                        maxlength: '昵称不能超过14字符/汉字'
                    }
                },
                rules: {
                    nickname: {
                        maxlength: 14
                    }
                }
            });

        }

        function updateUserInfoSuccess() {
            $('.resultMessage', '#tabHolder').text('设置成功');
        }

        function onProvinceChange() {
            var provinceId = $('option:selected', '#province').attr('value');
            renderCities(provinceId);
        }

        function renderCities(provinceId) {
            var cities = $.grep(globalProvinces, function (item, index) {
                return item.Id == provinceId;
            })[0].Cities;
            var cityTemplate = $('#locationTemplate').tmpl(cities);
            $('#city').empty().append('<option value="0"></option>');
            cityTemplate.appendTo('#city');
        }

        function renderEmail(userInfo) {
            globalUserInfo = userInfo;
            document.title = '用户邮箱设置 - 木头盒子';
            renderTemplate('#emailHolder', '#emailTemplate', userInfo);

            $('#updateEmailBtn').on('click', function () {
                $('form#emailForm').validate({
                    messages: {
                        email: {
                            required: '请输入信箱地址',
                            email: '信箱格式不正确！',
                            remote: '这个邮箱已经被别人使用啦！'
                        }
                    },
                    submitHandler: function () {
                        var newEmail = $('#email').val();
                        ajaxUpdateUserEmail(true, newEmail, function () {
                            $('#email').val(newEmail);
                            userInfo.email = newEmail;
                            alert('good');
                        }, handleFault);
                    }
                });
            });
        }

        function renderPassword() {
            var pwdHolder = $('#passwordHolder');
            pwdHolder.empty();
            var template = $('#resetPasswordTemplate').tmpl();
            pwdHolder.append(template);

            $('#resetPwdBtn').on('click', function () {
                $('form#pwdForm').validate({
                    messages: {
                        oldPwd: {
                            required: '请输入现在的密码'
                        },
                        newPwd: {
                            required: '请输入新密码'
                        },
                        repPwd: {
                            required: '请重复输入新密码',
                            equalTo: '两次密码输入必须一致'
                        }
                    },
                    submitHandler: function () {
                        var oldPwd = $('#oldPwd').val();
                        var newPwd = $('#newPwd').val();
                        ajaxUpdatePassword(true, oldPwd, newPwd, updatePasswordSuccess, handleFault);
                    }
                });
            });
        }

        function updatePasswordSuccess(result) {
            if (result) {
                $('.resultMessage', '#tabHolder').text('修改成功');
                $('#oldPwd').val('');
                $('#newPwd').val('');
                $('#repPwd').val('');
            }
            else {
                alert('现在的密码填写错误！');
            }
        }

        function renderReceiveEmail(userInfo) {
            if (userInfo) {
                renderTemplate('#receiveEmailHolder', '#receiveEmailTemplate', userInfo);
                $('input[name=receiveEmail][value=' + userInfo.receiveEmail + ']').attr('checked', true);
                globalUserInfo = userInfo;
                $('#updateReceiveEmailBtn').on('click', function () {
                    var result = $('input[name=receiveEmail]:checked').val();
                    ajaxUpdateReceiveEmail(true, result, function () {
                        $('.resultMessage', '#tabHolder').text('邮件订阅设置成功');
                    }, handleFault);
                });
            }
            else {
                ajaxGetUserInfo(true, renderReceiveEmail, handleFault);
            }
        }

        function renderAssociation(associationList) {
            renderTemplate('#associationHolder', '#associationListTemplate', { list: associationList });
            if ('<%= HasBoundError %>' === 'True') {
                alert('此第三方账号已与另一个木头盒子账号绑定。如要坚持绑定此账号，请用此第三方账号登录后取消与另一个账号的绑定。');
            }
            $('.bound_yes', '#associationHolder').on('click', removeAssociation);
        }

        function removeAssociation(event) {
            var target = $(event.target);
            var id = target.attr('data-id');
            ajaxRemoveAssociation(true, id, function () { ajaxGetAccountAssociations(true, renderAssociation, handleFault); }, handleFault);
        }

        function doAction() {

            //load province list
            globalProvinces = getLocationsSync(handleFault);

            globalTabs = $('#tabHolder').tabs({
                select: function (event, ui) {
                    var tabName = $(ui.tab).attr('href');
                    location.hash = tabName;
                    return true;
                },
                show: function (event, ui) {
                    var index = ($(this).tabs('option', 'selected'));
                    switch (index) {
                        case 0:
                            if (!globalFlags[0]) {
                                renderUploadTemplate();
                                if (!globalUserInfo) {
                                    ajaxGetUserInfo(true, renderGeneralInfo, handleFault);
                                }
                                else {
                                    renderGeneralInfo(globalUserInfo);
                                }
                            }
                            document.title = '用户基本信息设置 - 木头盒子';
                            break;
                        case 1:
                            if (!globalFlags[index]) {
                                renderPassword();
                            }
                            document.title = '用户密码设置 - 木头盒子';
                            break;
                        case 2:
                            ajaxGetAccountAssociations(true, renderAssociation, handleFault);
                            break;
                        case 3:
                            if (!globalFlags[index]) {
                                renderReceiveEmail(globalUserInfo);
                            }
                            document.title = '用户邮件订阅设置 - 木头盒子';
                            break;
                    }
                    globalFlags[index] = true;
                    $('.resultMessage', '#tabHolder').text('');
                }
            });
            $('#tabHolder').show();
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <style type="text/css">
        /*重构UI自带CSS代码*/
        .ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited{ color:#999;}
        .ui-state-active a
        {
            border: none;
            background: none;
        }
        
        .ui-corner-all
        {
            border-radius: 0px;
            border: none;
            margin: 0 auto;
        }
        .ui-widget-header
        {
            background: none;
            background: #ffffff;
            border: none;
        }
        .ui-corner-al
        {
            border: none;
            background: #FFFFFF;
        }
        .ui-tabs-selected, .ui-state-active, .ui-state-hover
        {
            border: none;
            background: none;
            border-radius: 0px;
            margin-top: -2px;
        }
        .ui-state-default
        {
            border: none;
            background: none;
            font-family: '微软雅黑';
            color: #aacd60;
            font-size: 16px;
        }
        .ui-state-default a:hover
        {
            color: #aacd60;
            font-size: 16px;
            background: none;
        }
        .ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited
        {
            color: #aacd60;
            text-decoration: none;
            cursor: pointer;
            margin-top: -2px;
            font-weight:100;
height:47px; background:#fff; color:#92b006; border:1px solid #e1e1e1; border-bottom:none; 
        }
        
        .ui-widget-content
        {
            border: none;
            background: none;
            overflow: hidden;
            margin: 0 auto;
        }
        .ui-widget-header
        {
            border: none;
            background: none;
            height: 60px;
        }
        .ui-widget-header .ui-icon
        {
            width: 35px;
            height: 35px;
            display: block;
            float: right;
            border: none;
        }
        .ui-icon:hover
        {
            display: block;
            margin-top: -2px;
            margin-right: -2px;
        }
        a.ui-dialog-titlebar-close a.ui-corner-all
        {
            border: 2px solid red;
        }
        
        .ui-resizable-se
        {
            display: none;
        }
        
        .ui-corner-all
        {
            background: none;
        }
        .ui-corner-all .ui-state-hover
        {
           
        }
        .ui-state-focus{ background:none;}
        
        .ui-tabs .ui-tabs-nav li a{ padding:0px; height:43px; width:88px; text-align:center; line-height:42px; background:#f1f1f1;argin:0px;}
        .ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited{ height:42px; width:88px; text-align:center; line-height:42px; background:#e1e1e1; border:none; margin:0px; margin-bottom:0px;}
        
    </style>

    <div id="tabHolder" class="UserModifyInfo" style="display: none; margin-top:30px;">
        <ul style="height:auto; border-bottom:1px solid #e1e1e1; padding:0px;">
            <li style="border: none; font-weight:100;"><a href="#tab1">基本信息</a></li>
            <li style="border: none; font-weight:100;"><a href="#tab3">密码</a></li>
            <li style="border: none; font-weight:100;"><a href="#tab4">账号绑定</a></li>
            <li style="border: none; font-weight:100;"><a href="#tab5">邮件订阅</a></li>
        </ul>
        <div id="tab1">
            <div id="photoHolder" class="UserModifyTab1">
            </div>
            <div id="generalInfoHolder" class="UserModifyTab2">
            </div>
        </div>

        <div id="tab3">
            <div id="passwordHolder" class="UserModifyTab4">
            </div>
        </div>

        <div id="tab4">
            <div id="associationHolder" class="UserModifyTab5">
                
            </div>
        </div>

        <div id="tab5">
            <div id="receiveEmailHolder" class="UserModifyTab6">
            </div>
        </div>
    </div>
    <!-- #Include virtual="/template/userProfileTemplate.html" -->
</asp:Content>
