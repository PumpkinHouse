﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserAlbum.aspx.cs" Inherits="PumpkinHouse.User.UserAlbum"
    MasterPageFile="~/User/UserMaster.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="UserScriptContent" runat="server">
    <script>
        function loadUserContent() {
            ajaxGetAlbumsOfUser(true, globalUsername, 0, true, 10, initMyAlbumsPagination, handleFault);
        }
    </script>
    
<!-- #Include virtual="/template/albumThumbnailTemplate.html" -->
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="UserContent" runat="server">
    <style type="text/css">
	    .tabDashMenu ul .liSelectedAlb a{height:40px; background:#e1e1e1; color:#8AB506;}
    </style>
    <div id="myAlbums" style="margin-top:-15px;"></div>
</asp:Content>
