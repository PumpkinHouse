﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserCollect.aspx.cs" Inherits="PumpkinHouse.User.UserCollect"
  MasterPageFile="~/User/UserMaster.master"
 %>
 <asp:Content ID="Content2" ContentPlaceHolderID="UserScriptContent" runat="server">
    <script>
        var globalUserCollectionPageNumber = 0;

        function getLoadFunction() {
            var mode = '<%= Mode %>';
            if (mode == 'all') {
                return ajaxGetAllCollectionsOfUser;
            } else if (mode == 'pub') {
                $(".tabDashMenu ul .liSelectedpub a").css({ "height": "40px", "background": "#e1e1e1", "color": "#8AB506"});
                return ajaxGetPicturesOfUser;
            } else if (mode == 'col') {
                $(".tabDashMenu ul .liSelectedcol").css({ "height": "40px", "background": "#e1e1e1", "color": "#8AB506" });
                return ajaxGetCollectionsOfUser;
            }
        }

        function loadUserContent() {

            var func = getLoadFunction();
            globalLock = 1;

            func(true, globalUsername, 0, function (pics) { renderPictures(pics); }, handleFault);

            func(true, globalUsername, 1, cacheLoading, handleFault);

            var holder = $('#myCollections');
            holder.scrollLoad({
                ScrollAfterHeight: 95,
                getDataAsync: function (callback) {
                    if (globalUserCollectionPageNumber < Math.floor((globalCount - 1) / 20)) {
                        globalUserCollectionPageNumber++;
                        globalLoadingIndicatorAtBottom = true;
                        renderCache(renderPictures);
                        func(true, globalUsername, globalUserCollectionPageNumber, callback, handleFault);
                    }
                    else {
                        callback(null);
                    }
                },
                onload: function (pics) {
                    if (pics) {
                        cacheLoading(pics);
                    }
                }
            });

        }
    </script>
<!-- #Include virtual="/template/collectTemplate.html" -->

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="UserContent" runat="server">
    <style type="text/css">
       /* 
	    .tabDashMenu ul .liSelectedpub{height:47px; background:#fff; color:#92b006; border:1px solid #e1e1e1; border-bottom:none; margin-top:1px;}
	    .tabDashMenu ul .liSelectedpub a{color:#8AB506;} 
	    
	    .tabDashMenu ul .liSelectedcol{height:47px; background:#fff; color:#92b006; border:1px solid #e1e1e1; border-bottom:none; margin-top:1px;}
	    .tabDashMenu ul .liSelectedcol a{color:#8AB506;} 
        */
    </style>

    <div class="PublicViewCon clearfix">
        <ul class="PublicList clearfix" id="myCollections">
        </ul>
    </div>
</asp:Content>