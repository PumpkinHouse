﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserLikeAlbum.aspx.cs"
    Inherits="PumpkinHouse.User.UserLikeAlbum" MasterPageFile="~/User/UserMaster.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="UserScriptContent" runat="server">
    <script>
        function loadUserContent() {
            ajaxGetAlbumsLikedBy(true, globalUsername, 0, initAlbumILikePagination, handleFault);
        }
    </script>
    <!-- #Include virtual="/template/albumThumbnailTemplate.html" -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="UserContent" runat="server">
    <style type="text/css">
	    .tabDashMenu ul .liSelectedLike a{height:40px; background:#e1e1e1; color:#8AB506;}
    </style>

    <div id="albumsILike"></div>
</asp:Content>