﻿<%@ Page Title="木头盒子 - 家居，创意家居，家居分享，淘宝家居，购物分享，装修指南，家装美图，家装效果图，装修交流，设计，简约，宜家" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="PumpkinHouse._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        defaultPage = true;
    </script>   
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">

    <asp:ScriptManagerProxy ID="ScriptManager2" runat="server">
            <Scripts>
               <asp:ScriptReference Path="/Scripts/default.js" />
               <asp:ScriptReference Path="/Scripts/likePicture.js" />            
            </Scripts>
    </asp:ScriptManagerProxy>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="Hot-Space-Style">
        <div class="HeadTag">
            <ul>
                <li><a href="javascript:void(0);" class="HeadTag-a">热门</a></li>
                <li><a href="javascript:void(0);" class="HeadTag-b">空间</a></li>
                <li><a href="javascript:void(0);" class="HeadTag-c">潮品</a></li>
                <li><a href="javascript:void(0);" class="HeadTag-d">风格</a></li>
            </ul>
            <div class="ad_158X33"><a href="/activities/20120708/20120708.aspx"><img src="/activities/20120708/images/index-ad.png" /></a></div>
        </div>
        <div class="Panel">
             <a href="javascript:moveLeft()" class="left-Btn"></a>
             <a href="javascript:moveRight()" class="right-Btn"></a>
             <div class="Pic-list">
                <% for (int i = 0; i < rTags.Length; i++)
                   { %>
                <ul class="Pic-list-ul-<%= (char) (i + 'a') %>" style="position: absolute; width: 2000px" display: <%= i==0 ? "block": "none"  %>">
                    <% foreach (var t in rTags[i])
                       { %>
                    <li><a href="/<% Response.Write(FindOrderString()); %>/<%= CommodityOnly ? "commodity" : "all" %>/<%= t.Name %>?cate=<%=i %>" class="imgUrl"><img src="<%= t.ImageUrl %>" width="150" height="150" /></a><a href="/hot/<%= CommodityOnly ? "commodity" : "all" %>/<%= t.Name %>" class="title"><%= t.Name%></a></li>
                    <% } %>
                </ul>
                <% } %>
             </div>       
        </div>
    </div>

        <script>
            $('.Hot-Space-Style').css('width', width);
            $('.Hot-Space-Style .HeadTag').css('width', width);
            $('.Hot-Space-Style .Panel').css('width', width - 24);
            $('.Hot-Space-Style .Pic-list').css('width', width - 180);
            if (width == 1200) {
                $('.Hot-Space-Style .Pic-list').css('width', width - 105); 
            }
        </script>

    <div class="Popu-Hot">
    <script>
        $('.Popu-Hot').css('width', width);
    </script>
        <div class="Btitle">
            <div class="leftPic float_l"></div>
            <div class="Con float_l"><% if (string.IsNullOrEmpty(Tag)) { Response.Write("全部"); } else { Response.Write(Tag); } %></div>
            <div class="rightPic float_l"></div>
        </div>
        <a href="/hot/<%= CommodityOnly ? "commodity" : "all" %>/<%= Tag %>" class="ch-tag hottag">人气</a>
        <a href="/latest/<%= CommodityOnly ? "commodity" : "all" %>/<%= Tag %>" class="ch-tag newtag">最新</a>
        <a class="onlyShow"><input type="checkbox" id="commodityOnly" style="margin-top:-2px; margin-top:-3px\0; *margin-top:0px; _margin-top:0px; margin-left:8px; +margin-left:4px;" /></a>
    </div>
        <script>
            var globalCommodityOnly = '<%= CommodityOnly %>' === 'True';
            var globalTag = '<%= Tag %>';
            var globalKeywordCategories;
            var globalAssociatedKeywords;
            var globalOrderMode = <%= (int)OrderMode %>;
            var globalPageNumber = 0;
            var globalLock = 0;
            var jsonStr = '<%= JsonObject %>';
            var json = $.parseJSON(jsonStr);
    
            var globalAllPictures;
            if (globalOrderMode == 1) {
                $(".hottag").css("color", "#aacd60");
            }else if(globalOrderMode == 2) {
                $(".newtag").css("color", "#aacd60");
            }else{
            }
            var globalTagCateId = <%= TagCategoryId %>
     </script>
    <!--line globalLoggedIn-->

    <style type="text/css">
        * + html .footer
        {
            display: none !important;
            display: block;
        }
    </style>

    <a href="#" class="bigPref" style="display:none"></a>
    <a href="#" class="bigNext" style="display:none"></a>

    <div class="PublicCon clearfix gallery">
        <ul class="PublicList clearfix">
        </ul>
        <div class="clearfix">
        </div>
    </div>
    <div class="hide" style="display:none">
        <% foreach (var pic in pictures)
           { %>
        <a href="/picture/<%= pic.Id %>">
            <img class="preview" alt="<%= Server.HtmlEncode(pic.Description) %>" src="<%=pic.Url %>_200_0_thumbnail.<%= pic.Extention %>" />
            <div>
                <%= Server.HtmlEncode(pic.Description) %></div>
            <div>
                <%= Server.HtmlEncode(pic.Tags) %></div>
        </a><a href="/User/<%= pic.Username %>">
            <%= Server.HtmlEncode(pic.NickName) %></a>
        <% if (pic.AlbumId == 0)
           { %>
        <a href="/album/default/<%= pic.Username %>">
            <%= Server.HtmlEncode(pic.AlbumName) %></a>
        <% }
           else
           { %>
        <a href="/album/<%= pic.AlbumId %>">
            <%= Server.HtmlEncode(pic.AlbumName) %></a>
        <% } %>
        <% } %>
    </div>
    <div id="paginationHolder"></div>
    <div id="bottom" class="clearfix">
    </div>
    <!-- #Include virtual="/template/collectTemplate.html" -->
    <!-- #Include virtual="/template/replyTemplate.html" -->

</asp:Content>
