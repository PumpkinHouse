﻿<%@ Page Language="C#" Title="我的提醒 - 木头盒子" AutoEventWireup="true" CodeBehind="Notice.aspx.cs" Inherits="PumpkinHouse.Notice" MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        var globalTabs;

        function renderAnnouncement(announceList) {
            renderTemplate('#tab3', '#announcementTemplate', announceList);
            document.title = '通知 - 木头盒子';
        }

        function renderNotice(noticeList) {
            renderTemplate('#tab1', '#noticeTemplate', noticeList.list);
            document.title = '提醒 - 木头盒子';
        }
        function renderReply(noticeList) {
            renderTemplate('#tab2', '#noticeTemplate', noticeList.list);
            document.title = '回复 - 木头盒子';
        }

        function initAnnoucementPagination() {

        }

        function initNoticePagination(result) {
            pagination('#pagination', result.count, 20, noticePageSelectCallback);
            renderNotice(result);
        }

        function noticePageSelectCallback(pageNumber, container) {
            ajaxGetNotice(
                true,
                11, //         Collect = 1, Fan = 2, Like = 8
                pageNumber,
                renderNotice,
                handleFault);
        }

        function initReplyPagination(result) {
            pagination('#pagination', result.count, 20, replyPageSelectCallback);
            renderReply(result);
        }

        function initAnnouncementPagination(result) {
            pagination('#pagination', 0, 20, replyPageSelectCallback);
            renderAnnouncement(result);
        }

        function replyPageSelectCallback(pageNumber, container) {
            ajaxGetNotice(
                true, 
                4, //         Collect = 1, Fan = 2, Like = 8
                pageNumber,
                renderReply,
                handleFault);
        }

        function doAction () {
            $.getTmplSync('/template/noticeTemplate.html');

            globalTabs = $('#noticeHolder').tabs({
                select: function (event, ui) {
                    var tabName = $(ui.tab).attr('href');
                    location.hash = tabName;
                    return true;
                },
                show: function (event, ui) {
                    var index = ($(this).tabs('option', 'selected'));
                    switch (index) {
                        case 0:
                            ajaxGetNotice(
                                true,
                                11, //         Collect = 1, Fan = 2, Like = 8
                                0,
                                initNoticePagination,
                                handleFault);
                            break;
                        case 1:
                            ajaxGetNotice(
                                true,
                                4, //         Reply = 4
                                0,
                                initReplyPagination,
                                handleFault);
                            break;
                        case 2:
                            ajaxGetNotice(
                                true,
                                16,
                                0, initAnnouncementPagination, handleFault);
                            break;
                    }
                }
            });

            $('#noticeHolder').show();

            $('.noticeLink').live('click', function () {
                var index = parseInt($(this).attr('data-tab-index'));
                if (index == globalTabs.tabs('option', 'selected')) return false;
                globalTabs.tabs('select', index);
                setTimeout(function () {
                    window.scrollTo(0, 0);
                    ajaxGetNoticeSummary(true, renderAlert, getSummaryFailed);
                }, 1);
            });
            
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<style type="text/css">
    /*重构UI自带CSS代码*/
	.ui-corner-all{border-radius:0px; border:none; border-top:1px solid #AEAEAE;}
	.ui-widget-header{background:none; background:#EDEDED; border:none;}
	.ui-corner-al{border:none; background:#FFFFFF}
	.ui-tabs-selected,.ui-state-active,.ui-state-hover{border:none; background:#FFFFFF; border-radius:0px; }
	.ui-state-default{border:none; background:none; font-family:'微软雅黑'; color:#28a7ff; font-size:16px;}
	ui-state-default a{background:none;}
	.ui-state-default a:hover{color:#28a7ff; font-size:16px; background:none;}
	.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited { color: #28a7ff; text-decoration: none; cursor:pointer;}
	.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus{ background:#FFFFFF;}	
	.ui-corner-all ul li a{background:none; border:1px solod red}
</style>
    <div class="siteLink clearfix" style="width:800px; height:55px; margin-top:50px;"><a href="/">木头盒子首页</a> >消息提醒</div>
    <div id="noticeHolder" class="NoticeBox" style="border: 0px solid red; background: none; display:none">
	    <ul style="border:none; background: #F6F6F6; height:37px;">
		    <li style="border:none; margin-left:10px;"><a href="#tab1">与我有关</a></li>
		    <li style="border:none;"><a href="#tab2">评论回复</a></li>
		    <li style="border:none;"><a href="#tab3">通知</a></li>
	    </ul>
	    <div id="tab1" style="border: 0px solid red; float:left">
		        
	    </div>
        <div id="tab2" style="border: 0px solid red; float:left">
		        
	    </div>
        <div id="tab3" style="border: 0px solid red; float:left">
		        
	    </div>
    </div>
    <div id="pagination"></div>
</asp:Content>