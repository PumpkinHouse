﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zt20120606.aspx.cs" Inherits="PumpkinHouse.zt._20120606.zt20120606" MasterPageFile="~/Site.master"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<style type="text/css">
.floatMenuHelp{ display:none;}
body{margin:0px; padding:0px; font-family:'微软雅黑'; font-size:14px; background:url(images/banner.jpg) #f1f1f1 no-repeat center 77px;}	
	a,img{text-decoration:none; border:0}
	.Guide{font-size:18px; color:#111111; height:auto; line-height:30px; margin:20px auto; padding:0 20px;}
	.Guide p{margin:0px; padding:0px; text-indent:36px;}
	.menu{font-size:24px; color:#f1f1f1; height:45px; line-height:45px;}
	.menu span{float:left; display:block;}
	.menu span.num{margin-right:20px; width:45px; text-align:center; font-size:48px; font-style:italic;}
	.tableConPic{border-collapse:collapse; border-color:#ddd; border:1px solid #ddd;margin-bottom:60px;}
	.tableConPic tr,.tableConPic tr td{border-collapse:collapse; border-color:#ddd; border:1px solid #ddd;}
	.tableMenu{}
	.picBox{width:199px; +width:198px; height:330px; padding:19px; background:#f6f6f6; border:1px solid #FFFFFF; color:#333; overflow:hidden}
	.picBox span{font-size:24px; color:#b11111}
	.picBox span.ms{font-size:13px; color:#666; font-family:'宋体'; line-height:22px;}
	.picBox a{color:#333333;}
	
	.tableBox{ background:#fff; width:961px; margin:0 auto;}
</style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <script>
        function doAction() { }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">



<table width="961" height="320"><tr><td></td></tr></table>
<div class="tableBox">
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td colspan="4" height="45">
    <div class="Guide">
<p>纪录片《舌尖上的中国》自开播以来，受到了海内外中华吃货的好评。据知情人士透露，作为这一系列纪录片的姐妹篇《舌尖上的厨房》也将于近日开拍，向大家讲述中华美食的背后，关于厨房里的故事。</p> 
<p>与《舌尖上的中国》相仿，姐妹篇《舌尖上的厨房》将分为6集进行拍摄，一位不愿意透露姓名的内部人士说，相关的工作计划将于猴年马月正式启动，并提供了整体的策划摘要。</p>
    </div>
	</td>
  </tr>
</table>

<table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="tableMenu">
  <tr>
    <td colspan="4" background="images/menu.jpg" height="45"><div class="menu"><span class="num">1</span><span>碗，生活在厨房最大的容器种族</span></div></td>
  </tr>
</table>
<table width="961" cellspacing="0" cellpadding="0" align="center" border="1" class="tableConPic">
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/25826" target="_blank"><img src="images/a1.jpg" width="200" height="200" /><br /><br />复古日式碗<br /><span>￥6.00</span><br /><span class="ms">日式传统卷草纹，纹饰笔触随意、自然。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/9326" target="_blank"><img src="images/a2.jpg" width="200" height="200" /><br /><br />粉色花边小碗<br /><span>￥18.00</span><br /><span class="ms">那淡淡的粉色花瓣和米黄色花蕊,虽是轻描淡写却很迷人。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/28360" target="_blank"><img src="images/a3.jpg" width="200" height="200" /><br /><br />京瓷碗套装<br /><span>￥99.00</span><br /><span class="ms">手绘菊花图，赏心悦目充满文化气息，可谓雅俗共赏。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/28580" target="_blank"><img src="images/a4.jpg" width="200" height="200" /><br /><br />巴洛克仿旧碗<br /><span>￥25.00</span><br /><span class="ms">绚烂的色彩，描绘出地中海的瑰丽颜色。</span></a></div></td>
  </tr>
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/6012" target="_blank"><img src="images/a5.jpg" width="200" height="200" /><br /><br />木槿花四角碗<br /><span>￥12.00</span><br /><span class="ms">素雅的花纹，独特的碗口设计，筷子不会担心掉下来哦。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/28370" target="_blank"><img src="images/a6.jpg" width="200" height="200" /><br /><br />稚气兔兔零食碗<br /><span>￥28.00</span><br /><span class="ms">小兔儿乖乖，把肚儿张开，零食要进来。哈哈，这碗太有童趣了。 </span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/22690" target="_blank"><img src="images/a7.jpg" width="200" height="200" /><br /><br />招财猫陶瓷碗<br /><span>￥49.00</span><br /><span class="ms">在日本，猫是高贵的，有神性的，已经成为一种“财缘”和“情缘”的文化符号。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29214" target="_blank"><img src="images/a8.jpg" width="200" height="200" /><br /><br />糖果色冰淇淋碗<br /><span>￥11.50</span><br /><span class="ms">粉粉嫩嫩的糖果色冰淇淋状，忍不住就想吃一口再吃一口。</span></a></div></td>
  </tr>
</table>

<table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="tableMenu">
  <tr>
    <td colspan="4" background="images/menu.jpg" height="45"><div class="menu"><span class="num">2</span><span>盛载美食的餐具，可爱造型的碟和盘子</span></div></td>
  </tr>
</table>
<table width="961" cellspacing="0" cellpadding="0" align="center" border="1" class="tableConPic">
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/23275" target="_blank"><img src="images/b1.jpg" width="200" height="200" /><br /><br />宝贝兔陶瓷碟<br /><span>￥22.00</span><br /><span class="ms">稚气的粉色兔子餐盘，让美食翻出童心的一面。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/22404" target="_blank"><img src="images/b2.jpg" width="200" height="200" /><br /><br />Zakka风格点心盘<br /><span>￥12.00</span><br /><span class="ms">简单的手绘小鸡盘，森林系女孩的最爱。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/23962" target="_blank"><img src="images/b3.jpg" width="200" height="200" /><br /><br />花边骨刺盘<br /><span>￥248.00</span><br /><span class="ms">神奇的自然界，各种昆虫图绘的花边盘。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/28608" target="_blank"><img src="images/b4.jpg" width="200" height="200" /><br /><br />带手柄的盘子<br /><span>￥25.00</span><br /><span class="ms">非常别致的彩绘小盘，装煎蛋、糕点、馅饼一定很棒。</span></a></div></td>
  </tr>
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/28604" target="_blank"><img src="images/b5.jpg" width="200" height="200" /><br /><br />田园风鸡蛋盘<br /><span>￥22.50</span><br /><span class="ms">色彩清淡素雅，小巧可爱，可以放三枚鸡蛋哦，三口之家非常适合。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/731" target="_blank"><img src="images/b6.jpg" width="200" height="200" /><br /><br />冰裂釉树叶碟<br /><span>￥9.80</span><br /><span class="ms">和风系餐碟，清新自然透着一股凉意。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/22341" target="_blank"><img src="images/b7.jpg" width="200" height="200" /><br /><br />美克美家餐盘<br /><span>￥25.00</span><br /><span class="ms">缤纷的色彩，幽默童趣的手绘插画盘让厨房别有一番风情。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/28751" target="_blank"><img src="images/b8.jpg" width="200" height="200" /><br /><br />公主家的餐盘<br /><span>￥110.00</span><br /><span class="ms">镂空花边餐盘，有公主范儿蕾丝的感觉。</span></a></div></td>
  </tr>
</table>

<table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="tableMenu">
  <tr>
    <td colspan="4" background="images/menu.jpg" height="45"><div class="menu"><span class="num">3</span><span>厨房的烹饪法宝，最之锅</span></div></td>
  </tr>
</table>
<table width="961" cellspacing="0" cellpadding="0" align="center" border="1" class="tableConPic">
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29135" target="_blank"><img src="images/c1.jpg" width="200" height="200" /><br /><br />最可爱的汤锅<br /><span>￥59.90</span><br /><span class="ms">红色的草莓搪瓷汤锅，将可爱进行到底。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29150" target="_blank"><img src="images/c2.jpg" width="200" height="200" /><br /><br />最古香古色的锅<br /><span>￥45.00</span><br /><span class="ms">精致的青花纹，给生活带来不一样的气息。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29165" target="_blank"><img src="images/c3.jpg" width="200" height="200" /><br /><br />最有爱的炖锅<br /><span>￥68.00</span><br /><span class="ms">把浓浓的爱用小火慢炖入浓浓的汤里。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29169" target="_blank"><img src="images/c4.jpg" width="200" height="200" /><br /><br />最高雅的米饭锅<br /><span>￥52.00</span><br /><span class="ms">造型高雅的陶瓷锅，可以带来烹饪的好心情哦。</span></a></div></td>
  </tr>
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29164" target="_blank"><img src="images/c5.jpg" width="200" height="200" /><br /><br />最实用的单柄砂锅<br /><span>￥42.80</span><br /><span class="ms">绚丽的颜色，便携式单柄，让你做的更轻松。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29213" target="_blank"><img src="images/c6.jpg" width="200" height="200" /><br /><br />最卡通的搪瓷锅<br /><span>￥45.00</span><br /><span class="ms">小黑猫造型的搪瓷锅，给厨房增添不少灵气。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29211" target="_blank"><img src="images/c7.jpg" width="200" height="200" /><br /><br />最浪漫的煲粥盅<br /><span>￥69.50</span><br /><span class="ms">粉紫色的圆点图案，让生活更浪漫舒适。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29136" target="_blank"><img src="images/c8.jpg" width="200" height="200" /><br /><br />最有创意的锅<br /><span>￥275.00</span><br /><span class="ms">给心爱的人做顿爱心早饭也是一件很有创意的事情。</span></a></div></td>
  </tr>
</table>

<table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="tableMenu">
  <tr>
    <td colspan="4" background="images/menu.jpg" height="45"><div class="menu"><span class="num">4</span><span>厨房的秘密武器，各式调料和他们的小家</span></div></td>
  </tr>
</table>
<table width="961" cellspacing="0" cellpadding="0" align="center" border="1" class="tableConPic">
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/25102" target="_blank"><img src="images/d1.jpg" width="200" height="200" /><br /><br />田园碎花调味罐<br /><span>￥19.80</span><br /><span class="ms">胖墩墩的样子，二侧还“长了”一对小翅膀，很是惹人喜爱。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29142" target="_blank"><img src="images/d2.jpg" width="200" height="200" /><br /><br />卡通奶牛调味瓶<br /><span>￥12.00</span><br /><span class="ms">圆滚滚奶牛陶瓷调料瓶,萌呀！</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29132" target="_blank"><img src="images/d3.jpg" width="200" height="200" /><br /><br />波点调味罐套装<br /><span>￥36.00</span><br /><span class="ms">水玉原点真好看，给厨房增添了不少幸福的味道。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/22256" target="_blank"><img src="images/d4.jpg" width="200" height="200" /><br /><br />简约调味罐三件套<br /><span>￥68.00</span><br /><span class="ms">素雅的白瓷搭配竹木的盖子，清新淡雅。</span></a></div></td>
  </tr>
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29148" target="_blank"><img src="images/d5.jpg" width="200" height="200" /><br /><br />绿野仙踪调味瓶<br /><span>￥35.00</span><br /><span class="ms">嫩绿的颜色，让厨房充满了森林的味道。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/28415" target="_blank"><img src="images/d6.jpg" width="200" height="200" /><br /><br />玻璃酱醋瓶<br /><span>￥59.00</span><br /><span class="ms">生活的幸福是在最真实的柴米油盐中懂得浪漫之道。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/5467" target="_blank"><img src="images/d7.jpg" width="200" height="200" /><br /><br />小豌豆调味罐<br /><span>￥12.80</span><br /><span class="ms">这个可爱实用的小豌豆，会让厨艺秀更加轻松多彩!</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29045" target="_blank"><img src="images/d8.jpg" width="200" height="200" /><br /><br />骨瓷椒盐罐<br /><span>￥55.00</span><br /><span class="ms">生活。一半是天使，一半是魔鬼。</span></a></div></td>
  </tr>
</table>

<table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="tableMenu">
  <tr>
    <td colspan="4" background="images/menu.jpg" height="45"><div class="menu"><span class="num">5</span><span>美食的闲暇时光，让壶当家</span></div></td>
  </tr>
</table>
<table width="961" cellspacing="0" cellpadding="0" align="center" border="1" class="tableConPic">
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/25373" target="_blank"><img src="images/e1.jpg" width="200" height="200" /><br /><br />清新森女范水壶<br /><span>￥208.00</span><br /><span class="ms">欧式乡村陶艺花器，胎体敦厚，色彩绚烂。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/22354" target="_blank"><img src="images/e2.jpg" width="200" height="200" /><br /><br />蔷薇花咖啡壶<br /><span>￥58.00</span><br /><span class="ms">清新优雅蔷薇花沿壶身依次绽放，颜色典雅而高贵，充满法式的优雅。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/23518" target="_blank"><img src="images/e3.jpg" width="200" height="200" /><br /><br />经典彩绘茶壶<br /><span>￥128.00</span><br /><span class="ms">古典雅致的造型，乡村田园的风格，为你带来不一样的体验。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29091" target="_blank"><img src="images/e4.jpg" width="200" height="200" /><br /><br />波西米亚风水壶<br /><span>￥128.00</span><br /><span class="ms">以一种呼唤大自然的姿态，重现70年代复古小清新的独特气质！</span></a></div></td>
  </tr>
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29133" target="_blank"><img src="images/e5.jpg" width="200" height="200" /><br /><br />爱尔兰风情奶壶<br /><span>￥25.00</span><br /><span class="ms">油画的色彩，静物的神韵，气场十足。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29092" target="_blank"><img src="images/e6.jpg" width="200" height="200" /><br /><br />欧式咖啡壶<br /><span>￥21.80</span><br /><span class="ms">纯手绘图标，可爱的壶嘴，简约却不简单。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29093" target="_blank"><img src="images/e7.jpg" width="200" height="200" /><br /><br />小红帽茶壶<br /><span>￥28.00</span><br /><span class="ms">壶盖是帽子，娃娃脸是可以独立使用的小杯子，超可爱的娃娃壶。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29094" target="_blank"><img src="images/e8.jpg" width="200" height="200" /><br /><br />清新自然的奶壶<br /><span>￥25.00</span><br /><span class="ms">清新田园风，可爱的瓢虫奶壶。</span></a></div></td>
  </tr>
</table>

<table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="tableMenu">
  <tr>
    <td colspan="4" background="images/menu.jpg" height="45"><div class="menu"><span class="num">6</span><span>终极美丽，厨房因收纳而更出众</span></div></td>
  </tr>
</table>
<table width="961" cellspacing="0" cellpadding="0" align="center" border="1" class="tableConPic">
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/9480" target="_blank"><img src="images/f1.jpg" width="200" height="200" /><br /><br />田园收纳挂柜<br /><span>￥296.00</span><br /><span class="ms">韩式田园实木挂柜，水洗白做旧风格哟。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/1398" target="_blank"><img src="images/f2.jpg" width="200" height="200" /><br /><br />原木杂货收纳盒<br /><span>￥26.00</span><br /><span class="ms">天然、环保、健康的原木家具透露着自然与原始之美。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/19939" target="_blank"><img src="images/f3.jpg" width="200" height="200" /><br /><br />欧式置物架<br /><span>￥130.00</span><br /><span class="ms">时尚铁艺，大方实用，为家居增添一份现代色彩。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/27946" target="_blank"><img src="images/f4.jpg" width="200" height="200" /><br /><br />日式收纳柜<br /><span>￥129.00</span><br /><span class="ms">浑厚沉实、明朗高尚，营造典雅悠游的空间表情。</span></a></div></td>
  </tr>
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29141" target="_blank"><img src="images/f5.jpg" width="200" height="200" /><br /><br />实木置物架<br /><span>￥14.50</span><br /><span class="ms">选用实木制作的家居盘子架，健康、朴实、清新、自然。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29112" target="_blank"><img src="images/f6.jpg" width="200" height="200" /><br /><br />橡木挂杯架<br /><span>￥49.90</span><br /><span class="ms">创意贴心的设计，给生活添加一份田园feel。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29116" target="_blank"><img src="images/f7.jpg" width="200" height="200" /><br /><br />母鸡形状鸡蛋篮<br /><span>￥32.00</span><br /><span class="ms">这才是真正的鸡笼装鸡蛋，生活就是要有些创意！</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/29103" target="_blank"><img src="images/f8.jpg" width="200" height="200" /><br /><br />双层回转收纳盘<br /><span>￥80.00</span><br /><span class="ms">多层旋转收纳木架，让凌乱的家更整洁。</span></a></div></td>
  </tr>
</table>
</div>
</asp:Content>
