﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zt20120703.aspx.cs" Inherits="PumpkinHouse.zt._20120703.zt20120703" MasterPageFile="~/Site.master"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<title>盛夏时节  让家清爽一夏（小编淘盒子N0.7）</title>
<style type="text/css">
body{margin:0px; padding:0px; font-family:'微软雅黑'; font-size:14px;}	
	a,img{text-decoration:none; border:0}
	.Guide{font-size:18px; color:#111111; height:auto; line-height:30px; margin:20px auto; padding:0 20px;}
	.Guide p{margin:0px; padding:0px; text-indent:36px;}
	.menu{font-size:24px; color:#f1f1f1; height:45px; line-height:45px;}
	.menu span{float:left; display:block;}
	.menu span.num{margin-right:20px; width:45px; text-align:center; font-size:48px; font-style:italic;}
	.tableConPic{border-collapse:collapse; border-color:#ddd; border:1px solid #ddd;margin-bottom:60px;}
	.tableConPic tr,.tableConPic tr td{border-collapse:collapse; border-color:#ddd; border:1px solid #ddd;}
	.tableMenu{}
	.picBox{width:199px; height:330px; padding:19px; background:#f6f6f6; border:1px solid #FFFFFF; color:#333}
	.picBox span{font-size:24px; color:#b11111}
	.picBox span.ms{font-size:13px; color:#666; font-family:'宋体'; line-height:22px;}
	.picBox a{color:#333333;}
	
	.tableBox{ background:#fff; width:961px; margin:0 auto;}
</style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptContent">
    <script>
        function doAction() { }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

<table width="961" height="auto" align="center"><tr><td><img src="images/banner.png" /></td></tr></table>
<div class="tableBox">
<table width="961" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td colspan="4" height="45">
    <div class="Guide">
<p>夏日炎炎，家成了避暑的港湾。躲在家中过盛夏，除了把冷气开大，有没有想过为家创造些清爽，以消除暑热带来的烦闷呢。</p> 
<p>生活家小编为你家量身订制了夏日清凉家居系列，营造新鲜感，让这个盛夏过得清爽一点。</p> 
	</div>
	</td>
  </tr>
</table>

<table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="tableMenu">
  <tr>
    <td colspan="4" background="http://www.mutouhezi.com/zt/20120626/images/menu.jpg" height="45"><div class="menu"><span class="num">1</span><span>清爽蓝，夏天家居的主打色，带给你仿若置身海边的悠闲心情</span></div></td>
  </tr>
</table>
<table width="961" cellspacing="0" cellpadding="0" align="center" border="1" class="tableConPic">
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81443" target="_blank"><img src="images/a1.jpg" width="200" height="200" /><br /><br />梦幻蓝家纺套装<br /><span>￥298.00</span><br /><span class="ms">蓝色纯棉床面，亲肤舒适，荷叶边围绕，精致公主。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81445" target="_blank"><img src="images/a2.jpg" width="200" height="200" /><br /><br />玉兔面具熊<br /><span>￥24.00</span><br /><span class="ms">蓝色经典小动物摆件，又萌又潮，爱死了。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81446" target="_blank"><img src="images/a3.jpg" width="200" height="200" /><br /><br />弧口冰纹花瓶<br /><span>￥189.00</span><br /><span class="ms">地中海风格蓝色单柄花瓶，做摆件也是可以的哟。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81448" target="_blank"><img src="images/a4.jpg" width="200" height="200" /><br /><br />熊公仔靠垫<br /><span>￥35.00</span><br /><span class="ms">韩国BUBU熊公仔，方形毛绒抱枕。</span></a></div></td>
  </tr>
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81450" target="_blank"><img src="images/a5.jpg" width="200" height="200" /><br /><br />摩洛哥桌布<br /><span>￥245.00</span><br /><span class="ms">跳跃的色彩，这个盛夏充满了生机与活力。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81537" target="_blank"><img src="images/a6.jpg" width="200" height="200" /><br /><br />蓝色高跟鞋碗<br /><span>￥9.00</span><br /><span class="ms">非常别致异形的曲柄碗，真有意思。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81536" target="_blank"><img src="images/a7.jpg" width="200" height="200" /><br /><br />毛绒河马公仔<br /><span>￥22.90</span><br /><span class="ms">HAPPY HOUSE家的粉蓝河马，给生活带来一丝乐趣。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81540" target="_blank"><img src="images/a8.jpg" width="200" height="200" /><br /><br />木质手绘花挂表<br /><span>￥30.00</span><br /><span class="ms">欧式田园风挂表，放家里很靓丽的哟。</span></a></div></td>
  </tr>
</table>

<table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="tableMenu">
  <tr>
        <td colspan="4" background="http://www.mutouhezi.com/zt/20120626/images/menu.jpg" height="45"><div class="menu"><span class="num">2</span><span>藤编家具，为你营造出清凉的居室环境</span></div></td>
  </tr>
</table>
<table width="961" cellspacing="0" cellpadding="0" align="center" border="1" class="tableConPic">
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81547" target="_blank"><img src="images/b1.jpg" width="200" height="200" /><br /><br />缅甸藤编杯垫（套）<br /><span>￥180.00</span><br /><span class="ms">浓烈的乡村气息，清新的田园风情！</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81541" target="_blank"><img src="images/b2.jpg" width="200" height="200" /><br /><br />欧典雅藤席<br /><span>￥172.00</span><br /><span class="ms">取材天然，透气亲肤，集竹席之凉爽，草席之柔软轻便等优点于一身。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81542" target="_blank"><img src="images/b3.jpg" width="200" height="200" /><br /><br />Zakka收纳筐<br /><span>￥15.80</span><br /><span class="ms">雏菊格子布迷你藤编筐，放桌上收纳些化妆品杂货等在合适不过了。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81546" target="_blank"><img src="images/b4.jpg" width="200" height="200" /><br /><br />藤编垃圾桶<br /><span>￥94.00</span><br /><span class="ms">田园风格的企鹅掌门人，盛装杂物废弃品，或作装饰都是不错的选择！</span></a></div></td>
  </tr>
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/19549" target="_blank"><img src="images/b5.jpg" width="200" height="200" /><br /><br />藤编小花车<br /><span>￥11.00</span><br /><span class="ms">很欧式的一款简约范儿小花瓶，爱田园爱柳编的你可千万别错过。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81535" target="_blank"><img src="images/b6.jpg" width="200" height="200" /><br /><br />手工编织椅垫<br /><span>￥29.00</span><br /><span class="ms">原色与染色混编出菱形的花纹，淳朴的气质适合各种家装风格。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81591" target="_blank"><img src="images/b7.jpg" width="200" height="200" /><br /><br />榻榻米坐垫<br /><span>￥29.00</span><br /><span class="ms">布艺装饰，搭配柔和的曲线，温文尔雅，冬暖夏凉。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81539" target="_blank"><img src="images/b8.jpg" width="200" height="200" /><br /><br />藤编吊灯<br /><span>￥39.90</span><br /><span class="ms">灯光从藤缝内投射出来，斑驳流离朦胧摇曳，真是美不胜收！</span></a></div></td>
  </tr>
</table>

<table width="961" border="0" cellspacing="0" cellpadding="0" align="center" class="tableMenu">
  <tr>
    <td colspan="4" background="http://www.mutouhezi.com/zt/20120626/images/menu.jpg" height="45"><div class="menu"><span class="num">3</span><span>没有西瓜的夏天是不完整的，这个夏天西瓜好忙</span></div></td>
  </tr>
</table>
<table width="961" cellspacing="0" cellpadding="0" align="center" border="1" class="tableConPic">
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81047" target="_blank"><img src="images/c1.jpg" width="200" height="200" /><br /><br />西瓜便签纸<br /><span>￥4.50</span><br /><span class="ms">熟熟的西瓜，仿佛被人用刀完美地切成了很多很多片，散开成一朵朵让人心醉的花。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81261" target="_blank"><img src="images/c2.jpg" width="200" height="200" /><br /><br />西瓜小花盆<br /><span>￥13.00</span><br /><span class="ms">手绘的西瓜小花盆，里面装点肉肉的小植物多美好。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/27455" target="_blank"><img src="images/c3.jpg" width="200" height="200" /><br /><br />西瓜靠枕<br /><span>￥69.00</span><br /><span class="ms">夏日冰冰凉，西瓜抱抱枕，清新爽目，赶走疲劳呵护我欢乐的小家。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81278" target="_blank"><img src="images/c4.jpg" width="200" height="200" /><br /><br />西瓜大碗<br /><span>￥17.00</span><br /><span class="ms">黝黑的西瓜子,加上红红的肉。看到它，我的饭量都增加了。</span></a></div></td>
  </tr>
  <tr>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81256" target="_blank"><img src="images/c5.jpg" width="200" height="200" /><br /><br />西瓜手工皂<br /><span>￥7.80</span><br /><span class="ms">非常好闻的皂皂，清新自然气泡多。孩子会很喜欢哦~</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81424" target="_blank"><img src="images/c6.jpg" width="200" height="200" /><br /><br />西瓜地毯<br /><span>￥198.00</span><br /><span class="ms">西瓜地毯，你忍心踩么？</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81263" target="_blank"><img src="images/c7.jpg" width="200" height="200" /><br /><br />西瓜烟灰缸<br /><span>￥16.50</span><br /><span class="ms">西瓜西瓜，肚圆如瓜。大热天最幸福的事情就是享受大西瓜的清凉。</span></a></div></td>
    <td><div class="picBox"><a href="http://www.mutouhezi.com/picture/81433" target="_blank"><img src="images/c8.jpg" width="200" height="200" /><br /><br />西瓜冰箱贴<br /><span>￥2.00</span><br /><span class="ms">西瓜就是要冰冰的才好吃。放冰箱里？NO，贴在外面也是可以的。</span></a></div></td>
  </tr>
</table>
<div style="width:960px; height:100px; position:relative;">
<h1 style="width:230px; height:50px; line-height:50px; position:absolute; z-index:33; left:365px; top:0px; font-size:30px; font-weight:100; color:#333; background:#fff; text-align:center;">往期专题回顾</h1>
<div style="width:960px; height:1px; position:absolute; z-index:11; top:49px; left:0px; border-top:1px solid #c1c1c1;"></div>
</div>



<style>
.oldpast{width:960px; height:auto; background:#fff}
.oldpast ul li{ float:left; text-align:center; margin:10px;}
.oldpast ul li img{border:1px solid #c1c1c1; padding:2px;}
</style>
<div class="oldpast clearfix">
	<ul>
        <li><a href="http://www.mutouhezi.com/zt/20120626/zt20120626.aspx"><img src="images/zt006.jpg" width="195" height="145" /></a><br />你的房子排毒了吗</li>
        <li><a href="http://www.mutouhezi.com/zt/20120619/zt20120619.aspx"><img src="http://www.mutouhezi.com/zt/20120626/images/zt005.jpg" width="195" height="145" /></a><br />欧洲杯，拒绝"足球寡妇"</li>
    	<li><a href="http://www.mutouhezi.com/zt/20120612/zt20120612.aspx "><img src="http://www.mutouhezi.com/zt/20120619/images/zt004.jpg" width="195" height="145" /></a><br />家居最炫民族风</li>
        <li><a href="http://www.mutouhezi.com/zt/20120606/zt20120606.aspx "><img src="http://www.mutouhezi.com/zt/20120619/images/zt003.jpg" width="195" height="145" /></a><br />舌尖上的厨房</li>
<!--        <li><a href="http://www.mutouhezi.com/zt/20120529/"><img src="http://www.mutouhezi.com/zt/20120619/images/zt002.jpg" width="195" height="145" /></a><br />那些年它们陪我长大</li>
        <li><a href="http://www.mutouhezi.com/zt/20120504/index.html"><img src="http://www.mutouhezi.com/zt/20120619/images/zt001.jpg" width="195" height="145" /></a><br />感恩母亲节，让我们来掌厨</li>-->

    </ul>
</div>



</div>

</asp:Content>
