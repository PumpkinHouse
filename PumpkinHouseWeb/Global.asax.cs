﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using PumpkinHouseDatabase;
using System.Configuration;
using log4net.Config;
using PumpkinHouse.Utils;
using Mail;

namespace PumpkinHouse
{
    public class Global : System.Web.HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Configure Log4Net
            XmlConfigurator.Configure();

            // Configure Email Client
            MailClient.LoadConfiguration(System.AppDomain.CurrentDomain.BaseDirectory.ToString() + ConfigurationManager.AppSettings["mail.Config"]);

            // Init Keyword Filter List
            KeywordFilterHelper.LoadKeywords(System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "data\\keywords.txt");

            // Code that runs on application startup
            DataUtils.ConnectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

            ImageHelper.ImageFilesRootPath = ConfigurationManager.AppSettings["imageRootFolder"];
            ImageHelper.ProfilePhotoRootFolder = ConfigurationManager.AppSettings["profilePhotoRootFolder"];

            KeywordHelper.GetKeywords(false);
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started
            if (string.IsNullOrEmpty(Session["username"] as string) && Context.User.Identity.Name != "")
            {
                using (DataUtils utils = new DataUtils())
                {
                    DB_User user = utils.FindUserByEmail(Context.User.Identity.Name);
                    if (user == null)
                    {
                        user = utils.FindUserByUsername(Context.User.Identity.Name);
                    }
                    if (user != null)
                    {
                        Session["username"] = user.Username;

                        foreach (var aa in user.DB_Account_Association)
                        {
                            if (aa.Token_Expire_Time > DateTime.Now)
                            {
                            }
                        }
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        Response.Redirect("/Account/Login.aspx");
                    }
                }
            }
        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.
        }

    }
}
