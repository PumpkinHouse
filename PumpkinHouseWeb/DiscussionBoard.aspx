﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiscussionBoard.aspx.cs" Inherits="PumpkinHouse.DiscussionBoard" MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        var globalBoardId = <%= BoardId %>;
        var globalBoardName = '<%= BoardName %>';
        function renderList(result) {
            renderTemplate('#list', '#discussionListTemplate', {boardName: globalBoardName, list: result.list});
        }

        function initPagination(list) {
            var num_entries = list.count;
            // Create pagination element
            pagination("#pagination", num_entries, 20, pageselectCallback);
            renderList(list);
        }

        function pageselectCallback(pageNumber, container) {
            ajaxGetDiscussions(true, globalBoardId, pageNumber, renderList, handleFault);            
        }

        function doAction() {
            ajaxGetDiscussions(true, globalBoardId, 0, initPagination, handleFault);
            renderTemplate('#ctrlPanel', '#discussionControlPanelTemplate', null);

        }
    
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div class="BoardList">
    <div class="siteLink clearfix float_l"><a href="/BoardList.aspx">讨论组首页</a> > 主题列表</div>
	<div class="BoardLeft float_l">
        <div class="NewSpeech" id="list">
            
        </div>
        <div id="pagination"></div>
    </div>
    <div class="BoardRight float_r" id="ctrlPanel">
        
    </div>
     <script type="text/javascript" src="http://v2.jiathis.com/code/jia.js" charset="utf-8"></script>
</div>
<!-- #Include virtual="/template/discussionTemplate.html" -->
</asp:Content>
