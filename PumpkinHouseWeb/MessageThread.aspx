﻿<%@ Page Language="C#" AutoEventWireup="true" Title="我的私信 - 木头盒子" CodeBehind="MessageThread.aspx.cs" Inherits="PumpkinHouse.MessageThread" MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<script type="text/javascript">
    function renderThreads(threads) {
        var holder = $('#threadsHolder');
        holder.empty();
        var template = $('#messageThreadTemplate').tmpl({ threads: threads.list });
        holder.append(template);
        $('.thread').on('click', 'a', function (e) {
            e.stopImmediatePropagation();
        });
        $('.thread').on('click', function (e) {
            var target = $(this).attr('data-target');
            document.location = '/message/' + target;
        });
    }

    function initPagination(threads) {
        var num_entries = threads.count;
        // Create pagination element
        pagination("#paginationHolder", num_entries, 20, pageselectCallback);

        renderThreads(threads);
    }

    function pageselectCallback(pageNumber, container) {
        ajaxGetMyThreads(true, pageNumber, renderThreads, handleFault);        
    }

    function doAction () {
        ajaxGetMyThreads(true, 0, initPagination, handleFault);
    }
</script>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div id="threadsHolder"></div>
    <div id="paginationHolder"></div>
<!-- #Include virtual="/template/messageTemplate.html" -->
</asp:Content>
