﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PumpkinHouse
{
    public partial class Search : System.Web.UI.Page
    {
        public string type;
        public string keyword;
        public bool commodityOnly = false;
        public int mode;
        protected void Page_Load(object sender, EventArgs e)
        {
            type = Request.Params["type"];
            keyword = Request.Params["keyword"];

            commodityOnly = type == "commodity";
            
            if (keyword != null) keyword = keyword.Trim();
            if (string.IsNullOrWhiteSpace(type))
                type = "picture";

            string title = "- 木头盒子";
            switch (type)
            {
                case "picture":
                case "commodity":
                    {
                        title = " 最热图片 - 木头盒子";
                        mode = 0;
                        break;
                    }
                case "album":
                    {
                        title = " 最热专辑 - 木头盒子";
                        mode = 1;
                        break;
                    }
                case "user":
                    {
                        title = " 盒友 - 木头盒子";
                        mode = 2;
                        break;
                    }

            }

            Page.Title = "搜索" + keyword + title;
        }
    }
}