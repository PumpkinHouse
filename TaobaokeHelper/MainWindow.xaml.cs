﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Top.Api.Request;
using Top.Api.Response;
using Top.Api;
using Top.Api.Domain;

namespace TaobaokeHelper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Regex taobaoItemRegex = new Regex(@"id=(\d+)");
        Regex shopRegex = new Regex(@"shop(\d+)\.taobao.com");


        private void txtItemLink_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox target = (TextBox)sender;
            string text = target.Text;
            btnGetItemLink.IsEnabled = !string.IsNullOrEmpty(text);
        }

        private void btnGetItemLink_Click(object sender, RoutedEventArgs e)
        {
            string text = txtItemLink.Text;
            Match match = taobaoItemRegex.Match(text);
            string idString;
            if (match.Success)
            {
                idString = match.Groups[1].Value;
                TaobaokeItem result = ConvertItem(idString);
                if (result == null)
                {
                    lblItemResult.Content = "本商品没有淘宝客链接";
                }
                else
                {
                    lblItemResult.Content = "价格：" + result.Price + "，返利率：" + (double.Parse(result.CommissionRate) / 100).ToString("0.0") + "%，卖家昵称：" + result.Nick ;
                    txtItemResultLink.Text = result.ClickUrl + "\n" + result.ShopClickUrl;
                    lblItemTitle.Content = result.Title;

                }
            }
            else
            {
                lblItemResult.Content = "链接读取失败，请确定链接是http://item.taobao.com/item.htm?id=***的形式";
            }

        }


        private void btnGetShopLink_Click(object sender, RoutedEventArgs e)
        {
            string text = txtShopLink.Text;
            Match match = shopRegex.Match(text);
            string idString;
            if (match.Success)
            {
                idString = match.Groups[1].Value;
                TaobaokeShop result = ConvertShop(idString);
                if (result == null)
                {
                    lblShopResult.Content = "没有淘宝链接";
                }
                else
                {
                    lblShopTitle.Content = result.ShopTitle;
                    lblShopResult.Content = "店铺返利率：" + result.CommissionRate + "%";
                    txtShopResultLink.Text = result.ClickUrl;
                }
            }
            else
            {
                lblShopResult.Content = "请确定链接是 http://shop******.taobao.com的形式";
            }

        }

        private TaobaokeItem ConvertItem(string id)
        {
            string serviceUrl = "http://gw.api.taobao.com/router/rest";
            string appkey = "12579490";
            string appsecret = "37b097f42d074b04f6cd091e66ec02ec";
            ITopClient client = new DefaultTopClient(serviceUrl, appkey, appsecret);

            TaobaokeItemsConvertRequest req = new TaobaokeItemsConvertRequest();
            req.Fields = "click_url,price,commission_rate,title,shop_click_url,nick";
            req.Nick = "raymanyoung";
            req.NumIids = id;
            try
            {
                TaobaokeItemsConvertResponse response = client.Execute(req);

                List<TaobaokeItem> items = response.TaobaokeItems;
                if (items.Count > 0)
                {
                    TaobaokeItem item = items[0];
                    return item;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private TaobaokeShop ConvertShop(string id)
        {
            string serviceUrl = "http://gw.api.taobao.com/router/rest";
            string appkey = "12579490";
            string appsecret = "37b097f42d074b04f6cd091e66ec02ec";
            ITopClient client = new DefaultTopClient(serviceUrl, appkey, appsecret);

            TaobaokeShopsConvertRequest req = new TaobaokeShopsConvertRequest();
            req.Fields = "shop_title,click_url,commission_rate";
            req.Nick = "raymanyoung";
            req.Sids = id;

            TaobaokeShopsConvertResponse response = client.Execute(req);

            List<TaobaokeShop> items = response.TaobaokeShops;
            if (items.Count > 0)
            {
                TaobaokeShop item = items[0];
                return item;
            }
            else
            {
                return null;
            }

        }

        private void txtShopLink_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            btnGetShopLink.IsEnabled = !string.IsNullOrEmpty(textBox.Text);
        }

    }
}
