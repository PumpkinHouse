﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PumpkinHouseDatabase;
using System.Text.RegularExpressions;
using Top.Api.Request;
using Top.Api.Response;
using Top.Api;
using Top.Api.Domain;
using System.Threading;
using System.IO;
using PumpkinHouse.Utils;
using Mail;
using System.Collections;

namespace Scheduler
{
    class Program
    {
        private static int Threshold = 1;

        static void Main(string[] args)
        {
//            MailClient.LoadConfiguration("mail.config");
            DataUtils.ConnectionString = Settings1.Default.ConnectionString;

            if (args.Length < 1)
            {
                PrintUsage();
                return;
            }

            if (args[0].Equals("-random", StringComparison.InvariantCultureIgnoreCase))
            {
                if (args.Length < 2)
                {
                    PrintUsage();
                }

                int threashold;
                if (int.TryParse(args[1], out threashold))
                {
                    RandomPictureSeed(threashold);
                    return;
                }
            }

            if (args[0].Equals("-guesslike", StringComparison.InvariantCultureIgnoreCase))
            {
                if (args.Length < 2)
                {
                    PrintUsage();
                    return;
                }

                GuessLike(int.Parse(args[1]));
                return;
            }

            if (args[0].Equals("-follow", StringComparison.InvariantCultureIgnoreCase))
            {
                FollowNewUsers();
            }

            PrintUsage();
            // ConvertTaobaokeLink();
            // LabelInvalidEmails();
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Scheduler.exe -random <threshold number of collection>");
            Console.WriteLine("Scheduler.exe -guesslike <threshold number of days>");
            Console.WriteLine("Scheduler.exe -follow");
        }

        private static void GuessLike(int day)
        {
            Console.WriteLine("Update Guess Like Data");
            using (DataUtils utils = new DataUtils())
            {
                var list = utils.FindViewCollection2().Where(c => c.Is_Original == 1).OrderByDescending(c => c.Id).Select(c => new { CollectionId = c.Id, Tags = c.Tags, Description = c.Description, AlbumName = c.Name, LikeCount = c.Like_Count, CollectCount = c.Collect_Count, PictureId = c.Picture_Id }).ToList();

                TimeSpan updateInterval = new TimeSpan(day, 0, 0, 0);

                for (int i = 0; i < list.Count; i++)
                {
                    var target = list[i];

                    if (string.IsNullOrWhiteSpace(target.Tags)) continue;

                    IList<DB_Guess_Like> existingList = utils.FindGuessLikeByTargetId(target.CollectionId).Take(1).ToList();

                    if (existingList.Count > 0 && DateTime.Now - existingList[0].Updated_Time > updateInterval )
                    {
                        continue;
                    }

                    var l = new[] { target }.ToList();
                    l.Clear(); // work around to create an anonymous list
                    for (int j = 0; j < list.Count; j++)
                    {
                        if (j == i) continue; // 跳过本身

                        var candidate = list[j];

                        string[] tags = target.Tags.Split(' ');
                        foreach (string s in tags)
                        {
                            if ((candidate.Tags != null && candidate.Tags.Contains(s)) || (candidate.Description != null && candidate.Description.Contains(s)) || (candidate.AlbumName != null && candidate.AlbumName.Contains(s)))
                            {
                                l.Add(candidate);
                                break;
                            }
                        }
                    }

                    l = l.OrderByDescending(t => t.CollectCount * 3 + t.LikeCount).Take(5).ToList();

                    for (int m = 0; m < l.Count; m++)
                    {
                        if (m < existingList.Count)
                        {
                            existingList[m].Index = (byte)m;
                            existingList[m].Updated_Time = DateTime.Now;
                            existingList[m].Related_Collection_Id = l[m].CollectionId;
                        }
                        else
                        {
                            utils.InsertGuessLike(target.CollectionId, l[m].CollectionId, (byte)m);
                        }
                    }
                    utils.Commit();
                }
            }
        }

        private static void FollowNewUsers()
        {
            Console.WriteLine("Following new registered users");

            string[] users = ParseUsers();

            using (DataUtils utils = new DataUtils(false))
            {
                IList<string> newUsers = utils.FindUsers().Where(u => u.Register_Time.AddDays(1) > DateTime.Now).Select(u => u.Username).ToList();
                foreach (var toFollow in newUsers)
                {
                    foreach (var follower in users)
                    {
                        Helper.FollowUser(toFollow, follower);
                    }
                    Console.WriteLine("Following user " + toFollow + " completed");
                }
            }

        }

        private static string GetRandomUser(string[] users)
        {
            Random random = new Random((int)DateTime.Now.Ticks);
            return users[random.Next(0, users.Length)];
        }

        private static string[] ParseUsers()
        {
            IList<string> list = new List<string>();

            using (StreamReader reader = new StreamReader(new FileStream("users.txt", FileMode.Open)))
            {
                string s = reader.ReadLine();
                while (!string.IsNullOrWhiteSpace(s))
                {
                    s = s.Trim();
                    list.Add(s);
                    s = reader.ReadLine();
                }
            }
            return list.ToArray();
        }

        private static void LabelInvalidEmails()
        {
            string input = @"E:\Work\tools\output31.txt";
            using (TextReader reader = new StreamReader(new FileStream(input, FileMode.Open)))
            {
                string line = null;
                using (DataUtils utils = new DataUtils())
                {
                    do
                    {
                        line = reader.ReadLine();
                        if (line == null) break;
                        string[] parts = line.Split('\t');

                        if ("Mailbox unavailable. The server response was: Connection frequency limited".Equals(parts[2]))
                        {
                            DB_User user = utils.FindUserByEmail(parts[0]);
                            if (user != null)
                            {
                                try
                                {
                                    EmailHelper.SendActivityEmail("我爱你 (520)“家”！ ---木头盒子之精华家居图片推荐", parts[0], @"activity\email20120518.html", new string[] { parts[1] });
                                    Console.Write(parts[0] + "\t" + parts[1] + "\tSuccess");
                                    //break;
                                }
                                catch (Exception e)
                                {
                                    Console.Write(parts[0] + "\t" + parts[1] + "\t" + e.Message);
                                }
                                Thread.Sleep(20000);
                            }
                        }
                    }
                    while (line != null);
                }
            }
        }

        private static void ConvertTaobaokeLink()
        {
            string url = "http://gw.api.taobao.com/router/rest";
            string appkey = "12579490";
            string appsecret = "37b097f42d074b04f6cd091e66ec02ec";
            ITopClient client = new DefaultTopClient(url, appkey, appsecret);

            using (DataUtils utils = new DataUtils())
            {
                TaobaokeItemsConvertRequest req = new TaobaokeItemsConvertRequest();
                req.Fields = "num_iid,click_url,iid,commission,commission_rate,commission_num,commission_volume";
                req.Nick = "raymanyoung";

                IList<DB_Picture> pics = utils.FindAllPictures(null, true).Where(p => p.Source_Page_Url != null && (p.Shop == 1 || p.Shop == 3)).ToList();
                foreach (var p in pics)
                {
                    if (string.IsNullOrEmpty(p.Source_Page_Url)) continue;

                    Regex regex = new Regex(@"id=(\d+)");
                    Match match = regex.Match(p.Source_Page_Url);
                    if (match.Success)
                    {
                        string id = match.Groups[1].Value;

                        req.NumIids = id;
                        TaobaokeItemsConvertResponse response = client.Execute(req);

                        List<TaobaokeItem> items = response.TaobaokeItems;
                        if (items.Count > 0)
                        {
                            TaobaokeItem item = items[0];
                            p.Taobaoke_Link = item.ClickUrl;
                            utils.Commit();
                            Console.WriteLine(string.Format("{3}\t{0}\t{1}\t{2}", item.ClickUrl, item.Commission, item.CommissionRate, p.Id));
                        }
                        else
                        {
                            Console.WriteLine(p.Id + "\tcannot find");
                        }
                        Thread.Sleep(600);
                    }
                    else
                    {
                        Console.WriteLine(p.Id + "\tfailed to parse");
                    }
                }
            }
        }

        private static void RandomPictureSeed( int threshold )
        {
            Console.WriteLine("Random Picture Seed. Threshold: " + threshold);
            Random random = new Random(DateTime.Now.Day * DateTime.Now.Millisecond);
            using (DataUtils utils = new DataUtils())
            {
                IList<DB_Picture> pics = utils.FindAllPictures(null, false).Where(p => p.DB_Collection.Count() > Threshold).ToList();
                foreach (var p in pics)
                {
                    Console.WriteLine("updating pic " + p.Id);
                    p.Seed = random.Next(0, 10000);
                    utils.Commit();
                }
            }
        }
    }
}
