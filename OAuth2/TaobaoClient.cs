﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QAuth2.Api;
using QAuth2.Models;
using QAuth2.Context;

namespace OAuth2
{
    public class TaobaoClient
    {
        RestApi restApi;
        public OAuthContext context ;

        public TaobaoClient(string verifierCode,string state)
        {
            context = new OAuthContext(verifierCode, "TaobaoSectionGroup/TaobaoSection");
            if (!string.IsNullOrEmpty(verifierCode))
            {
                this.OAuthToken = context.GetAccessToken(state); 
            }
            restApi = new RestApi(context);
        }

        /// <summary>
        /// 构造函数，用于用户已经完成授权后，将OAuthToken持久化保存后，使用这个函数从持久化介质中获取到的
        /// OAuthToken，进行后续的API调用。
        /// </summary>
        /// <param name="oAuthToken"></param>
        public TaobaoClient(OAuthToken oAuthToken)
        {
            this._oAuthToken = oAuthToken;
            context = new OAuthContext(oAuthToken, "TaobaoSectionGroup/TaobaoSection");
            restApi = new RestApi(context);
        }

        private OAuthToken _oAuthToken ;

        /// <summary>
        /// 访问QQ互联的SDK的AccessToken
        /// </summary>
        public OAuthToken OAuthToken 
        {
            get { return _oAuthToken; }
            set { _oAuthToken = value; }
        }

    }
}
