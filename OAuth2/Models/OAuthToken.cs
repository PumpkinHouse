﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QAuth2.Models
{
    /// <summary>
    /// QQ空间OAuth2.0
    /// </summary>
    [Serializable]
    public class OAuthToken
    {
        /// <summary>
        /// access token
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public int ExpiresAt { get; set; }

        public string NickName { get; set; }

        public string UserId { get; set; }

        public string RefreshToken { get; set; }

    }
}
