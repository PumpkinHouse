﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QAuth2.Context;
using QAuth2.Config;
using QAuth2.Models;
using QAuth2.Exceptions;
using QAuth2.Authenticators;
using RestSharp.Deserializers;
using System.Net;
using RestSharp;

namespace QAuth2.Api
{
    public partial class RestApi
    {
        /// <summary>
        /// 通过Authorization Code获取Access Token
        /// </summary>
        /// <param name="oAuthVericode"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public OAuthToken GetAccessToken(string oAuthVericode, string state)
        {
            var request = _requestHelper.CreateTokenRequest(context.Config, oAuthVericode, state);
            var response = Execute(request);
            var accessToken = GetUserAccessToken(response.Content);
            //var openid = GetOpenId(accessToken.AccessToken);
            // accessToken.OpenId = openid;
            return accessToken;
        }

        private string GetOpenId(string accessToken)
        {
            var request = _requestHelper.CreateOpenIDRequest(accessToken);
            var response = Execute(request);
            var openid = GetUserOpenId(response.Content);
            return openid;
        }

        private string GetUserOpenId(string content)
        {
            string strJson = content.Replace("callback(", "").Replace(");", "");
            var payload = Deserialize<Callback>(strJson);
            return payload.openid;
        }

        private OAuthToken GetUserAccessToken(string urlParams)
        {
            OAuthToken token = new OAuthToken();
            var parameters = urlParams.Replace("\"", "").Replace("\n", "").Replace("{", "").Replace("}", "").Split(',');
            foreach (var parameter in parameters)
            {
                var accessTokens = parameter.Split(':');
                if (accessTokens[0].Trim() == "access_token")
                {
                    token.AccessToken = accessTokens[1].Trim();
                     
                }
                if (accessTokens[0].Trim() == "re_expires_in")
                {
                    token.ExpiresAt = Convert.ToInt32(accessTokens[1]);             
                }

                if (accessTokens[0].Trim() == "taobao_user_nick")
                {
                    token.NickName = accessTokens[1].Trim();
                }

                if (accessTokens[0].Trim() == "taobao_user_id")
                {
                    token.UserId = accessTokens[1].Trim();
                }

                if (accessTokens[0].Trim() == "refresh_token")
                {
                    token.RefreshToken = accessTokens[1].Trim();
                }

            }
            return token;
        }

    }
}
