USE [PumpkinHouseDatabase]
GO
/****** Object:  ForeignKey [FK_DB_Album_DB_User]    Script Date: 02/21/2012 23:28:05 ******/
ALTER TABLE [dbo].[DB_Album] DROP CONSTRAINT [FK_DB_Album_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Announcement_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Announcement] DROP CONSTRAINT [FK_DB_Announcement_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Collection_DB_Album]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Collection] DROP CONSTRAINT [FK_DB_Collection_DB_Album]
GO
/****** Object:  ForeignKey [FK_DB_Collection_DB_Picture]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Collection] DROP CONSTRAINT [FK_DB_Collection_DB_Picture]
GO
/****** Object:  ForeignKey [FK_DB_Collection_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Collection] DROP CONSTRAINT [FK_DB_Collection_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Conversation_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Conversation] DROP CONSTRAINT [FK_DB_Conversation_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Conversation_DB_User1]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Conversation] DROP CONSTRAINT [FK_DB_Conversation_DB_User1]
GO
/****** Object:  ForeignKey [FK_DB_Discussion_DB_Discussion_Board]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Discussion] DROP CONSTRAINT [FK_DB_Discussion_DB_Discussion_Board]
GO
/****** Object:  ForeignKey [FK_DB_Discussion_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Discussion] DROP CONSTRAINT [FK_DB_Discussion_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Fan_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Fan] DROP CONSTRAINT [FK_DB_Fan_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Fan_DB_User1]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Fan] DROP CONSTRAINT [FK_DB_Fan_DB_User1]
GO
/****** Object:  ForeignKey [FK_DB_Keyword_DB_Keyword_Category]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Keyword] DROP CONSTRAINT [FK_DB_Keyword_DB_Keyword_Category]
GO
/****** Object:  ForeignKey [FK_DB_Like_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Like] DROP CONSTRAINT [FK_DB_Like_DB_User]
GO
/****** Object:  ForeignKey [FK_Like_Album]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Like] DROP CONSTRAINT [FK_Like_Album]
GO
/****** Object:  ForeignKey [FK_DB_Message_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Message] DROP CONSTRAINT [FK_DB_Message_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Message_DB_User1]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Message] DROP CONSTRAINT [FK_DB_Message_DB_User1]
GO
/****** Object:  ForeignKey [FK_DB_Notice_DB_Collection]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [FK_DB_Notice_DB_Collection]
GO
/****** Object:  ForeignKey [FK_DB_Notice_DB_Like]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [FK_DB_Notice_DB_Like]
GO
/****** Object:  ForeignKey [FK_DB_Notice_DB_Reply]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [FK_DB_Notice_DB_Reply]
GO
/****** Object:  ForeignKey [FK_DB_Notice_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [FK_DB_Notice_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Notice_DB_User1]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [FK_DB_Notice_DB_User1]
GO
/****** Object:  ForeignKey [FK_DB_Picture_DB_Album]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Picture] DROP CONSTRAINT [FK_DB_Picture_DB_Album]
GO
/****** Object:  ForeignKey [FK_DB_Picture_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Picture] DROP CONSTRAINT [FK_DB_Picture_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Reply_DB_Album]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply] DROP CONSTRAINT [FK_DB_Reply_DB_Album]
GO
/****** Object:  ForeignKey [FK_DB_Reply_DB_Discussion]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply] DROP CONSTRAINT [FK_DB_Reply_DB_Discussion]
GO
/****** Object:  ForeignKey [FK_DB_Reply_DB_Picture]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply] DROP CONSTRAINT [FK_DB_Reply_DB_Picture]
GO
/****** Object:  ForeignKey [FK_DB_Reply_DB_Reply]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply] DROP CONSTRAINT [FK_DB_Reply_DB_Reply]
GO
/****** Object:  ForeignKey [FK_DB_Reply_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply] DROP CONSTRAINT [FK_DB_Reply_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Tag_DB_User]    Script Date: 02/21/2012 23:28:07 ******/
ALTER TABLE [dbo].[DB_Tag] DROP CONSTRAINT [FK_DB_Tag_DB_User]
GO
/****** Object:  ForeignKey [FK_User_Location]    Script Date: 02/21/2012 23:28:07 ******/
ALTER TABLE [dbo].[DB_User] DROP CONSTRAINT [FK_User_Location]
GO
/****** Object:  Table [dbo].[DB_Notice]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [FK_DB_Notice_DB_Collection]
GO
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [FK_DB_Notice_DB_Like]
GO
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [FK_DB_Notice_DB_Reply]
GO
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [FK_DB_Notice_DB_User]
GO
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [FK_DB_Notice_DB_User1]
GO
ALTER TABLE [dbo].[DB_Notice] DROP CONSTRAINT [DF_DB_Notice_Has_Read]
GO
DROP TABLE [dbo].[DB_Notice]
GO
/****** Object:  Table [dbo].[DB_Reply]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply] DROP CONSTRAINT [FK_DB_Reply_DB_Album]
GO
ALTER TABLE [dbo].[DB_Reply] DROP CONSTRAINT [FK_DB_Reply_DB_Discussion]
GO
ALTER TABLE [dbo].[DB_Reply] DROP CONSTRAINT [FK_DB_Reply_DB_Picture]
GO
ALTER TABLE [dbo].[DB_Reply] DROP CONSTRAINT [FK_DB_Reply_DB_Reply]
GO
ALTER TABLE [dbo].[DB_Reply] DROP CONSTRAINT [FK_DB_Reply_DB_User]
GO
DROP TABLE [dbo].[DB_Reply]
GO
/****** Object:  Table [dbo].[DB_Collection]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Collection] DROP CONSTRAINT [FK_DB_Collection_DB_Album]
GO
ALTER TABLE [dbo].[DB_Collection] DROP CONSTRAINT [FK_DB_Collection_DB_Picture]
GO
ALTER TABLE [dbo].[DB_Collection] DROP CONSTRAINT [FK_DB_Collection_DB_User]
GO
ALTER TABLE [dbo].[DB_Collection] DROP CONSTRAINT [DF_DB_Collection_Collect_Type]
GO
DROP TABLE [dbo].[DB_Collection]
GO
/****** Object:  Table [dbo].[DB_Like]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Like] DROP CONSTRAINT [FK_DB_Like_DB_User]
GO
ALTER TABLE [dbo].[DB_Like] DROP CONSTRAINT [FK_Like_Album]
GO
DROP TABLE [dbo].[DB_Like]
GO
/****** Object:  Table [dbo].[DB_Picture]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Picture] DROP CONSTRAINT [FK_DB_Picture_DB_Album]
GO
ALTER TABLE [dbo].[DB_Picture] DROP CONSTRAINT [FK_DB_Picture_DB_User]
GO
ALTER TABLE [dbo].[DB_Picture] DROP CONSTRAINT [DF_DB_Picture_Is_Commodity]
GO
ALTER TABLE [dbo].[DB_Picture] DROP CONSTRAINT [DF_DB_Picture_Price]
GO
ALTER TABLE [dbo].[DB_Picture] DROP CONSTRAINT [DF_DB_Picture_Shop]
GO
DROP TABLE [dbo].[DB_Picture]
GO
/****** Object:  Table [dbo].[DB_Tag]    Script Date: 02/21/2012 23:28:07 ******/
ALTER TABLE [dbo].[DB_Tag] DROP CONSTRAINT [FK_DB_Tag_DB_User]
GO
DROP TABLE [dbo].[DB_Tag]
GO
/****** Object:  Table [dbo].[DB_Fan]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Fan] DROP CONSTRAINT [FK_DB_Fan_DB_User]
GO
ALTER TABLE [dbo].[DB_Fan] DROP CONSTRAINT [FK_DB_Fan_DB_User1]
GO
DROP TABLE [dbo].[DB_Fan]
GO
/****** Object:  Table [dbo].[DB_Conversation]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Conversation] DROP CONSTRAINT [FK_DB_Conversation_DB_User]
GO
ALTER TABLE [dbo].[DB_Conversation] DROP CONSTRAINT [FK_DB_Conversation_DB_User1]
GO
ALTER TABLE [dbo].[DB_Conversation] DROP CONSTRAINT [DF_DB_Conversation_Has_Read]
GO
ALTER TABLE [dbo].[DB_Conversation] DROP CONSTRAINT [DF_DB_Conversation_Count]
GO
DROP TABLE [dbo].[DB_Conversation]
GO
/****** Object:  Table [dbo].[DB_Discussion]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Discussion] DROP CONSTRAINT [FK_DB_Discussion_DB_Discussion_Board]
GO
ALTER TABLE [dbo].[DB_Discussion] DROP CONSTRAINT [FK_DB_Discussion_DB_User]
GO
DROP TABLE [dbo].[DB_Discussion]
GO
/****** Object:  Table [dbo].[DB_Album]    Script Date: 02/21/2012 23:28:05 ******/
ALTER TABLE [dbo].[DB_Album] DROP CONSTRAINT [FK_DB_Album_DB_User]
GO
ALTER TABLE [dbo].[DB_Album] DROP CONSTRAINT [DF_DB_Album_Number_Of_Like]
GO
DROP TABLE [dbo].[DB_Album]
GO
/****** Object:  Table [dbo].[DB_Announcement]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Announcement] DROP CONSTRAINT [FK_DB_Announcement_DB_User]
GO
DROP TABLE [dbo].[DB_Announcement]
GO
/****** Object:  Table [dbo].[DB_Message]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Message] DROP CONSTRAINT [FK_DB_Message_DB_User]
GO
ALTER TABLE [dbo].[DB_Message] DROP CONSTRAINT [FK_DB_Message_DB_User1]
GO
ALTER TABLE [dbo].[DB_Message] DROP CONSTRAINT [DF_DB_Message_Has_Read]
GO
DROP TABLE [dbo].[DB_Message]
GO
/****** Object:  View [dbo].[KeywordView]    Script Date: 02/21/2012 23:28:07 ******/
DROP VIEW [dbo].[KeywordView]
GO
/****** Object:  Table [dbo].[DB_Keyword]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Keyword] DROP CONSTRAINT [FK_DB_Keyword_DB_Keyword_Category]
GO
DROP TABLE [dbo].[DB_Keyword]
GO
/****** Object:  Table [dbo].[DB_User]    Script Date: 02/21/2012 23:28:07 ******/
ALTER TABLE [dbo].[DB_User] DROP CONSTRAINT [FK_User_Location]
GO
ALTER TABLE [dbo].[DB_User] DROP CONSTRAINT [DF_DB_User_Gender]
GO
ALTER TABLE [dbo].[DB_User] DROP CONSTRAINT [DF_DB_User_Active]
GO
ALTER TABLE [dbo].[DB_User] DROP CONSTRAINT [DF_DB_User_Email_Verified]
GO
ALTER TABLE [dbo].[DB_User] DROP CONSTRAINT [DF_DB_User_Receive_Email]
GO
ALTER TABLE [dbo].[DB_User] DROP CONSTRAINT [DF_DB_User_Locked]
GO
ALTER TABLE [dbo].[DB_User] DROP CONSTRAINT [DF_DB_User_Is_Test_User]
GO
ALTER TABLE [dbo].[DB_User] DROP CONSTRAINT [DF_DB_User_Is_Admin]
GO
DROP TABLE [dbo].[DB_User]
GO
/****** Object:  Table [dbo].[DB_UserCredential]    Script Date: 02/21/2012 23:28:07 ******/
DROP TABLE [dbo].[DB_UserCredential]
GO
/****** Object:  Table [dbo].[DB_Token]    Script Date: 02/21/2012 23:28:07 ******/
DROP TABLE [dbo].[DB_Token]
GO
/****** Object:  Table [dbo].[DB_Keyword_Category]    Script Date: 02/21/2012 23:28:06 ******/
DROP TABLE [dbo].[DB_Keyword_Category]
GO
/****** Object:  Table [dbo].[DB_Location]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Location] DROP CONSTRAINT [DF__location__fId__014935CB]
GO
DROP TABLE [dbo].[DB_Location]
GO
/****** Object:  Table [dbo].[DB_Discussion_Board]    Script Date: 02/21/2012 23:28:06 ******/
DROP TABLE [dbo].[DB_Discussion_Board]
GO
/****** Object:  Table [dbo].[DB_Discussion_Board]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Discussion_Board](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Board_Name] [nvarchar](50) NOT NULL,
	[Board_Description] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_DB_Discussion_Board] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Discussion_Board] ON
INSERT [dbo].[DB_Discussion_Board] ([Id], [Board_Name], [Board_Description]) VALUES (1, N'装修那点事儿', N' ')
INSERT [dbo].[DB_Discussion_Board] ([Id], [Board_Name], [Board_Description]) VALUES (2, N'爱美爱设计', N' ')
INSERT [dbo].[DB_Discussion_Board] ([Id], [Board_Name], [Board_Description]) VALUES (3, N'无闲聊，不生活', N' ')
INSERT [dbo].[DB_Discussion_Board] ([Id], [Board_Name], [Board_Description]) VALUES (4, N'木头站务专区', N' ')
SET IDENTITY_INSERT [dbo].[DB_Discussion_Board] OFF
/****** Object:  Table [dbo].[DB_Location]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Location](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Parent_Id] [bigint] NULL CONSTRAINT [DF__location__fId__014935CB]  DEFAULT (NULL),
	[Language] [int] NOT NULL,
 CONSTRAINT [PK__location__3213E83F7F60ED59] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (2, N'北京', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (3, N'北京', 2, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (4, N'上海', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (5, N'上海', 4, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (6, N'香港特别行政区', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (7, N'香港特别行政区', 6, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (8, N'澳门特别行政区', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (9, N'澳门特别行政区', 8, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (10, N'河北', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (11, N'石家庄', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (12, N'唐山', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (13, N'保定', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (14, N'衡水', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (15, N'秦皇岛', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (16, N'邯郸', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (17, N'邢台', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (18, N'保定', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (19, N'张家口', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (20, N'承德', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (21, N'沧州', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (22, N'廊坊', 10, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (23, N'山西', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (24, N'太原', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (25, N'大同', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (26, N'阳泉', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (27, N'长治', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (28, N'晋城', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (29, N'吕梁', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (30, N'晋中', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (31, N'运城', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (32, N'临汾', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (33, N'朔州', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (34, N'忻州', 23, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (35, N'辽宁', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (36, N'沈阳', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (37, N'大连', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (38, N'鞍山', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (39, N'抚顺', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (40, N'本溪', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (41, N'丹东', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (42, N'锦州', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (43, N'葫芦岛', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (44, N'营口', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (45, N'盘锦', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (46, N'阜新', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (47, N'朝阳', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (48, N'辽阳', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (49, N'铁岭', 35, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (50, N'吉林', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (51, N'长春', 50, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (52, N'四平', 50, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (53, N'辽源', 50, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (54, N'通化', 50, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (55, N'白山', 50, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (56, N'松原', 50, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (57, N'白城', 50, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (58, N'延边朝鲜族自治州', 50, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (59, N'吉林', 50, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (60, N'黑龙江', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (61, N'哈尔滨', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (62, N'齐齐哈尔', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (63, N'鹤 岗', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (64, N'双鸭山', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (65, N'鸡 西 ', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (66, N'大 庆', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (67, N'伊 春', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (68, N'牡丹江', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (69, N'佳木斯', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (70, N'七台河', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (71, N'黑 河', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (72, N'绥 化', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (73, N'大兴安岭地区', 60, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (74, N'江苏', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (75, N'南京', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (76, N'南通', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (77, N'苏州', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (78, N'扬州', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (79, N'无锡', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (80, N'常州', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (81, N'淮安', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (82, N'盐城', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (83, N'宿迁', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (84, N'徐州', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (85, N'连云港', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (86, N'镇江', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (87, N'泰州', 74, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (88, N'浙江', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (89, N'杭州', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (90, N'宁波', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (91, N'嘉兴', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (92, N'绍兴', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (93, N'温州', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (94, N'义乌', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (95, N'金华', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (96, N'舟山', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (97, N'湖州', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (98, N'衢州', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (99, N'台州', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (100, N'丽水', 88, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (101, N'安徽', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (102, N'合肥', 101, 0)
GO
print 'Processed 100 total records'
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (103, N'淮南', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (104, N'马鞍山', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (105, N'淮北', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (106, N'安庆', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (107, N'黄山', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (108, N'阜阳', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (109, N'宿州', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (110, N'六安', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (111, N'毫州', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (112, N'池州', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (113, N'宣城', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (114, N'巢湖', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (115, N'芜湖', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (116, N'蚌埠', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (117, N'铜陵', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (118, N'滁州', 101, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (119, N'福建', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (120, N'福州', 119, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (121, N'厦门', 119, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (122, N'泉州', 119, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (123, N'漳州', 119, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (124, N'三明', 119, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (125, N'莆田', 119, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (126, N'宁德', 119, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (127, N'南平', 119, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (128, N'龙岩', 119, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (129, N'江西', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (130, N'南昌', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (131, N'新余', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (132, N'庐山', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (133, N'景德镇', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (134, N'萍乡', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (135, N'九江', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (136, N'鹰潭', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (137, N'赣州', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (138, N'吉安', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (139, N'宜春', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (140, N'抚州', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (141, N'上饶', 129, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (142, N'山东', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (143, N'济南', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (144, N'青岛', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (145, N'淄博', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (146, N'枣庄', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (147, N'东营', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (148, N'烟台', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (149, N'威海', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (150, N'济宁', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (151, N'泰安', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (152, N'日照', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (153, N'聊城', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (154, N'潍坊', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (155, N'莱芜', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (156, N'临沂', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (157, N'德州', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (158, N'滨州', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (159, N'菏泽', 142, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (160, N'河南', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (161, N'郑州', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (162, N'开封', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (163, N'洛阳', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (164, N'平顶山', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (165, N'焦作', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (166, N'新乡', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (167, N'安阳', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (168, N'南阳', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (169, N'许昌', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (170, N'周口', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (171, N'鹤壁', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (172, N'濮阳', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (173, N'漯河', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (174, N'三门峡', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (175, N'商丘', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (176, N'信阳', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (177, N'驻马店', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (178, N'济源', 160, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (179, N'湖北', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (180, N'武汉', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (181, N'黄石', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (182, N'襄樊', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (183, N'荆州', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (184, N'荆门', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (185, N'宜昌', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (186, N'孝感', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (187, N'天门', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (188, N'仙桃', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (189, N'潜江', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (190, N'十堰', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (191, N'鄂州', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (192, N'黄冈', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (193, N'咸宁', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (194, N'随州', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (195, N'潜江', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (196, N'神农架林区', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (197, N'恩施土家族苗族自治州', 179, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (198, N'湖南', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (199, N'长沙', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (200, N'株洲', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (201, N'湘潭', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (202, N'衡阳', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (203, N'邵阳', 198, 0)
GO
print 'Processed 200 total records'
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (204, N'岳阳', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (205, N'常德', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (206, N'张家界', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (207, N'益阳', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (208, N'娄底', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (209, N'郴州', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (210, N'永州', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (211, N'怀化', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (212, N'湘西土家族苗族自治州', 198, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (213, N'广东', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (214, N'广州', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (215, N'深圳', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (216, N'珠海', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (217, N'汕头', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (218, N'韶关', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (219, N'佛山', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (220, N'中山', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (221, N'江门', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (222, N'汕尾', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (223, N'东莞', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (224, N'河源', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (225, N'梅州', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (226, N'湛江', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (227, N'茂名', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (228, N'肇庆', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (229, N'惠州', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (230, N'阳江', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (231, N'清远', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (232, N'潮州', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (233, N'揭阳', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (234, N'云浮', 213, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (235, N'甘肃', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (236, N'兰州', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (237, N'金昌', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (238, N'白银', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (239, N'天水', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (240, N'嘉峪关', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (241, N'武威', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (242, N'张掖', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (243, N'平凉', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (244, N'酒泉', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (245, N'庆阳', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (246, N'定西', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (247, N'陇南', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (248, N'临夏回族自治州', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (249, N'甘南藏族自治州', 235, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (250, N'四川', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (251, N'成都', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (252, N'自贡', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (253, N'攀枝花', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (254, N'泸州', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (255, N'德阳', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (256, N'绵阳', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (257, N'广元', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (258, N'遂宁', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (259, N'内江', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (260, N'乐山', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (261, N'南充', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (262, N'眉山', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (263, N'宜宾', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (264, N'广安', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (265, N'达州', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (266, N'雅安', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (267, N'巴中', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (268, N'资阳', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (269, N'阿坝藏族羌族自治州', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (270, N'甘孜藏族自治州', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (271, N'凉山彝族自治州', 250, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (272, N'贵州', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (273, N'贵阳', 272, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (274, N'六盘水', 272, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (275, N'遵义', 272, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (276, N'安顺', 272, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (277, N'铜仁地区', 272, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (278, N'毕节地区', 272, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (279, N'黔西南布依族苗族自治州', 272, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (280, N'黔东南苗族侗族自治州', 272, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (281, N'黔南布依族苗族自治州', 272, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (282, N'海南', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (283, N'海口', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (284, N'三亚', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (285, N'五指山', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (286, N'琼海', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (287, N'儋州', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (288, N'文昌', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (289, N'万宁', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (290, N'东方', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (291, N'澄迈', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (292, N'定安', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (293, N'屯昌', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (294, N'临高', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (295, N'白沙黎族自治县', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (296, N'昌江黎族自治县', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (297, N'乐东黎族自治县', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (298, N'陵水黎族自治县', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (299, N'保亭黎族苗族自治县', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (300, N'琼中黎族苗族自治县', 282, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (301, N'云南', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (302, N'昆明', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (303, N'曲靖', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (304, N'玉溪', 301, 0)
GO
print 'Processed 300 total records'
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (305, N'保山', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (306, N'昭通', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (307, N'丽江', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (308, N'思茅', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (309, N'临沧', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (310, N'文山壮族苗族自治州', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (311, N'红河哈尼族彝族自治州', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (312, N'西双版纳傣族自治州', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (313, N'楚雄彝族自治州', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (314, N'大理白族自治州', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (315, N'德宏傣族景颇族自治州', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (316, N'怒江傈傈族自治州', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (317, N'迪庆藏族自治州', 301, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (318, N'青海', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (319, N'西宁', 318, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (320, N'海东地区', 318, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (321, N'海北藏族自治州', 318, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (322, N'黄南藏族自治州', 318, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (323, N'海南藏族自治州', 318, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (324, N'果洛藏族自治州', 318, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (325, N'玉树藏族自治州', 318, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (326, N'海西蒙古族藏族自治州', 318, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (327, N'陕西', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (328, N'西安', 327, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (329, N'铜川', 327, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (330, N'宝鸡', 327, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (331, N'咸阳', 327, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (332, N'渭南', 327, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (333, N'延安', 327, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (344, N'汉中', 327, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (345, N'榆林', 327, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (346, N'安康', 327, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (347, N'商洛', 327, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (348, N'广西壮族自治区', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (349, N'南宁', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (350, N'柳州', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (351, N'桂林', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (352, N'梧州', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (353, N'北海', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (354, N'防城港', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (355, N'钦州', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (356, N'贵港', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (357, N'玉林', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (358, N'百色', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (359, N'贺州', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (360, N'河池', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (361, N'来宾', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (362, N'崇左', 348, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (363, N'西藏自治区', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (364, N'拉萨', 363, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (365, N'那曲地区', 363, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (366, N'昌都地区', 363, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (367, N'山南地区', 363, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (368, N'日喀则地区', 363, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (369, N'阿里地区', 363, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (370, N'林芝地区', 363, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (371, N'宁夏回族自治区', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (372, N'银川', 371, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (373, N'石嘴山', 371, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (374, N'吴忠', 371, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (375, N'固原', 371, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (376, N'中卫', 371, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (377, N'新疆维吾尔自治区', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (378, N'乌鲁木齐', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (379, N'克拉玛依', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (380, N'石河子', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (381, N'阿拉尔', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (382, N'图木舒克', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (383, N'五家渠', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (384, N'吐鲁番', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (385, N'阿克苏', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (386, N'喀什', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (387, N'哈密', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (388, N'和田', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (389, N'阿图什', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (390, N'库尔勒', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (391, N'昌吉', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (392, N'阜康', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (393, N'米泉', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (394, N'博乐', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (397, N'塔城', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (398, N'乌苏', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (399, N'阿勒泰', 377, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (400, N'内蒙古自治区', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (401, N'呼和浩特', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (402, N'包头', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (403, N'乌海', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (404, N'赤峰', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (405, N'通辽', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (406, N'鄂尔多斯', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (407, N'呼伦贝尔', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (408, N'巴彦卓尔', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (409, N'兴安盟', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (410, N'乌兰察布', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (411, N'锡林郭勒盟', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (412, N'阿拉善盟', 400, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (413, N'台湾省', NULL, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (414, N'台北', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (415, N'高雄', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (416, N'基隆', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (417, N'台中', 413, 0)
GO
print 'Processed 400 total records'
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (418, N'台南', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (419, N'新竹', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (420, N'嘉义', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (421, N'台北县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (422, N'宜兰县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (423, N'桃园县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (424, N'新竹县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (425, N'苗栗县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (426, N'台中县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (427, N'彰化县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (428, N'南投县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (429, N'云林县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (430, N'嘉义县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (431, N'台南县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (432, N'高雄县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (433, N'屏东县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (434, N'澎湖县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (435, N'台东县', 413, 0)
INSERT [dbo].[DB_Location] ([Id], [Name], [Parent_Id], [Language]) VALUES (436, N'花莲县', 413, 0)
/****** Object:  Table [dbo].[DB_Keyword_Category]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Keyword_Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_DB_Keyword_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Keyword_Category] ON
INSERT [dbo].[DB_Keyword_Category] ([Id], [Name]) VALUES (1, N'空间')
INSERT [dbo].[DB_Keyword_Category] ([Id], [Name]) VALUES (2, N'潮品')
INSERT [dbo].[DB_Keyword_Category] ([Id], [Name]) VALUES (3, N'风格')
INSERT [dbo].[DB_Keyword_Category] ([Id], [Name]) VALUES (10, N'')
SET IDENTITY_INSERT [dbo].[DB_Keyword_Category] OFF
/****** Object:  Table [dbo].[DB_Token]    Script Date: 02/21/2012 23:28:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DB_Token](
	[Email] [varchar](100) NOT NULL,
	[Code] [varchar](70) NOT NULL,
	[Time] [datetime] NOT NULL,
 CONSTRAINT [PK_DB_Token] PRIMARY KEY CLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[DB_Token] ([Email], [Code], [Time]) VALUES (N'raymanyoung@gmail.com', N'INb4QGQiif1zEdZoaxWq8T0FA3CnL2Ueblsd/vl92TUECLaQ==', CAST(0x00009FF400DEA350 AS DateTime))
/****** Object:  Table [dbo].[DB_UserCredential]    Script Date: 02/21/2012 23:28:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DB_UserCredential](
	[Email] [varchar](100) NOT NULL,
	[EncryptedPassword] [varchar](max) NOT NULL,
 CONSTRAINT [PK_DB_UserCredential] PRIMARY KEY CLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'799252345@qq.com', N'7FXJzN2sLdp7MYfw9zIc3YEyGmu1uPddNEcZ64edqyhImw/h8Qs=')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'admin@a.com', N'h4roX8r+dMoGxncv3Vo5nlzTKzx012D1zezXGywWViBD3UX0PiE=')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'rayman_young@hotmail.com', N'jQ7sihyhqTLkymvvxLowWbIreLbxQx3aOUAgVX0geuUnFzPf')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'raymanyoung@163.com', N'lXvKLInBTeyE6XqprrgQ8c+GWfABVBIGp/5pLi2wgoNwtB04B/pm')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'raymanyoung@gmail.com', N'FRFk2h7fxUr3PVRpJHVYkenr99P0/X0t6/qxmm2DSbi5ZDmwTuS9')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'test1@test.com', N'hI684Rqe6yuWw2hlkmv8zCEvsfCNPbkdx66+GsNPfwIhQqhvyQ==')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'test2@test.com', N'q4T6nluV0ydJKY0ZDj9Howgy6V1zsJ1pVwG673NdaOHINXUC3g==')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'test3@test.com', N'Wed+FTRG+s4rExdUrY1TCe33GqZUSoe5uaTs5AYxcZ9pFyQiVQ==')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'test4@test.com', N'wqdgKUcGB4wLw3DM2bSWNI2eyDYdvjH42eK8q1OKsJ+6gX5B3XJf')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'test5@test.com', N'EdYUttj8KjVeotIduU6gb7DAvL7SvPLALoMtKFE6wRxfRHv7KJDq')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'test6@test.com', N'p12AEwYvSOf69GwAia8m4giXrLWfQYwAF5bCy8zIXQo9ouBvFe0=')
INSERT [dbo].[DB_UserCredential] ([Email], [EncryptedPassword]) VALUES (N'test7@test.com', N'XdJLMgZQDRDuwNIho/CKnYirtg4+AjPRqPGuU+tpv6fCtc3IgQ==')
/****** Object:  Table [dbo].[DB_User]    Script Date: 02/21/2012 23:28:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DB_User](
	[Username] [nvarchar](20) NOT NULL,
	[Nick_Name] [nvarchar](20) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Gender] [tinyint] NOT NULL CONSTRAINT [DF_DB_User_Gender]  DEFAULT ((0)),
	[Birthday] [date] NOT NULL,
	[Location_Id] [bigint] NULL,
	[Self_Introduction] [nvarchar](200) NULL,
	[Active] [tinyint] NOT NULL CONSTRAINT [DF_DB_User_Active]  DEFAULT ((1)),
	[Email_Verified] [tinyint] NOT NULL CONSTRAINT [DF_DB_User_Email_Verified]  DEFAULT ((0)),
	[Receive_Email] [tinyint] NOT NULL CONSTRAINT [DF_DB_User_Receive_Email]  DEFAULT ((0)),
	[Is_Locked] [tinyint] NOT NULL CONSTRAINT [DF_DB_User_Locked]  DEFAULT ((0)),
	[Locked_Reason] [nvarchar](100) NULL,
	[Is_Test_User] [tinyint] NOT NULL CONSTRAINT [DF_DB_User_Is_Test_User]  DEFAULT ((0)),
	[Is_Admin] [tinyint] NOT NULL CONSTRAINT [DF_DB_User_Is_Admin]  DEFAULT ((0)),
	[Association] [nvarchar](10) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__DB_User__001BAD1002925FBF] UNIQUE NONCLUSTERED 
(
	[Nick_Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'2624014317', N'2624014317', N'', 1, CAST(0x00000000 AS Date), 5, N'', 0, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'admin', N'我是admin', N'admin@a.com', 0, CAST(0x00000000 AS Date), NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'l79889177889803', N'raymanyoung1', N'raymanyoung@163.com', 0, CAST(0x00000000 AS Date), NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'raymanyoung', N'我是raymanyoung', N'rayman_young@hotmail.com', 0, CAST(0x00000000 AS Date), NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'test1', N'我是test1', N'test1@test.com', 1, CAST(0x00000000 AS Date), 3, N'这句话是自我介绍33567', 0, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'test2', N'我是test2', N'test2@test.com', 0, CAST(0x00000000 AS Date), 37, N'自我介绍自我介绍4', 0, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'test3', N'我是test3', N'test3@test.com', 0, CAST(0x00000000 AS Date), NULL, NULL, 0, 0, 0, 0, NULL, 0, 1, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'test4', N'我是test4', N'test4@test.com', 0, CAST(0x00000000 AS Date), NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'test5', N'我是test5', N'test5@test.com', 0, CAST(0x00000000 AS Date), NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'test6', N'我是test6', N'test6@test.com', 0, CAST(0x00000000 AS Date), NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'test7', N'我是test7', N'test7@test.com', 0, CAST(0x00000000 AS Date), NULL, NULL, 0, 0, 0, 1, N'长得太丑', 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'UnitTestDefaultUser', N'UnitTestDefaultUser', N'unittestdefault@hello.com', 1, CAST(0x07240B00 AS Date), 5, N'hello', 1, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'猫猫', N'猫猫', N'799252345@qq.com', 0, CAST(0x00000000 AS Date), NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, NULL)
INSERT [dbo].[DB_User] ([Username], [Nick_Name], [Email], [Gender], [Birthday], [Location_Id], [Self_Introduction], [Active], [Email_Verified], [Receive_Email], [Is_Locked], [Locked_Reason], [Is_Test_User], [Is_Admin], [Association]) VALUES (N'杨垒', N'杨垒', N'raymanyoung@gmail.com', 0, CAST(0x00000000 AS Date), NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, NULL)
/****** Object:  Table [dbo].[DB_Keyword]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Keyword](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](10) NOT NULL,
	[Category_Id] [int] NOT NULL,
 CONSTRAINT [PK_DB_Keyword_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Keyword] ON
INSERT [dbo].[DB_Keyword] ([Id], [Name], [Category_Id]) VALUES (1, N'厨房', 1)
INSERT [dbo].[DB_Keyword] ([Id], [Name], [Category_Id]) VALUES (3, N'客厅', 1)
INSERT [dbo].[DB_Keyword] ([Id], [Name], [Category_Id]) VALUES (4, N'沙发', 10)
INSERT [dbo].[DB_Keyword] ([Id], [Name], [Category_Id]) VALUES (5, N'吊坠', 2)
INSERT [dbo].[DB_Keyword] ([Id], [Name], [Category_Id]) VALUES (6, N'床', 10)
INSERT [dbo].[DB_Keyword] ([Id], [Name], [Category_Id]) VALUES (7, N'可爱', 10)
INSERT [dbo].[DB_Keyword] ([Id], [Name], [Category_Id]) VALUES (10, N'卧室', 1)
SET IDENTITY_INSERT [dbo].[DB_Keyword] OFF
/****** Object:  View [dbo].[KeywordView]    Script Date: 02/21/2012 23:28:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[KeywordView]
AS
SELECT        dbo.DB_Keyword.Name AS Keyword_Name, dbo.DB_Keyword_Category.Name AS Category_Name, dbo.DB_Keyword.Id AS KeywordId
FROM            dbo.DB_Keyword LEFT OUTER JOIN
                         dbo.DB_Keyword_Category ON dbo.DB_Keyword.Category_Id = dbo.DB_Keyword_Category.Id
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[26] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DB_Keyword_Category"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 101
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DB_Keyword"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 101
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2445
         Alias = 2220
         Table = 2385
         Output = 2520
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'KeywordView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'KeywordView'
GO
/****** Object:  Table [dbo].[DB_Message]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Message](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Send_Username] [nvarchar](20) NOT NULL,
	[To_Username] [nvarchar](20) NOT NULL,
	[Text] [nvarchar](2000) NOT NULL,
	[Has_Read] [tinyint] NOT NULL CONSTRAINT [DF_DB_Message_Has_Read]  DEFAULT ((0)),
	[Sent_Time] [datetime] NOT NULL,
 CONSTRAINT [PK_DB_Message] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Message] ON
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (2, N'test1', N'test3', N'test2', 1, CAST(0x00009FCE010E2A74 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (3, N'test1', N'test3', N'test3', 1, CAST(0x00009FCE010F4E95 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (6, N'test3', N'test1', N'xxxx', 1, CAST(0x00009FCE010FE960 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (7, N'test1', N'test4', N'test test4', 1, CAST(0x00009FCE0172BC93 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (8, N'test1', N'test3', N'new message', 1, CAST(0x00009FCE0184BE16 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (9, N'test1', N'test3', N'new message 2', 1, CAST(0x00009FCE01866EF3 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (10, N'test3', N'test1', N'reply new message', 1, CAST(0x00009FCE0186A30A AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (11, N'test2', N'test1', N'new thread', 1, CAST(0x00009FCE0186BD96 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (12, N'test1', N'test3', N'有机菜', 1, CAST(0x00009FCF00B5A43B AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (13, N'test1', N'test3', N'测试测试', 1, CAST(0x00009FCF00B5AD59 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (14, N'test1', N'test3', N'another one', 1, CAST(0x00009FCF00B9F9D7 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (15, N'test1', N'test3', N'<script>', 1, CAST(0x00009FCF00B9FFD6 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (16, N'test1', N'test3', N'<script> alert(''a'')</script>', 1, CAST(0x00009FCF00BA0FB0 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (17, N'test3', N'test1', N'is it a new one?', 1, CAST(0x00009FCF00DE9796 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (19, N'test3', N'test1', N'new one again', 1, CAST(0x00009FCF00DF6787 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (20, N'test3', N'test1', N'test again and again', 1, CAST(0x00009FCF00DFAA27 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (21, N'test4', N'test1', N'the first 私信', 1, CAST(0x00009FCF00DFDB25 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (22, N'test1', N'test4', N'first reply', 1, CAST(0x00009FCF00DFFFCA AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (23, N'test1', N'test4', N'2nd reply', 1, CAST(0x00009FCF00E00AB4 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (24, N'test4', N'test1', N'new test', 1, CAST(0x00009FCF00E12447 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (25, N'test4', N'test1', N'new test2', 1, CAST(0x00009FCF00E13908 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (26, N'test4', N'test1', N'new test3', 1, CAST(0x00009FCF00E1C2F9 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (27, N'test1', N'test4', N'new reply', 1, CAST(0x00009FCF00E205A6 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (28, N'test2', N'test1', N'msg #2', 1, CAST(0x00009FE8011A0D64 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (29, N'test2', N'test1', N'msg #3', 1, CAST(0x00009FE8011A1126 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (30, N'test2', N'test1', N'msg #4', 1, CAST(0x00009FE8011A14F5 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (31, N'test2', N'test1', N'msg #5', 1, CAST(0x00009FE8011A19F1 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (32, N'test2', N'test4', N'test4 #1', 1, CAST(0x00009FE80122452C AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (33, N'test2', N'test3', N'test3 #1', 0, CAST(0x00009FE80122631C AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (34, N'test2', N'test6', N'test6 #1', 0, CAST(0x00009FE80122BD0C AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (35, N'test2', N'test7', N'test7 #1', 0, CAST(0x00009FE80122CE0C AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (36, N'test2', N'test5', N'test5 #1', 1, CAST(0x00009FE80124A43B AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (37, N'test1', N'test3', N'new #1', 0, CAST(0x00009FEB00A9F5A4 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (38, N'test1', N'test3', N'new #2', 0, CAST(0x00009FEB00A9F951 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (39, N'test1', N'test3', N'new #3', 0, CAST(0x00009FEB00A9FD2E AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (40, N'test1', N'test3', N'new #4', 0, CAST(0x00009FEB00AA01A1 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (41, N'test1', N'test3', N'new #5', 0, CAST(0x00009FEB00AA0730 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (42, N'test1', N'test3', N'new #6', 0, CAST(0x00009FEB00AA0D35 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (43, N'test1', N'test3', N'new #7', 0, CAST(0x00009FEB00AA10AD AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (44, N'test1', N'test3', N'new #8', 0, CAST(0x00009FEB00AA13F2 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (45, N'test1', N'test3', N'new #9', 0, CAST(0x00009FEB00AA16DF AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (46, N'test1', N'test3', N'new #10', 0, CAST(0x00009FEB00AA1989 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (47, N'test4', N'test2', N'testxxx', 1, CAST(0x00009FEC0121FE82 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (48, N'test4', N'test2', N'testxxx #2', 1, CAST(0x00009FEC0122092A AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (49, N'test4', N'test2', N'testxxx #3', 1, CAST(0x00009FEC012239DC AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (50, N'test4', N'test2', N'testxxx #4', 1, CAST(0x00009FEC01298817 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (51, N'test4', N'test2', N'testxxx #5', 1, CAST(0x00009FEC0129F5AD AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (52, N'test1', N'test3', N'after that', 0, CAST(0x00009FFE017B4D11 AS DateTime))
INSERT [dbo].[DB_Message] ([Id], [Send_Username], [To_Username], [Text], [Has_Read], [Sent_Time]) VALUES (53, N'test1', N'test3', N'after that', 0, CAST(0x00009FFE017BADC9 AS DateTime))
SET IDENTITY_INSERT [dbo].[DB_Message] OFF
/****** Object:  Table [dbo].[DB_Announcement]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Announcement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](1000) NOT NULL,
	[Post_Time] [datetime] NOT NULL,
	[Admin_Username] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_DB_Announcement] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Announcement] ON
INSERT [dbo].[DB_Announcement] ([Id], [Text], [Post_Time], [Admin_Username]) VALUES (2, N'大新闻！', CAST(0x0000A00700000000 AS DateTime), N'admin')
SET IDENTITY_INSERT [dbo].[DB_Announcement] OFF
/****** Object:  Table [dbo].[DB_Album]    Script Date: 02/21/2012 23:28:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Album](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Number_Of_Like] [int] NOT NULL CONSTRAINT [DF_DB_Album_Number_Of_Like]  DEFAULT ((0)),
	[Creation_Time] [datetime] NOT NULL,
	[Tags] [nvarchar](max) NULL,
 CONSTRAINT [PK_DB_Album] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Album] ON
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (1, N'UnitTestDefaultUser', N'UnitTest Default Album', N'This is for unittest. Do not delete', 0, CAST(0x00009FAC00000000 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (2, N'test1', N'test23', NULL, 0, CAST(0x00009FC500C6E23E AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (3, N'test1', N'收拾收拾123', NULL, 0, CAST(0x00009FC500E1AD30 AS DateTime), N'电影 ')
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (4, N'test1', N'方法方法', NULL, 0, CAST(0x00009FC500EA4704 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (5, N'test2', N'testaa', NULL, 0, CAST(0x00009FC6010B37BC AS DateTime), N'插画 旅行 电影 设计 愛愛愛 爱爱爱 bbb')
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (6, N'test2', N'海洋生物', NULL, 0, CAST(0x00009FC601125646 AS DateTime), N'旅行 电影 设计')
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (7, N'test2', N'这是啥', NULL, 0, CAST(0x00009FC601295ED7 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (8, N'test3', N'新专辑', NULL, 0, CAST(0x00009FC9010EEF45 AS DateTime), N'电影 旅行 ')
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (9, N'test3', N'专辑2', NULL, 0, CAST(0x00009FC9010FCCE2 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (10, N'test3', N'转机3', NULL, 0, CAST(0x00009FC9011032C0 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (11, N'test4', N'新的专辑', NULL, 0, CAST(0x00009FC901119CFB AS DateTime), N'我的标签 设计 电影 ')
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (12, N'test1', N'new', NULL, 0, CAST(0x00009FD600C2F8D0 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (13, N'test1', N'aaa', NULL, 0, CAST(0x00009FD700A87E68 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (14, N'test1', N'bbb', NULL, 0, CAST(0x00009FD700A8A830 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (15, N'test1', N'zzz', NULL, 0, CAST(0x00009FD700A93B2C AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (16, N'test1', N'xxx', NULL, 0, CAST(0x00009FD700A94C4D AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (17, N'test1', N'家居', NULL, 0, CAST(0x00009FD700A9DFB2 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (18, N'test1', N'沙发', NULL, 0, CAST(0x00009FD700B1A249 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (19, N'test2', N'test#1', NULL, 0, CAST(0x00009FE801261F67 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (20, N'test2', N'test#2', NULL, 0, CAST(0x00009FE80126224F AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (21, N'test2', N'test#3', NULL, 0, CAST(0x00009FE8012626FD AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (22, N'test1', N'kkk', NULL, 0, CAST(0x00009FF701039CFE AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (23, N'test1', N'test111', NULL, 0, CAST(0x00009FF70106C879 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (24, N'test1', N'test222', NULL, 0, CAST(0x00009FF70106F4C8 AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (25, N'test1', N'test333', NULL, 0, CAST(0x00009FF70107328F AS DateTime), NULL)
INSERT [dbo].[DB_Album] ([Id], [Username], [Name], [Description], [Number_Of_Like], [Creation_Time], [Tags]) VALUES (26, N'test1', N'test444', N'', 0, CAST(0x00009FFB01763781 AS DateTime), N'')
SET IDENTITY_INSERT [dbo].[DB_Album] OFF
/****** Object:  Table [dbo].[DB_Discussion]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Discussion](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Board_Id] [int] NOT NULL,
	[Subject] [nvarchar](100) NOT NULL,
	[Text] [nvarchar](1000) NOT NULL,
	[Post_Time] [datetime] NOT NULL,
	[Username] [nvarchar](20) NOT NULL,
	[Latest_Reply_Time] [datetime] NOT NULL,
 CONSTRAINT [PK_DB_Discussion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Discussion] ON
INSERT [dbo].[DB_Discussion] ([Id], [Board_Id], [Subject], [Text], [Post_Time], [Username], [Latest_Reply_Time]) VALUES (2, 1, N'软装和硬装的区别？', N'如题', CAST(0x00009FF500000000 AS DateTime), N'test2', CAST(0x00009FF600AC634B AS DateTime))
INSERT [dbo].[DB_Discussion] ([Id], [Board_Id], [Subject], [Text], [Post_Time], [Username], [Latest_Reply_Time]) VALUES (4, 1, N'硬装软件是什么意思?', N'如题', CAST(0x00009FF501499700 AS DateTime), N'test4', CAST(0x00009FF600AC784B AS DateTime))
INSERT [dbo].[DB_Discussion] ([Id], [Board_Id], [Subject], [Text], [Post_Time], [Username], [Latest_Reply_Time]) VALUES (8, 1, N'发新帖', N'试一下', CAST(0x00009FF600D5F9AE AS DateTime), N'test1', CAST(0x00009FFE017BF380 AS DateTime))
SET IDENTITY_INSERT [dbo].[DB_Discussion] OFF
/****** Object:  Table [dbo].[DB_Conversation]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Conversation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](20) NOT NULL,
	[Target_Username] [nvarchar](20) NOT NULL,
	[Action] [tinyint] NOT NULL,
	[Latest_Update_Time] [datetime] NOT NULL,
	[Text] [nvarchar](3000) NOT NULL,
	[Has_Read] [tinyint] NOT NULL CONSTRAINT [DF_DB_Conversation_Has_Read]  DEFAULT ((0)),
	[Count] [int] NOT NULL CONSTRAINT [DF_DB_Conversation_Count]  DEFAULT ((0)),
 CONSTRAINT [PK_DB_Conversation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is a redundant table to keep the conversation threads. All information is available in DB_Message table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DB_Conversation'
GO
SET IDENTITY_INSERT [dbo].[DB_Conversation] ON
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (1, N'test1', N'test3', 0, CAST(0x00009FFE017BADE4 AS DateTime), N'after that', 0, 23)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (2, N'test3', N'test1', 1, CAST(0x00009FFE017BADE4 AS DateTime), N'after that', 1, 23)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (3, N'test2', N'test1', 0, CAST(0x00009FE8011A19F4 AS DateTime), N'msg #5', 1, 5)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (4, N'test1', N'test2', 1, CAST(0x00009FE8011A19F4 AS DateTime), N'msg #5', 1, 5)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (5, N'test4', N'test1', 1, CAST(0x00009FCF00E205B1 AS DateTime), N'new reply', 1, 7)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (6, N'test1', N'test4', 0, CAST(0x00009FCF00E205B1 AS DateTime), N'new reply', 1, 7)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (7, N'test2', N'test4', 1, CAST(0x00009FEC0129F5B3 AS DateTime), N'testxxx #5', 1, 6)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (8, N'test4', N'test2', 0, CAST(0x00009FEC0129F5B3 AS DateTime), N'testxxx #5', 1, 6)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (9, N'test2', N'test3', 0, CAST(0x00009FE80122631E AS DateTime), N'test3 #1', 0, 1)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (10, N'test3', N'test2', 1, CAST(0x00009FE80122631F AS DateTime), N'test3 #1', 1, 1)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (11, N'test2', N'test6', 0, CAST(0x00009FE80122BD10 AS DateTime), N'test6 #1', 0, 1)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (12, N'test6', N'test2', 1, CAST(0x00009FE80122BD11 AS DateTime), N'test6 #1', 1, 1)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (13, N'test2', N'test7', 0, CAST(0x00009FE80122CE0D AS DateTime), N'test7 #1', 0, 1)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (14, N'test7', N'test2', 1, CAST(0x00009FE80122CE0F AS DateTime), N'test7 #1', 1, 1)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (15, N'test2', N'test5', 0, CAST(0x00009FE80124A43E AS DateTime), N'test5 #1', 1, 1)
INSERT [dbo].[DB_Conversation] ([Id], [Username], [Target_Username], [Action], [Latest_Update_Time], [Text], [Has_Read], [Count]) VALUES (16, N'test5', N'test2', 1, CAST(0x00009FE80124A440 AS DateTime), N'test5 #1', 1, 1)
SET IDENTITY_INSERT [dbo].[DB_Conversation] OFF
/****** Object:  Table [dbo].[DB_Fan]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Fan](
	[FansUsername] [nvarchar](20) NOT NULL,
	[WatchedUsername] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_DB_Fan] PRIMARY KEY CLUSTERED 
(
	[FansUsername] ASC,
	[WatchedUsername] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'test1', N'test1')
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'test1', N'test2')
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'test1', N'test3')
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'test1', N'test4')
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'test2', N'test2')
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'test2', N'test3')
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'test2', N'test6')
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'test3', N'test1')
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'test3', N'test2')
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'test4', N'test3')
INSERT [dbo].[DB_Fan] ([FansUsername], [WatchedUsername]) VALUES (N'杨垒', N'test1')
/****** Object:  Table [dbo].[DB_Tag]    Script Date: 02/21/2012 23:28:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Tag](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Username] [nvarchar](20) NULL,
	[Updated_Time] [datetime] NULL,
	[Type] [tinyint] NULL,
 CONSTRAINT [PK_DB_Tag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Tag] ON
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (1, N'电影', NULL, NULL, NULL)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (2, N'旅行', NULL, NULL, NULL)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (3, N'插画', NULL, NULL, NULL)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (4, N'设计', NULL, NULL, NULL)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (6, N'kkkkk', N'test2', CAST(0x00009FC700D6A8C3 AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (7, N'bdd', N'test2', CAST(0x00009FC6011116A4 AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (8, N'xxxx', N'test2', CAST(0x00009FC601114C82 AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (9, N'bbbb', N'test2', CAST(0x00009FC601114C88 AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (10, N'海洋', N'test2', CAST(0x00009FC60112871D AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (11, N'地貌', N'test2', CAST(0x00009FC60112B5FC AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (12, N'花', N'test2', CAST(0x00009FC601131F06 AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (15, N'爱爱爱', N'test1', CAST(0x00009FC800E78C6B AS DateTime), 1)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (16, N'愛愛愛', N'test1', CAST(0x00009FC800E88CF9 AS DateTime), 1)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (18, N'爱爱爱', N'test1', CAST(0x00009FC900F57CBE AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (19, N'我的标签', N'test4', CAST(0x00009FC90112732A AS DateTime), 1)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (20, N'我的标签', N'test4', CAST(0x00009FC90113D707 AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (21, N'床', N'test1', CAST(0x00009FD500D54817 AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (22, N'愛愛愛', N'test1', CAST(0x00009FD700A95A36 AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (23, N'bbb', N'test2', CAST(0x00009FEC00FCA55C AS DateTime), 1)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (24, N'沙发', N'test1', CAST(0x00009FF1011C8576 AS DateTime), 0)
INSERT [dbo].[DB_Tag] ([Id], [Name], [Username], [Updated_Time], [Type]) VALUES (25, N'客厅', N'test1', CAST(0x00009FF1011EDAF4 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[DB_Tag] OFF
/****** Object:  Table [dbo].[DB_Picture]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DB_Picture](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Source_Page_Url] [varchar](255) NULL,
	[Source_Page_Title] [nvarchar](50) NULL,
	[Image_Filename] [varchar](100) NOT NULL,
	[Image_File_Ext] [varchar](5) NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[Upload_Time] [datetime] NOT NULL,
	[Album_Id] [bigint] NULL,
	[Tags] [nvarchar](1000) NULL,
	[Username] [nvarchar](20) NOT NULL,
	[Is_Commodity] [tinyint] NOT NULL CONSTRAINT [DF_DB_Picture_Is_Commodity]  DEFAULT ((0)),
	[Price] [float] NOT NULL CONSTRAINT [DF_DB_Picture_Price]  DEFAULT ((0)),
	[Shop] [tinyint] NOT NULL CONSTRAINT [DF_DB_Picture_Shop]  DEFAULT ((0)),
	[Ratio] [float] NOT NULL,
	[Seed] [int] NULL,
 CONSTRAINT [PK_DB_Picture] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the album id is null, it means the picture is under "uncategoried" album' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DB_Picture', @level2type=N'COLUMN',@level2name=N'Album_Id'
GO
SET IDENTITY_INSERT [dbo].[DB_Picture] ON
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (5, NULL, NULL, N'20120109144136-uiygax', N'jpg', N'', CAST(0x00009FD300F22C59 AS DateTime), 2, N'插画 ', N'test1', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (6, NULL, NULL, N'20120109175540-etbvnt', N'jpg', N'', CAST(0x00009FD30127805C AS DateTime), 8, N'插画 旅行 床', N'test3', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (7, NULL, NULL, N'20120110103535-mteknj', N'jpg', N'地貌啊！', CAST(0x00009FD400AE9CF4 AS DateTime), 2, N'旅行 电影 ', N'test1', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (8, NULL, NULL, N'20120112122706-hqpsbl.jpg', N'jpg', N'', CAST(0x00009FD600CD395B AS DateTime), NULL, N'', N'test1', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (9, NULL, NULL, N'20120112123335-fveyfh.jpg', N'jpg', N'', CAST(0x00009FD600CEFF67 AS DateTime), 18, N'', N'test1', 0, 0, 0, 0.675, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (10, NULL, NULL, N'20120112124923-ygdxvk.jpg', N'jpg', N'visual studio', CAST(0x00009FD600D368D8 AS DateTime), 12, N'电影 ', N'test1', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (11, NULL, NULL, N'20120113101603-nsziss.jpg', N'jpg', N'test test', CAST(0x00009FD700A955BE AS DateTime), 16, N'旅行 愛愛愛 ', N'test1', 0, 0, 0, 1.333, 1000)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (12, NULL, NULL, N'20120113101820-fspvtc.jpg', N'jpg', N'沙发11', CAST(0x00009FD700A9E95B AS DateTime), 17, N'旅行 爱爱爱 电影 设计 床', N'test1', 0, 0, 0, 1.333, 1200)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (13, NULL, NULL, N'20120113104250-azxazk.jpg', N'jpg', N'沙发', CAST(0x00009FD700B0B0B8 AS DateTime), 14, N'电影 旅行 ', N'test1', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (14, NULL, NULL, N'20120113104636-rxmwwg.jpg', N'jpg', N'凤飞飞', CAST(0x00009FD700B1A9D0 AS DateTime), 18, N'旅行 设计 ', N'test1', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (15, NULL, NULL, N'20120113105011-ouzokj.jpg', N'jpg', N'', CAST(0x00009FD700B2A338 AS DateTime), 18, N'', N'test1', 0, 0, 0, 0.894, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (16, NULL, NULL, N'20120113164311-uqktdl.jpg', N'jpg', N'促销', CAST(0x00009FD70113995B AS DateTime), 13, N'', N'test1', 0, 0, 0, 2.112, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (17, NULL, NULL, N'20120113164541-shobdr.jpg', N'jpg', N'促销', CAST(0x00009FD70114477B AS DateTime), NULL, N'插画 ', N'test1', 0, 0, 0, 1, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (19, NULL, NULL, N'20120130113026-ovsfef', N'jpg', N'', CAST(0x00009FE800BDAB1C AS DateTime), 6, N'旅行 ', N'test2', 0, 0, 0, 1, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (21, NULL, NULL, N'20120130153429-kbslst.jpg', N'jpg', N'', CAST(0x00009FE80100C7D8 AS DateTime), NULL, N'', N'test2', 0, 0, 0, 1, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (22, NULL, NULL, N'20120130161616-nusuke.jpg', N'jpg', N'', CAST(0x00009FE8010D406B AS DateTime), NULL, N'', N'test2', 1, 55, 1, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (23, N'http://item.taobao.com/item.htm?spm=566.119373.208833.1&id=14067892419', N'【围巾新品】 2012春秋新款包邮中国风水墨印染格子流苏围巾多色-淘宝网', N'20120130161616-nusuke.jpg', N'jpg', N'', CAST(0x00009FE8010D9C43 AS DateTime), NULL, N'', N'test2', 1, 55, 1, 1, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (24, NULL, NULL, N'20120130170118-nkrwqy', N'jpg', N'', CAST(0x00009FE801188A8A AS DateTime), 6, N'旅行 电影', N'test2', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (25, NULL, NULL, N'20120201115604-ivxaen', N'jpg', N'', CAST(0x00009FEA00C4BEB2 AS DateTime), 19, N'插画 ', N'test2', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (26, NULL, NULL, N'20120201120823-wtufoa', N'gif', N'', CAST(0x00009FEA00C8210B AS DateTime), 7, N'', N'test2', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (27, NULL, NULL, N'20120201120958-hrblwk', N'gif', N'aaabbbb', CAST(0x00009FEA00C88035 AS DateTime), NULL, N'', N'test2', 0, 0, 0, 1.333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (28, NULL, NULL, N'20120207135808-fxmlxc', N'jpg', N'', CAST(0x00009FF000E63C7F AS DateTime), NULL, N'', N'test1', 0, 0, 0, 1.3333333333333333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (29, N'http://games.sina.com.cn/o/z/wow/2011-12-23/1022435136.shtml', N'WOW官方网站合作专区_魔兽世界_4.3_暮光审判_大地的裂变_大灾变', N'20120207141215-rskaru', N'jpg', N'aaa', CAST(0x00009FF000EA1EE0 AS DateTime), NULL, N'', N'test1', 0, 0, 0, 0.8, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (30, N'http://office.tong1.cn/tuku/jxtk/200806/20080624005825_2870.html', N'简约而不简单-创意家居装饰网-家居装饰行业门户网站', N'20120208171539-bsszur', N'jpg', N'', CAST(0x00009FF1011C855C AS DateTime), 17, N'沙发', N'test1', 0, 0, 0, 1.3313609467455621, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (31, N'http://office.tong1.cn/tuku/jxtk/200709/20070925111515_645.html', N'3张精彩家居客厅实例图片欣赏-创意家居装饰网-家居装饰行业门户网站', N'20120208171615-rplitj', N'jpg', N'', CAST(0x00009FF1011CA97C AS DateTime), 17, N'沙发', N'test1', 0, 0, 0, 1.7605633802816902, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (32, N'http://office.tong1.cn/tuku/jxtk/200709/20070925111515_645.html', N'3张精彩家居客厅实例图片欣赏-创意家居装饰网-家居装饰行业门户网站', N'20120208171636-hjmbtt', N'jpg', N'', CAST(0x00009FF1011CC8E1 AS DateTime), 17, N'沙发', N'test1', 0, 0, 0, 1.3333333333333333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (33, N'http://office.tong1.cn/tuku/jxtk/200709/20070925111515_645.html', N'3张精彩家居客厅实例图片欣赏-创意家居装饰网-家居装饰行业门户网站', N'20120208171939-tvqrgm', N'jpg', N'', CAST(0x00009FF1011D955F AS DateTime), 17, N'沙发', N'test1', 0, 0, 0, 0.70953436807095349, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (34, N'http://office.tong1.cn/tuku/jxtk/200709/20070925111515_645.html', N'3张精彩家居客厅实例图片欣赏-创意家居装饰网-家居装饰行业门户网站', N'20120208172029-jhmyec', N'jpg', N'', CAST(0x00009FF1011DD55F AS DateTime), 17, N'沙发', N'test1', 0, 0, 0, 1.3333333333333333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (35, N'http://office.tong1.cn/tuku/jxtk/200709/20070925111510_641.html', N'客厅隔断参考效果图收集-创意家居装饰网-家居装饰行业门户网站', N'20120208172142-qkdaqe', N'jpg', N'', CAST(0x00009FF1011E2CC1 AS DateTime), 17, N'客厅 沙发 床', N'test1', 0, 0, 0, 1.6748768472906404, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (36, N'http://office.tong1.cn/tuku/jxtk/200709/20070925111510_641.html', N'客厅隔断参考效果图收集-创意家居装饰网-家居装饰行业门户网站', N'20120208172142-qkdaqe', N'jpg', N'aaa', CAST(0x00009FF1011ED63C AS DateTime), 18, N'客厅 沙发 电影', N'test1', 0, 0, 0, 1.6748768472906404, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (37, NULL, N'', N'20120218231250-hhhoeb', N'jpg', N'', CAST(0x00009FFB017E95D9 AS DateTime), NULL, N'插画 ', N'test2', 0, 0, 0, 1.3333333333333333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (38, N'http://www.so668.com/news_show.asp?id=552&n_type=55', N'', N'20120218232412-kchens', N'jpg', N'西藏', CAST(0x00009FFB0181ADAE AS DateTime), NULL, N'旅行 ', N'test2', 0, 0, 0, 1.3333333333333333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (39, NULL, N'', N'20120218232435-svgxgh', N'jpg', N'西藏', CAST(0x00009FFB0181CA94 AS DateTime), NULL, N'旅行 ', N'test2', 0, 0, 0, 1.3333333333333333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (40, NULL, N'', N'20120218232455-ftzlnp', N'jpg', N'', CAST(0x00009FFB018262DA AS DateTime), NULL, N'插画 ', N'test2', 0, 0, 0, 1.3333333333333333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (41, NULL, N'', N'20120218232656-fdjkhe', N'jpg', N'啊啊啊啊', CAST(0x00009FFB018272F9 AS DateTime), 6, N'插画 ', N'test2', 0, 0, 0, 1.3333333333333333, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (42, N'http://www.duitang.com/myhome/', N'我的首页 - 堆糖', N'20120219101609-gsnlty', N'jpg', N'', CAST(0x00009FFC00A94653 AS DateTime), NULL, N'插画 电影 ', N'test2', 0, 0, 0, 0.10863661053775123, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (43, N'http://www.duitang.com/people/mblog/18972031/detail/', N'变色雨伞(Colour changing umbrella) 来自duitang在堆糖网的分享', N'20120219101743-hyngei', N'jpg', N'', CAST(0x00009FFC00A9AFBA AS DateTime), 7, N'电影 ', N'test2', 0, 0, 0, 0.775, NULL)
INSERT [dbo].[DB_Picture] ([Id], [Source_Page_Url], [Source_Page_Title], [Image_Filename], [Image_File_Ext], [Description], [Upload_Time], [Album_Id], [Tags], [Username], [Is_Commodity], [Price], [Shop], [Ratio], [Seed]) VALUES (44, N'http://www.duitang.com/people/mblog/15882954/detail/', N'画中画、人中人。来自乌克兰艺术家Oleg Shupl… 来自duitang在堆糖网的分享', N'20120219102304-jmkmhc', N'jpg', N'', CAST(0x00009FFC00AB2D82 AS DateTime), 7, N'插画 电影', N'test2', 0, 0, 0, 0.13858267716535433, NULL)
SET IDENTITY_INSERT [dbo].[DB_Picture] OFF
/****** Object:  Table [dbo].[DB_Like]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Like](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Album_Id] [bigint] NOT NULL,
	[Username] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_DB_Like_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Like] ON
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (1, 2, N'test1')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (2, 2, N'test2')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (3, 2, N'test3')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (4, 3, N'test2')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (5, 4, N'test2')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (6, 5, N'test1')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (7, 5, N'test2')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (8, 8, N'test1')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (9, 11, N'test4')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (10, 12, N'test2')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (11, 7, N'test1')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (13, 5, N'test3')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (14, 6, N'test3')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (15, 7, N'test3')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (16, 19, N'test3')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (17, 5, N'test4')
INSERT [dbo].[DB_Like] ([Id], [Album_Id], [Username]) VALUES (18, 17, N'test2')
SET IDENTITY_INSERT [dbo].[DB_Like] OFF
/****** Object:  Table [dbo].[DB_Collection]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Collection](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Album_Id] [bigint] NULL,
	[Picture_Id] [bigint] NOT NULL,
	[Collect_Time] [datetime] NOT NULL,
	[Collect_Type] [tinyint] NOT NULL CONSTRAINT [DF_DB_Collection_Collect_Type]  DEFAULT ((0)),
	[Username] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_DB_Collection_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the album id is null, it means the collection is under "uncategoried" album' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DB_Collection', @level2type=N'COLUMN',@level2name=N'Album_Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Picture is owned by this user; 1 - Picture is collected by this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DB_Collection', @level2type=N'COLUMN',@level2name=N'Collect_Type'
GO
SET IDENTITY_INSERT [dbo].[DB_Collection] ON
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (6, 2, 5, CAST(0x00009FD300F22C63 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (7, NULL, 5, CAST(0x00009FD30102DC7B AS DateTime), 1, N'test3')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (8, 8, 6, CAST(0x00009FD301278062 AS DateTime), 0, N'test3')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (9, 2, 7, CAST(0x00009FD400AE9D00 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (10, NULL, 6, CAST(0x00009FD40118B331 AS DateTime), 1, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (11, NULL, 8, CAST(0x00009FD600CD396F AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (12, NULL, 9, CAST(0x00009FD600CEFF74 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (13, 12, 10, CAST(0x00009FD600D368DF AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (14, 16, 11, CAST(0x00009FD700A955CA AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (15, 17, 12, CAST(0x00009FD700A9E962 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (16, 14, 13, CAST(0x00009FD700B0B0C1 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (17, 18, 14, CAST(0x00009FD700B1A9D4 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (18, NULL, 15, CAST(0x00009FD700B2A342 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (19, 9, 15, CAST(0x00009FD7010E556F AS DateTime), 1, N'test3')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (20, 13, 16, CAST(0x00009FD70113995F AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (21, NULL, 17, CAST(0x00009FD70114477D AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (25, 6, 19, CAST(0x00009FE800BDAB23 AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (28, NULL, 21, CAST(0x00009FE80100C7EA AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (29, NULL, 22, CAST(0x00009FE8010D4078 AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (30, NULL, 23, CAST(0x00009FE8010D9C4E AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (31, 6, 24, CAST(0x00009FE801188A8F AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (32, 20, 23, CAST(0x00009FE900A35677 AS DateTime), 1, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (33, 21, 7, CAST(0x00009FE900AF3F50 AS DateTime), 1, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (34, 21, 10, CAST(0x00009FE900B00E2B AS DateTime), 1, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (35, 21, 14, CAST(0x00009FE900B045DD AS DateTime), 1, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (36, 21, 13, CAST(0x00009FE900B05E81 AS DateTime), 1, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (37, 5, 24, CAST(0x00009FE900BEE758 AS DateTime), 1, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (38, 19, 25, CAST(0x00009FEA00C4BEBB AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (39, 7, 26, CAST(0x00009FEA00C82113 AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (40, NULL, 27, CAST(0x00009FEA00C8803E AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (41, 13, 27, CAST(0x00009FEA011F913D AS DateTime), 1, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (42, 8, 24, CAST(0x00009FEC011F6F3F AS DateTime), 1, N'test3')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (43, NULL, 28, CAST(0x00009FF000E63C88 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (44, NULL, 29, CAST(0x00009FF000EA1EEC AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (45, 17, 30, CAST(0x00009FF1011C8566 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (46, 17, 31, CAST(0x00009FF1011CA980 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (47, 17, 32, CAST(0x00009FF1011CC8E9 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (48, 17, 33, CAST(0x00009FF1011D9567 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (49, 17, 34, CAST(0x00009FF1011DD563 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (50, 17, 35, CAST(0x00009FF1011E2CC5 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (51, 17, 36, CAST(0x00009FF1011ED640 AS DateTime), 0, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (52, 26, 12, CAST(0x00009FF901278C97 AS DateTime), 1, N'test1')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (53, NULL, 37, CAST(0x00009FFB017E95EB AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (54, NULL, 38, CAST(0x00009FFB0181ADB1 AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (55, NULL, 39, CAST(0x00009FFB0181CA95 AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (56, NULL, 40, CAST(0x00009FFB018262E0 AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (57, 6, 41, CAST(0x00009FFB018272FB AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (58, NULL, 42, CAST(0x00009FFC00A946BF AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (59, 7, 43, CAST(0x00009FFC00A9AFC6 AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (60, 7, 44, CAST(0x00009FFC00AB2D88 AS DateTime), 0, N'test2')
INSERT [dbo].[DB_Collection] ([Id], [Album_Id], [Picture_Id], [Collect_Time], [Collect_Type], [Username]) VALUES (61, 2, 42, CAST(0x00009FFE018237BC AS DateTime), 1, N'test1')
SET IDENTITY_INSERT [dbo].[DB_Collection] OFF
/****** Object:  Table [dbo].[DB_Reply]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Reply](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Picture_Id] [bigint] NULL,
	[Album_Id] [bigint] NULL,
	[Discussion_Id] [bigint] NULL,
	[Replier_Username] [nvarchar](20) NOT NULL,
	[Text] [ntext] NOT NULL,
	[Post_Time] [datetime] NOT NULL,
	[Reply_Id] [bigint] NULL,
	[Reply_To] [nvarchar](20) NULL,
 CONSTRAINT [PK_DB_Reply] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Reply] ON
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (7, 7, NULL, NULL, N'test1', N'这是啥', CAST(0x00009FD400B7B51F AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (8, 7, NULL, NULL, N'test1', N'回复1', CAST(0x00009FD400C1E238 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (9, 7, NULL, NULL, N'test1', N'回复2', CAST(0x00009FD400C1EB46 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (10, 7, NULL, NULL, N'test1', N'回复3', CAST(0x00009FD400C1F01E AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (11, 7, NULL, NULL, N'test1', N'回复4', CAST(0x00009FD400C1F6DF AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (12, 5, NULL, NULL, N'test1', N'hello hello', CAST(0x00009FD40102C71B AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (16, 27, NULL, NULL, N'test1', N'可爱啊', CAST(0x00009FEC009BB0EC AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (17, 27, NULL, NULL, N'test2', N'添加你的评论...', CAST(0x00009FEC011C6C55 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (18, NULL, 5, NULL, N'test3', N'跳跳跳', CAST(0x00009FEC011DD916 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (19, NULL, 5, NULL, N'test3', N'爱爱爱', CAST(0x00009FEC011E0292 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (20, 26, NULL, NULL, N'test1', N'添加你的评论...', CAST(0x00009FEF00DBC0AF AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (21, 26, NULL, NULL, N'test1', N'test', CAST(0x00009FEF00DBCA61 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (25, NULL, NULL, 2, N'test3', N'我也不知道', CAST(0x00009FF500000000 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (26, NULL, NULL, 2, N'test1', N'test1', CAST(0x00009FF600A48E25 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (27, NULL, NULL, 2, N'test1', N'test2', CAST(0x00009FF600A4F929 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (28, NULL, NULL, 2, N'test1', N'test3', CAST(0x00009FF600A55DB2 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (29, NULL, NULL, 2, N'test1', N'test4', CAST(0x00009FF600A60170 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (30, NULL, NULL, 2, N'test1', N'test5', CAST(0x00009FF600A609B6 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (31, NULL, NULL, 2, N'test1', N'test6', CAST(0x00009FF600A61E67 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (32, NULL, NULL, 2, N'test1', N'test7', CAST(0x00009FF600A8000A AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (33, NULL, NULL, 2, N'test1', N'test8', CAST(0x00009FF600A9A8CB AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (34, NULL, NULL, 2, N'test1', N'test9', CAST(0x00009FF600A9EB66 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (35, NULL, NULL, 2, N'test1', N'test10', CAST(0x00009FF600AA092F AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (36, NULL, NULL, 2, N'test1', N'test 1#1', CAST(0x00009FF600AA21BC AS DateTime), 34, N'test1')
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (37, NULL, NULL, 2, N'test1', N'test11', CAST(0x00009FF600AC634B AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (38, NULL, NULL, 4, N'test1', N'test1', CAST(0x00009FF600AC784B AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (39, 36, NULL, NULL, N'test1', N'test', CAST(0x00009FF600E81476 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (40, 36, NULL, NULL, N'test1', N'test2', CAST(0x00009FF600E85AF9 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (41, 36, NULL, NULL, N'test1', N'test3', CAST(0x00009FF600E8E372 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (42, 27, NULL, NULL, N'test1', N'aaa', CAST(0x00009FF600F838A6 AS DateTime), NULL, NULL)
INSERT [dbo].[DB_Reply] ([Id], [Picture_Id], [Album_Id], [Discussion_Id], [Replier_Username], [Text], [Post_Time], [Reply_Id], [Reply_To]) VALUES (43, NULL, NULL, 8, N'test1', N'new test', CAST(0x00009FFE017BF380 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[DB_Reply] OFF
/****** Object:  Table [dbo].[DB_Notice]    Script Date: 02/21/2012 23:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Notice](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](20) NOT NULL,
	[Action_Username] [nvarchar](20) NOT NULL,
	[Notice_Type] [tinyint] NOT NULL,
	[Has_Read] [tinyint] NOT NULL CONSTRAINT [DF_DB_Notice_Has_Read]  DEFAULT ((0)),
	[Notice_Time] [datetime] NOT NULL,
	[Collect_Id] [bigint] NULL,
	[Reply_Id] [bigint] NULL,
	[Like_Id] [bigint] NULL,
 CONSTRAINT [PK_DB_Notice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DB_Notice] ON
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (1, N'test2', N'test1', 1, 1, CAST(0x00009FEB01259AB4 AS DateTime), 41, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (5, N'test2', N'test1', 4, 1, CAST(0x00009FEC009BB0F1 AS DateTime), NULL, 16, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (6, N'test2', N'test1', 2, 1, CAST(0x00009FEC00A3A5FD AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (7, N'test2', N'test1', 8, 1, CAST(0x00009FEC00A53891 AS DateTime), NULL, NULL, 11)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (8, N'test2', N'test3', 2, 1, CAST(0x00009FEC011670B6 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (10, N'test2', N'test3', 8, 1, CAST(0x00009FEC011744D2 AS DateTime), NULL, NULL, 13)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (11, N'test1', N'test2', 2, 1, CAST(0x00009FEC011770DA AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (12, N'test1', N'test2', 2, 1, CAST(0x00009FEC01195B0E AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (13, N'test1', N'test2', 2, 1, CAST(0x00009FEC01195C84 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (14, N'test2', N'test3', 8, 1, CAST(0x00009FEC011A6CDA AS DateTime), NULL, NULL, 14)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (15, N'test2', N'test3', 8, 1, CAST(0x00009FEC011B2853 AS DateTime), NULL, NULL, 15)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (16, N'test2', N'test3', 8, 1, CAST(0x00009FEC011BB164 AS DateTime), NULL, NULL, 16)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (17, N'test2', N'test3', 4, 0, CAST(0x00009FEC011DD918 AS DateTime), NULL, 18, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (18, N'test2', N'test3', 4, 0, CAST(0x00009FEC011E0294 AS DateTime), NULL, 19, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (19, N'test2', N'test3', 1, 1, CAST(0x00009FEC011F6F48 AS DateTime), 42, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (20, N'test2', N'test4', 8, 1, CAST(0x00009FEC012A1F2E AS DateTime), NULL, NULL, 17)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (21, N'test2', N'test1', 4, 0, CAST(0x00009FEF00DBC0B9 AS DateTime), NULL, 20, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (22, N'test2', N'test1', 4, 0, CAST(0x00009FEF00DBCA65 AS DateTime), NULL, 21, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (23, N'test2', N'test1', 4, 0, CAST(0x00009FF600A48E2C AS DateTime), NULL, 26, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (24, N'test2', N'test1', 4, 0, CAST(0x00009FF600A51293 AS DateTime), NULL, 27, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (25, N'test2', N'test1', 4, 0, CAST(0x00009FF600A55DC7 AS DateTime), NULL, 28, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (26, N'test2', N'test1', 4, 0, CAST(0x00009FF600A60178 AS DateTime), NULL, 29, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (27, N'test2', N'test1', 4, 0, CAST(0x00009FF600A609BC AS DateTime), NULL, 30, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (28, N'test2', N'test1', 4, 0, CAST(0x00009FF600A61E6E AS DateTime), NULL, 31, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (29, N'test2', N'test1', 4, 0, CAST(0x00009FF600A8000D AS DateTime), NULL, 32, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (30, N'test2', N'test1', 4, 0, CAST(0x00009FF600A9A8CD AS DateTime), NULL, 33, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (31, N'test2', N'test1', 4, 0, CAST(0x00009FF600A9EB6D AS DateTime), NULL, 34, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (32, N'test2', N'test1', 4, 0, CAST(0x00009FF600AA0936 AS DateTime), NULL, 35, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (33, N'test2', N'test1', 4, 0, CAST(0x00009FF600AA21C2 AS DateTime), NULL, 36, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (34, N'test2', N'test1', 4, 0, CAST(0x00009FF600AC6360 AS DateTime), NULL, 37, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (35, N'test4', N'test1', 4, 0, CAST(0x00009FF600AC7854 AS DateTime), NULL, 38, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (36, N'test2', N'test1', 4, 0, CAST(0x00009FF600F838B2 AS DateTime), NULL, 42, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (37, N'test2', N'test1', 2, 0, CAST(0x00009FF600F9CBB4 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (38, N'test2', N'test1', 2, 0, CAST(0x00009FF600FB3B08 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (39, N'test1', N'杨垒', 2, 1, CAST(0x00009FF700DD6FE8 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (40, N'test1', N'test2', 8, 1, CAST(0x00009FFB0176B5AE AS DateTime), NULL, NULL, 18)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (41, N'test3', N'test1', 1, 0, CAST(0x00009FFE017FC2A4 AS DateTime), 10, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (42, N'test3', N'test1', 1, 0, CAST(0x00009FFE017FCB52 AS DateTime), 10, NULL, NULL)
INSERT [dbo].[DB_Notice] ([Id], [Username], [Action_Username], [Notice_Type], [Has_Read], [Notice_Time], [Collect_Id], [Reply_Id], [Like_Id]) VALUES (43, N'test2', N'test1', 1, 0, CAST(0x00009FFE018237C1 AS DateTime), 61, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DB_Notice] OFF
/****** Object:  ForeignKey [FK_DB_Album_DB_User]    Script Date: 02/21/2012 23:28:05 ******/
ALTER TABLE [dbo].[DB_Album]  WITH CHECK ADD  CONSTRAINT [FK_DB_Album_DB_User] FOREIGN KEY([Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Album] CHECK CONSTRAINT [FK_DB_Album_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Announcement_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Announcement]  WITH CHECK ADD  CONSTRAINT [FK_DB_Announcement_DB_User] FOREIGN KEY([Admin_Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Announcement] CHECK CONSTRAINT [FK_DB_Announcement_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Collection_DB_Album]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Collection]  WITH CHECK ADD  CONSTRAINT [FK_DB_Collection_DB_Album] FOREIGN KEY([Album_Id])
REFERENCES [dbo].[DB_Album] ([Id])
GO
ALTER TABLE [dbo].[DB_Collection] CHECK CONSTRAINT [FK_DB_Collection_DB_Album]
GO
/****** Object:  ForeignKey [FK_DB_Collection_DB_Picture]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Collection]  WITH CHECK ADD  CONSTRAINT [FK_DB_Collection_DB_Picture] FOREIGN KEY([Picture_Id])
REFERENCES [dbo].[DB_Picture] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DB_Collection] CHECK CONSTRAINT [FK_DB_Collection_DB_Picture]
GO
/****** Object:  ForeignKey [FK_DB_Collection_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Collection]  WITH CHECK ADD  CONSTRAINT [FK_DB_Collection_DB_User] FOREIGN KEY([Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Collection] CHECK CONSTRAINT [FK_DB_Collection_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Conversation_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Conversation]  WITH CHECK ADD  CONSTRAINT [FK_DB_Conversation_DB_User] FOREIGN KEY([Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Conversation] CHECK CONSTRAINT [FK_DB_Conversation_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Conversation_DB_User1]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Conversation]  WITH CHECK ADD  CONSTRAINT [FK_DB_Conversation_DB_User1] FOREIGN KEY([Target_Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Conversation] CHECK CONSTRAINT [FK_DB_Conversation_DB_User1]
GO
/****** Object:  ForeignKey [FK_DB_Discussion_DB_Discussion_Board]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Discussion]  WITH CHECK ADD  CONSTRAINT [FK_DB_Discussion_DB_Discussion_Board] FOREIGN KEY([Board_Id])
REFERENCES [dbo].[DB_Discussion_Board] ([Id])
GO
ALTER TABLE [dbo].[DB_Discussion] CHECK CONSTRAINT [FK_DB_Discussion_DB_Discussion_Board]
GO
/****** Object:  ForeignKey [FK_DB_Discussion_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Discussion]  WITH CHECK ADD  CONSTRAINT [FK_DB_Discussion_DB_User] FOREIGN KEY([Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Discussion] CHECK CONSTRAINT [FK_DB_Discussion_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Fan_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Fan]  WITH CHECK ADD  CONSTRAINT [FK_DB_Fan_DB_User] FOREIGN KEY([FansUsername])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Fan] CHECK CONSTRAINT [FK_DB_Fan_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Fan_DB_User1]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Fan]  WITH CHECK ADD  CONSTRAINT [FK_DB_Fan_DB_User1] FOREIGN KEY([WatchedUsername])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Fan] CHECK CONSTRAINT [FK_DB_Fan_DB_User1]
GO
/****** Object:  ForeignKey [FK_DB_Keyword_DB_Keyword_Category]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Keyword]  WITH CHECK ADD  CONSTRAINT [FK_DB_Keyword_DB_Keyword_Category] FOREIGN KEY([Category_Id])
REFERENCES [dbo].[DB_Keyword_Category] ([Id])
GO
ALTER TABLE [dbo].[DB_Keyword] CHECK CONSTRAINT [FK_DB_Keyword_DB_Keyword_Category]
GO
/****** Object:  ForeignKey [FK_DB_Like_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Like]  WITH CHECK ADD  CONSTRAINT [FK_DB_Like_DB_User] FOREIGN KEY([Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Like] CHECK CONSTRAINT [FK_DB_Like_DB_User]
GO
/****** Object:  ForeignKey [FK_Like_Album]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Like]  WITH CHECK ADD  CONSTRAINT [FK_Like_Album] FOREIGN KEY([Album_Id])
REFERENCES [dbo].[DB_Album] ([Id])
GO
ALTER TABLE [dbo].[DB_Like] CHECK CONSTRAINT [FK_Like_Album]
GO
/****** Object:  ForeignKey [FK_DB_Message_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Message]  WITH CHECK ADD  CONSTRAINT [FK_DB_Message_DB_User] FOREIGN KEY([Send_Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Message] CHECK CONSTRAINT [FK_DB_Message_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Message_DB_User1]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Message]  WITH CHECK ADD  CONSTRAINT [FK_DB_Message_DB_User1] FOREIGN KEY([To_Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Message] CHECK CONSTRAINT [FK_DB_Message_DB_User1]
GO
/****** Object:  ForeignKey [FK_DB_Notice_DB_Collection]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice]  WITH CHECK ADD  CONSTRAINT [FK_DB_Notice_DB_Collection] FOREIGN KEY([Collect_Id])
REFERENCES [dbo].[DB_Collection] ([Id])
GO
ALTER TABLE [dbo].[DB_Notice] CHECK CONSTRAINT [FK_DB_Notice_DB_Collection]
GO
/****** Object:  ForeignKey [FK_DB_Notice_DB_Like]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice]  WITH CHECK ADD  CONSTRAINT [FK_DB_Notice_DB_Like] FOREIGN KEY([Like_Id])
REFERENCES [dbo].[DB_Like] ([Id])
GO
ALTER TABLE [dbo].[DB_Notice] CHECK CONSTRAINT [FK_DB_Notice_DB_Like]
GO
/****** Object:  ForeignKey [FK_DB_Notice_DB_Reply]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice]  WITH CHECK ADD  CONSTRAINT [FK_DB_Notice_DB_Reply] FOREIGN KEY([Reply_Id])
REFERENCES [dbo].[DB_Reply] ([Id])
GO
ALTER TABLE [dbo].[DB_Notice] CHECK CONSTRAINT [FK_DB_Notice_DB_Reply]
GO
/****** Object:  ForeignKey [FK_DB_Notice_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice]  WITH CHECK ADD  CONSTRAINT [FK_DB_Notice_DB_User] FOREIGN KEY([Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Notice] CHECK CONSTRAINT [FK_DB_Notice_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Notice_DB_User1]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Notice]  WITH CHECK ADD  CONSTRAINT [FK_DB_Notice_DB_User1] FOREIGN KEY([Action_Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Notice] CHECK CONSTRAINT [FK_DB_Notice_DB_User1]
GO
/****** Object:  ForeignKey [FK_DB_Picture_DB_Album]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Picture]  WITH CHECK ADD  CONSTRAINT [FK_DB_Picture_DB_Album] FOREIGN KEY([Album_Id])
REFERENCES [dbo].[DB_Album] ([Id])
GO
ALTER TABLE [dbo].[DB_Picture] CHECK CONSTRAINT [FK_DB_Picture_DB_Album]
GO
/****** Object:  ForeignKey [FK_DB_Picture_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Picture]  WITH CHECK ADD  CONSTRAINT [FK_DB_Picture_DB_User] FOREIGN KEY([Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Picture] CHECK CONSTRAINT [FK_DB_Picture_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Reply_DB_Album]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply]  WITH CHECK ADD  CONSTRAINT [FK_DB_Reply_DB_Album] FOREIGN KEY([Album_Id])
REFERENCES [dbo].[DB_Album] ([Id])
GO
ALTER TABLE [dbo].[DB_Reply] CHECK CONSTRAINT [FK_DB_Reply_DB_Album]
GO
/****** Object:  ForeignKey [FK_DB_Reply_DB_Discussion]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply]  WITH CHECK ADD  CONSTRAINT [FK_DB_Reply_DB_Discussion] FOREIGN KEY([Discussion_Id])
REFERENCES [dbo].[DB_Discussion] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DB_Reply] CHECK CONSTRAINT [FK_DB_Reply_DB_Discussion]
GO
/****** Object:  ForeignKey [FK_DB_Reply_DB_Picture]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply]  WITH CHECK ADD  CONSTRAINT [FK_DB_Reply_DB_Picture] FOREIGN KEY([Picture_Id])
REFERENCES [dbo].[DB_Picture] ([Id])
GO
ALTER TABLE [dbo].[DB_Reply] CHECK CONSTRAINT [FK_DB_Reply_DB_Picture]
GO
/****** Object:  ForeignKey [FK_DB_Reply_DB_Reply]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply]  WITH CHECK ADD  CONSTRAINT [FK_DB_Reply_DB_Reply] FOREIGN KEY([Reply_Id])
REFERENCES [dbo].[DB_Reply] ([Id])
GO
ALTER TABLE [dbo].[DB_Reply] CHECK CONSTRAINT [FK_DB_Reply_DB_Reply]
GO
/****** Object:  ForeignKey [FK_DB_Reply_DB_User]    Script Date: 02/21/2012 23:28:06 ******/
ALTER TABLE [dbo].[DB_Reply]  WITH CHECK ADD  CONSTRAINT [FK_DB_Reply_DB_User] FOREIGN KEY([Replier_Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Reply] CHECK CONSTRAINT [FK_DB_Reply_DB_User]
GO
/****** Object:  ForeignKey [FK_DB_Tag_DB_User]    Script Date: 02/21/2012 23:28:07 ******/
ALTER TABLE [dbo].[DB_Tag]  WITH CHECK ADD  CONSTRAINT [FK_DB_Tag_DB_User] FOREIGN KEY([Username])
REFERENCES [dbo].[DB_User] ([Username])
GO
ALTER TABLE [dbo].[DB_Tag] CHECK CONSTRAINT [FK_DB_Tag_DB_User]
GO
/****** Object:  ForeignKey [FK_User_Location]    Script Date: 02/21/2012 23:28:07 ******/
ALTER TABLE [dbo].[DB_User]  WITH CHECK ADD  CONSTRAINT [FK_User_Location] FOREIGN KEY([Location_Id])
REFERENCES [dbo].[DB_Location] ([Id])
GO
ALTER TABLE [dbo].[DB_User] CHECK CONSTRAINT [FK_User_Location]
GO
