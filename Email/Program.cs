﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PumpkinHouse.Utils;
using Mail;
using PumpkinHouseDatabase;
using System.Net.Mail;
using System.Threading;

namespace Email
{
    class Program
    {
        private static void PrintHelp()
        {
            Console.WriteLine("usage: SendEmail.exe [/reset] [<邮件主题> <模板路径>]");
        }

        private static void ResetRecords()
        {
            using (DataUtils utils = new DataUtils())
            {
                foreach (var u in utils.FindAllUsers().ToList())
                {
                    u.Email_Sent = 0;
                }
                utils.Commit();
            }
        }

        private static void SendEmail(string subject, string template, bool test)
        {
            if (test)
            {
                Console.WriteLine("Test Mode");
            }
            MailClient.LoadConfiguration("mail.config");
            // EmailHelper.SendActivityEmail(subject, "728635054@qq.com", template, new string[] { "测试邮件" });
            using (DataUtils utils = new DataUtils())
            {
                bool start = true;
                var result = utils.FindAllUsers().Where(u => u.Receive_Email == 1 && u.Email != "" && u.Email_Verified != 2 && u.Email_Sent != 1).ToList();

                foreach (var r in result)
                {
                    //if (r.Email == "iujijvjn@126.com")
                    //{
                    //    //start = true;
                    //}
                    if (!start) { Console.WriteLine("\tskip"); continue; }

                    bool retry;
                    do
                    {
                        retry = false;
                        try
                        {
                            SendEmail(subject, template, utils, r, test);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(r.Email + "\t" + r.Nick_Name + "\t" + e.Message);
                            if (e.Message.Equals("Mailbox unavailable. The server response was: Connection frequency limited"))
                            {
                                Thread.Sleep(1000 * 60 * 10); // 停止发送10分钟
                                Console.WriteLine(r.Email + "\t" + r.Nick_Name + "\tRetrying");
                                retry = true;
                            }
                        }
                    } while (retry);
                    if (!test)
                    {
                        Thread.Sleep(25000);
                    }
                }
            }
        }

        private static void SendEmail(string subject, string template, DataUtils utils, DB_User r, bool test)
        {
            if (r.Email == "raymanyoung@gmail.com" || !test)
            {
                EmailHelper.SendActivityEmail(subject, r.Email, template, new string[] { r.Nick_Name });
                r.Email_Sent = 1;
                utils.Commit();
                Console.WriteLine(r.Email + "\t" + r.Nick_Name + "\tSuccess");
            }
        }

        static void Main(string[] args)
        {
            if (args.Length < 1 || args.Length > 3)
            {
                PrintHelp();
                return;
            }

            DataUtils.ConnectionString = @"Server=localhost\SQLExpress;Database=MutouheziDatabase;Integrated Security=true"; //305906536

            if (args.Length == 1)
            {
                if (args[0].Equals("/reset", StringComparison.OrdinalIgnoreCase))
                {
                    ResetRecords();
                    return;
                }
                else
                {
                    PrintHelp();
                    return;
                }
            }

            if (args.Length == 3)
            {
                if (args[2].Equals("/prod", StringComparison.OrdinalIgnoreCase))
                {
                    SendEmail(args[0], args[1], false);
                }
                else
                {
                    PrintHelp();
                    return;
                }
            }
            if (args.Length == 2)
            {
                SendEmail(args[0], args[1], true);
            }

        }
    }
}
