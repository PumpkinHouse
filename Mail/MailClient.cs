﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Xml;
using System.Globalization;

namespace Mail
{
    public class MailClient : IDisposable
    {
        private SmtpClient client;

        private static MailConfiguration Configuration { get; set; }

        #region IDisposable Methods

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (client != null)
                    {
                        client.Dispose();
                    }
                }

                // Note disposing has been done.
                disposed = true;
            }
        }

        #endregion

        public static void LoadConfiguration(string configPath)
        {
            XmlDocument newDoc = new XmlDocument();
            newDoc.Load(configPath);

            XmlElement configElement = newDoc.DocumentElement;
            Configuration = new MailConfiguration
            {
                Hostname = configElement.SelectSingleNode("//host").InnerText,
                Password = configElement.SelectSingleNode("//password").InnerText,
                Port = int.Parse(configElement.SelectSingleNode("//port").InnerText),
                Username = configElement.SelectSingleNode("//username").InnerText,
                EnableSsl = bool.Parse(configElement.SelectSingleNode("//ssl").InnerText),
                DisplayName = configElement.SelectSingleNode("//displayName").InnerText
            };
        }

        public MailClient()
        {
            client = new SmtpClient(Configuration.Hostname, Configuration.Port);
            client.EnableSsl = Configuration.EnableSsl;

            NetworkCredential myCreds = new NetworkCredential(Configuration.Username, Configuration.Password);
            client.Credentials = myCreds;
        }

        public void SendMessage(string[] to, string[] cc, string subject, string content)
        {
            if (to == null)
            {
                throw new ArgumentNullException("to");
            }

            if (subject == null)
            {
                throw new ArgumentNullException("subject");
            }

            using (MailMessage message = new MailMessage())
            {
                message.From = new MailAddress(string.Format(CultureInfo.InvariantCulture, "{0}<{1}>", Configuration.DisplayName, Configuration.Username));
                message.IsBodyHtml = true;


                foreach (string t in to)
                {
                    message.To.Add(new MailAddress(t));
                }

                if (cc != null)
                {
                    foreach (string c in cc)
                    {
                        message.CC.Add(new MailAddress(c));
                    }
                }

                message.Subject = subject;
                message.Body = content;

                client.Send(message);
            }
        }
    }

    public class MailConfiguration
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string Hostname { get; set; }
        public bool EnableSsl { get; set; }
        public string DisplayName { get; set; }
    }
}

