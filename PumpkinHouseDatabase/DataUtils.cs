﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using PumpkinHouseDatabase.Contract;
using log4net;

namespace PumpkinHouseDatabase
{
    public class DataUtils : IDisposable
    {
        #region IDisposable Methods

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (context != null)
                    {
#if DEBUG
                        context.Log.Dispose();
#endif
                        context.Dispose();
                    }
                }

                // Note disposing has been done.
                disposed = true;
            }
        }

        #endregion

        private static ILog Log = LogManager.GetLogger(typeof(DataUtils));

        public static string ConnectionString
        {
            get;
            set;
        }

        public MutouheziDatabase context;

        static DataLoadOptions dataLoadOption;
        static DataLoadOptions GetDataLoadOption()
        {
            if (dataLoadOption == null) {
                dataLoadOption = new DataLoadOptions();
                dataLoadOption.LoadWith<DB_Album>(alb => alb.DB_Picture);
//                dataLoadOption.LoadWith<DB_Album>(alb => alb.DB_Collection);
            }
            return dataLoadOption;
        }

        public DataUtils(bool enableTracking = true)
        {
            context = new MutouheziDatabase(ConnectionString);
            context.LoadOptions = GetDataLoadOption();
            context.ObjectTrackingEnabled = enableTracking;
#if DEBUG
            context.Log = new Log4netWriter(this.GetType());
#endif
        }

        public void Commit()
        {
            context.SubmitChanges();
        }

        #region Users
        public IQueryable<DB_User> FindAllUsers()
        {
            return from u in context.DB_User select u;
        }

        /// <summary>
        /// Find user by user email.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>User object. Return null when there is no corresponding user</returns>
        public DB_User FindUserByEmail(string email)
        {
            var query = from u in context.DB_User
                        where u.Email == email
                        select u;
            return query.SingleOrDefault();
        }

        public DB_User FindUserByNickname(string nickname)
        {
            var query = from u in context.DB_User
                        where u.Nick_Name == nickname
                        select u;
            return query.SingleOrDefault();
        }

        public IQueryable<DB_User> FindUsers()
        {
            return from u in context.DB_User select u;
        }

        /// <summary>
        /// Find user by username
        /// </summary>
        /// <param name="username"></param>
        /// <returns>User object. Return null if user is not found</returns>
        public DB_User FindUserByUsername(string username)
        {
            var query = from u in context.DB_User
                        where u.Username == username
                        select u;
            return query.SingleOrDefault();
        }

        public DB_User FindUserByNameOrEmail(string usernameOrEmail)
        {
            DB_User user = FindUserByUsername(usernameOrEmail);
            if (user == null)
            {
                user = FindUserByEmail(usernameOrEmail);
            }
            return user;
        }

        public DB_User FindUserByAssociation(string associationId, AssociationParty party)
        {
            return (from aa in context.DB_Account_Association where aa.External_Username == associationId && aa.Party == (byte)party select aa.DB_User).SingleOrDefault();
        }

        public DB_User InsertUser(DB_User user)
        {
            DB_User u = FindUserByUsername(user.Username);
            if (u == null)
            {
                context.DB_User.InsertOnSubmit(user);

                context.SubmitChanges();
            }

            return u;
        }

        public IQueryable<DB_User> SearchUsers(string keyword)
        {
            return from u in context.DB_User
                   where u.Nick_Name.IndexOf(keyword) >= 0
                       || u.Self_Introduction.IndexOf(keyword) >= 0
                   select u;
        }
        #endregion

        #region 账号关联
        public DB_Account_Association FindAccountAssociationById(long id)
        {
            return (from aa in context.DB_Account_Association where aa.Id == id select aa).SingleOrDefault();
        }

        public DB_Account_Association InsertAccountAssociation(DB_Account_Association association)
        {
            DB_Account_Association aa = FindAccountAssociationById(association.Id);
            if (aa == null)
            {
                context.DB_Account_Association.InsertOnSubmit(association);
                context.SubmitChanges();
                return association;
            }
            return aa;
        }

        public IQueryable<DB_Account_Association> FindAccountAssociation()
        {
            return from aa in context.DB_Account_Association select aa;
        }

        public void DeleteAccountAssociation(DB_Account_Association aa)
        {
            if (aa != null)
            {
                context.DB_Account_Association.DeleteOnSubmit(aa);
                context.SubmitChanges();
            }
        }

        #endregion

        #region Message related

        public DB_Conversation FindConverstaionById(long id)
        {
            return (from c in context.DB_Conversation where c.Id == id select c).SingleOrDefault();
        }

        public DB_Conversation FindConverstaionByUser(string username, string targetUsername)
        {
            return (from c in context.DB_Conversation where c.Username == username && c.Target_Username == targetUsername select c).SingleOrDefault();
        }

        public DB_Conversation InsertConversation(DB_Conversation conversation)
        {
            DB_Conversation c = FindConverstaionById(conversation.Id);
            if (c == null)
            {
                conversation.Id = 0;
                context.DB_Conversation.InsertOnSubmit(conversation);
                context.SubmitChanges();
                return conversation;
            }
            return c;
        }

        public void DeleteConversation(DB_Conversation conversation)
        {
            DB_Conversation c = FindConverstaionById(conversation.Id);
            if (c != null)
            {
                context.DB_Conversation.DeleteOnSubmit(c);
                context.SubmitChanges();
            }
        }

        public void InsertMessage(MessageToSend message)
        {
            DB_Message m = new DB_Message
            {
                Text = message.Text,
                To_Username = message.ToUsername,
                Send_Username = message.FromUsername,
                Sent_Time = DateTime.Now
            };

            context.DB_Message.InsertOnSubmit(m);
            context.SubmitChanges();
        }

        public IQueryable<DB_Message> FindMessage(string user1, string user2)
        {
            return from m in context.DB_Message
                   where (m.Send_Username == user1 && m.To_Username == user2)
                   || (m.Send_Username == user2 && m.To_Username == user1)
                   orderby m.Sent_Time descending
                   select m;
        }

        public IQueryable<DB_Message> FindUnreadMessage(string username)
        {
            return from m in context.DB_Message
                   where m.To_Username == username
                   orderby m.Sent_Time descending
                   select m;
        }

        public IQueryable<DB_Conversation> FindThread(string targetUsername)
        {
            // Note there is some confusion here in the naming.
            // For example user A is checking the messages
            // then we are looking for the messages with "target" username == A
            // however from user A's perspective, the target should be B.

            // find the conversations
            return from c in context.DB_Conversation
                   where c.Target_Username == targetUsername
                   orderby c.Latest_Update_Time descending
                   select c;

        }

        #endregion

        #region Fan related
        public DB_Fan InsertFan(DB_Fan fan)
        {
            DB_Fan newFan = new DB_Fan { FansUsername = fan.FansUsername, WatchedUsername = fan.WatchedUsername, Action_Time = DateTime.Now };
            context.DB_Fan.InsertOnSubmit(newFan);

            context.SubmitChanges();

            return newFan;
        }

        public IQueryable<DB_Fan> FindFans(string username)
        {
            return from f in context.DB_Fan
                   where f.WatchedUsername == username
                   orderby f.Action_Time descending
                   select f;
        }

        public IQueryable<DB_User> FindAttentions(string username)
        {
            return from f in context.DB_Fan
                   where f.FansUsername == username
                   orderby f.Action_Time descending
                   select f.WatchedUsernameDB_User;
        }

        public DB_Fan FindFan(string fanUsername, string targetUsername)
        {
            return (from f in context.DB_Fan where f.FansUsername == fanUsername where f.WatchedUsername == targetUsername select f)
                .SingleOrDefault();
        }

        public bool IsWatching(string watcherUsername, string targetUsername)
        {
            return FindFan(watcherUsername, targetUsername) != null;
        }

        public void DeleteFan(string fanUsername, string watchedUsername)
        {
            DB_Fan f = FindFan(fanUsername, watchedUsername);
            if (f != null)
            {
                context.DB_Fan.DeleteOnSubmit(f);
                context.SubmitChanges();
            }
        }

        #endregion

        #region Album related
        public IQueryable<DB_Album> FindAlbums()
        {
            return (from album in context.DB_Album select album);
        }

        public IQueryable<View_Album> FindViewAlbums()
        {
            return (from album in context.View_Album select album);
        }

        public View_Album FindViewAlbumById(long id)
        {
            return (from album in context.View_Album where album.Id == id select album).SingleOrDefault();
        }

        public DB_Album FindAlbumById(long id)
        {
            return (from album in context.DB_Album where album.Id == id select album).SingleOrDefault();
        }

        public IQueryable<DB_Album> FindAlbumByOwnerUsername(string username, bool includePicture)
        {
            return from album in context.DB_Album where album.Username == username select album;
        }

        public IQueryable<View_Album> FindViewAlbumByOwnerUsername(string username, bool includePicture)
        {
            return from album in context.View_Album where album.Username == username select album;
        }

        public DB_Album InsertAlbum(DB_Album album)
        {
            DB_Album alb = FindAlbumById(album.Id);
            if (alb == null)
            {
                alb = new DB_Album();
                alb.Creation_Time = DateTime.Now;

                alb.Name = album.Name;
                alb.Number_Of_Like = album.Number_Of_Like;
                alb.Username = album.Username;
                alb.Description = album.Description;
                alb.Tags = album.Tags;

                context.DB_Album.InsertOnSubmit(alb);

                context.SubmitChanges();
            }


            return alb;
        }

        public IQueryable<View_Album> FindAlbumByLikeUsername(string username)
        {
            var query = from alb in context.View_Album
                        from l in context.DB_Like
                        where alb.Id == l.Album_Id
                        where l.Username == username
                        select alb;
            return query;
        }

        public IQueryable<DB_User> FindUsersLikeAlbum(long albumId)
        {
            return from user in context.DB_User
                   from l in context.DB_Like
                   where user.Username == l.Username
                   where l.Album_Id == albumId
                   select user;
        }

        public IQueryable<DB_Album> FindAlbumsCollectingPicture(long pictureId, bool includePicture)
        {
            return from col in context.DB_Collection
                   where col.Picture_Id == pictureId
                   where col.Album_Id != null
                   where col.Collect_Type == 1
                   select col.DB_Album;
        }

        public IQueryable<DB_Collection> FindCollectionsByPictureId(long pictureId)
        {
            return (from col in context.DB_Collection
                    where col.Picture_Id == pictureId
                    where col.Collect_Type == 1
                    select col).Distinct();
        }

        public int FindCollectionCountByPictureId(long pictureId)
        {
            Log.Debug("using compiled query");
            return CompiledQueryLib.FindCollectionCountByPictureIdCompiledQuery.Invoke(context, pictureId, 1);
        }

        public bool DoesAlbumBelongTo(string username, long albumId)
        {
            return (from alb in context.DB_Album where alb.Username == username where alb.Id == albumId select alb).SingleOrDefault() != null;
        }

        public IQueryable<View_Album> SearchAlbums(string keyword)
        {
            return from alb in context.View_Album
                   where alb.Name.IndexOf(keyword) >= 0 || alb.Description.IndexOf(keyword) >= 0
                   select alb;
        }

        public void DeleteAlbum(DB_Album album)
        {
            if (album != null)
            {
                context.DB_Album.DeleteOnSubmit(album);
                context.SubmitChanges();
            }
        }
        #endregion

        #region Like Album related


        private DB_Like FindLike(long albumId, string username)
        {
            return (from l in context.DB_Like where l.Album_Id == albumId where l.Username == username select l).SingleOrDefault();
        }

        public bool DoesLikeBy(long albumId, string username)
        {
            return FindLike(albumId, username) != null;
        }

        public DB_Like InsertLike(DB_Like like)
        {
            if (!DoesLikeBy(like.Album_Id, like.Username))
            {
                context.DB_Like.InsertOnSubmit(like);
                context.SubmitChanges();
            }
            return like;
        }

        public void DeleteLike(DB_Like like)
        {
            DB_Like l = FindLike(like.Album_Id, like.Username);
            if (l != null)
            {
                context.DB_Like.DeleteOnSubmit(l);
                context.SubmitChanges();
            }
        }
        #endregion

        #region Like Picture related
        public IQueryable<DB_Like_Picture> FindLikePictures()
        {
            return from lp in context.DB_Like_Picture select lp;
        }
        public DB_Like_Picture FindLikePictureById(long id)
        {
            return FindLikePictures().Where(lp => lp.Id == id).SingleOrDefault();
        }

        public DB_Like_Picture InsertLikePicture(DB_Like_Picture lp)
        {
            DB_Like_Picture l = FindLikePictureById(lp.Id);

            if (l == null)
            {
                context.DB_Like_Picture.InsertOnSubmit(lp);
                context.SubmitChanges();
                return lp;
            }
            return l;
        }

        public void DeleteLikePicture(DB_Like_Picture lp)
        {
            if (lp != null)
            {
                context.DB_Like_Picture.DeleteOnSubmit(lp);
                context.SubmitChanges();
            }
        }

        #endregion

        #region Picture Related
        public IQueryable<DB_Picture> FindPictures()
        {
            return from p in context.DB_Picture select p;
        }

        public IQueryable<View_Picture> FindViewPictures()
        {
            return from p in context.View_Picture select p;
        }

        public DB_Picture FindPictureById(long id)
        {
            return (from p in context.DB_Picture where p.Id == id select p).SingleOrDefault();
        }

        public IQueryable<DB_Picture2> FindPicture2()
        {
            return (from p in context.DB_Picture2 select p);
        }

        public DB_Picture2 FindPicture2ById(long id)
        {
            return (from p in context.DB_Picture2 where p.Id == id select p).SingleOrDefault();
        }

        public View_Picture FindViewPictureById(long id)
        {
            return (from p in context.View_Picture where p.Id == id select p).SingleOrDefault();
        }

        public IQueryable<View_Collection_With_Original> FindUncategorizedCollections(string username)
        {
            return from c in context.View_Collection_With_Original where c.Album_Id == null && c.Username == username orderby c.Id descending select c;
        }

        public IQueryable<View_Picture> FindUncategorizedViewPictures(string username)
        {
            return from p in context.View_Picture where p.Album_Id == null && p.Username == username orderby p.Upload_Time descending select p;
        }

        public IQueryable<View_Collection2> FindAllOriginalPictures(string keyword, bool commodityOnly)
        {
            IQueryable<View_Collection2> query = from c in context.View_Collection2 where c.Is_Original == 1 orderby c.Collect_Time descending select c;
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                query = query.Where(p => p.Tags.IndexOf(keyword) >= 0 || p.Description.IndexOf(keyword) >= 0 || p.Name.IndexOf(keyword) >= 0);
            }

            if (commodityOnly)
            {
                query = query.Where(p => p.Is_Commodity != 0 && p.Price > 0);
            }

            return query;
        }

        public IQueryable<DB_Picture> FindAllPictures(string keyword, bool commodityOnly)
        {
            IQueryable<DB_Picture> query = from p in context.DB_Picture orderby p.Upload_Time descending select p;

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                query = query.Where(p => p.Tags.IndexOf(keyword) >= 0 || p.Description.IndexOf(keyword) >= 0 || p.DB_Album.Name.IndexOf(keyword) >= 0);
            }

            if (commodityOnly)
            {
                query = query.Where(p => p.Is_Commodity != 0 && p.Price > 0);
            }

            return query;

            //if (string.IsNullOrWhiteSpace(keyword))
            //{
            //    return from p in context.DB_Picture orderby p.Upload_Time descending select p;
            //}
            //else
            //{
            //    return from p in context.DB_Picture where p.Tags.IndexOf(keyword) >= 0 orderby p.Upload_Time descending select p;
            //}
        }

        private static readonly Func<MutouheziDatabase, IQueryable<View_Picture>> viewPictureCompiledQuery
            = CompiledQuery.Compile<MutouheziDatabase, IQueryable<View_Picture>> (ctx => from p in ctx.View_Picture orderby p.Upload_Time descending select p);

        public IQueryable<View_Picture> FindAllViewPictures(string keyword, bool commodityOnly)
        {
            IQueryable<View_Picture> query = from p in context.View_Picture select p;

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                query = query.Where(p => p.Tags.IndexOf(keyword) >= 0 || p.Description.IndexOf(keyword) >= 0 || p.Name.IndexOf(keyword) >= 0);
            }

            if (commodityOnly)
            {
                query = query.Where(p => p.Is_Commodity != 0 && p.Price > 0);
            }

            return query;
        }

        public void DeletePicture(DB_Picture pic)
        {
            if (pic != null)
            {
                context.DB_Picture.DeleteOnSubmit(pic);
                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Must use in transaction
        /// </summary>
        /// <param name="picture"></param>
        /// <returns></returns>
        public DB_Picture2 InsertPicture(DB_Picture2 picture)
        {
            DB_Picture2 pic = FindPicture2ById(picture.Id);
            if (pic == null)
            {
                if (!string.IsNullOrEmpty(picture.Source_Page_Title) && picture.Source_Page_Title.Length > 50)
                {
                    picture.Source_Page_Title = picture.Source_Page_Title.Substring(0, 49);
                }
                context.DB_Picture2.InsertOnSubmit(picture);
                context.SubmitChanges();
                return picture;
            }
            return null;
        }
        #endregion

        #region Collection related
        public DB_Collection FindCollection(long albumId, long pictureId, string username)
        {
            if (albumId == 0)
            {
                return (from c in context.DB_Collection where c.Album_Id == null where username == c.Username where c.Picture_Id == pictureId select c).SingleOrDefault();
            }
            else
            {
                return (from c in context.DB_Collection where c.Album_Id == albumId where username == c.Username where c.Picture_Id == pictureId select c).SingleOrDefault();
            }
        }

        public IQueryable<DB_Collection> FindCollection(long albumId, string username)
        {
            if (albumId == 0)
            {
                return from c in context.DB_Collection where c.Album_Id == null where username == c.Username select c;
            }
            else
            {
                return from c in context.DB_Collection where c.Album_Id == albumId where username == c.Username select c;
            }
        }

        public IQueryable<DB_Collection> FindCollection()
        {
            return from c in context.DB_Collection select c;
        }

        public IQueryable<View_Collection> FindViewCollection(long albumId, string username)
        {
            if (albumId == 0)
            {
                return from c in context.View_Collection where c.Collect_Album_Id == null where username == c.Collect_Username select c;
            }
            else
            {
                return from c in context.View_Collection where c.Collect_Album_Id == albumId where username == c.Collect_Username select c;
            }
        }

        public View_Collection_With_Original FindViewCollectionWithOriginalById(long collectionId)
        {
            return (from c in context.View_Collection_With_Original where c.Id == collectionId select c).SingleOrDefault();
        }

        public IQueryable<View_Collection_With_Original> FindViewCollectionWithOriginal()
        {
            return from c in context.View_Collection_With_Original select c;
        }

        public IQueryable<View_Collection_With_Original> FindViewCollectionWithOriginal(long albumId, string username)
        {
            if (albumId == 0)
            {
                return from c in context.View_Collection_With_Original where c.Album_Id == null where username == c.Username select c;
            }
            else
            {
                return from c in context.View_Collection_With_Original where c.Album_Id == albumId where username == c.Username select c;
            }
        }

        public IQueryable<View_Collection_With_Original> FindViewCollectionWithOriginal(string keyword, bool commodityOnly)
        {
            IQueryable<View_Collection_With_Original> query = from c in context.View_Collection_With_Original select c;

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                query = query.Where(c => c.Tags.IndexOf(keyword) >= 0 || c.Description.IndexOf(keyword) >= 0 || c.Name.IndexOf(keyword) >= 0);
            }

            if (commodityOnly)
            {
                query = query.Where(c => c.Is_Commodity != 0 && c.Price > 0);
            }

            return query;
        }

        public bool HasCollected(long albumId, long pictureId, string username)
        {
            return FindCollection(albumId, pictureId, username) != null;
        }

        public IQueryable<View_Collection_With_Original> FindCollectedItemsOfUser(string username)
        {
            return from c in context.View_Collection_With_Original where c.Username == username && c.Is_Original != 1 orderby c.Collect_Time descending select c;
        }

        public IQueryable<View_Collection_With_Original> FindAllCollectionsOfUser(string username)
        {
            return from c in context.View_Collection_With_Original where c.Username == username orderby c.Collect_Time descending select c;
        }

        public DB_Collection FindCollection(string username, long pictureId, byte collectType)
        {
            return (from c in context.DB_Collection where c.Username == username && c.Picture_Id == pictureId && c.Collect_Type == collectType select c).SingleOrDefault();
        }

        public DB_Collection2 FindCollection2(string username, long pictureId, bool isOriginal)
        {
            return (from c in context.DB_Collection2 where c.Username == username && c.Picture_Id == pictureId && c.Is_Original == (isOriginal ? 1: 0) select c).SingleOrDefault();
        }

        public View_Collection2 FindViewCollection2(string username, long pictureId, bool isOriginal)
        {
            return (from c in context.View_Collection2 where c.Username == username && c.Picture_Id == pictureId && c.Is_Original == (isOriginal ? 1 : 0) select c).SingleOrDefault();
        }

        public View_Collection2 FindViewCollection2(string username, long pictureId)
        {
            return (from c in context.View_Collection2 where c.Username == username && c.Picture_Id == pictureId select c).SingleOrDefault();
        }

        public DB_Collection2 FindCollection2(string username, long pictureId)
        {
            return (from c in context.DB_Collection2 where c.Username == username && c.Picture_Id == pictureId select c).SingleOrDefault();
        }

        public DB_Collection2 FindCollection2(long collectionId)
        {
            return (from c in context.DB_Collection2 where c.Id == collectionId select c).SingleOrDefault();
        }

        public IQueryable<DB_Collection2> FindCollection2()
        {
            return (from c in context.DB_Collection2 select c);
        }
        
        public View_Collection2 FindViewCollection2(long id)
        {
            return FindViewCollection2().Where(c => c.Id == id).SingleOrDefault();
        }

        public IQueryable<View_Collection2> FindViewCollection2()
        {
            return from c in context.View_Collection2 select c;
        }
        /// <summary>
        /// Insert collection. 
        /// </summary>
        /// <param name="albumId">0 for uncategories album. none 0 value for real </param>
        /// <param name="pictureId"></param>
        /// <param name="username">the user who collect or upload the picture</param>
        /// <param name="isOwner">true if the picture is owned by the user</param>
        public DB_Collection InsertCollection(long albumId, long pictureId, string username, bool isOwner)
        {
            DB_Album album = FindAlbumById(albumId);

            DB_Collection c = new DB_Collection { Picture_Id = pictureId, Username = username, Collect_Type = (byte)(isOwner ? 0 : 1), Collect_Time = DateTime.Now };
            if (albumId != 0)
            {
                // if albumId is 0 it is uncategorized album
                c.Album_Id = albumId;
            }

            context.DB_Collection.InsertOnSubmit(c);

            context.SubmitChanges();

            return c;
        }

        public DB_Collection2 InsertCollection2(DB_Collection2 collection)
        {
            context.DB_Collection2.InsertOnSubmit(collection);

            context.SubmitChanges();

            return collection;
        }
        
        public DB_Collection2 InsertCollection2(long albumId, long pictureId, string username, bool isOwner, string description, string tags)
        {
            DB_Collection2 c = new DB_Collection2
            {
                Picture_Id = pictureId,
                Username = username,
                Is_Original = (byte)(isOwner ? 1 : 0),
                Collect_Time = DateTime.Now,
                Description = description,
                Tags = tags
            };
            if (albumId != 0)
            {
                // if albumId is 0 it is uncategorized album
                c.Album_Id = albumId;
            }

            context.DB_Collection2.InsertOnSubmit(c);

            context.SubmitChanges();

            return c;
        }


        public void DeleteCollection(long albumId, long pictureId, string username)
        {
            DB_Collection c = FindCollection(albumId, pictureId, username);
            if (c != null)
            {
                context.DB_Collection.DeleteOnSubmit(c);
                context.SubmitChanges();
            }
        }

        public void DeleteCollection(DB_Collection2 col)
        {
            if (col != null)
            {
                context.DB_Collection2.DeleteOnSubmit(col);
                context.SubmitChanges();
            }
        }

        public IQueryable<View_Collection_With_Original> FindMyInterest(string username)
        {
            List<string> users = FindAttentions(username).Select(u => u.Username).ToList();
            users.Add(username);

            return from collection in context.View_Collection_With_Original
                   where users.Contains(collection.Username)
                   orderby collection.Collect_Time descending
                   select collection;
        }

        public IQueryable<View_Collection_With_Original> FindAllCollections()
        {
            return from collection in context.View_Collection_With_Original
                   orderby collection.Id descending
                   select collection;
        }
        #endregion

        #region Reply Related
        public DB_Reply FindReplyById(long id)
        {
            return (from r in context.DB_Reply where r.Id == id select r).SingleOrDefault();
        }

        public DB_Reply InsertReply(DB_Reply reply)
        {
            DB_Reply r = FindReplyById(reply.Id);
            if (r == null)
            {
                context.DB_Reply.InsertOnSubmit(reply);
                context.SubmitChanges();
                return reply;
            }
            return r;
        }

        public IQueryable<DB_Reply> FindReplies()
        {
            return (from r in context.DB_Reply select r);
        }

        public IQueryable<View_Reply> FindViewReplies()
        {
            return (from r in context.View_Reply select r);
        }

        public IQueryable<View_Reply> FindViewRepliesByCollectionId(long collectionId)
        {
            return (from r in context.View_Reply where r.Collection_Id == collectionId orderby r.Post_Time descending select r);
        }

        public IQueryable<DB_Reply> FindRepliesByCollectionId(long collectionId)
        {
            return (from r in context.DB_Reply where r.Collection_Id == collectionId orderby r.Post_Time descending select r);
        }

        public IQueryable<DB_Reply> FindRepliesByReplyId(long replyId)
        {
            return (from r in context.DB_Reply where r.Reply_Id == replyId orderby r.Post_Time descending select r);
        }

        public IQueryable<View_Reply> FindRepliesByAlbumId(long albumId)
        {
            return (from r in context.View_Reply where r.Album_Id == albumId orderby r.Post_Time descending select r);
        }

        public IQueryable<View_Reply> FindRepliesByDiscussionId(long discussionId)
        {
            return (from r in context.View_Reply where r.Discussion_Id == discussionId orderby r.Post_Time descending select r);
        }

        public void DeleteReply(DB_Reply r)
        {
            if (r != null)
            {
                context.DB_Reply.DeleteOnSubmit(r);
                context.SubmitChanges();
            }
        }

        public bool DeleteReplyById(long id)
        {
            DB_Reply r = FindReplyById(id);
            if (r != null)
            {
                context.DB_Reply.DeleteOnSubmit(r);
                context.SubmitChanges();
                return true;
            }
            return false;
        }
        #endregion

        #region Tag Related
        public IQueryable<DB_Tag> FindTagsByUsername(string username)
        {
            return (from t in context.DB_Tag where t.Username == username select t).OrderBy(t => t.Name);
        }

        public IQueryable<DB_Tag> FindCommonTags()
        {
            return (from t in context.DB_Tag where t.Username == null select t).OrderBy(t => t.Name);
        }

        public DB_Tag FindTagById(long id)
        {
            return (from t in context.DB_Tag where t.Id == id select t).SingleOrDefault();
        }

        public DB_Tag InsertTag(DB_Tag tag)
        {
            DB_Tag existingTag = FindTagById(tag.Id);
            if (existingTag == null)
            {
                existingTag = new DB_Tag { Id = tag.Id };

                existingTag.Name = tag.Name;
                existingTag.Username = tag.Username;
                existingTag.Updated_Time = DateTime.Now;
                existingTag.Type = tag.Type;

                context.DB_Tag.InsertOnSubmit(existingTag);

                context.SubmitChanges();
            }

            return existingTag;
        }
        #endregion

        #region Notice Related
        public DB_Notice FindNoticeById(long id)
        {
            return (from n in context.DB_Notice
                    where n.Id == id
                    select n).SingleOrDefault();
        }

        public DB_Notice InsertNotice(DB_Notice notice)
        {
            DB_Notice n = FindNoticeById(notice.Id);
            if (n == null)
            {
                context.DB_Notice.InsertOnSubmit(notice);
                context.SubmitChanges();
            }
            return n;
        }

        public IQueryable<DB_Notice> FindNoticeByFan(string fanUsername, string watchedUsername)
        {
            return (from notice in context.DB_Notice
                    where notice.Username == watchedUsername
                    where notice.Action_Username == fanUsername
                    where notice.Notice_Type == (byte)NoticeType.Fan
                    select notice);
        }

        public IQueryable<DB_Notice> FindNotice(string username, NoticeType type)
        {
            return from notice in context.DB_Notice
                   where notice.Username == username
                   where (notice.Notice_Type & (byte)type) > 0
                   orderby notice.Notice_Time descending
                   select notice;
        }

        public void DeleteNotice(DB_Notice notice)
        {
            if (notice != null)
            {
                context.DB_Notice.DeleteOnSubmit(notice);
                context.SubmitChanges();
            }
        }

        public IQueryable<DB_Notice> FindNoticeByAssociationId(long id, NoticeType type)
        {
            switch (type)
            {
                case NoticeType.Collect:
                    {
                        return from notice in context.DB_Notice
                               where notice.Collect_Id == id
                               orderby notice.Notice_Time descending
                               select notice;
                    }
                case NoticeType.Like:
                    {
                        return from notice in context.DB_Notice
                               where notice.Like_Id == id
                               orderby notice.Notice_Time descending
                               select notice;
                    }
                case NoticeType.Reply:
                    {
                        return from notice in context.DB_Notice
                               where notice.Reply_Id == id
                               orderby notice.Notice_Time descending
                               select notice;
                    }
                default: return null;
            }
        }

        public IQueryable<DB_Notice> FindUnreadNotice(string username)
        {
            return from notice in context.DB_Notice
                   where notice.Username == username
                   where notice.Has_Read == 0
                   orderby notice.Notice_Time descending
                   select notice;
        }

        #endregion

        #region Credential Related
        public DB_UserCredential FindCredentialByEmail(string email)
        {
            return (from c in context.DB_UserCredential where c.Email == email select c).SingleOrDefault();
        }

        public DB_UserCredential InsertCredential(DB_UserCredential credential)
        {
            DB_UserCredential cred = FindCredentialByEmail(credential.Email);
            if (cred == null)
            {
                cred = new DB_UserCredential { Email = credential.Email };
                cred.EncryptedPassword = credential.EncryptedPassword;

                context.DB_UserCredential.InsertOnSubmit(cred);
                context.SubmitChanges();
            }


            return cred;
        }
        #endregion

        #region Location related
        public List<DB_Location> FindAllProvinces()
        {
            var query = from l in context.DB_Location
                        select l;
            return query.ToList();
        }

        public DB_Location FindLocationById(long id)
        {
            return (from l in context.DB_Location where l.Id == id select l).SingleOrDefault();
        }
        #endregion

        #region Discussion
        public IQueryable<DB_Discussion_Board> FindAllDiscussionBoards()
        {
            return from board in context.DB_Discussion_Board
                   select board;
        }

        public DB_Discussion_Board FindDiscussionBoard(long id)
        {
            return (from board in context.DB_Discussion_Board
                    where board.Id == id
                    select board).SingleOrDefault();
        }

        public IQueryable<DB_Discussion> FindAllDiscussions()
        {
            return from d in context.DB_Discussion
                   select d;
        }

        public IQueryable<DB_Discussion> FindRepliedDiscussions(string username)
        {
            return from d in context.DB_Discussion
                   from r in context.DB_Reply
                   where r.Discussion_Id == d.Id
                   where r.Replier_Username == username
                   orderby r.Post_Time
                   select d;
        }

        public DB_Discussion FindDiscussion(long id)
        {
            return (from d in context.DB_Discussion
                    where d.Id == id
                    select d).SingleOrDefault();
        }

        public DB_Discussion InsertDiscussion(DB_Discussion discussion)
        {
            DB_Discussion d = FindDiscussion(discussion.Id);
            if (d == null)
            {
                context.DB_Discussion.InsertOnSubmit(discussion);
                context.SubmitChanges();
            }
            return discussion;
        }

        public void DeleteDiscussion(long discussionId)
        {
            DB_Discussion d = FindDiscussion(discussionId);
            DeleteDiscussion(d);
        }

        public void DeleteDiscussion(DB_Discussion discussion)
        {
            if (discussion != null)
            {
                context.DB_Discussion.DeleteOnSubmit(discussion);
                context.SubmitChanges();
            }
        }

        #endregion

        #region Announcement
        public DB_Announcement FindAnnouncementById(int id)
        {
            return (from a in context.DB_Announcement where a.Id == id select a).SingleOrDefault();
        }

        public DB_Announcement InsertAnnouncement(DB_Announcement announcement)
        {
            DB_Announcement a = FindAnnouncementById(announcement.Id);
            if (a == null)
            {
                context.DB_Announcement.InsertOnSubmit(announcement);
                context.SubmitChanges();
                return announcement;
            }
            return a;
        }

        public IQueryable<DB_Announcement> FindAllAnnouncements()
        {
            return from a in context.DB_Announcement select a;
        }
        #endregion

        #region GuessLike
        public IQueryable<DB_Guess_Like> FindGuessLike()
        {
            return (from g in context.DB_Guess_Like select g);
        }

        public IQueryable<DB_Guess_Like> FindGuessLikeByTargetId(long collectionId)
        {
            return FindGuessLike().Where(g => g.Collection_Id == collectionId);
        }

        public DB_Guess_Like InsertGuessLike(long collectionId, long likeCollectionId, byte index)
        {
            DB_Guess_Like g = new DB_Guess_Like { Collection_Id = collectionId, Related_Collection_Id = likeCollectionId, Index = index, Updated_Time = DateTime.Now };
            context.DB_Guess_Like.InsertOnSubmit(g);
            context.SubmitChanges();
            return g;
        }

        public void DeleteGuessLike(DB_Guess_Like g)
        {
            if (g != null)
            {
                context.DB_Guess_Like.DeleteOnSubmit(g);
                context.SubmitChanges();
            }
        }
        #endregion

        #region Survey
        public IQueryable<DB_Survey> FindSurveys()
        {
            return from s in context.DB_Survey select s;
        }

        public DB_Survey InsertSurvey(DB_Survey s)
        {
            DB_Survey survey = FindSurveys().Where(sur => sur.Id == s.Id).SingleOrDefault();
            if (survey == null)
            {
                context.DB_Survey.InsertOnSubmit(s);
                context.SubmitChanges();
                return s;
            }
            return survey;
        }

        #endregion
        #region Misc

        public DB_Keyword_Category FindKeywordCategoryById(long id)
        {
            return (from kc in context.DB_Keyword_Category where kc.Id == id select kc).SingleOrDefault();
        }

        public DB_Keyword_Category InsertKeywordCategory(DB_Keyword_Category kc)
        {
            DB_Keyword_Category c = FindKeywordCategoryById(kc.Id);
            if (c == null)
            {
                context.DB_Keyword_Category.InsertOnSubmit(kc);
                context.SubmitChanges();
                return kc;
            }
            return c;
        }

        public DB_Keyword FindKeywordById(int id)
        {
            return (from k in context.DB_Keyword where k.Id == id select k).SingleOrDefault();
        }

        public DB_Keyword InsertKeyword(DB_Keyword keyword)
        {
            DB_Keyword k = FindKeywordById(keyword.Id);
            if (k == null)
            {
                context.DB_Keyword.InsertOnSubmit(keyword);
                context.SubmitChanges();
                return keyword;
            }
            return k;
        }

        public IQueryable<DB_Keyword_Category> FindKeywordCategories()
        {
            return from kc in context.DB_Keyword_Category select kc;
        }

        public DB_Keyword_Association FindKeywordAssociation(string keyword1, string keyword2)
        {
            return (from kc in context.DB_Keyword_Association
                    where (kc.Keyword1 == keyword1 && kc.Keyword2 == keyword2) || (kc.Keyword1 == keyword2 && kc.Keyword2 == keyword1)
                    select kc).SingleOrDefault();
        }

        public void InsertKeywordAssication(string keyword1, string keyword2)
        {
            DB_Keyword_Association ka = FindKeywordAssociation(keyword1, keyword2);
            if (ka == null)
            {
                context.DB_Keyword_Association.InsertOnSubmit(new DB_Keyword_Association { Keyword1 = keyword1, Keyword2 = keyword2 });
                context.SubmitChanges();
            }
        }

        public void RemoveKeywordAssication(string keyword1, string keyword2)
        {
            DB_Keyword_Association ka = FindKeywordAssociation(keyword1, keyword2);
            if (ka != null)
            {
                context.DB_Keyword_Association.DeleteOnSubmit(ka);
                context.SubmitChanges();
            }
        }

        public IQueryable<DB_Keyword_Association> FindKeywordAssociations()
        {
            return from ka in context.DB_Keyword_Association select ka;
        }

        public DB_Token FindTokenByEmail(string email)
        {
            return (from t in context.DB_Token where t.Email == email select t).SingleOrDefault();
        }

        public DB_Token InsertOrUpdateToken(DB_Token token)
        {
            DB_Token t = FindTokenByEmail(token.Email);
            if (t == null)
            {
                t = token;
                context.DB_Token.InsertOnSubmit(token);
            }
            else
            {
                t.Code = token.Code;
                t.Time = token.Time;
            }

            context.SubmitChanges();
            return t;
        }

        public void InsertUserAction(DB_User_Action action)
        {
            context.DB_User_Action.InsertOnSubmit(action);
            context.SubmitChanges();
        }

        public IQueryable<DB_User_Action> FindUserAction()
        {
            return from action in context.DB_User_Action select action;
        }

        public IQueryable<DB_Recommended_Tag> FindRecommendedTags()
        {
            return from t in context.DB_Recommended_Tag select t;
        }

        public IQueryable<View_Recommended_Tag> FindRecommendedViewTags()
        {
            return from t in context.View_Recommended_Tag select t;
        }

        public DB_Recommended_Tag InsertRecommendedTag(DB_Recommended_Tag tag)
        {
            DB_Recommended_Tag et = FindRecommendedTags().Where(t => t.Id == tag.Id).SingleOrDefault();
            if (et == null)
            {
                context.DB_Recommended_Tag.InsertOnSubmit(tag);
                context.SubmitChanges();
                return tag;
            }
            return et;
        }

        public void DeleteRecommendedTag(DB_Recommended_Tag tag)
        {
            DB_Recommended_Tag et = FindRecommendedTags().Where(t => t.Id == tag.Id).SingleOrDefault();
            if (et != null)
            {
                context.DB_Recommended_Tag.DeleteOnSubmit(et);
                context.SubmitChanges();
            }
        }

        #endregion

        #region Advertisement
        public DB_Advertisement FindAdvertisement()
        {
            return (from a in context.DB_Advertisement select a).SingleOrDefault();
        }
        #endregion

        #region Activities

        public IQueryable<DB_Activity> FindActivities()
        {
            return from a in context.DB_Activity select a;
        }

        public IQueryable<DB_Lucky> FindLucky()
        {
            return from l in context.DB_Lucky select l;
        }

        public IQueryable<View_Lucky> FindViewLucky()
        {
            return from l in context.View_Lucky select l;
        }

        public DB_Lucky InsertLucky(DB_Lucky lucky)
        {
            DB_Lucky existing = FindLucky().Where(l => l.Username == lucky.Username && l.Activity_Id == lucky.Activity_Id ).SingleOrDefault();
            if (existing == null)
            {
                context.DB_Lucky.InsertOnSubmit(lucky);
                context.SubmitChanges();
                return lucky;
            }
            return existing;
        }

        public void DeleteLucky(DB_Lucky lucky)
        {
            DB_Lucky existing = FindLucky().Where(l => l.Username == lucky.Username).SingleOrDefault();
            if (existing != null)
            {
                context.DB_Lucky.DeleteOnSubmit(lucky);
                context.SubmitChanges();
            }
        }
        #endregion
    }

    static class CompiledQueryLib
    {
        public static readonly Func<MutouheziDatabase, long, byte, int> FindCollectionCountByPictureIdCompiledQuery
    = CompiledQuery.Compile<MutouheziDatabase, long, byte, int>((ctx, pictureId, type) => (from col in ctx.DB_Collection
                                                                               where col.Picture_Id == pictureId
                                                                               where col.Collect_Type == type
                                                                               select col).Count());    
    }
}
