﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PumpkinHouseDatabase
{
    public partial class DB_Tag
    {
        public static int SizeLimit = 20;
    }

    [Flags]
    public enum NoticeType
    {
        Collect = 1,
        Fan = 2,
        Reply = 4,
        Like = 8,
        Announcement = 16
    }

    public enum AssociationParty
    {
        Sina = 1,
        QQ = 2,
        QQWB = 3,
        Taobao = 4
    }

    public enum HomePageMode
    {
        Default = 0,
        Hot = 1,
        Latest = 2
    }

    public enum UserActionType
    {
        SearchByKeyword = 1,
        SearchByTag = 2,
        AddPicture = 3
    }
}
