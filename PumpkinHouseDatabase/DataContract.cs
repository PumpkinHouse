﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace PumpkinHouseDatabase.Contract
{
    [DataContract]
    public class PagingList<T>
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }
        [DataMember(Name = "list")]
        public IList<T> List { get; set; }
    }

    [DataContract]
    public class Province
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<City> Cities { get; set; }
    }

    [DataContract]
    public class City
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    public enum Gender
    {
        NotDefined = 0,
        Male = 1,
        Female = 2
    }

    /// <summary>
    /// 用于注册的用户信息
    /// </summary>
    [DataContract]
    public class UserForRegistration
    {
        /// <summary>
        /// 昵称。14个字符/汉字以内。后台如果超出20个字符会报错
        /// </summary>
        [DataMember(Name = "nickname", EmitDefaultValue = false)]
        public string NickName { get; set; }

        /// <summary>
        /// 邮箱。
        /// </summary>
        [DataMember(Name = "email", EmitDefaultValue = false)]
        public string Email { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [DataMember(Name = "password", EmitDefaultValue = false)]
        public string Password { get; set; }

        /// <summary>
        /// 用户自我介绍
        /// </summary>
        public string SelfIntroduction { get; set; }

        /// <summary>
        /// 头像地址
        /// </summary>
        [DataMember(Name = "avatar", EmitDefaultValue = false)]
        public string AvatarUrl { get; set; }

        /// <summary>
        /// 是否关注官方
        /// </summary>
        [DataMember(Name = "follow", EmitDefaultValue = false)]
        public bool Follow { get; set; }

        /// <summary>
        /// 是否发送微博
        /// </summary>
        [DataMember(Name = "advertise", EmitDefaultValue = false)]
        public bool Advertise { get; set; }
    }

    /// <summary>
    /// 用于显示的用户信息
    /// </summary>
    [DataContract]
    public class UserGeneralInfo
    {
        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "displayName")]
        public string NickName { get; set; }

        public Gender Gender { get; set; }

        [DataMember(Name = "gender")]
        public string GenderString
        {
            get { return Enum.GetName(typeof(Gender), this.Gender); }
            set { this.Gender = (Gender)Enum.Parse(typeof(Gender), value); }
        }

        [DataMember(Name = "cityId")]
        public long CityId { get; set; }

        [DataMember(Name = "cityName")]
        public string CityName { get; set; }

        [DataMember(Name = "provinceId")]
        public long ProvinceId { get; set; }

        [DataMember(Name = "provinceName")]
        public string ProvinceName { get; set; }

        [DataMember(Name = "intro")]
        public string SelfIntroduction { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "receiveEmail")]
        public bool ReceiveEmail { get; set; }
    }

    [DataContract]
    public class UserGeneralInfoToUpdate
    {
        public Gender Gender { get; set; }

        [DataMember(Name = "gender")]
        public string GenderString
        {
            get { return Enum.GetName(typeof(Gender), this.Gender); }
            set { this.Gender = (Gender)Enum.Parse(typeof(Gender), value); }
        }

        [DataMember(Name = "nickname")]
        public string NickName { set; get; }

        [DataMember(Name = "locationId", EmitDefaultValue = false)]
        public long LocationId { get; set; }

        [DataMember(Name = "intro")]
        public string SelfIntroduction;
    }

    [DataContract]
    public class AccountAssociation
    {
        [DataMember(Name = "id")]
        public long Id;

        [DataMember(Name = "partyName")]
        public string PartyName;

        [DataMember(Name = "partyId")]
        public int PartyId;

        [DataMember(Name = "bound")]
        public bool IsBound;

        [DataMember(Name = "urlToBind")]
        public string UrlToBind;
    }

    public enum TagType
    {
        Collection,
        Album
    }

    [DataContract]
    public class PictureToUpload
    {
        [DataMember(Name = "name")]
        public string ImageName { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "albumId")]
        public long AlbumId { get; set; }

        [DataMember(Name = "tags")]
        public string Tags { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "src")]
        public string SourcePageUrl { get; set; }

        [DataMember(Name = "originalName")]
        public string SourceImageUrl { get; set; }

        [DataMember(Name = "sync")]
        public byte[] SyncToAssociationAccount { get; set; }

        /// <summary>
        /// 1: 本地上传
        /// 2: 粘贴网址
        /// 3: 收纳工具
        /// </summary>
        [DataMember(Name="uploadTool")]
        public int UploadTool { get; set; }
    }

    [DataContract]
    public class AlbumToCreate
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "description")]
        public string Description { get; set; }
        [DataMember(Name = "tags")]
        public string Tags { get; set; }
    }

    [DataContract]
    public class EntityName
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "id")]
        public long Id { get; set; }
    }


    [DataContract]
    public class CollectionLink
    {
        [DataMember(Name = "id")]
        public long CollectionId { get; set; }
        [DataMember(Name = "url")]
        public string ImageUrl { get; set; }

        [DataMember(Name = "ext")]
        public string ImageExt { get; set; }
    }

    [DataContract]
    public class MessageToSend
    {
        [DataMember(Name = "from")]
        public string FromUsername { get; set; }

        [DataMember(Name = "to")]
        public string ToUsername { get; set; }

        [DataMember(Name = "text")]
        public string Text { get; set; }
    }

    [DataContract]
    public class ThreadToDisplay
    {
        [DataMember(Name = "target")]
        public string Target { get; set; }

        [DataMember(Name = "targetDisplay")]
        public string TargetDisplay { get; set; }

        [DataMember(Name = "me")]
        public string Me { get; set; }

        [DataMember(Name = "myDispolayName")]
        public string MyDisplayName { get; set; }
        
        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "time")]
        public DateTime Time { get; set; }

        [DataMember(Name = "newNumber")]
        public int New{ get; set; }

        [DataMember(Name = "count")]
        public int Count { get; set; }
    }

    [DataContract]
    public class MessageToDisplay
    {
        [DataMember(Name = "target")]
        public string Target { get; set; }

        [DataMember(Name = "targetDisplay")]
        public string TargetDisplayName { get; set; }
        
        [DataMember(Name = "sentByMe")]
        public bool SentByMe { get; set; }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "time")]
        public DateTime Time { get; set; }

        [DataMember(Name = "me")]
        public string Me { get; set; }

        [DataMember(Name = "myDispolayName")]
        public string MyDisplayName { get; set; }

        [DataMember(Name = "hasRead")]
        public bool Has_Read { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CollectionToDisplay
    {
        [DataMember(Name = "id")]
        public long Id { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "displayName")]
        public string NickName { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "original")]
        public bool IsOriginal { get; set; }

        [DataMember(Name = "tags")]
        public string Tags { get; set; }

        [DataMember(Name = "albumId")]
        public long AlbumId { get; set; }

        [DataMember(Name = "albumName")]
        public string AlbumName { get; set; }

        [DataMember(Name = "colTime")]
        public DateTime CollectedTime { get; set; }

        [DataMember(Name = "replyCol")]
        public PagingList<ReplyToDisplay> ReplyCollection { get; set; }

        #region Picture Info
        [DataMember(Name = "pictureId")]
        public long PictureId { get; set; }

        [DataMember(Name = "uploadTime")]
        public Nullable<DateTime> UploadTime { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "ext")]
        public string Extention { get; set; }

        [DataMember(Name = "numberOfCol")]
        public int NumberOfCollects { get; set; }

        [DataMember(Name = "likeCount")]
        public int NumberOfLikes { get; set; }

        [DataMember(Name = "pageUrl")]
        public string OriginalPageUrl { get; set; }

        [DataMember(Name = "pageTitle")]
        public string OriginalPageTitle { get; set; }

        [DataMember(Name = "shop")]
        public int Shop { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "ratio")]
        public double Ratio { get; set; }

        public string FileName { get; set; }

        [DataMember(Name = "taobaolink")]
        public string TaobaoLink { get; set; }
        #endregion

        [DataMember(Name = "owner")]
        public string OwnerUsername { get; set; }

        [DataMember(Name = "ownerNick")]
        public string OwnerNickname { get; set; }

        [DataMember(Name = "originalAlbumId")]
        public long OriginalAlbumId { get; set; }

        [DataMember(Name = "originalAlbumName")]
        public string OriginalAlbumName { get; set; }

        [DataMember(Name = "myColAlbumId")]
        public long CollectedInMyAblbumId { get; set; }

        [DataMember(Name = "myColAlbumName")]
        public string CollectedInMyAlbumName { get; set; }

        [DataMember(Name = "mine")]
        public bool IsMine { get; set; }

        [DataMember(Name = "likedByMe")]
        public bool LikedByMe { get; set; }

        [DataMember(Name = "recommended")]
        public bool Recommended { get; set; }

    }

    [DataContract]
    [Serializable]
    public class PictureToDisplay
    {
        [DataMember(Name = "id")]
        
        public long Id { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "displayName")]
        public string NickName { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "mine")]
        public bool IsMine { get; set; }

        [DataMember(Name = "tags")]
        public string Tags { get; set; }

        [DataMember(Name = "albumId")]
        public long AlbumId { get; set; }

        [DataMember(Name = "albumName")]
        public string AlbumName { get; set; }

        [DataMember(Name = "uploadTime")]
        public DateTime UploadTime { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "ext")]
        public string Extention { get; set; }

        [DataMember(Name = "replyCol")]
        public PagingList<ReplyToDisplay> ReplyCollection { get; set; }

        [DataMember(Name = "colBy")]
        public string CollectedByUsername { get; set; }

        [DataMember(Name = "colByDisplayName")]
        public string CollectedByDisplay { get; set; }

        [DataMember(Name = "colAlbumId")]
        public long CollectedInAblbumId { get; set; }

        [DataMember(Name = "colAlbumName")]
        public string CollectedInAlbumName { get; set; }

        [DataMember(Name = "myColAlbumId")]
        public long CollectedInMyAblbumId { get; set; }

        [DataMember(Name = "myColAlbumName")]
        public string CollectedInMyAlbumName { get; set; }

        [DataMember(Name = "colTime")]
        public DateTime CollectedTime { get; set; }

        [DataMember(Name = "numberOfCol")]
        public int NumberOfCollects { get; set; }

        [DataMember(Name = "pageUrl")]
        public string OriginalPageUrl { get; set; }

        [DataMember(Name = "pageTitle")]
        public string OriginalPageTitle { get; set; }

        [DataMember(Name = "shop")]
        public int Shop { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "ratio")]
        public double Ratio { get; set; }

        public string FileName { get; set; }

        [DataMember(Name = "taobaolink")]
        public string TaobaoLink { get; set; }
    }

    [DataContract]
    public class BriefPictureInfo {
        [DataMember(Name = "url")]
        public string Url;
        [DataMember(Name = "ext")]
        public string FileExt;
        [DataMember(Name = "id")]
        public long PictureId;
    }

    [DataContract]
    public class ReplyToPost
    {
        [DataMember(Name = "text")]
        public string Text;

        /// <summary>
        /// Set this value if the reply is to a picture
        /// </summary>
        [DataMember(Name = "collectionId")]
        public long? CollectionId { get; set; }

        /// <summary>
        /// Set this value if the reply is to an album
        /// </summary>
        [DataMember(Name = "albumId")]
        public long? AlbumId { get; set; }


        [DataMember(Name = "discussionId")]
        public long? DiscussionId { get; set; }
        
        /// <summary>
        /// Set this value if this reply is to another reply
        /// </summary>
        [DataMember(Name = "replyId")]
        public long? ReplyId { get; set; }

    }

    [DataContract]
    public class ReplyToDisplay
    {
        [DataMember(Name = "id")]
        public long Id { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "displayName")]
        public string NickName { get; set; }

        [DataMember(Name = "content")]
        public string Content { get; set; }

        [DataMember(Name = "time")]
        public DateTime Time { get; set; }

        [DataMember(Name = "replyTo")]
        public string ReplyTo { get; set; }

        [DataMember(Name = "replyToNickname")]
        public string ReplyToNickname { get; set; }

        [DataMember(Name = "isMe")]
        public bool IsMe { get; set; }
    }

    [DataContract]
    public class UserToDisplay
    {
        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "displayName")]
        public string NickName { get; set; }

        [DataMember(Name = "location")]
        public string Location { get; set; }

        [DataMember(Name = "selfIntroduction")]
        public string SelfIntroduction { get; set; }

        [DataMember(Name = "hasMyAttention")]
        public bool HasMyAttention { get; set; }

        [DataMember(Name = "isMyFan")]
        public bool IsMyFan { get; set; }

        [DataMember(Name = "numberOfAttentions")]
        public int NumberOfAttentions { get; set; }

        [DataMember(Name = "attentions")]
        public IList<LinkItem> Attentions { get; set; }

        [DataMember(Name = "numberOfFans")]
        public int NumberOfFans { get; set; }

        [DataMember(Name = "fans")]
        public IList<LinkItem> Fans { get; set; }

        [DataMember(Name = "numberOfOriginal")]
        public int NumberOfOriginal { get; set; }

        [DataMember(Name = "numberOfCollect")]
        public int NumberOfCollect { get; set; }

        [DataMember(Name = "numberOfAlbums")]
        public int NumberOfAlbums { get; set; }

        [DataMember(Name = "numberOfLikedAlbums")]
        public int NumberOfLikedAlbums { get; set; }

        [DataMember(Name = "pictureList")]
        public IList<BriefPictureInfo> PictureList { get; set; }

        [DataMember(Name = "relation")]
        public int RelationWithMe { get { return (HasMyAttention ? 2 : 0) + (IsMyFan ? 1 : 0); } set { } }

        [DataMember(Name = "qqWbName")]
        public string QQWeiboLinkName { get; set; }

        [DataMember(Name = "sinaWbName")]
        public string SinaWeiboLinkName { get; set; }

        [DataMember(Name = "qqSpaceName")]
        public string QQSpaceLinkName { get; set; }
        
        [DataMember(Name = "identified")]
        public bool IsIdentified { get; set; }

        [DataMember(Name = "identificationInfo")]
        public string IdentificationInfo { get; set; }

        [DataMember(Name = "likeCount")]
        public int LikeCount { get; set; }
    }


    [DataContract]
    public class LinkItem
    {
        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "displayName")]
        public string NickName { get; set; }

        [DataMember(Name = "thumbnailPath")]
        public string ThumbnailPath { get; set; }
    }

    [DataContract]
    public class AlbumToDisplay
    {
        [DataMember(Name = "id")]
        public long Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "tags")]
        public string Tags { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "displayName")]
        public string NickName { get; set; }

        [DataMember(Name = "numberOfPic")]
        public int NumberOfPictures { get; set; }

        [DataMember(Name = "numberOfLike")]
        public int NumberOfLike { get; set; }

        [DataMember(Name = "likedByMe")]
        public bool LikedByMe { get; set; }

        [DataMember(Name = "pictures")]
        public PagingList<CollectionToDisplay> Pictures { get; set; }

        [DataMember(Name = "replyCol")]
        public PagingList<ReplyToDisplay> ReplyCollection { get; set; }
    }

    [DataContract]
    public class NoticeToDisplay
    {
        /// <summary>
        /// The username did the action which triggered this notice
        /// </summary>
        [DataMember(Name = "username")]
        public string ActionUsername { get; set; }

        [DataMember(Name = "displayName")]
        public string ActionUserNickName { get; set; }

        /// <summary>
        /// 回复主体的拥有者（专辑，收纳或者讨论主题）
        /// 仅当提醒是回复类有效。此属性是用来区别此回复是针对于另外一个回复还是对于主题（图片，专辑，讨论帖）
        /// </summary>
        [DataMember(Name = "owner")]
        public string OwnerUsername { get; set; }

        /// <summary>
        /// 此提醒针对的用户名
        /// </summary>
        [DataMember(Name = "target")]
        public string TargetUsername { get; set; }

        [DataMember(Name = "time")]
        public DateTime ActionTime { get; set; }

        [DataMember(Name = "type")]
        public int ActionType { get; set; }

        [DataMember(Name = "albumId")]
        public long AlbumId { get; set; }

        [DataMember(Name = "albumName")]
        public string AlbumName { get; set; }

        [DataMember(Name = "collectionId")]
        public long CollectionId { get; set; }

        [DataMember(Name = "collectionDesc")]
        public string CollectionDesc { get; set; }

        [DataMember(Name = "discussionId")]
        public long DiscussionId { get; set; }

        [DataMember(Name = "discussionSubject")]
        public string DiscussionSubject { get; set; }

        [DataMember(Name = "replyId")]
        public long ReplyId { get; set; }

        [DataMember(Name = "repliedReplyId")]
        public long RepliedReplyId { get; set; }

        [DataMember(Name = "reply")]
        public string Reply { get; set; }

        [DataMember(Name = "userImageUrl")]
        public string UserImageUrl { get; set; }

        [DataMember(Name = "targetImageUrl")]
        public string TargetImageUrl { get; set; }

        [DataMember(Name = "targetImageExt")]
        public string TargetImageExt { get; set; }

        [DataMember(Name = "announcement")]
        public string AnnouncementText { get; set; }
    }

    [DataContract]
    public class NoticeSummary
    {
        [DataMember(Name = "like")]
        public int Like { get; set; }

        [DataMember(Name = "collect")]
        public int Collect { get; set; }
        
        [DataMember(Name = "fan")]
        public int Fan { get; set; }
        
        [DataMember(Name = "reply")]
        public int Reply { get; set; }

        [DataMember(Name = "announcement")]
        public int Announcement { get; set; }
        
        [DataMember(Name = "message")]
        public int Message { get; set; }

        [DataMember(Name = "total")]
        public int Total { get; set; }
    }

    [DataContract]
    public class KeywordCategory
    {
        [DataMember(Name="keywords")]
        public IList<string> Keywords { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
    }

    [DataContract]
    public class DiscussionBoardToDisplay
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "numOfDisc")]
        public int NumberOfDiscussion { get; set; }

        [DataMember(Name = "numOfNew")]
        public int NumberOfNew { get; set; }
    }

    [DataContract]
    public class DiscussionToDisplay
    {
        [DataMember(Name = "id")]
        public long Id { get; set; }

        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "boardId")]
        public int BoardId { get; set; }

        [DataMember(Name = "boardName")]
        public string BoardName { get; set; }

        [DataMember(Name = "username")]
        public string Username { set; get; }

        [DataMember(Name = "displayName")]
        public string NickName { get; set; }

        [DataMember(Name = "time")]
        public DateTime PostTime { get; set; }

        [DataMember(Name = "replyList")]
        public PagingList<ReplyToDisplay> ReplyList { get; set; }
    }

    [DataContract]
    public class AnnoucementToDisplay
    {
        [DataMember(Name="id")]
        public int Id { get; set; }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "time")]
        public DateTime PostTime { get; set; }
    }

    [DataContract]
    public class Advertisement
    {
        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "image")]
        public string ImageUrl { get; set; }

        [DataMember(Name = "link")]
        public string LinkUrl { get; set; }
    }
}