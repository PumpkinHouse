﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Top.Api;
using Top.Api.Domain;
using Top.Api.Request;
using Top.Api.Response;
using PumpkinHouseDatabase.Contract;
using PumpkinHouse.Utils;
using PumpkinHouseDatabase;
using System.Threading;

namespace TaobaoCrawlerHelper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ITopClient client;

        public MainWindow()
        {
            InitializeComponent();
            InitFramework();
            InitData();
        }

        public void InitFramework()
        {
            ImageHelper.ImageFilesRootPath = @"E:\work\images\";
            DataUtils.ConnectionString = @"Server=localhost\SQLExpress;Database=MutouheziDatabase;Integrated Security=true";
        }

        public void InitData()
        {
            string serviceUrl = "http://gw.api.taobao.com/router/rest";
            string appkey = "12579490";
            string appsecret = "37b097f42d074b04f6cd091e66ec02ec";
            client = new DefaultTopClient(serviceUrl, appkey, appsecret);

            IList<ItemCat> rootCategories = GetTaobaoCategories(client, 0);
            // listCate1.ItemsSource = rootCategories;
            listCate1.DataContext = rootCategories;
            //listCate1.ItemStringFormat
        }

        public IList<ItemCat> GetTaobaoCategories(ITopClient client, long categoryId)
        {
            ItemcatsGetRequest req = new ItemcatsGetRequest();
            req.Fields = "cid,parent_cid,name,is_parent";
            req.ParentCid = categoryId;

            ItemcatsGetResponse response = client.Execute(req);
            return response.ItemCats;
        }

        private void listCate1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ItemCat cat = (ItemCat)e.AddedItems[0];
            IList<ItemCat> categories = GetTaobaoCategories(client, cat.Cid);

            listCate2.DataContext = categories;
        }

        private void listCate2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            txtLog.Text = "";
            string keyword = txtSearchKeywords.Text;


            long cateId = 0;
            ItemCat itemCat2 = listCate2.SelectionBoxItem as ItemCat;
            if (itemCat2 != null)
            {
                cateId = itemCat2.Cid;
            }
            else
            {
                ItemCat itemCat = listCate1.SelectionBoxItem as ItemCat;
                if (itemCat != null)
                {
                    cateId = itemCat.Cid;
                }
            }

            bool hasMore = true;
            int maxCount = int.Parse(txtCount.Text);
            int count = 0;
            int pageNumber = 0;
            while (count < maxCount && hasMore)
            {
                count += Crawl(keyword, cateId, maxCount, pageNumber++, out hasMore);
            }
        }

        private int Crawl(string keyword, long cateId, int maxCount, int pageNumber, out bool hasMore)
        {
            TaobaokeItemsGetRequest req = new TaobaokeItemsGetRequest();
            req.Fields = "num_iid,title,pic_url,cid,price,click_url,num_iid,volume";
            req.Sort = "commissionNum_desc";
            //req.IsMall = true;
            //req.IsXinpin = true;
            req.StartTotalnum = "10";
            req.StartCredit = "5heart";
            req.StartPrice = "10";
            req.EndPrice = "500";
            req.Keyword = keyword;
            req.PageNo = pageNumber;

            req.Nick = "raymanyoung";

            if (maxCount < 40)
            {
                req.PageSize = maxCount;
            }
            if (cateId != 0)
            {
                req.Cid = cateId;
            }

            TaobaokeItemsGetResponse response = client.Execute(req);
            List<TaobaokeItem> items = response.TaobaokeItems;
            long total = response.TotalResults;
            hasMore = total > 40 * ((long)pageNumber + 1);

            int successCount = 0;
            foreach (var item in items)
            {
                item.Title = item.Title.Replace("<span class=H>", "").Replace("</span>", "");

                txtLog.Text += item.Title + "\n";
                txtLog.Text += item.PicUrl + "\n";
                txtLog.Text += item.Price + "\n";
                txtLog.Text += item.ClickUrl + "\n";
                
                PictureToUpload pic = new PictureToUpload
                {
                    AlbumId = 0,
                    Description = item.Title,
                    SourceImageUrl = item.PicUrl,
                    SourcePageUrl = item.ClickUrl,
                    Title = item.Title,
                    Url = item.PicUrl,
                    Tags = keyword
                };
                FileName name = null;

                float price = float.Parse(item.Price);
                using (DataUtils utils = new DataUtils())
                {
                    int count = utils.FindPicture2().Where(p => p.Source_Image_Name == pic.SourceImageUrl).Count();
                    if (count != 0)
                    {
                        continue;
                    }

                    try
                    {
                        string filename = ImageHelper.DownloadFile(item.PicUrl);

                        name = ImageHelper.ParseFileName(filename);

                        Helper.AddPictureEntry(pic, RandomUser(), name, utils, PumpkinHouse.Utils.Shop.Tmall, price, item.ClickUrl);
                        successCount++;
                    }
                    catch (Exception e)
                    {
                    }
                    Thread.Sleep(2000);
                }
            }
            return successCount;
        }

        private static string[] Users = { "l23649061143806", "l59991875805645", "l81866040613594", "l95019359571676", "l54316126984903", "l24797073392579", "l30058406975812", "l73479832057797", "l19041536628398", "l32035279699602" };
        private string RandomUser()
        {
            Random random = new Random((int)DateTime.Now.Ticks);
            int index = random.Next(0, Users.Length);
            return Users[index];
        }

        private static string Tags = "风扇 置物架 植物 马克杯 收纳 餐具 窗帘 家纺 抱枕 地垫 照片墙 装饰画 摆件 iPhone壳 森系 可爱 Zakka 怀旧复古 客厅 卧室 厨房 餐厅 工作台 阳台";

        private static string[] ParseTags()
        {
            return Tags.Split(' ');
        }
    }
}
