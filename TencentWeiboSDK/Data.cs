﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QWeiboSDK
{
    public class DataWrapper<T> where T : JsonData
    {
        public T data;
        public int errorCode;
        public string msg;
    }

    public interface JsonData
    {
    }

    public class User : JsonData
    {
        public string birth_day;
        public string birth_month;
        public string birth_year;
        public string email;
        public string introduction;
        public string name;
        public string nick;
        public string openid;
        public string head;
    }

}
